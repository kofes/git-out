"use strict";

function Human(name, age) {
  this.name = name;
  this.age = age;
}

Human.prototype.grownUp = function() {
  this.age++;
}

Human.prototype.rename = function(name) {
  this.name = name;
}

Human.prototype.getRows = function() {
  return "name: " + this.name + ", age: " + this.age;
}

var Tom = new Human('Tom', 39);

Tom.grownUp();
Tom.rename("Tomas");
alert(Tom.getRows());
