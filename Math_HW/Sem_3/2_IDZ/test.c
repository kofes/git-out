#include <stdio.h>
#include <gmp.h>
static mpf_t sum;
static mpf_t result;
void Sum(void (*f)(unsigned long int), unsigned int from, unsigned long int _eps){
	unsigned long int n;
	mpf_t dec_eps;
	n = from;
	mpf_set_d(sum, 0);
	mpf_init_set_d(dec_eps, 0.1);
	mpf_pow_ui(dec_eps, dec_eps, _eps);
	do{
		f(n);
		mpf_add(sum, sum, result);
		++n;
	}while (mpf_cmp(dec_eps, result) < 0);
}

void f(unsigned long int n){
	mpz_t fac;
	mpf_t dfac;
	
	mpz_init(fac);
	mpf_init(dfac);

	mpz_fac_ui(fac, 2*n + 2);
	mpf_set_z(result, fac);
	
	mpz_fac_ui(fac, 3*n + 2);
	mpf_set_z(dfac, fac);
	mpf_div(result, result, dfac);
	
	mpf_set_ui(dfac, 2);
	mpf_pow_ui(dfac, dfac, n);

	mpf_div(result, result, dfac);
}

int main(){
	unsigned long int eps = 0;
	printf("INPUT: ");
	scanf("%i", &eps);
	mpf_set_default_prec(65556);	//количество бит, выделяемых под степень экспоненты
	mpf_inits(result, sum, NULL);
	Sum(f, 1, eps);
	gmp_fprintf(stdout, "ANSWER: %.*Ff\n", eps, sum);
	return 0;
}
