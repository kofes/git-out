#include <stdio.h>
#include <gmp.h>
static mpf_t sum;
static mpf_t result;
void Sum(void (*f)(unsigned long int), unsigned int from, unsigned long int _eps){
	unsigned long int n;
	mpf_t dec_eps;
	n = from;
	mpf_set_d(sum, 0);
	mpf_init_set_d(dec_eps, 0.1);
	mpf_pow_ui(dec_eps, dec_eps, _eps);
	do{
		f(n);
		mpf_add(sum, sum, result);
		++n;
	}while (mpf_cmp(dec_eps, result) < 0);
}

void f(unsigned long int n){
	mpf_t pow_f;
	mpf_init_set_ui(pow_f, 4);
	mpf_set_ui(result, n);
	mpf_pow_ui(pow_f, pow_f, n);
	mpf_sub_ui(pow_f, pow_f, 3);
	mpf_div(result, result, pow_f);
}

int main(){
	unsigned long int eps = 0;
	printf("INPUT: ");
	scanf("%i", &eps);
	mpf_set_default_prec(65556);	//количество бит, выделяемых под степень экспоненты
	mpf_inits(result, sum, NULL);
	Sum(f, 1, eps);
	gmp_fprintf(stdout, "ANSWER: %.*Ff\n", eps, sum);
	return 0;
}