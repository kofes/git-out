<?php
	session_start();
	include 'config.php';
	include 'functions.php';

	$db = new SQLite3(SQLITE_NAME);

	$menu = get_menu($db);
	if (!$menu){
		exit($db->escapeString());
	}

	$text = get_articles($db);
	if (!$text)
		exit($db->escapeString());
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta name="" content="">
		<link rel="stylesheet" href="style.css">
		<meta charset="utf-8">
	</head>
	<body>
		<div id="wrap">
			<div id="header">
				<h1>Label</h1>
			</div>
			<div id="menu">
				<?php
						echo "<ul>";
					foreach ($menu as $item) {
						echo "<li><a href='".$item['link_menu']."'>".$item['name_menu']."</a></li>";
					}
						echo "</ul>";
				?>
			</div>
			<div id="content">
				<?php
					foreach ($text as $value) {
						echo "<h3><a href='?id=".$value['id_articles']."'>".$value['name_articles']."</a></h3><br>";
						echo "<p>".$value['text_articles']."</p>";
					}
				?>
			</div>
			<div id="footer">
				<h4>footer</h4>
			</div>
		</div>
	</body>
</html>
