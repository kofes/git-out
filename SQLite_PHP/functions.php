<?php
	function clear($str){
		$str = trim($str);
		$str = strip_tags($str);

		if (get_magic_quotes_gpc()){
			$str = stripslashes($str);
		}
		$sql = SQLITE3::escapeString($str);	//WORK?!

		return $sql;
	}

	function insert_menu($db, $name_menu, $link_menu){
		$sql = "INSERT INTO menu (name_menu, link_menu)
				VALUES('$name_menu','$link_menu');";

		$result = $db->query($sql);

		if ($result) return TRUE;
		else
			return 'ERROR: Can\'t add menu\'s object'.$db->lastErrorMsg()."\n";
	}

	function insert_articles($db, $name_articles, $text_articles){
		$sql = "INSERT INTO articles (name_articles, text_articles)
				VALUES('$name_articles','$text_articles');";

		$result = $db->query($sql);

		if ($result) return TRUE;
		else
			return 'ERROR: Can\'t add article\'s object'.$db->lastErrorMsg()."\n";
	}

	function get_menu($db){
		$sql = "SELECT name_menu, link_menu FROM menu;";

		$result = $db->query($sql);

		for ($i = 0; $i < $result->numColumns(); $i++){
			$row[] = $result->fetchArray(SQLITE3_ASSOC);
		}
		$result->finalize();
		return $row;
	}
	function get_articles($db){
		$sql = "SELECT id_articles, name_articles, text_articles FROM articles;";

		$result = $db->query($sql);

		for ($i = 0; $i < $result->numColumns(); $i++){
			$row[] = $result->fetchArray(SQLITE3_ASSOC);
		}
		$result->finalize();
		return $row;
	}
?>
