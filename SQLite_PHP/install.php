<?php
	include 'config.php';
if (!file_exists(SQLITE_NAME)){

	$db = new SQLite3(SQLITE_NAME);	//В db сохраняем дескриптор db;

	if (!$db)
		exit('ERROR connection_aborted: '.$db->lastErrorMsg()."\n");
	echo ''.SQLITE_NAME.' connected'."\n";

	//AUTOINCREMENT $ NOT NULL - автоматически при PRIMARY KEY
	$sql = "CREATE TABLE articles(
		id_articles INTEGER PRIMARY KEY,
		name_articles,
		text_articles
		);";

	$result = $db->query($sql);

	if (!$result)
		exit('ERROR connection_aborted: '.$db->lastErrorMsg()."\n");
	echo 'Table articles created'."\n";

	$sql2 = "CREATE TABLE menu(
		id_menu INTEGER PRIMARY KEY,
		name_menu,
		link_menu
		);";

	$result2 = $db->query($sql2);

	if (!$result2)
		exit('ERROR connection_aborted: '.$db->lastErrorMsg()."\n");
	echo 'Table menu created'."\n";

	$db->close();
	unset($db);
}
else
	echo ''.SQLITE_NAME.' exists'."\n";
?>
