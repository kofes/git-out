<?php
	session_start();
	include 'config.php';
	include 'functions.php';

	if ($_SERVER['REQUEST_METHOD'] == 'POST'){
		$db = new SQLite3(SQLITE_NAME);
		if (isset($_POST['submit_menu'])){
			$name_menu = clear($_POST['name_menu']);
			$link_menu = clear($_POST['link_menu']);

			$msg = insert_menu($db, $name_menu, $link_menu);
			if ($msg)
				$_SESSION['msg'] = 'Added menu\'s object';
			else
				$_SESSION['msg'] = $msg;
		}

		if (isset($_POST['submit_articles'])){
			$name_articles = clear($_POST['name_articles']);
			$text_articles = clear($_POST['text_articles']);

			$msg = insert_articles($db, $name_articles, $text_articles);
			if ($msg)
				$_SESSION['msg'] = 'Added article\'s object';
			else
				$_SESSION['msg'] = $msg;
		}
		$db->close();
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<link rel="stylesheet" href="style.css">
		<meta charset="utf-8">
	</head>
	<body>
		<div id="wrap">
			<h2>ADMIN PANEL</h2>
			<?php
				echo $_SESSION['msg']."\n";
			?>
			<div id="insert_menu">
				<p>Adding new object to menu</p>
				<form name="insert_menu" method="POST">
					Name menu<br>
					<input type="text" name="name_menu">
					<br>Link<br>
					<input type="text" name="link_menu">
					<br>
					<input type="submit" name="submit_menu">
				</form>
			</div>
			<div id="insert_articles">
				<p>Adding new article</p>
				<form name="insert_articles" method="POST">
					Name article<br>
					<input type="text" name="name_articles">
					<br>Text<br>
					<textarea name="text_articles" rows="10" cols="34"></textarea>
					<input type="submit" name="submit_articles">
				</form>
			</div>
			<div id="footer">
				<h4>footer</h4>
			</div>
		</div>
	</body>
</html>
