#include "lib_string.h"

typedef struct{
	unsigned char ref_count;
	size_t size;
	LIB_STRING_t_char *str;
}t_pointer;

int LIB_STRING_new(LIB_STRING_t_hndl *hndl, const char *buffer, size_t size){
	t_pointer *new;
	if ((hndl == NULL)||(size == 0))
		return LIB_STRING_ERR_DATA;
	if ((new = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_STRING_ERR_NULL;
	if ((new->str = (LIB_STRING_t_char *)malloc(size*sizeof(LIB_STRING_t_char))) == NULL)
	{
		free(new);
		return LIB_STRING_ERR_NULL;
	}
	new->ref_count = 1;
	new->size = size;
	memcpy(new->str, buffer, size);
    LIB_STRING_off(hndl);
	*hndl = (LIB_STRING_t_hndl )new;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_off(LIB_STRING_t_hndl *hndl){
	t_pointer *new;
	if (hndl == NULL)
		return LIB_STRING_ERR_DATA;
	new = (t_pointer *)(*hndl);
    if (*hndl == NULL)
        return LIB_STRING_ERR_NORM;
    --(new->ref_count);
	if (new->ref_count == 0){
		free(new->str);
		free(new);
	}
    *hndl = NULL;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_set(LIB_STRING_t_hndl *hndl, LIB_STRING_t_hndl source){
	t_pointer *new;
	if (hndl == NULL)
		return LIB_STRING_ERR_DATA;
    if (source == NULL)
        return LIB_STRING_off(hndl);
    if (*hndl == source)
        return LIB_STRING_ERR_NORM;
	new = (t_pointer *)source;
	if (new->ref_count == 255)
		return LIB_STRING_ERR_NULL;
	++(new->ref_count);
    LIB_STRING_off(hndl);
	*hndl = (LIB_STRING_t_hndl )new;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_cpy(LIB_STRING_t_hndl *hndl, LIB_STRING_t_hndl source){
	t_pointer *new;
	if (hndl == NULL)
		return LIB_STRING_ERR_DATA;
    if (source == NULL)
        return LIB_STRING_off(hndl);
	if ((new = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_STRING_ERR_NULL;
	if ((new->str = (LIB_STRING_t_char *)malloc(((t_pointer *)source)->size * sizeof(LIB_STRING_t_char))) == NULL){
		free(new);
		return LIB_STRING_ERR_NULL;
	}
	new->size = ((t_pointer *)source)->size;
	new->ref_count = 1;
	memcpy(new->str, ((t_pointer *)source)->str, new->size);
    LIB_STRING_off(hndl);
	*hndl = (LIB_STRING_t_hndl )new;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_sub(LIB_STRING_t_hndl *result, LIB_STRING_t_hndl source, size_t start, size_t size){
	t_pointer *new;
	size_t i;
	if ((result == NULL)||(source == NULL)||(size == 0)||(((t_pointer *)source)->size < start))
		return LIB_STRING_ERR_DATA;
	if ((start + size) > (((t_pointer *)source)->size))
		size = (((t_pointer *)source)->size) - start;
	if ((new = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_STRING_ERR_NULL;
	if ((new->str = (LIB_STRING_t_char *)malloc((size + start)*sizeof(LIB_STRING_t_char))) == NULL){
		free(new);
		return LIB_STRING_ERR_NULL;
	}
	new->ref_count = 1;
	for (i = 0; i < size; ++i)
		new->str[i] = ((t_pointer *)source)->str[start + i];
	new->size = start + size;
    LIB_STRING_off(result);
	*result = new;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_cat(LIB_STRING_t_hndl *result, LIB_STRING_t_hndl left, LIB_STRING_t_hndl right){
	t_pointer *new, *n_left, *n_right;
	size_t i;
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return LIB_STRING_ERR_DATA;
	n_left = (t_pointer *)left;
	n_right = (t_pointer *)right;
	if ((new = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_STRING_ERR_NULL;
	if ((new->str = (LIB_STRING_t_char *)malloc((n_left->size + n_right->size)*sizeof(LIB_STRING_t_char))) == NULL){
		free(new);
		return LIB_STRING_ERR_NULL;
	}
	new->size = n_left->size + n_right->size;
	new->ref_count = 1;
	for (i = 0; i < n_left->size; ++i)
		new->str[i] = n_left->str[i];
	for (i = 0; i < n_right->size; ++i)
		new->str[n_left->size + i] = n_right->str[i];
	LIB_STRING_off(result);
	*result = (LIB_STRING_t_hndl )new;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_add(LIB_STRING_t_hndl *result, LIB_STRING_t_hndl left, LIB_STRING_t_char end){
	t_pointer *new;
	if ((result == NULL)||(left == NULL))
		return LIB_STRING_ERR_DATA;
	if ((new = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_STRING_ERR_NULL;
	new->ref_count = 1;
	if ((new->str = (LIB_STRING_t_char *)malloc((((t_pointer *)left)->size + 1)*sizeof(LIB_STRING_t_char))) == NULL){
		free(new);
		return LIB_STRING_ERR_NULL;
	}
	new->size = ((t_pointer *)left)->size + 1;
	memcpy(new->str, ((t_pointer *)left)->str, (new->size - 1));
	new->str[new->size - 1] = end;
    LIB_STRING_off(result);
	*result = (LIB_STRING_t_hndl )new;
	return LIB_STRING_ERR_NORM;
}

int LIB_STRING_cmp(LIB_STRING_t_hndl left, LIB_STRING_t_hndl right){
	t_pointer *n_left, *n_right;
	size_t i;
	if ((left == NULL)||(right == NULL))
		return LIB_STRING_ERR_DATA;
	n_left = (t_pointer *)left;
	n_right = (t_pointer *)right;
	for (i = 0; (i < n_left->size)&&(i < n_right->size); ++i){
		if (n_left->str[i] > n_right->str[i])
			return 1;
		if (n_left->str[i] < n_right->str[i])
			return -1;
	}
	if (n_left->size > n_right->size)
		return 1;
	if (n_left->size < n_right->size)
		return -1;
    return 0;
}

void LIB_STRING_set_char(LIB_STRING_t_hndl hndl, size_t ind, LIB_STRING_t_char val){
	if ((hndl == NULL)||(((t_pointer *)hndl)->size <= ind))
		return;
	((t_pointer *)hndl)->str[ind] = val;
}

LIB_STRING_t_char LIB_STRING_get_char(LIB_STRING_t_hndl hndl, size_t ind){
	if ((hndl == NULL)||(((t_pointer *)hndl)->size <= ind))
		return 0;
	return ((t_pointer *)hndl)->str[ind];
}

size_t LIB_STRING_get_size(LIB_STRING_t_hndl hndl){
	if (hndl == NULL)
		return 0;
	return ((t_pointer *)hndl)->size;
}
