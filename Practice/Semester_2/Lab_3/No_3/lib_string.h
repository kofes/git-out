/**
 * Текущий заголовочный файл содержит интерфейсы функций,
 * предназначенных для работы со строками
 * посредством "умных указателей" (SmartPointer)
 * с подсчетом ссылок.
 */

#ifndef LIB_STRING_H
#define LIB_STRING_H

#include <string.h>
#include <stdlib.h>

/**
 * Объявление используемых типов данных:
 */

/* Тип символа в строке */
typedef char LIB_STRING_t_char;

/* Тип дескриптора строки */
typedef void *LIB_STRING_t_hndl;

/* Признак открепленного дескриптора (используется при инициализации) */
#define LIB_STRING_NULL (NULL)

/* Коды ошибок */
#define LIB_STRING_ERR_NORM (0)	//Ошибки отсутствуют;
#define LIB_STRING_ERR_NULL (1)	//Ошибка выделения памяти;
#define LIB_STRING_ERR_DATA (2)	//Некорректные входные параметры;

/**
 * Функции для работы со строками:
 */

/* Генерирует новую строку на основе массива символов и связывает ее с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_STRING_new(LIB_STRING_t_hndl *hndl, const char *buffer, size_t size);

/* Открепляет указатель от связанной с ним строки */
/* Возвращает код ошибки */
extern int LIB_STRING_off(LIB_STRING_t_hndl *hndl);

/* Присваивает указателю ссылку на строку, связанную с другим указателем */
/* Возвращает код ошибки */
extern int LIB_STRING_set(LIB_STRING_t_hndl *hndl, LIB_STRING_t_hndl source);

/* Копирует строку в новую область памяти и связывает ее с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_STRING_cpy(LIB_STRING_t_hndl *hndl, LIB_STRING_t_hndl source);

/* Копирует подстроку в новую область памяти и связывает ее с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_STRING_sub(LIB_STRING_t_hndl *result, LIB_STRING_t_hndl source, size_t start, size_t size);

/* Выполняет конкатенацию двух строк и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_STRING_cat(LIB_STRING_t_hndl *result, LIB_STRING_t_hndl left, LIB_STRING_t_hndl right);

/* Генерирует новую строку, путем добавления символа в конец существующей строки,
   и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_STRING_add(LIB_STRING_t_hndl *result, LIB_STRING_t_hndl left, LIB_STRING_t_char end);

/* Выполняет сравнение двух строк в лексикографическом порядке */
/* Возвращает:
   0 -- если строки совпадают,
  +1 -- если первая строка больше второй,
  -1 -- если меньше */
extern int LIB_STRING_cmp(LIB_STRING_t_hndl left, LIB_STRING_t_hndl right);

/* Задает значение символа строки по индексу */
extern void LIB_STRING_set_char(LIB_STRING_t_hndl hndl, size_t ind, LIB_STRING_t_char val);

/* Возвращает значение символа по индексу */
extern LIB_STRING_t_char LIB_STRING_get_char(LIB_STRING_t_hndl hndl, size_t ind);

/* Возвращает размер строки */
extern size_t LIB_STRING_get_size(LIB_STRING_t_hndl hndl);

#endif //LIB_STRING_H
