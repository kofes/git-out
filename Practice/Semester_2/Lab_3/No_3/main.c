#include <stdio.h>
#include "lib_string.h"

int main(){
    LIB_STRING_t_hndl p1, p2, p3;
    char *buff;
    size_t i, len;
    len = 5;
    p1 = LIB_STRING_NULL;
    p2 = LIB_STRING_NULL;
    p3 = LIB_STRING_NULL;
    if ((buff = (char *)malloc(len*sizeof(char))) == NULL)
        return 1;
    for (i = 0; i < len; ++i)
        buff[i] = 97 + i;
    LIB_STRING_new(&p1, buff, len);
    LIB_STRING_new(&p2, buff, len);
    for (i = 0; i < LIB_STRING_get_size(p1); ++i)
        printf("%c", LIB_STRING_get_char(p1, i));
    printf("\n");
    LIB_STRING_cat(&p3, p1, p2);
    LIB_STRING_set_char(p3, 2, 'o');
    LIB_STRING_add(&p3, p3, 'f');
    for (i = 0; i < LIB_STRING_get_size(p3); ++i)
        printf("%c", LIB_STRING_get_char(p3, i));
    printf("\n");
    if (LIB_STRING_cmp(p3, p1) == 1)
        printf("string 3 > string 1\n");
    LIB_STRING_off(&p1);
    LIB_STRING_set(&p1, p2);
    LIB_STRING_off(&p2);
    for (i = 0; i < LIB_STRING_get_size(p1); ++i)
        printf("%c", LIB_STRING_get_char(p1, i));
    printf("\n");
    LIB_STRING_off(&p1);
    LIB_STRING_sub(&p1, p3, 0, 5);
    for (i = 0; i < LIB_STRING_get_size(p1); ++i)
        printf("%c", LIB_STRING_get_char(p1, i));
    printf("\n");
    LIB_STRING_off(&p1);
    LIB_STRING_off(&p3);
    return 0;
}