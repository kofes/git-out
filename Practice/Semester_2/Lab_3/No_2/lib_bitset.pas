{Текущий модуль содержит объявление функций, предназначенных для работы с битовыми множествами,
 реализация которых вынесена в файл 'lib_bitset.dll'}
unit LIB_BITSET;

interface

uses
  Classes, SysUtils;

type
  //Тип дескриптора для битовой карты:
  LIB_BITSET_t_hndl = pointer;
  //Тип элемента во множестве:
  LIB_BITSET_t_elem = char;

const
  //Признак открепленного дескриптора (используется при инициализации):
  LIB_BITSET_NULL = nil;
  //Максимальное и минимальное значение элемента во множестве:
  LIB_BITSET_MAX_ELEM = high(char);
  LIB_BITSET_MIN_ELEM = 0;
  //Коды ошибок:
  LIB_BITSET_ERR_NORM = 0;	//Ошибки отсутствуют;
  LIB_BITSET_ERR_NULL = 1;	//Ошибка выделения памяти;
  LIB_BITSET_ERR_DATA = 2;	//Некорректные входные параметры;

{Функции для работы с битовыми множествами:}

//Выделяет память под битовую карту пустого множества и связывает ее с заданным указателем;
//Возвращает код ошибки;
function LIB_BITSET_new(var HNDL: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Открепляет указатель от битовой карты;
//Возвращает код ошибки;
function LIB_BITSET_off(var HNDL: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Перемещает указатель на множество, связанное с другим указателем;
//Возвращает код ошибки;
function LIB_BITSET_set(var HNDL: LIB_BITSET_t_hndl; SRC: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Копирует множество в новую область памяти и связывает ее с заданным указателем;
//Возвращает код ошибки;
function LIB_BITSET_cpy(var HNDL: LIB_BITSET_t_hndl; SRC: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Выполняет объединение двух множеств и связывает результат с заданным указателем;
//Возвращает код ошибки;
function LIB_BITSET_add(var ANS: LIB_BITSET_t_hndl; OP1, OP2: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Вычитает одно множество из другого и связывает результат с заданным указателем;
//Возвращает код ошибки;
function LIB_BITSET_sub(var ANS: LIB_BITSET_t_hndl; OP1, OP2: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Выполняет пересечение двух множеств и связывает результат с заданным указателем;
//Возвращает код ошибки;
function LIB_BITSET_mul(var ANS: LIB_BITSET_t_hndl; OP1, OP2: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Вычисляет симметрическую разность двух множеств и связывает результат с заданным указателем;
//Возвращает код ошибки;
function LIB_BITSET_div(var ANS: LIB_BITSET_t_hndl; OP1, OP2: LIB_BITSET_t_hndl): integer;
cdecl; external 'lib_bitset.dll';

//Очищает битовую карту (заполняет нулями);
procedure LIB_BITSET_clr(HNDL: LIB_BITSET_t_hndl);
cdecl; external 'lib_bitset.dll';

//Добавляет элемент во множество;
procedure LIB_BITSET_set_elem(HNDL: LIB_BITSET_t_hndl; ELEM: LIB_BITSET_t_elem);
cdecl; external 'lib_bitset.dll';

//Исключает элемент из множества;
procedure LIB_BITSET_off_elem(HNDL: LIB_BITSET_t_hndl; ELEM: LIB_BITSET_t_elem);
cdecl; external 'lib_bitset.dll';

//Проверяет принадлежность элемента множеству;
function LIB_BITSET_get_elem(HNDL: LIB_BITSET_t_hndl; ELEM: LIB_BITSET_t_elem): boolean;
cdecl; external 'lib_bitset.dll';

//Возвращает текущее количество элементов во множестве;
function LIB_BITSET_get_size(HNDL: LIB_BITSET_t_hndl): dword;
cdecl; external 'lib_bitset.dll';

implementation

end.
