#include <stdio.h>
#include "lib_bitset.h"

//Объединение множеств, содержащих элементы с 0-99 и 200-255
void test1(){
    LIB_BITSET_t_hndl p1, p2, p3;
    unsigned short i;
    p1 = LIB_BITSET_NULL;
    p2 = LIB_BITSET_NULL;
    p3 = LIB_BITSET_NULL;
    LIB_BITSET_new(&p1);
    LIB_BITSET_new(&p2);
    LIB_BITSET_new(&p3);
    for (i = 0; i < 100; ++i)
        LIB_BITSET_set_elem(p1, i);
    for (i = 200; i < 256; ++i)
        LIB_BITSET_set_elem(p2, i);
    LIB_BITSET_add(&p3, p1, p2);
    for (i = 0; i < 256; ++i)
        if (LIB_BITSET_get_elem(p3, i))
            printf("%i ", i);
    printf("\n");
    LIB_BITSET_off(&p1);
    LIB_BITSET_off(&p2);
    LIB_BITSET_off(&p3);
}
//Пересечение множеств, содержащих элементы с 0-200 и 150-255
void test2(){
    LIB_BITSET_t_hndl p1, p2, p3;
    unsigned short i;
    p1 = LIB_BITSET_NULL;
    p2 = LIB_BITSET_NULL;
    p3 = LIB_BITSET_NULL;
    LIB_BITSET_new(&p1);
    LIB_BITSET_new(&p2);
    LIB_BITSET_new(&p3);
    for (i = 0; i < 201; ++i)
        LIB_BITSET_set_elem(p1, i);
    for (i = 150; i < 256; ++i)
        LIB_BITSET_set_elem(p2, i);
    LIB_BITSET_mul(&p3, p1, p2);
    for (i = 0; i < 256; ++i)
        if (LIB_BITSET_get_elem(p3, i))
            printf("%i ", i);
    printf("\n");
    LIB_BITSET_off(&p1);
    LIB_BITSET_off(&p2);
    LIB_BITSET_off(&p3);
}
//Вычитание из множество, содержащего элементы с 0-150, множество,
//содержащее элементы с 100 - 255
void test3(){
    LIB_BITSET_t_hndl p1, p2, p3;
    unsigned short i;
    p1 = LIB_BITSET_NULL;
    p2 = LIB_BITSET_NULL;
    p3 = LIB_BITSET_NULL;
    LIB_BITSET_new(&p1);
    LIB_BITSET_new(&p2);
    LIB_BITSET_new(&p3);
    for (i = 0; i < 150; ++i)
        LIB_BITSET_set_elem(p1, i);
    for (i = 100; i < 256; ++i)
        LIB_BITSET_set_elem(p2, i);
    LIB_BITSET_sub(&p3, p1, p2);
    for (i = 0; i < 256; ++i)
        if (LIB_BITSET_get_elem(p3, i))
            printf("%i ", i);
    printf("\n");
    LIB_BITSET_off(&p1);
    LIB_BITSET_off(&p2);
    LIB_BITSET_off(&p3);
}
//Симметрическая разность множеств, содержащих элементы с
////0-150 и c 100-256
void test4(){
    LIB_BITSET_t_hndl p1, p2, p3;
    unsigned short i;
    p1 = LIB_BITSET_NULL;
    p2 = LIB_BITSET_NULL;
    p3 = LIB_BITSET_NULL;
    LIB_BITSET_new(&p1);
    LIB_BITSET_new(&p2);
    LIB_BITSET_new(&p3);
    for (i = 0; i < 150; ++i)
        LIB_BITSET_set_elem(p1, i);
    for (i = 100; i < 256; ++i)
        LIB_BITSET_set_elem(p2, i);
    LIB_BITSET_div(&p3, p1, p2);
    for (i = 0; i < 256; ++i)
        if (LIB_BITSET_get_elem(p3, i))
            printf("%i ", i);
    printf("\n");
    LIB_BITSET_off(&p1);
    LIB_BITSET_off(&p2);
    LIB_BITSET_off(&p3);
}
//Новый указатель ссылается на старый блок памяти
void test5(){
    LIB_BITSET_t_hndl p1, p2;
    unsigned short i;
    p1 = LIB_BITSET_NULL;
    p2 = LIB_BITSET_NULL;
    LIB_BITSET_new(&p1);
    LIB_BITSET_set(&p2, p1);
    for (i = 0; i < 150; ++i)
        LIB_BITSET_set_elem(p1, i);
    LIB_BITSET_off(&p1);
    for (i = 0; i < 256; ++i)
        if (LIB_BITSET_get_elem(p2, i))
            printf("%i ", i);
    printf("\n");
    LIB_BITSET_off(&p2);
}
//Выполняет копирование блока памяти в новое место
void test6(){
    LIB_BITSET_t_hndl p1, p2;
    unsigned short i;
    p1 = LIB_BITSET_NULL;
    p2 = LIB_BITSET_NULL;
    LIB_BITSET_new(&p1);
    for (i = 0; i < 150; ++i)
        LIB_BITSET_set_elem(p1, i);
    LIB_BITSET_cpy(&p2, p1);
    LIB_BITSET_clr(p1);
    for (i = 0; i < 256; ++i)
        if (LIB_BITSET_get_elem(p2, i))
            printf("%i ", i);
    printf("\n");
    LIB_BITSET_off(&p1);
    LIB_BITSET_off(&p2);
}


int main(){
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
	return 0;
}