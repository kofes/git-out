/**
 * Текущий заголовочный файл содержит интерфейсы функций,
 * предназначенных для работы с битовыми множествами
 * посредством "умных указателей" (SmartPointer)
 * с подсчетом ссылок.
 */

#ifndef LIB_BITSET_H
#define LIB_BITSET_H

#include <limits.h>
#include <stdlib.h>

/**
 * Объявление используемых типов данных:
 */

/* Тип элемента множества */
typedef unsigned char LIB_BITSET_t_elem;

/* Максимальное и минимальное значение элемента во множестве */
#define LIB_BITSET_MAX_ELEM (UCHAR_MAX)
#define LIB_BITSET_MIN_ELEM (0)

/* Тип дескриптора множества */
typedef void *LIB_BITSET_t_hndl;

/* Признак открепленного дескриптора (используется при инициализации) */
#define LIB_BITSET_NULL (NULL)

/* Коды ошибок */
#define LIB_BITSET_ERR_NORM (0)	//Ошибки отсутствуют;
#define LIB_BITSET_ERR_NULL (1)	//Ошибка выделения памяти;
#define LIB_BITSET_ERR_DATA (2)	//Некорректные входные параметры;

/**
 * Функции для работы с битовыми множествами:
 */

/* Выделяет память под битовую карту пустого множества и связывает ее с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_new(LIB_BITSET_t_hndl *hndl);

/* Открепляет указатель от битовой карты */
/* Возвращает код ошибки */
extern int LIB_BITSET_off(LIB_BITSET_t_hndl *hndl);

/* Перемещает указатель на множество, связанное с другим указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_set(LIB_BITSET_t_hndl *hndl, LIB_BITSET_t_hndl source);

/* Копирует множество в новую область памяти и связывает ее с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_cpy(LIB_BITSET_t_hndl *hndl, LIB_BITSET_t_hndl source);

/* Выполняет объединение двух множеств и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_add(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right);

/* Вычитает одно множество из другого и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_sub(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right);

/* Выполняет пересечение двух множеств и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_mul(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right);

/* Вычисляет симметрическую разность двух множеств и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITSET_div(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right);

/* Очищает битовую карту (заполняет нулями) */
extern void LIB_BITSET_clr(LIB_BITSET_t_hndl hndl);

/* Добавляет элемент во множество */
extern void LIB_BITSET_set_elem(LIB_BITSET_t_hndl hndl, LIB_BITSET_t_elem elem);

/* Исключает элемент из множества */
extern void LIB_BITSET_off_elem(LIB_BITSET_t_hndl hndl, LIB_BITSET_t_elem elem);

/* Проверяет принадлежность элемента множеству */
/* Возвращает:
   1 -- если элемент содержится во множестве,
   0 -- в противном случае */
extern int LIB_BITSET_get_elem(LIB_BITSET_t_hndl hndl, LIB_BITSET_t_elem elem);

/* Возвращает текущее количество элементов во множестве */
extern size_t LIB_BITSET_get_size(LIB_BITSET_t_hndl hndl);

#endif //LIB_BITSET_H
