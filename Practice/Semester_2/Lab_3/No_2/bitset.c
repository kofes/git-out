#include "lib_bitset.h"
//
#include <stdio.h>
//
#define BITMAP_SIZE ((LIB_BITSET_MAX_ELEM+1)/CHAR_BIT)

typedef struct{
	unsigned char ref_counter;
	unsigned char BITMAP[BITMAP_SIZE];
}t_pointer;

int LIB_BITSET_new(LIB_BITSET_t_hndl *hndl){
	t_pointer *pointer;
	size_t i;
	if (hndl == NULL)
		return LIB_BITSET_ERR_DATA;
	if ((pointer = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_BITSET_ERR_NULL;
	pointer->ref_counter = 1;
	for (i = 0; i < BITMAP_SIZE; ++i)
		pointer->BITMAP[i] = 0;
    LIB_BITSET_off(hndl);
	*hndl = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_off(LIB_BITSET_t_hndl *hndl){
	t_pointer *pointer;
	if (hndl == NULL)
		return LIB_BITSET_ERR_DATA;
	pointer = (t_pointer *)(*hndl);
	if (pointer == NULL)
		return LIB_BITSET_ERR_NORM;
    --(pointer->ref_counter);
	if (pointer->ref_counter == 0)
		free(*hndl);
    *hndl = NULL;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_set(LIB_BITSET_t_hndl *hndl, LIB_BITSET_t_hndl source){
	t_pointer *pointer;
	if (hndl == NULL)
		return LIB_BITSET_ERR_DATA;
    if (*hndl == source)
        return LIB_BITSET_ERR_NORM;
    LIB_BITSET_off(hndl);
    if (source == NULL)
        return LIB_BITSET_ERR_NORM;
	pointer = (t_pointer *)source;
	if (pointer->ref_counter == UCHAR_MAX)
		return LIB_BITSET_ERR_NULL;
	++(pointer->ref_counter);
	*hndl = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_cpy(LIB_BITSET_t_hndl *hndl, LIB_BITSET_t_hndl source){
	t_pointer *pointer;
	size_t i;
	if (hndl == NULL)
		return LIB_BITSET_ERR_DATA;
    if (source == NULL)
        return LIB_BITSET_off(hndl);
	if ((pointer = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_BITSET_ERR_NULL;
	pointer->ref_counter = 1;
	for (i = 0; i < BITMAP_SIZE; ++i)
		pointer->BITMAP[i] = ((t_pointer *)source)->BITMAP[i];
    LIB_BITSET_off(hndl);
	*hndl = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_add(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right){
	t_pointer *pointer;
	size_t i;
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return LIB_BITSET_ERR_DATA;
	if ((pointer = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_BITSET_ERR_NULL;
	for (i = 0; i < BITMAP_SIZE; ++i)
		pointer->BITMAP[i] = (((t_pointer *)right)->BITMAP[i])|(((t_pointer *)left)->BITMAP[i]);
    LIB_BITSET_off(result);
	*result = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_sub(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right){
	t_pointer *pointer;
	size_t i;
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return LIB_BITSET_ERR_DATA;
	if ((pointer = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_BITSET_ERR_NULL;
	for (i = 0; i < BITMAP_SIZE; ++i)
		pointer->BITMAP[i] = (((t_pointer *)left)->BITMAP[i])&(~(((t_pointer *)right)->BITMAP[i]));
    LIB_BITSET_off(result);
	*result = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_mul(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right){
	t_pointer *pointer;
	size_t i;
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return LIB_BITSET_ERR_DATA;
	if ((pointer = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_BITSET_ERR_NULL;
	for (i = 0; i < BITMAP_SIZE; ++i)
		pointer->BITMAP[i] = (((t_pointer *)right)->BITMAP[i])&(((t_pointer *)left)->BITMAP[i]);
    LIB_BITSET_off(result);
	*result = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

int LIB_BITSET_div(LIB_BITSET_t_hndl *result, LIB_BITSET_t_hndl left, LIB_BITSET_t_hndl right){
	t_pointer *pointer;
	size_t i;
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return LIB_BITSET_ERR_DATA;
	if ((pointer = (t_pointer *)malloc(sizeof(t_pointer))) == NULL)
		return LIB_BITSET_ERR_NULL;
	for (i = 0; i < BITMAP_SIZE; ++i)
		pointer->BITMAP[i] = ((((t_pointer *)right)->BITMAP[i])&(~((t_pointer *)left)->BITMAP[i]))|((~((t_pointer *)right)->BITMAP[i])&(((t_pointer *)left)->BITMAP[i]));
    LIB_BITSET_off(result);
	*result = (LIB_BITSET_t_hndl )pointer;
	return LIB_BITSET_ERR_NORM;
}

void LIB_BITSET_clr(LIB_BITSET_t_hndl hndl){
	size_t i;
	if (hndl == NULL)
		return;
	for (i = 0; i < BITMAP_SIZE; ++i)
		((t_pointer *)hndl)->BITMAP[i] = 0;
}

void LIB_BITSET_set_elem(LIB_BITSET_t_hndl hndl, LIB_BITSET_t_elem elem){
	if (hndl == NULL)
		return;
	((t_pointer *)hndl)->BITMAP[elem/CHAR_BIT] |= (1<<(elem % CHAR_BIT));
}

void LIB_BITSET_off_elem(LIB_BITSET_t_hndl hndl, LIB_BITSET_t_elem elem){
	if (hndl == NULL)
		return;
	((t_pointer *)hndl)->BITMAP[elem/CHAR_BIT] &= ~(1<<(elem % CHAR_BIT));
}

int LIB_BITSET_get_elem(LIB_BITSET_t_hndl hndl, LIB_BITSET_t_elem elem){
	if (hndl == NULL)
		return 0;
	if ((((t_pointer *)hndl)->BITMAP[elem/CHAR_BIT])&(1<<(elem % CHAR_BIT)))
		return 1;
	return 0;
}

size_t LIB_BITSET_get_size(LIB_BITSET_t_hndl hndl){
	size_t i, k, size;
	if (hndl == NULL)
		return 0;
    size = 0;
	for (k = 0; k < BITMAP_SIZE; ++k)
		for (i = 0; i < CHAR_BIT; ++i)
			if (((t_pointer *)hndl)->BITMAP[k] & (1<<i))
				++size;
	return size;
}
