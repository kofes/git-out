#include "lib_number.h"
#include <stdint.h>
#include <string.h>
#define TO_VOID(NUM) ((LIB_NUMBER_t_hndl )(NUM))
#define MIN_KARATSUBA (SIZE_MAX)
#define LN(VOID) ((t_num *)(VOID))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
typedef struct{
    LIB_NUMBER_t_byte *buff;
    LIB_NUMBER_t_base base;
    LIB_NUMBER_t_sign sign;
    size_t size, ref_count;
}t_num;

int LIB_NUMBER_new(LIB_NUMBER_t_hndl *hndl, long long number, LIB_NUMBER_t_base base){
    t_num *num;
    size_t i, size;
    long long dnum;
    if ((hndl == NULL)||(base > LIB_NUMBER_MAX_BASE)||(base < LIB_NUMBER_MIN_BASE))
        return LIB_NUMBER_ERR_DATA;
    if ((num = (t_num *)malloc(sizeof(t_num))) == NULL)
        return LIB_NUMBER_ERR_NULL;
    num->sign = 1;
    if (number < 0){
        number *= -1;
        num->sign = -1;
    }
    dnum = number;
    size = 0;
    do{
        dnum /= base;
        ++size;
    }while (dnum != 0);
    if ((num->buff = (LIB_NUMBER_t_byte *)malloc(size*sizeof(LIB_NUMBER_t_byte))) == NULL){
        free(num);
        return LIB_NUMBER_ERR_NULL;
    }
    num->size = size;
    num->base = base;
    num->ref_count = 1;
    for (i = 0; i < size; ++i){
        num->buff[i] = number%base;
        number /= base;
    }
    LIB_NUMBER_off(hndl);
    *hndl = (LIB_NUMBER_t_hndl )num;
    return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_off(LIB_NUMBER_t_hndl *hndl){
    t_num *num;
    if (hndl == NULL)
        return LIB_NUMBER_ERR_DATA;
    if (*hndl == NULL)
        return LIB_NUMBER_ERR_NORM;
    num = (t_num *)(*hndl);
    --(num->ref_count);
    if (num->ref_count == 0){
        free(num->buff);
        free(num);
    }
    *hndl = NULL;
    return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_set(LIB_NUMBER_t_hndl *hndl, LIB_NUMBER_t_hndl source){
    if (hndl == NULL)
        return LIB_NUMBER_ERR_DATA;
    if (source == NULL)
        return LIB_NUMBER_off(hndl);
    if (*hndl == source)
        return LIB_NUMBER_ERR_NORM;
    LIB_NUMBER_off(hndl);
    if (LN(source)->ref_count == SIZE_MAX)
        return LIB_NUMBER_ERR_NULL;
    *hndl = source;
    ++(LN(source)->ref_count);
    return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_cpy(LIB_NUMBER_t_hndl *hndl, LIB_NUMBER_t_hndl source){
  size_t i;
  if (hndl == NULL)
    return LIB_NUMBER_ERR_DATA;
  if (source == NULL)
    return LIB_NUMBER_off(hndl);
  if ((*hndl == source)&&(LN(source)->ref_count == 1))
    return LIB_NUMBER_ERR_NORM;
  LIB_NUMBER_off(hndl);
  if ((*hndl = (t_num *)malloc(sizeof(t_num))) == NULL)
    return LIB_NUMBER_ERR_NULL;
  LN(*hndl)->ref_count = 1;
  LN(*hndl)->size = LN(source)->size;
  LN(*hndl)->sign = LN(source)->sign;
  LN(*hndl)->base = LN(source)->base;
  if ((LN(*hndl)->buff = (LIB_NUMBER_t_byte *)malloc((LN(source)->size)*sizeof(LIB_NUMBER_t_byte))) == NULL){
    free(*hndl);
    return LIB_NUMBER_ERR_NULL;
  }
  for (i = 0; i < LN(source)->size; ++i)
    LN(*hndl)->buff[i] = LN(source)->buff[i];
  return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_add(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right){
  t_num *num;
  size_t i, size, min;
  if ((result == NULL)||(left == NULL)||(right == NULL)||(LN(left)->base != LN(right)->base))
    return LIB_NUMBER_ERR_DATA;
  if (LN(left)->sign != LN(right)->sign){
    int err;
    LN(right)->sign *= -1;
    err = LIB_NUMBER_sub(result, left, right);
    LN(right)->sign *= -1;
    return err;
  }
  if ((num = (t_num *)malloc(sizeof(t_num))) == NULL)
    return LIB_NUMBER_ERR_NULL;
  size = MAX(LN(left)->size, LN(right)->size);
  min = MIN(LN(left)->size, LN(right)->size);
// size++ = MAX(LN(left)->size, LN(right)->size);
  ++size;
  if ((num->buff = (LIB_NUMBER_t_byte *)malloc(size*sizeof(LIB_NUMBER_t_byte))) == NULL){
    free(num);
    return LIB_NUMBER_ERR_NULL;
  }
  memset(num->buff, 0, size);
  num->base = LN(left)->base;
  num->sign = LN(left)->sign;
  num->size = size;
  num->ref_count = 1;
  for (i = 0; i < min; ++i){
    num->buff[i] += (LN(left)->buff[i] + LN(right)->buff[i])%(num->base);
    num->buff[i+1] = (LN(left)->buff[i] + LN(right)->buff[i])/(num->base);
  }
  while (i < LN(left)->size){
    num->buff[i+1] = (num->buff[i] + LN(left)->buff[i])/(num->base);
    num->buff[i] = (num->buff[i] + LN(left)->buff[i])%(num->base);
    ++i;
  }
  while (i < LN(right)->size){
    num->buff[i+1] = (num->buff[i] + LN(right)->buff[i])/(num->base);
    num->buff[i] = (num->buff[i] + LN(right)->buff[i])%(num->base);
    ++i;
  }
  LIB_NUMBER_off(result);
  if (num->buff[size - 1] == 0){
    --(num->size);
    num->buff = realloc(num->buff, num->size);
  }
  *result = (LIB_NUMBER_t_hndl )num;
  return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_sub(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right){
  t_num *num;
  size_t i, size, min;
  short diff;
  char carry;
  if ((result == NULL)||(left == NULL)||(right == NULL)||(LN(left)->base != LN(right)->base))
    return LIB_NUMBER_ERR_DATA;
  if (LN(left)->sign != LN(right)->sign){
    int err;
    LN(right)->sign *= -1;
    err = LIB_NUMBER_add(result, left, right);
    LN(right)->sign *= -1;
    return err;
  }
  if ((num = (t_num *)malloc(sizeof(t_num))) == NULL)
    return LIB_NUMBER_ERR_NULL;
  size = MAX(LN(left)->size, LN(right)->size);
  min = MIN(LN(left)->size, LN(right)->size);
// size++ = MAX(LN(left)->size, LN(right)->size);
  ++size;
  if ((num->buff = (LIB_NUMBER_t_byte *)malloc(size*sizeof(LIB_NUMBER_t_byte))) == NULL){
    free(num);
    return LIB_NUMBER_ERR_NULL;
  }
  memset(num->buff, 0, size);
  num->base = LN(left)->base;
  num->sign = LN(left)->sign;
  LN(left)->sign *= num->sign;
  LN(right)->sign *= num->sign;
  num->size = size;
  num->ref_count = 1;
  for (i = 0, diff = 0, carry = 0; i < min; ++i){
    diff = (LN(left)->buff[i] - LN(right)->buff[i] - carry);
    if (diff < 0){ carry = 1; diff += num->base;} else carry = 0;
    num->buff[i] += diff%(num->base);
    num->buff[i+1] = diff/(num->base);
  }
  while (i < LN(left)->size){
    diff = LN(left)->buff[i] - carry;
    if (diff < 0){ carry = 1; diff += num->base;} else carry = 0;
    num->buff[i] += diff%(num->base);
    num->buff[i+1] = diff/(num->base);
    ++i;
  }
  while (i < LN(right)->size){
    diff = -(LN(right)->buff[i] + carry);
    if (diff < 0){ carry = 1; diff += num->base;} else carry = 0;
    num->buff[i] += diff%(num->base);
    num->buff[i+1] = diff/(num->base);
    ++i;
  }
  if (carry){
    for (i = 0, carry = 10; i < (size - 1); carry = 9, ++i)
      num->buff[i] = carry - num->buff[i];
  }
  LN(left)->sign *= num->sign;
  LN(right)->sign *= num->sign;
  if (carry) num->sign *= -1;
  while ((num->size > 1)&&(num->buff[num->size - 1] == 0)){
    --(num->size);
    num->buff = realloc(num->buff, num->size);
  }
  LIB_NUMBER_off(result);
  *result = (LIB_NUMBER_t_hndl )num;
  return LIB_NUMBER_ERR_NORM;
}

#define NEW_NODE(NODE) (NODE = (t_num *)malloc(sizeof(t_num)))
#define NEW_BUFF(NODE) ((NODE)->buff = (LIB_NUMBER_t_byte *)malloc(((NODE)->size)*sizeof(LIB_NUMBER_t_byte)))

int LIB_NUMBER_mul(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right){
  t_num *num;
  size_t base;
  if ((result == NULL)||(left == NULL)||(right == NULL)||(LN(left)->base != LN(right)->base))
    return LIB_NUMBER_ERR_DATA;
  base = LN(left)->base;
  if (((LN(left)->size == 1)&&(LN(left)->buff[0] == 0))||
      ((LN(right)->size == 1)&&(LN(right)->buff[0] == 0))){
    if ((num = (t_num *)malloc(sizeof(t_num))) == NULL)
      return LIB_NUMBER_ERR_NULL;
    num->ref_count = 1;
    if ((num->buff = (LIB_NUMBER_t_byte *)malloc(sizeof(LIB_NUMBER_t_byte))) == NULL){
      free(num);
      return LIB_NUMBER_ERR_NULL;
    }
    num->size = 1;
    num->base = base;
    num->buff[0] = 0;
    *result = (LIB_NUMBER_t_hndl )num;
    return LIB_NUMBER_ERR_NORM;
  }
//  printf("right->size = %llu\n", LN(right)->size);
  t_num *SMPL(t_num *a, t_num *b){
    t_num *node;
    size_t i, j, curr, diff;
    if (NEW_NODE(node) == NULL) return NULL;
    node->size = a->size + b->size + 2;
    // printf("a->size = %llu, b->size = %llu, node->size =%llu\n", a->size, b->size, node->size);
    LIB_NUMBER_write(right, stdout);
    node->base = base;
    if (NEW_BUFF(node) == NULL){ free(node); return NULL;}
    memset(node->buff, 0, (node->size)*sizeof(LIB_NUMBER_t_byte));
    for (i = 0; i < (a->size); diff = 0, ++i){
      for (j = 0, diff = 0; j < (b->size); ++j){
        curr = (a->buff[i])*(b->buff[j]) + diff + node->buff[i+j];
        node->buff[i+j] = curr%(base);
        diff = curr/(base);
      }
      while (diff != 0){
        node->buff[i + j++] = diff % (base);
        diff /= base;
      }
    }
    return node;
  }
  num = SMPL(LN(left), LN(right));
  if (num == NULL) return LIB_NUMBER_ERR_NULL;
  num->base = base;
  num->sign = (LN(left)->sign)*(LN(right)->sign);
  num->ref_count = 1;
  LIB_NUMBER_off(result);
  while ((num->size > 1)&&(num->buff[num->size - 1] == 0))
    --(num->size);
  t_num *buff;
  buff = realloc(num->buff, num->size);
  if (buff != NULL)
    num->buff = buff;
  *result = (LIB_NUMBER_t_hndl )num;
  return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_div(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl *remain,
                        LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right){
  t_num *tmod, *tdiv;
  size_t i;
  if ((result == NULL)||(remain == NULL)||(left == NULL)
      ||(right == NULL)||(LN(left)->base != LN(right)->base))
    return LIB_NUMBER_ERR_DATA;
  if ((LN(right)->size == 1)&&(LN(left)->buff[0] == 1))
    return LIB_NUMBER_ERR_DIV0;
  if (LN(left)->size < LN(right)->size){
    LIB_NUMBER_new(result, 0, LN(left)->base);
    LIB_NUMBER_cpy(remain, right);
    return LIB_NUMBER_ERR_NORM;
  }
  if (NEW_NODE(tmod) == NULL) return LIB_NUMBER_ERR_NULL;
  if (NEW_NODE(tdiv) == NULL){ free(tmod); return LIB_NUMBER_ERR_NULL;}
  tdiv->size = (LN(left)->size)/(LN(right)->size) + 1;
  return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_cmp(LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right){
  size_t i;
  int diff;
  if (left == NULL){ if (right == NULL) return 0; else return -1;}
  else{ if (right == NULL) return 1;}
  diff = LN(left)->sign - LN(right)->sign;
  if (diff) return diff/2;
  diff = LN(left)->sign;
  diff = diff*((long long)(LN(left)->size) - (long long)(LN(right)->size));
  if (diff) return (diff > 0 ? 1 : -1);
  diff = LN(left)->sign;
  for (i = 0; i < LN(left)->size; ++i){
    diff = diff*(LN(left)->buff[LN(left)->size-i-1] - LN(right)->buff[LN(left)->size-i-1]);
    if (diff) return (diff > 0 ? 1 : -1);
    diff = LN(left)->sign;
  }
  return 0;
}

void LIB_NUMBER_inv(LIB_NUMBER_t_hndl hndl){
  if (hndl == NULL) return;
  LN(hndl)->sign *= -1;
}

void LIB_NUMBER_set_byte(LIB_NUMBER_t_hndl hndl, size_t ind, LIB_NUMBER_t_byte val){
  if ((hndl == NULL)||(ind > LN(hndl)->size)||(val > LN(hndl)->base))
    return;
  LN(hndl)->buff[ind] = val;
}

LIB_NUMBER_t_byte LIB_NUMBER_get_byte(LIB_NUMBER_t_hndl hndl, size_t ind){
  if ((hndl == NULL)||(ind > LN(hndl)->size))
    return LN(hndl)->base + 1;
  return LN(hndl)->buff[ind];
}

LIB_NUMBER_t_sign LIB_NUMBER_get_sign(LIB_NUMBER_t_hndl hndl){
  if (hndl == NULL) return 2;
  return LN(hndl)->sign;
}

LIB_NUMBER_t_base LIB_NUMBER_get_base(LIB_NUMBER_t_hndl hndl){
  if (hndl == NULL) return 1;
  return LN(hndl)->base;
}

size_t LIB_NUMBER_get_size(LIB_NUMBER_t_hndl hndl){
  if (hndl == NULL) return 0;
  return LN(hndl)->size;
}

int LIB_NUMBER_read(LIB_NUMBER_t_hndl *hndl, FILE *fin){
  unsigned char ch;
  size_t pos;
  if ((hndl == NULL)||(fin == NULL))
    return LIB_NUMBER_ERR_DATA;
  LIB_NUMBER_off(hndl);
  if ((*hndl = (t_num *)malloc(sizeof(t_num))) == NULL)
    return LIB_NUMBER_ERR_NULL;
  fscanf(fin, "%zu", &(LN(*hndl)->base));
  if ((LN(*hndl)->base > LIB_NUMBER_MAX_BASE)||(LN(*hndl)->base < LIB_NUMBER_MIN_BASE)){
    free(*hndl);
    return LIB_NUMBER_ERR_DATA;
  }
  while ((((ch = fgetc(fin)) != '-')&&(ch != '+'))&&(!feof(fin)));
  if (feof(fin)){ free(*hndl); return LIB_NUMBER_ERR_DATA;}
  if (ch == '-') LN(*hndl)->sign = -1;
  else if (ch == '+') LN(*hndl)->sign = 1;
    else {free(*hndl); return LIB_NUMBER_ERR_DATA;}
  if ((LN(*hndl)->buff = (LIB_NUMBER_t_byte *)malloc(sizeof(LIB_NUMBER_t_byte))) == NULL){
    free(*hndl);
    return LIB_NUMBER_ERR_NULL;
  }
  LN(*hndl)->size = 1;
  pos = 0;
  while (((ch = fgetc(fin)) != '\n')&&(!feof(fin)));
  if (feof(fin)){free(*hndl); return LIB_NUMBER_ERR_DATA;}
  while ((ch = fgetc(fin))&&(!feof(fin))){
    if (pos == LN(*hndl)->size){
      LN(*hndl)->size <<= 1;
      LN(*hndl)->buff = realloc(LN(*hndl)->buff, LN(*hndl)->size);
    }
    if (ch > LN(*hndl)->base){free(LN(*hndl)->buff); free(LN(*hndl)); return LIB_NUMBER_ERR_DATA;}
    LN(*hndl)->buff[pos++] = ch;
  }
  LN(*hndl)->buff = realloc(LN(*hndl)->buff, pos);
  LN(*hndl)->size = pos;
  for (pos = 0; pos < (LN(*hndl)->size)/2; ++pos){
    ch = LN(*hndl)->buff[pos];
    LN(*hndl)->buff[pos] = LN(*hndl)->buff[LN(*hndl)->size - pos - 1];
    LN(*hndl)->buff[LN(*hndl)->size - pos - 1] = ch;
  }
  while ((LN(*hndl)->size > 1)&&(LN(*hndl)->buff[LN(*hndl)->size - 1] == 0))
    --(LN(*hndl)->size);
  LN(*hndl)->buff = realloc(LN(*hndl)->buff, LN(*hndl)->size);
  return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_write(LIB_NUMBER_t_hndl hndl, FILE *fout){
  size_t i;
  if ((hndl == NULL)||(fout == NULL))
    return LIB_NUMBER_ERR_DATA;
//  fprintf(fout, "BASE = %zu\n", LN(hndl)->base);
  if (LN(hndl)->sign < 0)
    fprintf(fout, "-");
  for (i = 0; i < LN(hndl)->size; ++i)
    fprintf(fout, "%u", LN(hndl)->buff[LN(hndl)->size-i-1]);
  fprintf(fout, "\n");
  return LIB_NUMBER_ERR_NORM;
}
