/**
 * Текущий заголовочный файл содержит интерфейсы функций,
 * предназначенных для работы с длинными целыми
 * посредством "умных указателей" (SmartPointer)
 * с подсчетом ссылок.
 */

#ifndef LIB_NUMBER_H
#define LIB_NUMBER_H

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * Объявление используемых типов данных:
 */

/* Тип цифры в числе */
typedef unsigned char LIB_NUMBER_t_byte;

/* Тип основания системы счисления */
typedef size_t LIB_NUMBER_t_base;

/* Тип знака числа */
typedef int LIB_NUMBER_t_sign;

/* Верхний и нижний пределы для основания системы счисления */
#define LIB_NUMBER_MAX_BASE (UCHAR_MAX + 1)
#define LIB_NUMBER_MIN_BASE (2)

/* Тип указателя на длинное целое */
typedef void *LIB_NUMBER_t_hndl;

/* Признак открепленного указателя (используется при инициализации) */
#define LIB_NUMBER_NULL (NULL)

/* Коды ошибок */
#define LIB_NUMBER_ERR_NORM (0)	//Ошибки отсутствуют;
#define LIB_NUMBER_ERR_NULL (1)	//Ошибка выделения памяти;
#define LIB_NUMBER_ERR_DATA (2)	//Некорректные входные параметры;
#define LIB_NUMBER_ERR_DIV0 (4)	//Деление на ноль;

/**
 * Функции для работы с длинными целыми:
 */

/* Генерирует число произвольной разрядности на основе целочисленной переменной
   в заданной системе счисления и связывает его с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_new(LIB_NUMBER_t_hndl *hndl, long long number, LIB_NUMBER_t_base base);

/* Открепляет указатель от связанного с ним числа */
/* Возвращает код ошибки */
extern int LIB_NUMBER_off(LIB_NUMBER_t_hndl *hndl);

/* Присваивает указателю ссылку на число, связанное с другим указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_set(LIB_NUMBER_t_hndl *hndl, LIB_NUMBER_t_hndl source);

/* Копирует число в новую область памяти и связывает его с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_cpy(LIB_NUMBER_t_hndl *hndl, LIB_NUMBER_t_hndl source);

/* Выполняет сложение двух длинных чисел и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_add(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Выполняет разность двух длинных чисел и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_sub(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Выполняет умножение двух длинных чисел и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_mul(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Выполняет деление двух длинных чисел и связывает полученную целую часть, а также
   остаток от деления, с заданными указателями */
/* Возвращает код ошибки */
extern int LIB_NUMBER_div(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl *remain, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Выполняет сравнение двух длинных чисел */
/* Возвращает:
   0 -- если числа совпадают,
  +1 -- если первое число больше второго,
  -1 -- если меньше */
extern int LIB_NUMBER_cmp(LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Меняет знак числа */
extern void LIB_NUMBER_inv(LIB_NUMBER_t_hndl hndl);

/* Задает значение цифры в числе по индексу */
extern void LIB_NUMBER_set_byte(LIB_NUMBER_t_hndl hndl, size_t ind, LIB_NUMBER_t_byte val);

/* Возвращает значение цифры по индексу */
extern LIB_NUMBER_t_byte LIB_NUMBER_get_byte(LIB_NUMBER_t_hndl hndl, size_t ind);

/* Возвращает знак числа */
extern LIB_NUMBER_t_sign LIB_NUMBER_get_sign(LIB_NUMBER_t_hndl hndl);

/* Возвращает основание системы счисления */
extern LIB_NUMBER_t_base LIB_NUMBER_get_base(LIB_NUMBER_t_hndl hndl);

/* Возвращает количество цифр в числе */
extern size_t LIB_NUMBER_get_size(LIB_NUMBER_t_hndl hndl);

/* Считывает из файла число в следующем порядке:
 * первая строка - основание системы счисления,
 * вторая строка - знак числа,
 * третья строка - число в заданной системе счисления
 * ОСТОРОЖНО: цифры числа считываются согласно их
 * расположению в таблице ASCII!
**/
extern int LIB_NUMBER_read(LIB_NUMBER_t_hndl *hndl, FILE *fin);

/* Выводит на печать число в его системе счисления и систему счисления*/
extern int LIB_NUMBER_write(LIB_NUMBER_t_hndl hndl, FILE *fout);

#endif //LIB_NUMBER_H
