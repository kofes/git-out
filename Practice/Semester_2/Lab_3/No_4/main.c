#include "lib_number.h"
#include <stdio.h>
#define ERR_FILE (3)
#define SIGNAL(err) {                                               \
  switch (err) {                                                    \
    case LIB_NUMBER_ERR_DATA:{printf("ERROR: DATA\n"); return;}     \
    case LIB_NUMBER_ERR_NULL:{printf("ERROR: NULL\n"); return;}     \
    case 3:{printf("ERROR: FILE\n"); return;}                       \
    case LIB_NUMBER_ERR_DIV0:{printf("ERROR: div 0\n"); return;}    \
  }                                                                 \
}                                                                   \

void test0(long long number, LIB_NUMBER_t_base base, FILE *fout){
  LIB_NUMBER_t_hndl hndl;
  size_t i, size;
  int err;
  if (fout == NULL)
    SIGNAL(3);
  hndl = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&hndl, number, base);
  SIGNAL(err);
  size = LIB_NUMBER_get_size(hndl);
  fprintf(fout, "size of %lli = %zu\n", number, size);
  fprintf(fout, "%lli = ", number);
  if (LIB_NUMBER_get_sign(hndl) < 0)
    fprintf(fout, "-");
  for (i = 0; i < size; ++i)
    fprintf(fout, "%u", LIB_NUMBER_get_byte(hndl, size - i -1));
  printf("?\n");
  LIB_NUMBER_off(&hndl);
}
void test1(long long number, LIB_NUMBER_t_base base, FILE *fout){
  LIB_NUMBER_t_hndl hndl[2];
  size_t i, size;
  int err;
  if (fout == NULL)
    SIGNAL(3);
  hndl[0] = LIB_NUMBER_NULL;
  hndl[1] = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&(hndl[0]), number, base);
  SIGNAL(err);
  err = LIB_NUMBER_set(&(hndl[1]), hndl[0]);
  SIGNAL(err);
  err = LIB_NUMBER_off(&(hndl[0]));
  SIGNAL(err);
  size = LIB_NUMBER_get_size(hndl[1]);
  fprintf(fout, "%lli = ", number);
  if (LIB_NUMBER_get_sign(hndl[1]) < 0)
    fprintf(fout, "-");
  for (i = 0; i < size; ++i)
    fprintf(fout, "%u", LIB_NUMBER_get_byte(hndl[1], size - i - 1));
  printf("?\n");
  LIB_NUMBER_off(&(hndl[1]));
}
void test2(long long number, LIB_NUMBER_t_base base, FILE *fout){
  LIB_NUMBER_t_hndl hndl[2];
  size_t i, size;
  int err;
  if (fout == NULL)
    SIGNAL(3);
  hndl[0] = LIB_NUMBER_NULL;
  hndl[1] = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&(hndl[0]), number, base);
  SIGNAL(err);
  err = LIB_NUMBER_cpy(&(hndl[1]), hndl[0]);
  SIGNAL(err);
  err = LIB_NUMBER_off(&(hndl[0]));
  SIGNAL(err);
  size = LIB_NUMBER_get_size(hndl[1]);
  fprintf(fout, "%lli = ", number);
  if (LIB_NUMBER_get_sign(hndl[1]) < 0)
    fprintf(fout, "-");
  for (i = 0; i < size; ++i)
    fprintf(fout, "%u", LIB_NUMBER_get_byte(hndl[1], size - i - 1));
  printf("?\n");
  LIB_NUMBER_off(&(hndl[1]));
}
void test3(long long left, long long right,
  LIB_NUMBER_t_base lbase, LIB_NUMBER_t_base rbase, FILE *fout){
  LIB_NUMBER_t_hndl hndl[3];
  size_t i, size;
  int err;
  if (fout == NULL)
    SIGNAL(3);
  hndl[0] = LIB_NUMBER_NULL;
  hndl[1] = LIB_NUMBER_NULL;
  hndl[2] = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&(hndl[0]), left, lbase);
  SIGNAL(err);
  err = LIB_NUMBER_new(&(hndl[1]), right, rbase);
  SIGNAL(err);
  err = LIB_NUMBER_add(&(hndl[2]), hndl[0], hndl[1]);
  SIGNAL(err);
  size = LIB_NUMBER_get_size(hndl[2]);
  fprintf(fout, "%lli = ", left + right);
  if (LIB_NUMBER_get_sign(hndl[2]) < 0)
    fprintf(fout, "-");
  for (i = 0; i < size; ++i)
    fprintf(fout, "%u", LIB_NUMBER_get_byte(hndl[2], size - i - 1));
  printf("?\n");
  LIB_NUMBER_off(&(hndl[0]));
  LIB_NUMBER_off(&(hndl[1]));
  LIB_NUMBER_off(&(hndl[2]));
}
void test4(long long left, long long right,
  LIB_NUMBER_t_base lbase, LIB_NUMBER_t_base rbase, FILE *fout){
  LIB_NUMBER_t_hndl hndl[3];
  size_t i, size;
  int err;
  if (fout == NULL)
    SIGNAL(3);
  hndl[0] = LIB_NUMBER_NULL;
  hndl[1] = LIB_NUMBER_NULL;
  hndl[2] = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&(hndl[0]), left, lbase);
  SIGNAL(err);
  err = LIB_NUMBER_new(&(hndl[1]), right, rbase);
  SIGNAL(err);
  err = LIB_NUMBER_mul(&(hndl[2]), hndl[0], hndl[1]);
  SIGNAL(err);
  size = LIB_NUMBER_get_size(hndl[2]);
  fprintf(fout, "%lli = ", left*right);
  if (LIB_NUMBER_get_sign(hndl[2]) < 0)
    fprintf(fout, "-");
  for (i = 0; i < size; ++i)
    fprintf(fout, "%u", LIB_NUMBER_get_byte(hndl[2], size - i - 1));
  printf("?\n");
  LIB_NUMBER_off(&(hndl[0]));
  LIB_NUMBER_off(&(hndl[1]));
  LIB_NUMBER_off(&(hndl[2]));
}
void test5(FILE *fin, FILE *fout){
  LIB_NUMBER_t_hndl hndl;
  int err;
  if ((fin == NULL)||(fout == NULL))
    SIGNAL(3);
  hndl = LIB_NUMBER_NULL;
  err = LIB_NUMBER_read(&hndl, fin);
  SIGNAL(err);
  err = LIB_NUMBER_write(hndl, fout);
  SIGNAL(err);
  LIB_NUMBER_off(&hndl);
}

int test_main(){
  FILE *in, *out;
  test0(100, 10, stdout);                    //WORK!
  test1(-1000, 10, stdout);                  //WORK!
  test2(73, 10, stdout);                     //WORK!
  test3(-101, -120, 10, 10, stdout);         //WORK!
  if ((in = fopen("input.txt", "rt")) == NULL)
    return 3;
  if ((out = fopen("output.txt", "wt")) == NULL){
    fclose(in); return 3;
  }
  test5(in, out);                             //WORK!
  fclose(in);
  fclose(out);
  test4(11111, 11111, 10, 10, stdout);        //ONLY NAIV!
  return 0;
}

int FIB(){
  LIB_NUMBER_t_hndl f1, f2;
  FILE *fp;
  int err;
  size_t i;
  if ((fp = fopen("output.txt", "at")) == NULL)
    return 1;
  f1 = LIB_NUMBER_NULL;
  f2 = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&f1, 1, 10);
  err = LIB_NUMBER_new(&f2, 0, 10);
  for (i = 0; i < 100000; ++i){
    LIB_NUMBER_add(&f1, f1, f2);
    printf("N = %i\n\n", i);
    LIB_NUMBER_sub(&f2, f1, f2);
  }
  LIB_NUMBER_write(f2, fp);
  LIB_NUMBER_off(&f1);
  LIB_NUMBER_off(&f2);
  fclose(fp);
  return 0;
}
int FAC(){
  LIB_NUMBER_t_hndl f1, f2, num;
  FILE *fp;
  int err;
  size_t i;
  if ((fp = fopen("output.txt", "wt")) == NULL)
    return 1;
  f1 = LIB_NUMBER_NULL;
  f2 = LIB_NUMBER_NULL;
  num = LIB_NUMBER_NULL;
  err = LIB_NUMBER_new(&f1, 1, 10);
  err = LIB_NUMBER_new(&f2, 1, 10);
  err = LIB_NUMBER_new(&num, 1, 10);
  for (i = 0; i < 10000; ++i){
    err = LIB_NUMBER_mul(&f1, f1, f2);
    printf("N = %i\n\n", i);
    if (err){ return 1;}
    LIB_NUMBER_add(&f2, f2, num);
  }
  LIB_NUMBER_write(f1, fp);
  LIB_NUMBER_off(&f1);
  LIB_NUMBER_off(&f2);
  fclose(fp);
  return 0;
}
int main(){
  FAC();
  return FIB();
}
