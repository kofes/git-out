#include "lib_bitarr.h"
#include "string.h"
#define BR(VOID) ((t_array *)(VOID))

typedef struct{
	LIB_BITARR_t_elem *ARRAY;
	size_t bits, size;
	size_t ref_counter;
}t_array;

int LIB_BITARR_del(LIB_BITARR_t_hndl *hndl){
	if (hndl == NULL)
		return LIB_BITARR_ERR_DATA;
	if (*hndl == NULL)
		return LIB_BITARR_ERR_NORM;
	--(BR(*hndl)->ref_counter);
	if (!(BR(*hndl)->ref_counter))
		free(BR(*hndl)->ARRAY);
	return LIB_BITARR_ERR_NORM;
}

int LIB_BITARR_new(LIB_BITARR_t_hndl *hndl, size_t size, size_t bits){
	unsigned long long count;
	if ((hndl == NULL)||(size == 0)||(bits == 0)||(bits > LIB_BITARR_MAX_BITS))
		return LIB_BITARR_ERR_DATA;
	count = (size*bits)/sizeof(LIB_BITARR_t_elem);
	++count;
	if (*hndl != NULL)
		LIB_BITARR_del(hndl);
	*hndl = LIB_BITARR_NULL;
	if ((*hndl = (t_array *)malloc(sizeof(t_array))) == NULL)
		return LIB_BITARR_ERR_NULL;
	if ((BR(*hndl)->ARRAY = (LIB_BITARR_t_elem *)malloc(count*sizeof(LIB_BITARR_t_elem))) == NULL){
		free(*hndl);
		return LIB_BITARR_ERR_NULL;
	}
	BR(*hndl)->ref_counter = 1;
	BR(*hndl)->size = size;
	BR(*hndl)->bits = bits;
	memset(BR(*hndl)->ARRAY, 0, count*sizeof(LIB_BITARR_t_elem));
	return LIB_BITARR_ERR_NORM;
}

void LIB_BITARR_srt(LIB_BITARR_t_hndl hndl, int dir){
	if ((hndl == NULL)||(dir == 0))
		return;

}

void LIB_BITARR_set_elem(LIB_BITARR_t_hndl hndl, size_t ind, LIB_BITARR_t_elem elem){
	size_t i;
	if ((hndl == NULL)||(BR(hndl)->size <= ind))
		return;
	for (i = 0; i < BR(hndl)->bits; ++i)
		BR(hndl)->ARRAY[((BR(hndl)->bits)*ind)/sizeof(LIB_BITARR_t_elem)] &=
														(elem & (1 << i));
}

LIB_BITARR_t_elem LIB_BITARR_get_elem(LIB_BITARR_t_hndl hndl, size_t ind){
	LIB_BITARR_t_elem elem;
	size_t i;
	elem = LIB_BITARR_MAX_BITS;
	if (hndl == NULL)
		return 0;
	for (i = 0; i < BR(hndl)->bits; ++i)
		elem &= (BR(hndl)->ARRAY[((BR(hndl)->bits)*ind)/sizeof(LIB_BITARR_t_elem)] &
				(1 << (i + (ind - ((BR(hndl)->bits)*ind)/sizeof(LIB_BITARR_t_elem)))));
	return elem;
}

size_t LIB_BITARR_get_size(LIB_BITARR_t_hndl hndl){
	if (hndl == NULL)
		return 0;
	return BR(hndl)->size;
}

size_t LIB_BITARR_get_bits(LIB_BITARR_t_hndl hndl){
	if (hndl == NULL)
		return 0;
	return BR(hndl)->bits;
}