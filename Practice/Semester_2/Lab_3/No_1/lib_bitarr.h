/**
 * Текущий заголовочный файл содержит интерфейсы функций,
 * предназначенных для работы с битовыми массивами.
 */

#ifndef __INCLUDE_LIB_BITARR_H
#define __INCLUDE_LIB_BITARR_H

#include <limits.h>
#include <stdlib.h>

/**
 * Объявление используемых типов данных:
 */

/* Тип элемента в массиве */
typedef unsigned long long LIB_BITARR_t_elem;

/* Максимально допустимое число бит на элемент */
#define LIB_BITARR_MAX_BITS (CHAR_BIT * sizeof(LIB_BITARR_t_elem))

/* Тип дескриптора для битового массива */
typedef void *LIB_BITARR_t_hndl;

/* Признак открепленного дескриптора (используется при инициализации) */
#define LIB_BITARR_NULL (NULL)

/* Коды ошибок */
#define LIB_BITARR_ERR_NORM (0)	//Ошибки отсутствуют;
#define LIB_BITARR_ERR_NULL (1)	//Ошибка выделения памяти;
#define LIB_BITARR_ERR_DATA (2)	//Некорректные входные параметры;

/**
 * Функции для работы с битовыми массивами:
 */

/* Выделяет память под битовый массив заданной длины с указанным числом бит на элемент
   и связывает его с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_BITARR_new(LIB_BITARR_t_hndl *hndl, size_t size, size_t bits);

/* Освобождает память из-под массива и открепляет указатель */
/* Возвращает код ошибки */
extern int LIB_BITARR_del(LIB_BITARR_t_hndl *hndl);

/* Выполняет сортировку массива в заданном направлении (по возрастанию/убыванию) */
extern void LIB_BITARR_srt(LIB_BITARR_t_hndl hndl, int dir);

/* Задает значение элемента по индексу */
extern void LIB_BITARR_set_elem(LIB_BITARR_t_hndl hndl, size_t ind, LIB_BITARR_t_elem elem);

/* Возвращает значение элемента по индексу */
extern LIB_BITARR_t_elem LIB_BITARR_get_elem(LIB_BITARR_t_hndl hndl, size_t ind);

/* Возвращает число элементов в массиве */
extern size_t LIB_BITARR_get_size(LIB_BITARR_t_hndl hndl);

/* Возвращает число бит на элемент */
extern size_t LIB_BITARR_get_bits(LIB_BITARR_t_hndl hndl);

#endif //__INCLUDE_LIB_BITARR_H
