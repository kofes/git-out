#include "lib_bitarr.h"

int main(){
	LIB_BITARR_t_hndl hndl;
	hndl = LIB_BITARR_NULL;
	LIB_BITARR_new(hndl, 4, 2);
	LIB_BITARR_set_elem(hndl, 2, 2);
	printf("%llu", LIB_BITARR_get_elem(hndl, 2));
	LIB_BITARR_del(hndl);
	return 0;
}