#include "lib_list.h"

int f1(LIB_LIST_t_elem elem){
  *(int *)elem = 0;
  return 0;
}
int f2(LIB_LIST_t_elem elem, LIB_LIST_t_elem source){
  *(int *)elem = *(int *)source;
  return 0;
}
int f3(LIB_LIST_t_elem elem){
  *(int *)elem = 0;
  free(elem);
  return 0;
}
int f4(LIB_LIST_t_elem left, LIB_LIST_t_elem right){
  return *(int *)left - *(int *)right;
}
int main(){
  LIB_LIST_t_hndl hndl;
  LIB_LIST_t_iter iter;
  hndl = LIB_LIST_NULL;
  LIB_LIST_new(&hndl, sizeof(int), f1, f2, f3, f4);
  LIB_LIST_add_head(&iter, hndl);
  LIB_LIST_off(&hndl);
  return 0;
}
