#ifndef __INCLUDE_BTREE_H
#define __INCLUDE_BTREE_H

#include <stdlib.h>
#include <stdio.h>
#include "lib_list.h"
/*Struct of main list's node*/
//nearby[0] - to head;
//nearby[1] - to tail;
typedef struct t_node{
    struct t_node *nearby[2];
    LIB_LIST_t_elem elem;
}t_node;

//top[0] - head;
//top[1] - tail;
/*Struct of list*/
typedef struct{
    t_node *top[2];
    size_t size_list;  //Размер списка
    size_t size_hndl;   //Количество указателей на список
    LIB_LIST_t_call_new new;
    LIB_LIST_t_call_cpy cpy;
    LIB_LIST_t_call_del del;
    LIB_LIST_t_call_cmp cmp;
    size_t size_elem;    //Размер элемента в узле
}t_list;

/*Struct of iterator*/
typedef struct t_iter{
    struct t_iter **next;
    t_list *list;
    t_node *curr;
}t_iter;

/*Struct of iterator's list*/
typedef struct{
    t_iter **head;
}ti_list;

//Тип ключа:
typedef size_t t_btree_key;

//Тип значения:
typedef ti_list *t_btree_val;

//Тип дескриптора двоичного дерева:
typedef void *t_btree;

//Признак открепленного дескриптора:
#define BTREE_NULL (NULL)

//Генерирует пустое дерево и возвращает его дескриптор:
extern t_btree btree_new();

//Освобождает память из-под дерева:
extern void btree_del(t_btree btree);

//Выполняет вставку элемента по ключу:
extern t_btree_val *btree_add_elem(t_btree btree, t_btree_key key);

//Выполняет поиск элемента по ключу:
extern t_btree_val *btree_get_elem(t_btree btree, t_btree_key key);

//Выполняет удаление элемента по ключу:
extern void btree_del_elem(t_btree btree, t_btree_key key);

//Выводит содержимое дерева в виде дерева в выходной поток:
extern void btree_out_btree(t_btree *btree, FILE *fout);

//Выводит содержимое дерева в выходной поток:
extern void btree_out(t_btree btree, FILE *fout);

#endif //__INCLUDE_BTREE_H
