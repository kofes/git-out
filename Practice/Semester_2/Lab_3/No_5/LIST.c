#include "lib_list.h"
#include "lab-2.2.h"

#define ITR(VOID) ((t_iter *)(VOID)) /*Приведение к типу итератора*/
#define TOL(VOID) ((t_list *)(VOID)) /*Приведение к типу контейнера*/
static t_btree map;

ti_list *ilist_new(){
    ti_list *list;
    if ((list = (ti_list *)malloc(sizeof(ti_list))) == NULL)
        return NULL;
    list->head = NULL;
    return list;
}
void ilist_del(ti_list *list){
    t_iter **curr, **node;
    if (list == NULL)
        return NULL;
    curr = list->head;
    while (curr != NULL){
        node = curr;
        curr = (*curr)->next;
        free(*node);
        *node = NULL;
        free(node);
    }
    list->head = NULL;
}
t_iter **ilist_ade(ti_list *list){
    t_iter **iter;
    if (list == NULL)
        return NULL;
    if ((iter = (t_iter **)malloc(sizeof(t_iter*))) == NULL)
        return NULL;
    (*iter)->next = list->head;
    list->head = iter;
    return iter;
}
void ilist_dle(ti_list *list, t_iter *iter){
    t_iter **curr, **node;
    if ((list == NULL)||(iter == NULL))
        return NULL;
    node = NULL;
    curr = list->head;
    while ((curr != NULL)&&(*curr != iter)){
        node = curr;
        curr = (*curr)->next;
    }
    if (curr != NULL){
        if (node != NULL){
            (*node)->next = (*curr)->next;
        }else list->head = (*curr)->next;
        free(*curr);
        *curr = NULL;
        free(curr);
    }
}
int LIB_LIST_new(LIB_LIST_t_hndl *hndl, size_t elem_size,
                        LIB_LIST_t_call_new call_new_elem, LIB_LIST_t_call_cpy call_cpy_elem,
                        LIB_LIST_t_call_del call_del_elem, LIB_LIST_t_call_cmp call_cmp_elem){
    t_list *list;
    if ((hndl == NULL)||(elem_size == 0)||(call_new_elem == NULL)||(call_cpy_elem == NULL)||(call_del_elem == NULL)||(call_cmp_elem == NULL))
        return LIB_LIST_ERR_DATA;
    if ((list = (t_list *)malloc(sizeof(t_list))) == NULL)
        return LIB_LIST_ERR_NULL;
    list->new = call_new_elem;
    list->cpy = call_cpy_elem;
    list->del = call_del_elem;
    list->cmp = call_cmp_elem;
    list->size_elem = elem_size;
    list->size_hndl = 1;
    list->size_list = 0;
    list->top[0] = NULL;
    list->top[1] = NULL;
    LIB_LIST_off(hndl);
    *hndl = (LIB_LIST_t_hndl )list;
    return LIB_LIST_ERR_NORM;
}
int LIB_LIST_off(LIB_LIST_t_hndl *hndl){
    t_list *list;
    if (hndl == NULL)
        return LIB_LIST_ERR_DATA;
    list = (t_list *)(*hndl);
    if (list == NULL)
      return LIB_LIST_ERR_NORM;
    --(list->size_hndl);
    if (list->size_hndl == 0){
        t_node *node;
        while (list->top[0] != NULL){
            node = list->top[0];
            list->top[0] = node->nearby[1];
            btree_del_elem(map, (size_t)node);
            list->del(node->elem);
            free(node->elem);
            free(node);
        }
        free(list);
    }
    else *hndl = NULL;
    return LIB_LIST_ERR_NORM;
}
int LIB_LIST_set(LIB_LIST_t_hndl *hndl, LIB_LIST_t_hndl source){
    t_list *list = (t_list *)source;
    if ((hndl == NULL)||(source == NULL))
        return LIB_LIST_ERR_DATA;
    if ((*hndl == source))
        return LIB_LIST_ERR_NORM;
    ++(list->size_hndl);
    LIB_LIST_off(hndl);
    *hndl = source;
    return LIB_LIST_ERR_NORM;
}
int LIB_LIST_cpy(LIB_LIST_t_hndl *hndl, LIB_LIST_t_hndl source){
    t_list *old;
    t_node *node, *curr, *node_old;
    size_t i;
    if ((hndl == NULL)||(source == NULL))
        return LIB_LIST_ERR_DATA;
    if (*hndl == source)
        return LIB_LIST_ERR_NORM;
    old = (t_list *)source;
    LIB_LIST_off(hndl);
    if (LIB_LIST_new(hndl, old->size_elem, old->new, old->cpy, old->del, old->cmp))
        return LIB_LIST_ERR_NULL;
    TOL(*hndl)->size_list = old->size_list;
    if ((TOL(*hndl)->top[0] = (t_node *)malloc(sizeof(t_node))) == NULL){
      LIB_LIST_off(hndl); return LIB_LIST_ERR_NULL;
    }
    TOL(*hndl)->top[0]->nearby[0] = NULL; TOL(*hndl)->top[0]->nearby[1] = NULL;
    TOL(*hndl)->top[1] = TOL(*hndl)->top[0];
    if ((TOL(*hndl)->top[0]->elem = (LIB_LIST_t_elem)malloc(TOL(*hndl)->size_elem)) == NULL){
      LIB_LIST_off(hndl); return LIB_LIST_ERR_NULL;
    }
    TOL(*hndl)->new(TOL(*hndl)->top[0]->elem);
    if (TOL(*hndl)->cpy(TOL(*hndl)->top[0]->elem, old->top[0]->elem)){
      LIB_LIST_off(hndl); return LIB_LIST_ERR_NULL;
    }
    node = TOL(*hndl)->top[0]; node_old = old->top[0]->nearby[1];
    for (i = 1; i < TOL(*hndl)->size_list; ++i){
      if ((node->nearby[1] = (t_node *)malloc(sizeof(t_node))) == NULL){
        LIB_LIST_off(hndl); return LIB_LIST_ERR_NULL;
      }
      curr = node->nearby[1]; curr->nearby[0] = node; curr->nearby[1] = NULL;
      if ((curr->elem = (LIB_LIST_t_elem)malloc(TOL(*hndl)->size_elem)) == NULL){
        LIB_LIST_off(hndl); return LIB_LIST_ERR_NULL;
      }
      TOL(*hndl)->top[1] = curr; TOL(*hndl)->new(curr->elem);
      if (TOL(*hndl)->cpy(curr->elem, node_old->elem)){
        LIB_LIST_off(hndl); return LIB_LIST_ERR_NULL;
      }
      node = curr;
      node_old = node_old->nearby[1];
    }
    return LIB_LIST_ERR_NORM;
}
int LIB_LIST_get(LIB_LIST_t_hndl *hndl, LIB_LIST_t_iter iter){
  if (hndl == NULL)
    return LIB_LIST_ERR_DATA;
  if (iter == NULL)
    return LIB_LIST_off(hndl);
  if (TOL(*hndl) == ITR(iter)->list)
    return LIB_LIST_ERR_NORM;
  LIB_LIST_off(hndl);
  *hndl = (LIB_LIST_t_hndl )(ITR(iter)->list);
  return LIB_LIST_ERR_NORM;
}
void LIB_LIST_srt(LIB_LIST_t_hndl hndl, int dir){
  t_node *curr, *node;
  if ((hndl == NULL)||(dir == 0))
    return;
  t_node *sl_list(t_node *first, t_node *second){
    t_node *p1, *p2, *s1, *head;
    if (first == NULL) return second;
    else if (second == NULL) return first;
      else{
        p1 = first;
        p2 = second;
        if (dir*(TOL(hndl)->cmp(p1->elem, p2->elem))){
          head = p1;
          p1 = p1->nearby[1];
        }
        else{
          head = p2;
          p2 = p2->nearby[1];
        }
        s1 = head;
        while ((p1 != NULL)&&(p2 != NULL)){
          if (dir*(TOL(hndl)->cmp(p1->elem, p2->elem))){
            s1->nearby[1] = p1;
            p1 = p1->nearby[1];
          }else{
            s1->nearby[1] = p2;
            p2 = p2->nearby[1];
          }
          s1 = s1->nearby[1];
        }
        if (p1 != NULL) s1->nearby[1] = p1;
        if (p2 != NULL) s1->nearby[1] = p2;
      }
    first = NULL; second = NULL;
    return head;
  }
  t_node *m_sort(t_node *head, size_t h){
    t_node *p1, *p2, *note;
    size_t i;
    if (head == NULL) return NULL;
    p1 = head; p2 = head;
    if (h == 1){
      head->nearby[1] = NULL;
      return head;
    }
    if (h == 2){
      p2 = p1->nearby[1];
      p2->nearby[1] = NULL;
      head = p1;
      if (dir*(TOL(hndl)->cmp(p1->elem, p2->elem))){
        p2->nearby[1] = p1;
        p1->nearby[1] = NULL;
        head = p2;
      }
      return head;
    }
    i = 0;
    while (i < (h/2)){
      note = p2;
      p2 = p2->nearby[1];
      ++i;
    }
    note->nearby[1] = NULL;
    p1 = m_sort(p1, h/2);
    p2 = m_sort(p2, h - h/2);
    return sl_list(p1, p2);
  }
  TOL(hndl)->top[0] = m_sort(TOL(hndl)->top[0], TOL(hndl)->size_list);
  curr = TOL(hndl)->top[0];
  curr->nearby[0] = NULL;
  while (curr->nearby[1] != NULL){
    node = curr;
    curr = curr->nearby[1];
    curr->nearby[0] = node;
  }
  TOL(hndl)->top[1] = curr;
}
size_t LIB_LIST_get_size(LIB_LIST_t_hndl hndl){
  if (hndl == NULL)
    return 0;
  return TOL(hndl)->size_list;
}
int LIB_LIST_add_head(LIB_LIST_t_iter *iter, LIB_LIST_t_hndl hndl){
  t_node *node;
  ti_list *list;
  t_iter *it;
  if ((iter == NULL)||(hndl == NULL))
    return LIB_LIST_ERR_DATA;
  list = ilist_new();
  if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
    return LIB_LIST_ERR_NULL;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL){
    free(node); return LIB_LIST_ERR_NULL;
  }
  node->nearby[0] = NULL;
  node->nearby[1] = TOL(hndl)->top[0];
  if (TOL(hndl)->new(node->elem)){
    free(node); free(it); return LIB_LIST_ERR_NULL;
  }
  TOL(hndl)->top[0] = node;
  if (TOL(hndl)->top[1] == NULL)
    TOL(hndl)->top[1] = node;
  ++(TOL(hndl)->size_list);
  it->curr = node;
  it->list = TOL(hndl);
  *ilist_ade(list) = it;/*================================================================*/
  *btree_add_elem(map, (size_t)node) = list;
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_add_tail(LIB_LIST_t_iter *iter, LIB_LIST_t_hndl hndl){
  t_node *node;
  ti_list *list;
  t_iter *it;
  if ((iter == NULL)||(hndl == NULL))
    return LIB_LIST_ERR_DATA;
  list = ilist_new();
  if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
    return LIB_LIST_ERR_NULL;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL){
    free(node); return LIB_LIST_ERR_NULL;
  }
  node->nearby[1] = NULL; node->nearby[0] = TOL(hndl)->top[1];
  if (TOL(hndl)->new(node->elem)){
    free(node); free(it); return LIB_LIST_ERR_NULL;
  }
  TOL(hndl)->top[1] = node;
  if (TOL(hndl)->top[0] == NULL)
    TOL(hndl)->top[0] = node;
  ++(TOL(hndl)->size_list);
  it->curr = node;
  it->list = TOL(hndl);
  *ilist_ade(list) = it;/*================================================================*/
  *btree_add_elem(map, (size_t)node) = list;
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_get_head(LIB_LIST_t_iter *iter, LIB_LIST_t_hndl hndl){
  ti_list *list;
  t_iter *it;
  if ((iter == NULL)||(hndl == NULL))
    return LIB_LIST_ERR_DATA;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL)
    return LIB_LIST_ERR_NULL;
  LIB_LIST_off_iter(iter);
  it->curr = TOL(hndl)->top[0];
  it->list = TOL(hndl);
  if ((list = *btree_get_elem(map, (size_t)(it->curr))) == NULL){
    list = ilist_new();
    *btree_add_elem(map, (size_t)(it->curr)) = list;
  }
  *ilist_ade(list) = it;/*================================================================*/
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_get_tail(LIB_LIST_t_iter *iter, LIB_LIST_t_hndl hndl){
  ti_list *list;
  t_iter *it;
  if ((iter == NULL)||(hndl == NULL))
    return LIB_LIST_ERR_DATA;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL)
    return LIB_LIST_ERR_NULL;
  LIB_LIST_off_iter(iter);
  it->curr = TOL(hndl)->top[1];
  it->list = TOL(hndl);
  if ((list = *btree_get_elem(map, (size_t)(it->curr))) == NULL){
    list = ilist_new();
    *btree_add_elem(map, (size_t)(it->curr)) = list;
  }
  *ilist_ade(list) = it;/*================================================================*/
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_add_next(LIB_LIST_t_iter *iter, LIB_LIST_t_iter curr){
  ti_list *list;
  t_node *node;
  t_iter *it;
  if ((iter == NULL)||(curr == NULL))
    return LIB_LIST_ERR_DATA;
  if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
    return LIB_LIST_ERR_NULL;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL){
    free(node);
    return LIB_LIST_ERR_NULL;
  }
  node->nearby[1] = ITR(curr)->curr;
  node->nearby[0] = ITR(curr)->curr->nearby[0];
  if (node->nearby[0] == NULL)
    ITR(curr)->list->top[0] = node;
  ++(ITR(curr)->list->size_list);
  it->curr = node;
  list = ilist_new();
  *btree_add_elem(map, (size_t)node) = list;
  *ilist_ade(list) = it;/*================================================================*/
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_add_last(LIB_LIST_t_iter *iter, LIB_LIST_t_iter curr){
  ti_list *list;
  t_node *node;
  t_iter *it;
  if ((iter == NULL)||(curr == NULL))
    return LIB_LIST_ERR_DATA;
  if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
    return LIB_LIST_ERR_NULL;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL){
    free(node);
    return LIB_LIST_ERR_NULL;
  }
  node->nearby[0] = ITR(curr)->curr;
  node->nearby[1] = ITR(curr)->curr->nearby[1];
  if (node->nearby[1] == NULL)
    ITR(curr)->list->top[1] = node;
  ++(ITR(curr)->list->size_list);
  it->curr = node;
  list = ilist_new();
  *btree_add_elem(map, (size_t)node) = list;
  *ilist_ade(list) = it;/*================================================================*/
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_get_next(LIB_LIST_t_iter *iter, LIB_LIST_t_iter curr){
  ti_list *list;
  t_iter *it;
  if ((iter == NULL)||(curr == NULL))
    return LIB_LIST_ERR_DATA;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL)
    return LIB_LIST_ERR_NULL;
  it->list = ITR(curr)->list;
  if ((it->curr = ITR(curr)->curr->nearby[0]) == NULL){
    free(it);
    return LIB_LIST_off_iter(iter);
  }
  if ((list = *btree_get_elem(map, (size_t)(it->curr))) == NULL){
    list = ilist_new();
    *btree_add_elem(map, (size_t)(it->curr)) = list;
  }
  *ilist_ade(list) = it;/*================================================================*/
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_get_last(LIB_LIST_t_iter *iter, LIB_LIST_t_iter curr){
  ti_list *list;
  t_iter *it;
  if ((iter == NULL)||(curr == NULL))
    return LIB_LIST_ERR_DATA;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL)
    return LIB_LIST_ERR_NULL;
  it->list = ITR(curr)->list;
  if ((it->curr = ITR(curr)->curr->nearby[1]) == NULL){
    free(it);
    return LIB_LIST_off_iter(iter);
  }
  if ((list = *btree_get_elem(map, (size_t)(it->curr))) == NULL){
    list = ilist_new();
    *btree_add_elem(map, (size_t)(it->curr)) = list;
  }
  *ilist_ade(list) = it;/*================================================================*/
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_set_iter(LIB_LIST_t_iter *iter, LIB_LIST_t_iter curr){
  t_iter *it;
  ti_list *list;
  if ((iter == NULL)||(curr == NULL))
    return LIB_LIST_ERR_DATA;
  if (ITR(*iter) == ITR(curr))
    return LIB_LIST_ERR_NORM;
  if ((it = (t_iter *)malloc(sizeof(t_iter))) == NULL)
    return LIB_LIST_ERR_NULL;
  it->list = ITR(curr)->list;
  it->curr = ITR(curr)->curr;
  list = *btree_get_elem(map, (size_t)(it->curr));
  *ilist_ade(list) = it;/*================================================================*/
  LIB_LIST_off_iter(iter);
  *iter = (LIB_LIST_t_iter )it;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_del_iter(LIB_LIST_t_iter *iter){
  t_node *node;
  ti_list *list;
  t_list *tl;
  if (iter == NULL)
    return LIB_LIST_ERR_DATA;
  if (*iter == NULL)
    return LIB_LIST_ERR_NORM;
  node = ITR(*iter)->curr;
  tl = ITR(*iter)->list;
  list = *btree_get_elem(map, (size_t)(node));
  ilist_del(list);
  btree_del_elem(map, (size_t)(ITR(*iter)->curr));
  --(tl->size_list);
  if (node == tl->top[0]){
    tl->top[0] = node->nearby[1];
    if (node->nearby[1] == NULL)
      tl->top[1] = NULL;
  }
  if (node == tl->top[1]){
    tl->top[1] = node->nearby[0];
    if (node->nearby[0] == NULL)
      tl->top[0] = NULL;
  }
  tl->del(node->elem);
  free(node->elem);
  free(node);
  *iter = NULL;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_off_iter(LIB_LIST_t_iter *iter){
  ti_list *list;
  if (iter == NULL)
    return LIB_LIST_ERR_DATA;
  if (*iter == NULL)
    return LIB_LIST_ERR_NORM;
  list = *btree_get_elem(map, (size_t)(ITR(iter)->curr));
  ilist_dle(list, ITR(iter));
  if (list->head == NULL)
    btree_del_elem(map, (size_t)(ITR(iter)->curr));
  ITR(*iter)->curr = NULL;
  ITR(*iter)->list = NULL;
  free(*iter);
  *iter = NULL;
  return LIB_LIST_ERR_NORM;
}
int LIB_LIST_own_iter(LIB_LIST_t_iter iter, LIB_LIST_t_hndl hndl){
  if ((iter == NULL)||(hndl == NULL))
    return 0;
  return (ITR(iter)->list == TOL(hndl));
}
LIB_LIST_t_elem LIB_LIST_get_elem(LIB_LIST_t_iter iter){
  if (iter == NULL)
    return NULL;
  return ITR(iter)->curr->elem;
}
