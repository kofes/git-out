#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
double Gauss_in(double **A, double *B, size_t n, double *X)
{
	double **A1, *B1;
	double s, R, R1, m;
	double *d;
	int i, j;
	int k = 0, o = 0, l;

	if((A1 = (double **)malloc(n*sizeof(double *))) == NULL)
		return -1;
	else
		for(i = 0; i<n; ++i)
			A1[i] = (double *)malloc(n*sizeof(double));
	for(i = 0; i < n; ++i)
		for(j = 0; j < n; ++j)
			A1[i][j] = A[i][j];

	if((B1 = (double *)malloc(n*sizeof(double))) == NULL)
	{
		return -1;
	}
	for(i = 0; i < n; ++i)
		B1[i] = B[i];
//заполнили массивы

	while ((k+o)<(n-1))
	{

		i = k+1;// i = 1;
		while (i<n)
		{
			while (((k+o)<n) && (A1[k][k+o] == 0))
			{
				l = k;
				for (i = k+1; (i<n)&&(i!=l); ++i)
					if (fabs(A1[i][k+o]) > fabs(A1[l][k+o]))
						l = i;
				if (l != k) //change the line
				{
					double tmp;
					d = A1[k];
					A1[k] = A1[l];
					A1[l] = d;

					tmp = B1[k];
					B1[k] = B[l];
					B1[l] = tmp;
				}
				else
					++o;

				i = k+1;
				if (k+o >= n)//костыль для проверки обращения к несущ. ячейке
				{
					for (i=k; i<n; ++i)
						if ((ceil(B1[k]*1e+5)) != 0)
						{
							for (j=0; j<n; ++j)
								X[j] = 1/(+0.0);
							return 0;
						}
					for (j = 0; j<n; ++j)
					X[j] = NAN;

					return 0;
				}
			}

			m = A1[i][k+o]/A1[k][k+o];
			A1[i][k+o] = 0;
			for (j = (k+1+o); j<n; ++j)
				A1[i][j] -= m*A1[k][j];

			B1[i] -= m*B1[k];

			++i;
		}
		++k;
	}
	R = 0;
	if ((ceil(A1[n-1][n-1]*1e+5)) != 0) //погрешность
	{
		X[n-1] = B1[n-1] / A1[n-1][n-1];
		for (i = n-2; i>=0; --i)
		{
			s = 0;
			for (j = i+1; j!=n; ++j)
				s -= A1[i][j] * X[j];
			X[i] = (B1[i]+s) / A1[i][i];
		}

		R = 0;
		for (i = 0; i<n; ++i)
		{
			R1 = 0;
			for (j = 0; j<n; ++j)
				R1 = R1 + A[i][j] * X[j];

			R1 = B[i]-R1;
			R1 = fabs(R1);
			if (R1 > R)
				R = R1;
		}
	}
	else if ((ceil(B1[n-1]*1e+5)) == 0)
		{
			for (i=0; i<n; ++i)
				X[i] = 1/(+0.0);
		}
		else
			{
				for (i = 0; i<n; ++i)
					X[i] = NAN;
			}

	//free memory
	for (i = 0; i < n; i++)
		free(A1[i]);
	free(A1);
	free(B1);

	return R;
}

char test1()
{
	unsigned char i, k;
	size_t n = 3;
	double **A, *B, *X;

	srand(time(NULL));
	if ((X = (double *)malloc(n*sizeof(double))) == NULL)
		return 1;
	else
		for (i=0; i<n; ++i)
			X[i]=1;


	if((A = (double **)malloc(n*sizeof(double *))) == NULL) //get mem
		return 1;
	else if ((B = (double *)malloc(n*sizeof(double))) == NULL)
			return 1;
		else
	{
		for(i = 0; i<n; ++i)
			if ((A[i] = (double *)malloc(n*sizeof(double))) == NULL)
				return 1;
		A[0][0] = 0;	A[1][0] = 3;	A[0][2] = 3;	B[0] = 1;
		A[0][1] = 2;	A[1][1] = 5;	A[1][2] = 3;	B[1] = 2;
		A[2][0] = 1;	A[2][1] = 2;	A[2][2] = 0;	B[2] = 3;
		for (i=0; i<n; ++i)
		{
			for (k=0; k<n; ++k)
			{
				A[i][k]=0;
				printf("%lf\t", A[i][k]);
			}
				B[i] = 0;
				printf("%lf", B[i]);
				printf("\n");
		}
	}
	printf("R = %le\n", Gauss_in(A, B, n, X));
	for (i=0; i<n; ++i)
		printf("x(%i) = %lf\n", i, X[i]);
	free(X);
	free(B);
	for (i=0; i<n; ++i)
		free(A[i]);
	free(A);
	
	return 0;
}
char main()
{
	return test1();
}