#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f(double x)
{
	return 4/(x*x + 1);
}
double integral(double (*f)(double), double a, double b, unsigned long n, unsigned long m, double eps)
{ 
	unsigned long i;
	double Sf, Sd, h;
	double F, G;
	unsigned long k;
	double y, c, t;
	Sd = 0;
	i = 0;
	do{
		h = (b-a)/n;
		Sf = Sd;
		Sd = 0;
		F = 0;
		y = 0;
		c = 0;
		t = 0;
		G = f(a);
		for (k=1; k<n; ++k)
			{
				F = f(a + k*h); //<-
				//Sd += (h/2)*(G + F);
				y = (h/2)*(G + F) - c;
				t = Sd + y;
				c = (t - Sd) - y;
				Sd = t;
				G = F; //<-
			}
		++i;
		n<<=1;
		if (m<=i)
			return Sd;
	}while (fabs(Sf - Sd) > eps);
	return Sd;
}
void main()
{
	double a, b;
	a = 0;
	b = 1;
	//printf("A :\t");
	//scanf("%lf", &a);
	//printf("B :\t");
	//scanf("%lf", &b);
	printf("Answer : %0.8lf\n", integral(f, a, b, 16, 1000, 1e-9));
}