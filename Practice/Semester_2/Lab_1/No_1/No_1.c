#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define eps 1e-8
char ans_quad(const double a, const double b, const double c, double *x1, double *x2)
{
	double D;
	if (a == 0) {
		if (b == 0) {
			if (c == 0)
				return -1;
			else
				return 0;
		}
		*x1 = -c/b;
		return 1;
	}
	D = (b*b - 4*a*c);
	if (fabs(D) < eps) {
		*x1 = -b/2*a;
		return 1;
	}
	if (D > 0) {
		D = sqrt(D);
		//printf("%lf\n",D);
		*x1 = (-b + D)/(2*a);
		*x2 = -(b + D)/(2*a);
		return 2;
	}
	D = sqrt(fabs(D));
	//printf("%lf\n", D);
	*x1 = -b/(2*a);
	*x2 = (D/(2*a));
	return -2;
}

typedef void (*answer)(double X1, double X2);

void answer_2(double X1, double X2)
{
	printf("Answer: %.3lf + %.3lf*i, %.3lf - %.3lf*i\n", X1, X2, X1, X2);
}
void answer_1(double X1, double X2)
{
	printf("Answer: Inf\n");
}
void answer0(double X1, double X2)
{
	printf("Answer: Nothing\n");
}
void answer1(double X1, double X2)
{
	printf("Answer: %.3lf\n", X1);
}
void answer2(double X1, double X2)
{
	printf("Answer: %.3lf, %.3lf\n", X1, X2);
}
void main()
{
	answer answers[5];
	double a, b, c, x1, x2;
	char sign;

	answers[0] = *answer_2;
	answers[1] = *answer_1;
	answers[2] = *answer0;
	answers[3] = *answer1;
	answers[4] = *answer2;
	printf("A:");
	scanf("%lf", &a);
	printf("B:");
	scanf("%lf", &b);
	printf("C:");
	scanf("%lf", &c);
	sign = ans_quad(a, b, c, &x1, &x2) + 2;
	answers[sign](x1, x2);
}
