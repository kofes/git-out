#include "stack.h"
#include <string.h>
#define ERR_NORM (0)
#define ERR_DIV0 (1)
#define ERR_OPEN (2)
#define ERR_SIGN (3)
#define ERR_CHAR (4)
#define GET_NUM(NUM) {                         \
  NUM = stack_get_elem(NUMB)->val;             \
  stack_del_elem(NUMB);                        \
}                                              \

#define SIGNAL(ERR) {                          \
  switch(ERR){                                 \
    case (1): printf("ERROR: DIV 0\n"); break; \
    case (2): printf("ERROR: OPEN\n"); break;  \
    case (3): printf("ERROR: SIGN\n"); break;  \
    case (4): printf("ERROR: CHAR\n"); break;  \
  }                                            \
}                                              \

typedef int (*t_func)(char** pos);
int get_type(char pos);
int RS (char **pos);
int RN (char **pos);
int RU (char **pos);
int RB (char **pos);

int priority(char ch){
  switch (ch){
    case '(': return -1;
    case '\0': return 0;
    case '\n': return 0;
    case ')': return 1;
    case '+': return 2;
    case '-': return 2;
    case '*': return 3;
    case '/': return 3;
    case -'-': return 4;
  }
}

static t_stack NUMB, SIGN;

const static char STAT [3][8] = {
  {0, 1, 2, -1, 0, -1, -1, -1},
  {1, -2, 2, 2, -1, 1, 3, -1},
  {2, 1, -2, -2, 0, -1, -1, -1}
};

const static t_func FUNC [3][8] = {
  {&RS, &RN, &RU, NULL, &RU, NULL, NULL, NULL},
  {&RS, &RN, &RB, &RB, NULL, &RB, NULL, NULL},
  {&RS, &RN, NULL, NULL, &RU, NULL, NULL, NULL}
};

int parser(char* str, double* val){
  char alph_table[256], *pos = str;
  int flag, t, g, err;
  memset(alph_table, 7, 256);
  alph_table[' '] = 0;
  alph_table['-'] = 2;
  memset(alph_table + 48, 1, 10);/*0-9*/
  alph_table['+'] = 3; alph_table['*'] = 3; alph_table['/'] = 3;
  alph_table['('] = 4; alph_table[')'] = 5;
  alph_table['\0'] = 6; alph_table['\n'] = 6;
  flag = 0;
  err = ERR_NORM;
  NUMB = stack_new();
  SIGN = stack_new();
  while (1){
    t = alph_table[*pos];
    g = flag;
    flag = STAT[g][t];
    if (flag<0){
      err = (flag) == (-2) ? ERR_SIGN : ERR_CHAR;
      break;
    }
    if (flag>2){
      err = RB(&pos);
      break;
    }
    err = FUNC [g][t](&pos);
    if (err != ERR_NORM){
      break;
    }
  }
  if (err == ERR_NORM){
    *val = stack_get_elem(NUMB)->val;
  }
  stack_del(SIGN);
  stack_del(NUMB);
  return err;
}

int RS(char **pos){
  for (; **pos == ' '; ++(*pos));
  return 0;
}

int RN(char **pos){
  stack_add_elem(NUMB)->val = strtod(*pos, pos);
  return 0;
}

int RU(char **pos){
  stack_add_elem(SIGN)->sign = (**pos) == ('-') ? (-**pos) : (**pos);
  ++(*pos);
  return 0;
}
#define EPS (1e-5)
int RB(char** pos){
  char ch = **pos;
  int p = priority(ch);
  while ((stack_get_elem(SIGN) != NULL) && (priority(stack_get_elem(SIGN)->sign) >= p)){
    char sym = stack_get_elem(SIGN)->sign;
    stack_del_elem(SIGN);
    double num1, num2;
    switch (sym){
      case - '-':
        GET_NUM(num1);
        stack_add_elem(NUMB)->val = -num1;
        break;
      case '+':
        GET_NUM(num1); GET_NUM(num2);
        stack_add_elem(NUMB)->val = num2 + num1;
        break;
      case '-':
        GET_NUM(num1); GET_NUM(num2);
        stack_add_elem(NUMB)->val = num2 - num1;
        break;
      case '/':
        GET_NUM(num1); GET_NUM(num2);
        if ((num1 - EPS < 0)&&(num1 + EPS > 0)) return 1;
        stack_add_elem(NUMB)->val = num2 / num1;
        break;
      case '*':
        GET_NUM(num1); GET_NUM(num2);
        stack_add_elem(NUMB)->val = num2 * num1;
        break;
      }
  }
  if (ch == ')'){
    if (stack_get_elem(SIGN) == NULL)
      return 2;
    stack_del_elem(SIGN);
    ++(*pos);
    return ERR_NORM;
  }
  if ((ch == '\0')||(ch == '\n')){
    if (stack_get_elem(SIGN) != NULL)
      return ERR_OPEN;
    return ERR_NORM;
  }
  stack_add_elem(SIGN)->sign = ch;
  ++(*pos);
  return 0;
}

int test(char* str){
  double x;
  char *pos;
  int err, i;
  err = parser(str, &x);
  if (!err){
    for (i = 0; (str[i] != '\0')&&(str[i] != '\n'); ++i)
      if (str[i] != ' ') printf("%c", str[i]);
    printf(" = %0.3lf\n", x);
  }
  return err;
}
int main(/*int argc, char *argv[]*/){
  int err;
  err = test("22+()");
  SIGNAL(err);
  err = test("-22+ 240 - 2*2");
  SIGNAL(err);
  err = test("((1+9)");
  SIGNAL(err);
  err = test("22-+99");
  SIGNAL(err);
  err = test("22/(55 - 55.522 + 0.522)");
  SIGNAL(err);
/*
  double x;
  if (argc > 1){
    err = parser(argv[1], &x);
    if (err == ERR_NORM)
      printf("%s = %.3lf\n", argv[1], x);
    else printf("ERROR CODE = %i\n", err);
  }
*/
  return 0;
}
