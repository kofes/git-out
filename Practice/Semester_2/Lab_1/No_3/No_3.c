#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>
#include <inttypes.h>
typedef
	struct
		{
			uint64_t num;
			uint64_t *val, *ord;
		}t_fact;

t_fact *new_fact(uint64_t NUMBER)
{
/*
	size_t <- тип максимальновозможного указателя
	num = 2;
	val = {2, 7};
	val = {5, 3};
*/
	t_fact *FACT;
	uint64_t i, m, r, last;
	if (NUMBER <=1)
		return NULL;
	for (m = 0, r = NUMBER; r != 0; ++m, r>>=1);
	FACT = (t_fact *)malloc(sizeof(t_fact));
	FACT->val = (uint64_t *)malloc(m*sizeof(uint64_t));
	FACT->ord = (uint64_t *)malloc(m*sizeof(uint64_t));
	FACT->num = 0;
	last = 0;
	for (i=2; i<=NUMBER/i; ++i)
		{
			
			while (NUMBER % i == 0)
			{
				if (i!= last)
					{
						FACT->val[FACT->num] = i;
						FACT->ord[FACT->num] = 1;
						++(FACT->num);
						last = i;
					}
				else
					++(FACT->ord[(FACT->num)-1]);
				NUMBER /= i;
			}
		}
	if (NUMBER !=1)
	{
		FACT->val[FACT->num] = NUMBER;
		FACT->ord[FACT->num] = 1;
	}
	return FACT;
}
void del_fact(t_fact * Fact)
{
	if (Fact != NULL)
	{
	free((Fact->ord));
	free((Fact->val));
	free(Fact);
	}
}
void main()
{
	t_fact *factor;
	uint64_t k;
	uint64_t NUM;
	//scanf("%lld", &NUM);
	for (NUM = 1ULL << 15; NUM>0; --NUM)
	{

		factor = new_fact(NUM);
		if (factor == NULL)
			printf(":(\n");
		else
		{
			printf("%" PRIu64 " = ", NUM);
			if (NUM < 0)
				printf("-");
			for (k=0; (k<=(factor->num))&&((factor->val[k])!=0); ++k)
				{
					printf("(%li^%li)", (factor->val[k]), (factor->ord[k]));
				}
		}
		printf("\n");
		del_fact(factor);
	}
}