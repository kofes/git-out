#include "lab-2.5.h"

int main(){
	FILE *fp;
	t_graph graph;
	size_t i, size, i1, i2, dist, num, *mass;
	char info;
	if ((fp = fopen("input.txt", "rt")) == NULL)
		return 1;
	fscanf(fp, "%zu", &size);
	graph = graph_new(size);
	while (!feof(fp)){
		fscanf(fp, "%zu %zu %zu", &i1, &i2, &dist);
		graph_add_edge(graph, i1, i2, dist);
		graph_add_edge(graph, i2, i1, dist);
	}
	fclose(fp);
	if (graph_dfs_crcl(graph, &info)){
		graph_del(graph);
		return 1;
	}
	if (info) printf("Graph has cyrcle \n");
	else printf("Graph hasn't cyrcles \n");
	if (graph_dfs_cmps(graph, &num)){
		graph_del(graph);
		return 1;
	}
	printf("Graph has %zu components \n", num);
	if (graph_bfs_dstc(graph, 0, 4, &num)){
		graph_del(graph);
		return 1;
	}
	printf("Sum edges between %u and %u equals %zu \n", 0, 4, num);
	if (graph_dijkstra(graph, 0, &mass)){
		graph_del(graph);
		return 1;
	}
	for (i = 0; i < size; ++i){
		if (mass[i] != INF_NUM)
			printf("Distance between %u and %zu equals %zu \n", 0, i, mass[i]);
		else
			printf("No ways between %u and %zu \n", 0, i);
	}
	graph_del(graph);
	return 0;
}