#ifndef __INCLUDE_GRAPH_
#define __INCLUDE_GRAPH_

#include <stdio.h>
#include <stdint.h>

typedef void *t_graph;

#define ERR_NORM (0) //Функция отработала без ошибок;
#define ERR_DATA (1) //Неправильные параметры на входе в функцию;
#define ERR_NULL (2) //Ошибка при выделении памяти;
#define INF_NUM (SIZE_MAX) //Максимальное расстояние между вершинами;

//Инициализация графа:
extern t_graph *graph_new(size_t numv);

//Деструктор графа:
extern void graph_del(t_graph graph);

//Добавление ребра из вершины i1 в i2 с весом distance:
extern int graph_add_edge(t_graph graph, size_t i1, size_t i2, size_t distance);

//Подсчет компонент связности графа:
extern int graph_dfs_cmps(t_graph graph, size_t *NUMC);

//Проверка графа на ацикличность:
extern int graph_dfs_crcl(t_graph graph, char *CRCL);

//Поиск наименьшего количество ребер между вершинами i1 и i2:
extern int graph_bfs_dstc(t_graph graph, size_t i1, size_t i2, size_t *DSTC);

//Нахождение наименьшего пути из вершины indv в остальные:
extern int graph_dijkstra(t_graph graph, size_t indv, size_t **distance);

#endif //__INCLUDE_GRAPH_