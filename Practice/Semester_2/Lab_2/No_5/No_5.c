#include "lab-2.2.h"
#include "queue.h"
#include "lab-2.5.h"

typedef struct{
	size_t numv;
	t_btree *vertex;
}t_ugraph;

t_graph *graph_new(size_t numv){
	t_ugraph *graph;
	size_t i;
	if (numv == 0)
		return NULL;
	if ((graph = (t_ugraph *)malloc(sizeof(t_ugraph))) == NULL)
		return NULL;
	if ((graph->vertex = (t_btree *)malloc(numv*sizeof(t_btree))) == NULL){
		free(graph);
		return NULL;
	}
	for (i = 0; i < numv; ++i){
		graph->vertex[i] = BTREE_NULL;
		if ((graph->vertex[i] = btree_new()) == NULL){
			while (i != 0){
				--i;
				btree_del(graph->vertex[i]);
			}
			free(graph->vertex);
			free(graph);
			return NULL;
		}
	}
	graph->numv = numv;
	return (t_graph *)graph;
}

void graph_del(t_graph graph){
	t_ugraph *ugr;
	size_t i;
	if (graph == NULL)
		return;
	ugr = (t_ugraph *)graph;
	for (i = 0; i < ugr->numv; ++i)
		btree_del(ugr->vertex[i]);
	free(ugr->vertex);
	free(ugr);
}

int graph_add_edge(t_graph graph, size_t i1, size_t i2, size_t distance){
	t_ugraph *ugr;
	ugr = (t_ugraph *)graph;
	if ((ugr == NULL)||(i1 >= ugr->numv)||(i2 >= ugr->numv)||(distance == INF_NUM))
		return ERR_DATA;
	*btree_add_elem(ugr->vertex[i1], i2) = distance;
	return ERR_NORM;
}

int graph_del_edge(t_graph graph, size_t i1, size_t i2){
	t_ugraph *ugr;
	ugr = (t_ugraph *)graph;
	if ((ugr == NULL)||(i1 >= ugr->numv)||(i2 >= ugr->numv))
		return ERR_DATA;
	btree_del_elem(ugr->vertex[i1], i2);
	return ERR_NORM;
}

int graph_dfs_cmps(t_graph graph, size_t *NUMC){
	t_ugraph *ugr;
	char *Mark;
	size_t i;
	if ((graph == NULL)||(NUMC == NULL))
		return ERR_DATA;
	ugr = (t_ugraph *)graph;
	if ((Mark = (char *)malloc((ugr->numv)*sizeof(char))) == NULL)
		return ERR_NULL;
	*NUMC = 0;
	for (i = 0; i < ugr->numv; Mark[i] = 0, ++i);
	void DFS(size_t indv){
		size_t i;
		Mark[indv] = 1;
		for (i = 0; i < ugr->numv; ++i)
			if ((!Mark[i])&&(btree_get_elem(ugr->vertex[indv], i) != NULL))
				DFS(i);
	}
	for (i = 0; i < ugr->numv; ++i)
		if (!Mark[i]){
			DFS(i);
			++(*NUMC);
		}
	free(Mark);
	return ERR_NORM;
}

int graph_dfs_crcl(t_graph graph, char *CRCL){
	t_ugraph *ugr;
	char *Mark;
	size_t i;
	if ((graph == NULL)||(CRCL == NULL))
		return ERR_DATA;
	ugr = (t_ugraph *)graph;
	if ((Mark = (char *)malloc((ugr->numv)*sizeof(char))) == NULL)
		return ERR_NULL;
	*CRCL = 0;
	for (i = 0; i < ugr->numv; Mark[i] = 0, ++i);
	void DFS(size_t indv, size_t last){
		size_t i;
		if (Mark[indv]){
			*CRCL = 1;
			return;
		}
		Mark[indv] = 1;
		for (i = 0; i < ugr->numv; ++i)
			if ((i != last)&&(!(*CRCL))&&(btree_get_elem(ugr->vertex[indv], i) != NULL))
				DFS(i, indv);
	}
	for (i = 0; i < ugr->numv; ++i)
		if ((!Mark[i])&&(!(*CRCL)))
			DFS(i, i);
	free(Mark);
	return ERR_NORM;
}

int graph_bfs_dstc(t_graph graph, size_t i1, size_t i2, size_t *DSTC){
	t_ugraph *ugr;
	t_queue que;
	char *Mark;
	size_t i;
	que = QUEUE_NULL;
	que = queue_new();
	ugr = (t_ugraph *)graph;
	if ((graph == NULL)||(DSTC == NULL)||(i1 >= ugr->numv)||(i2 >= ugr->numv))
		return ERR_DATA;
	if ((Mark = (char *)malloc((ugr->numv)*sizeof(char))) == NULL)
		return ERR_NULL;
	*DSTC = INF_NUM;
	for (i = 0; i < ugr->numv; Mark[i] = 0, ++i);
	void BFS(size_t indv, size_t len){
		t_queue_elem elem;
		elem.nume = len;
		size_t i;
		if (i2 == indv){
			*DSTC = len - 1;
			return;
		}
		for (i = 0; i < ugr->numv; ++i)
			if ((!Mark[i])&&(btree_get_elem(ugr->vertex[indv], i) != NULL)){
				elem.key = i;
				*queue_add_elem(que) = elem;
				Mark[i] = 1;
			}
		while (queue_hht(que)){
			elem = *queue_get_elem(que);
			queue_del_elem(que);
			BFS(elem.key, elem.nume + 1);
		}
	}
	Mark[i1] = 1;
	BFS(i1, 1);
	free(Mark);
	queue_del(que);
	return ERR_NORM;
}

int graph_dijkstra(t_graph graph, size_t indv, size_t **distance){
	t_ugraph *ugr;
	char *Mark;
	size_t *Dist, *Last, i, j, k, min;
	ugr = (t_ugraph *)graph;
	if ((graph == NULL)||(indv >= ugr->numv)||(distance == NULL))
		return ERR_DATA;
	if ((Mark = (char *)malloc((ugr->numv)*sizeof(char))) == NULL)
		return ERR_NULL;
	if ((Dist = (size_t *)malloc((ugr->numv)*sizeof(size_t))) == NULL){
		free(Mark);
		return ERR_NULL;
	}
	if ((Last = (size_t *)malloc((ugr->numv)*sizeof(size_t))) == NULL){
		free(Mark);
		free(Dist);
		return ERR_NULL;
	}
	for (i = 0; i < ugr->numv; ++i){
		Mark[i] = 0;
		Dist[i] = INF_NUM;
		if (btree_get_elem(ugr->vertex[indv], i) != NULL)
			Dist[i] = *btree_get_elem(ugr->vertex[indv], i);
	}
	Mark[indv] = 1;
	Dist[indv] = 0;
	Last[indv] = 0;
	for (k = 0; k < ugr->numv; ++k){
		min = INF_NUM;
		j = indv;
		for (i = 0; i < ugr->numv; ++i)
			if ((!Mark[i])&&(min > Dist[i])){
				min = Dist[i];
				j = i;
			}
		if (j == indv){
			*distance = Dist;
			return ERR_NORM;
		}
		Mark[j] = 1;
		for (i = 0; i < ugr->numv; ++i)
			if ((!Mark[i])&&(btree_get_elem(ugr->vertex[j], i) != NULL))
				if (Dist[i] > Dist[j] + *btree_get_elem(ugr->vertex[j], i)){
					Dist[i] = Dist[j] + *btree_get_elem(ugr->vertex[j], i);
					Last[k] = j;
				}
	}
	*distance = Dist;
	return ERR_NORM;
}