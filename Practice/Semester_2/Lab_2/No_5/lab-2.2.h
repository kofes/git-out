#ifndef __INCLUDE_BTREE_H
#define __INCLUDE_BTREE_H

#include <stdlib.h>
#include <stdio.h>

//Ключ - индекс вершины
typedef size_t t_btree_key;

//Значение - расстояние до вершины
typedef size_t t_btree_val;

//Тип дескриптора двоичного дерева:
typedef void *t_btree;

//Признак открепленного дескриптора:
#define BTREE_NULL (NULL)

//Генерирует пустое дерево и возвращает его дескриптор:
extern t_btree btree_new();

//Освобождает память из-под дерева:
extern void btree_del(t_btree btree);

//Выполняет вставку элемента по ключу:
extern t_btree_val *btree_add_elem(t_btree btree, t_btree_key key);

//Выполняет поиск элемента по ключу:
extern t_btree_val *btree_get_elem(t_btree btree, t_btree_key key);

//Выполняет удаление элемента по ключу:
extern void btree_del_elem(t_btree btree, t_btree_key key);

//Выводит содержимое дерева в виде дерева в выходной поток:
extern void btree_out_btree(t_btree *btree, FILE *fout);

//Выводит содержимое дерева в выходной поток:
extern void btree_out(t_btree btree, FILE *fout);

#endif //__INCLUDE_BTREE_H
