#include "lab-2.2.h"

char test_add(){
	FILE *out;
	t_btree tree;
	t_btree_val *val;
	size_t i, size = 100;
	if ((out = fopen("output.txt", "wt")) == NULL)
		return 1;
	tree = btree_new();
	for (i = 0; i<size; ++i){
		*(btree_add_elem(tree, i)) = i;
		//printf("!%i!\n", i);
	}
		btree_out_btree(tree, out);
	btree_del(tree);
	fclose(out);
}

char test_del(){
	FILE *out;
	t_btree tree;
	size_t i, size = 10000;
	if ((out = fopen("output.txt", "wt")) == NULL)
		return 1;
	tree = btree_new();
	for (i = 0; i<size; ++i)
		*(btree_add_elem(tree, i)) = i;
	for (i = 0; i<size; ++i){
		btree_del_elem(tree, 2*i);
	}
	btree_out(tree, out);
	btree_del(tree);
	fclose(out);
	return 0;
}

int main(){
	char i1, i2;
	i1 = test_add();
	i2 = test_del();
	if (i1 > i2)
		return i1;
	return i2;
}