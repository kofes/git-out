#include <stdio.h>
#include <stdlib.h>
#include "lab-2.2.h"

typedef struct t_btree_node{
	struct t_btree_node *child[2];
	t_btree_key key;
	t_btree_val val;
	char diff;
}t_btree_node;
typedef struct{
	t_btree_node *root;
}t_utree;

t_btree btree_new(){

	t_utree *tree;

	if ((tree = (t_utree *)malloc(sizeof(t_utree))) == NULL)
		return NULL;

	tree->root = NULL;

	return (t_btree)tree;
}

void btree_del(t_btree btree){

	t_utree *tree;
	tree = (t_utree *)btree;

	void btree_del_node(t_btree_node *node){

		if (node == NULL)
			return;

		btree_del_node(node->child[0]);
		btree_del_node(node->child[1]);
		free(node);
	}

	if (tree == NULL)
		return;
	btree_del_node(tree->root);
}

t_btree_node *lit_turn(t_btree_node *root, char inv){
	t_btree_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

t_btree_node *turn(t_btree_node *node, char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

t_btree_val *btree_add_elem(t_btree btree, t_btree_key key){

	char info = 0;
	t_utree *tree;
	t_btree_val *val = NULL;
	tree = (t_utree *)btree;
	t_btree_node *btree_add_node(t_btree_node *node){
		char i;
		if (node == NULL){

			if ((node = (t_btree_node *)malloc(sizeof(t_btree_node))) == NULL){
				info = 1;
				return NULL;
			}
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			node->key = key;
			val = &(node->val);

			return node;
		}

		i = (key >= node->key);

		node->child[i] = btree_add_node(node->child[i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turn(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = btree_add_node(tree->root);
	return val;
}

t_btree_val *btree_get_elem(t_btree btree, t_btree_key key){

	t_utree *tree = (t_utree *)btree;
	t_btree_node *node;
	char i;
	if (tree == NULL)
		return NULL;
	node = tree->root;
	while (node != NULL){
		if (node->key == key)
			return &(node->val);
		else{
			i = (key >= node->key);
			node = node->child[i];
		}
	}
	return NULL;
}

void btree_del_elem(t_btree btree, t_btree_key key){

	char info = 0;
	t_utree *tree = (t_utree *)btree;

	t_btree_node *del_min(t_btree_node *node){
		if (node->child[0] == NULL)
			return node->child[1];
		node->child[0] = del_min(node->child[0]);
		return turn(node, 1);
	}
	t_btree_node *btree_del_node(t_btree_node *node){
		char i;
		if (node == NULL){
			info = 1;
			return NULL;
		}
		if (node->key == key){
			t_btree_node *left = node->child[0], *right = node->child[1];
			char diff = node->diff;
			free(node);
			if (right != NULL){
				t_btree_node *min;

				min = right;
				while (min->child[0] != NULL)
					min = min->child[0];
				min->child[1] = del_min(right);
				min->child[0] = left;
				return turn(min, 0);
			}
			else
				return left;
		}
		i = (key >= node->key);
		node->child[i] = btree_del_node(node->child[i]);
		node->diff += (2*i - 1)*((info + 1)%2);
		node = turn(node, (i+1)%2);
		if ((info == 0)&&(abs(node->diff) == 1))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return;
	tree->root = btree_del_node(tree->root);
}

void btree_out(t_btree btree, FILE *fout){

	t_utree *tree = (t_utree *)btree;
	void btree_out_node(t_btree_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		fprintf(fout, "%i ", node->val);
		btree_out_node(node->child[1]);
	}

	if ((tree == NULL)||(fout == NULL))
		return;

	btree_out_node(tree->root);
}

void btree_out_btree(t_btree *btree, FILE *fout)
{
	t_utree *tree;
	tree = (t_utree *)btree;

	void btree_out_btree_node(t_btree_node *node, size_t count)
	{
		size_t i;

		if (node == NULL)
			return;
		btree_out_btree_node(node->child[1], count + 1);
		for (i = 0; i<2*count; ++i)
			fprintf(fout, "  ");
		fprintf(fout, "(%i-%i)\n", node->diff, node->val);
		btree_out_btree_node(node->child[0], count + 1);
	}

	if ((tree == NULL)||(fout == NULL))
		return;

	btree_out_btree_node(tree->root, 0);
}