#ifndef __INCLUDE_TABLE_H
#define __INCLUDE_TABLE_H

#include <stdlib.h>
#include <stdio.h>

//Тип ключа:
typedef int t_htbl_key;

//Тип значения:
typedef int t_htbl_val;

//Тип хеш-функции:
typedef size_t (*t_htbl_hash) (t_htbl_key);

//Тип дескриптора хеш-таблицы:
typedef void *t_table;

//Признак открепленного дескриптора:
#define TABLE_NULL (NULL)

//Возможные типы хеш-таблиц:
#define TABLE_TYPE_LIST (0)	//Хеш-таблица с цепочками;
#define TABLE_TYPE_OPEN (1)	//Хеш-таблица с открытой адресацией;

//Генерирует пустую хеш-таблицу и возвращает ее дескриптор:
extern t_table table_new(size_t len, t_htbl_hash hash, int type);

//Освобождает память из-под хеш-таблицы:
extern void table_del(t_table table);

//Выполняет вставку элемента по ключу (возвращает указатель на поле с новым значением):
extern t_htbl_val *table_add_elem(t_table table, t_htbl_key key);

//Выполняет поиск элемента по ключу (возвращает указатель на поле с искомым значением):
extern t_htbl_val *table_get_elem(t_table table, t_htbl_key key);

//Выполняет удаление элемента по ключу:
extern void table_del_elem(t_table table, t_htbl_key key);

//Выводит содержимое хеш-талицы в выходной поток:
extern void table_out(t_table table, FILE *fout);

#endif //__INCLUDE_TABLE_H
