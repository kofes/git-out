#include "lab-2.3.h"

int void_test(){

	size_t i, len = 100;
	t_table table;
	size_t hash(t_htbl_key key){
		return key;
	}

	table = table_new(len, hash, TABLE_TYPE_LIST);
	for (i = 0; i < len; ++i)
		*table_add_elem(table, 2*i) = i;
	table_out(table, stdout);
	printf("\n");
	for (i = 0; i<50; ++i)
		table_del_elem(table, 2*i);
	table_out(table, stdout);
	printf("\n");
	table_del(table);
	return 0;
}

int main(){

	char info;
	//info = open_test();
	//info = list_test();
	info = void_test();

	return info;
}