/*
:	flag:
:			0 	<- empty.
:			1 	<- blocked.
:		   -1 	<- free.
*/
#include "lab-2.3.h"
/*
=========================================================================================================================
*/
typedef struct{
	char flag;
	t_htbl_key key;
	t_htbl_val val;
}t_htbl_open_rec;

typedef struct{
	size_t len;
	size_t num;
	t_htbl_open_rec *table;
	t_htbl_hash hash;
}t_htbl_open;

typedef struct t_htbl_list_rec{
	struct t_htbl_list_rec *next;
	t_htbl_key key;
	t_htbl_val val;
}t_htbl_list_rec;

typedef struct{
	size_t len;
	size_t num;
	t_htbl_list_rec **table;
	t_htbl_hash hash;
}t_htbl_list;

typedef struct{
	int type; //1 - open, 0 - list;
	union{
		t_htbl_open *open;
		t_htbl_list *list;
	};
}t_htbl_type;
/*
=========================================================================================================================
*/
t_htbl_open *htbl_open_init(size_t len, t_htbl_hash hash){

	t_htbl_open *open;
	size_t i;

	if ((hash == NULL)||(len == 0))
		return NULL;

	if ((open = (t_htbl_open *)malloc(sizeof(t_htbl_open))) == NULL)
		return NULL;

	if ((open->table = (t_htbl_open_rec *)malloc(len*sizeof(t_htbl_open_rec))) == NULL){
		free(open);
		return NULL;
	}

	for (i=0; i<len; ++i){
		open->table[i].key = 0;
		open->table[i].val = 0;
		open->table[i].flag = 0;
	}
	open->len = len;
	open->num = 0;
	open->hash = hash;

	return open;
}
/*
=========================================================================================================================
*/
t_htbl_list *htbl_list_init(size_t len, t_htbl_hash hash){

	t_htbl_list *list;
	size_t i;

	if ((hash == NULL)||(len == 0))
		return NULL;
	if ((list = (t_htbl_list *)malloc(sizeof(t_htbl_list))) == NULL)
		return NULL;

	if ((list->table = (t_htbl_list_rec **)malloc(len*sizeof(t_htbl_list_rec*))) == NULL){
		free(list);
		return NULL;
	}

	for (i=0; i<len; ++i)
		list->table[i] = NULL;
	list->len = len;
	list->num = 0;
	list->hash = hash;

	return list;
}
/*
=========================================================================================================================
*/
t_table table_new(size_t len, t_htbl_hash hash, int type){

	t_htbl_type *table;

	if ((hash == NULL)||(len == 0)||((type % 2) != type))
		return NULL;

	if ((table = (t_htbl_type *)malloc(sizeof(t_htbl_type))) == NULL)
		return NULL;

	if (type == TABLE_TYPE_OPEN){
		table->type = TABLE_TYPE_OPEN;
		table->open = htbl_open_init(len, hash);
	}
	else{
		table->type = TABLE_TYPE_LIST;
		table->list = htbl_list_init(len, hash);
	}

	return (t_table)table;
}
/*
=========================================================================================================================
*/
void htbl_open_delit(t_htbl_open *open){

	if (open == NULL)
		return;
	free(open->table);
	free(open);
}
/*
=========================================================================================================================
*/
void htbl_list_delit(t_htbl_list *list){

	size_t i;
	t_htbl_list_rec *node;

	if (list == NULL)
		return;

	for (i = 0; i < list->len; ++i)
		while (list->table[i] != NULL){
			node = list->table[i];
			list->table[i] = list->table[i]->next;
			free(node);
		}
	free(list->table);
	free(list);
}
/*
=========================================================================================================================
*/
void table_del(t_table table){

	t_htbl_type *Ttable = (t_htbl_type *)table;

	if (Ttable == NULL)
		return;

	if (Ttable->type == TABLE_TYPE_OPEN)
		htbl_open_delit(Ttable->open);
	else
		htbl_list_delit(Ttable->list);
	free(Ttable);
}
/*
=========================================================================================================================
*/
t_htbl_val *htbl_open_add(t_htbl_open *open, t_htbl_key key){

	size_t i, id;

	if (open == NULL)
		return NULL;

	for (i = 0, id = (open->hash(key) % open->len); i < open->len; ++i)
		if ((open->table[id].flag != 1)||(open->table[id].key == key)){
			++(open->num);
			open->table[id].key = key;
			open->table[id].flag = 1;
			return &(open->table[id].val);
		}
		else
			id = (open->hash(key) + i) % open->len;

	return NULL;
}
/*
=========================================================================================================================
*/
t_htbl_val *htbl_list_add(t_htbl_list *list, t_htbl_key key){

	size_t id;
	t_htbl_list_rec *node;

	if (list == NULL)
		return NULL;

	id = list->hash(key) % list->len;

	if ((node = (t_htbl_list_rec *)malloc(sizeof(t_htbl_list_rec))) == NULL)
		return NULL;
	node->key = key;
	node->next = list->table[id];
	list->table[id] = node;
	++(list->num);
	return &(node->val);
}
/*
=========================================================================================================================
*/
t_htbl_val *table_add_elem(t_table table, t_htbl_key key){

	t_htbl_type *Ttable = (t_htbl_type *)table;

	if (Ttable == NULL)
		return NULL;

	if (Ttable->type == TABLE_TYPE_OPEN)
		return htbl_open_add(Ttable->open, key);
	else
		return htbl_list_add(Ttable->list, key);
}
/*
=========================================================================================================================
*/
t_htbl_val *htbl_open_get(t_htbl_open *open, t_htbl_key key){

	size_t i, id;

	if (open == NULL)
		return NULL;

	for (i = 0, id = (open->hash(key) % open->len); (open->table[id].flag != 0)&&(i < open->len); ++i)
		if (open->table[id].key == key)
			return &(open->table[id].val);
		else
			id = (open->hash(key) + i) % open->len;

	return NULL;
}
/*
=========================================================================================================================
*/
t_htbl_val *htbl_list_get(t_htbl_list *list, t_htbl_key key){

	size_t id;
	t_htbl_list_rec *curr;

	if (list == NULL)
		return NULL;

	id = list->hash(key) % list->len;
	curr = list->table[id];

	while (curr != NULL)
		if (curr->key == key)
			return &(curr->val);
		else
			curr = curr->next;

	return NULL;
}
/*
=========================================================================================================================
*/
t_htbl_val *table_get_elem(t_table table, t_htbl_key key){

	t_htbl_type *Ttable = (t_htbl_type *)table;

	if (Ttable == NULL)
		return NULL;

	if (Ttable->type == TABLE_TYPE_OPEN)
		return htbl_open_get(Ttable->open, key);	
	else
		return htbl_list_get(Ttable->list, key);
}
/*
=========================================================================================================================
*/
void htbl_open_del(t_htbl_open *open, t_htbl_key key){

	size_t i, id;

	if (open == NULL)
		return;

	for (i = 0, id = (open->hash(key) % open->len); (open->table[id].flag != 0)&&(i < open->len); ++i)
		if (open->table[id].key == key){
			open->table[id].flag = -1;
			return;
		}
		else
			id = (open->hash(key) + i) % open->len;
}
/*
=========================================================================================================================
*/
void htbl_list_del(t_htbl_list *list, t_htbl_key key){

	size_t id;
	t_htbl_list_rec *node;

	if (list == NULL)
		return;

	id = list->hash(key) % list->len;
	node = list->table[id];
	if (node != NULL){
		if (node->key == key){
			list->table[id] = list->table[id]->next;
			--(list->num);
			free(node);
			return;
		}
		while (node->next != NULL)
			if (node->next->key == key){
				
				t_htbl_list_rec *del_node;

				del_node = node->next;
				node->next = del_node->next;
				--(list->num);
				free(del_node);
				return;
			}
	}
}
/*
=========================================================================================================================
*/
void table_del_elem(t_table table, t_htbl_key key){

	t_htbl_type *Ttable = (t_htbl_type *)table;

	if (Ttable == NULL)
		return;

	if (Ttable->type == TABLE_TYPE_OPEN)
		return htbl_open_del(Ttable->open, key);
	else
		return htbl_list_del(Ttable->list, key);
}
/*
=========================================================================================================================
*/
void htbl_open_out(t_htbl_open *open, FILE *fout){

	size_t i;

	if ((open == NULL)||(fout == NULL))
		return;

	for (i = 0; i < open->len; ++i)
		if (open->table[i].flag == 1)
			fprintf(fout, "%i ", open->table[i].val);
}
/*
=========================================================================================================================
*/
void htbl_list_out(t_htbl_list *list, FILE *fout){

	size_t i;
	t_htbl_list_rec *curr;

	if ((list == NULL)||(fout == NULL))
		return;
	for (i = 0; i < list->len; ++i){
		curr = list->table[i];
		while (curr != NULL){
			fprintf(fout, "%i ", curr->val);
			curr = curr->next;
		}
	}
}
/*
=========================================================================================================================
*/
void table_out(t_table table, FILE *fout){

	t_htbl_type *Ttable = (t_htbl_type *)table;

	if (Ttable == NULL)
		return;

	if (Ttable->type == TABLE_TYPE_OPEN)
		return htbl_open_out(Ttable->open, fout);
	else
		return htbl_list_out(Ttable->list, fout);
}
/*
=========================================================================================================================
*/
