#ifndef __INCLUDE_PTREE_H
#define __INCLUDE_PTREE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//Тип атомарного ключа:
typedef unsigned char t_ptree_key;

//Тип значения:
typedef int t_ptree_val;

//Тип дескриптора дерева:
typedef void *t_ptree;

//Признак открепленного дескриптора:
#define PTREE_NULL (NULL)

//Генерирует пустое дерево для заданного алфавита ключей и возвращает его дескриптор:
extern t_ptree ptree_new(size_t len, const t_ptree_key *alph, t_ptree_key end);

//Освобождает память из-под префиксного дерева:
extern void ptree_del(t_ptree ptree);

//Выполняет вставку элемента по ключу (возвращает указатель на поле с новым значением):
extern t_ptree_val *ptree_add_elem(t_ptree ptree, const t_ptree_key *key);

//Выполняет поиск элемента по ключу (возвращает указатель на поле с искомым значением):
extern t_ptree_val *ptree_get_elem(t_ptree ptree, const t_ptree_key *key);

//Выполняет удаление элемента по ключу:
extern void ptree_del_elem(t_ptree ptree, const t_ptree_key *key);

//Выводит содержимое дерева в выходной поток:
extern void ptree_out(t_ptree ptree, FILE *fout);

#endif //__INCLUDE_PTREE_H
