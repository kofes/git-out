#include "lab-2.4.h"

typedef struct t_ptree_node{
	struct t_ptree_node **child;
	t_ptree_val *val;
	t_ptree_key *key;
}t_ptree_node;

typedef struct{
	t_ptree_node **root;
	size_t len, size, height;
	t_ptree_key *alph;
	t_ptree_key end;
}t_utree;

int diff(const void *a, const void *b){
	return *(t_ptree_key *)a - *(t_ptree_key *)b;
}

t_ptree ptree_new(size_t len, const t_ptree_key *alph, t_ptree_key end){
	t_utree *tree;
	size_t i;
	if ((alph == NULL)||(len == 0))
		return NULL;

	if ((tree = (t_utree *)malloc(sizeof(t_utree))) == NULL)
		return NULL;
	if ((tree->alph = (t_ptree_key *)malloc(len*sizeof(t_ptree_key))) == NULL){
		free(tree);
		return NULL;
	}
	memcpy(tree->alph, alph, len);
	qsort(tree->alph, len, sizeof(t_ptree_key), diff);
	for (i = 1; i < len; ++i)
		if (tree->alph[i-1] == tree->alph[i]){
			free(tree->alph);
			free(tree);
			return NULL;
		}
	if ((tree->root = (t_ptree_node **)malloc(len*sizeof(t_ptree_node *))) == NULL){
		free(tree->alph);
		free(tree);
		return NULL;
	}
	for (i = 0; i < len; ++i)
		tree->root[i] = NULL;
	tree->size = 0;
	tree->end = end;
	tree->len = len;
	tree->height = 0;
	return (t_ptree )tree;
}

void ptree_del(t_ptree ptree){
	t_utree *tree = (t_utree *)ptree;
	size_t i;

	void ptree_del_node(t_ptree_node *node){
		size_t i;
		if (node == NULL)
			return;
		free(node->val);
		free(node->key);
		for (i = 0; i < tree->len; ++i)
			ptree_del_node(node->child[i]);
		free(node->child);
		free(node);
	}

	if (tree == NULL)
		return;
	free(tree->alph);
	for (i = 0; i < tree->len; ++i)
		ptree_del_node(tree->root[i]);
	free(tree->root);
	free(tree);
}

size_t get_mid(size_t last, const t_ptree_key *alph, t_ptree_key key, char *err){
	size_t mid, first;
	if (alph == NULL){
		*err = 1;
		return 0;
	}
	first = 0;
	mid = first + (last - first)/2;
	while ((first < last)&&(key != alph[mid])){
		mid = first + (last - first)/2;
		if (key <= alph[mid])
			last = mid;
		else
			first = mid + 1;
	}
	if (key != alph[mid]){
		*err = 1;
		return 0;
	}
	return mid;
}

t_ptree_node *new_node(size_t len){
	t_ptree_node *node;
	size_t i;
	if ((node = (t_ptree_node *)malloc(sizeof(t_ptree_node))) == NULL)
		return NULL;
	if ((node->child = (t_ptree_node **)malloc(len*sizeof(t_ptree_node))) == NULL){
		free(node);
		return NULL;
	}
	for (i = 0; i < len; ++i)
		node->child[i] = NULL;
	node->key = NULL;
	node->val = NULL;
	return node;
}

size_t get_len(t_ptree_key end, const t_ptree_key *key){
	size_t len;
	if (key == NULL)
		return 0;
	for (len = 0; key[len] != end; ++len);
	return (len+1);
}

t_ptree_val *ptree_add_elem(t_ptree ptree, const t_ptree_key *key){
	t_utree *tree = (t_utree *)ptree;
	t_ptree_val *val = NULL;
	size_t mid, num, key_last;
	key_last = 1;
	char err = 0;
	if ((tree == NULL)||(key == NULL))
		return NULL;
	num = get_len(tree->end, key);
    if (num > tree->height)
        tree->height = num;
	mid = get_mid(tree->len, tree->alph, key[0], &err);
	if (err) return NULL;
	t_ptree_node *ptree_add_node(t_ptree_node *node){
		size_t mid, i, ln_key;
		if (node == NULL){
			node = new_node(tree->len);
			if ((node->key = (t_ptree_key *)malloc((num - key_last)*sizeof(t_ptree_key))) == NULL){
				free(node->child);
				free(node);
				return NULL;
			}
			memcpy(node->key, key + key_last, num - key_last);
			node->key[num - key_last - 1] = tree->end;
			if ((node->val = (t_ptree_val *)malloc(sizeof(t_ptree_val))) == NULL){
				free(node->child);
				free(node->key);
				free(node);
				return NULL;
			}
			val = node->val;
			*(node->val) = 0;
			return node;
		}
		ln_key = get_len(tree->end, node->key);
		for (i = 0; (i < (ln_key - 1))&&(i < (num - key_last - 1)); ++i)
			if (node->key[i] != key[key_last + i]){
				t_ptree_node *fork;
				t_ptree_key *mem;
				mid = get_mid(tree->len, tree->alph, node->key[i], &err);
				if ((fork = new_node(tree->len)) == NULL)
					return node;
				if ((fork->key = (t_ptree_key *)malloc((i+1)*sizeof(t_ptree_key))) == NULL){
					free(fork->child);
					free(fork);
					return node;
				}
				memcpy(fork->key, node->key, i);
				fork->key[i] = tree->end;
				fork->child[mid] = node;
				mem = (t_ptree_key *)malloc((ln_key - i)*sizeof(t_ptree_key));
				memcpy(mem, node->key + i, ln_key - i);
				node->key = (t_ptree_key *)realloc(node->key, ln_key - i);
				memcpy(node->key, mem, ln_key - i);
				node->key[ln_key - i] = tree->end;
				free(mem);
				mid = get_mid(tree->len, tree->alph, key[i + key_last], &err);
				if (err){
					free(fork->key);
					free(fork->child);
					free(fork);
					return node;
				}
				if ((fork->child[mid] = new_node(tree->len)) == NULL){
					free(fork->child);
					free(fork->key);
					free(fork);
					return node;
				}
				if ((fork->child[mid]->key = (t_ptree_key *)malloc((num - key_last)*sizeof(t_ptree_key))) == NULL){
					free(fork->child[mid]);
					free(fork->child);
					free(fork->key);
					free(fork);
					return node;
				}
				if ((fork->child[mid]->val = (t_ptree_val *)malloc(sizeof(t_ptree_val))) == NULL){
					free(fork->child[mid]->key);
					free(fork->child[mid]);
					free(fork->child);
					free(fork);
					return node;
				}
				node = fork->child[mid];
				memcpy(node->key, key + key_last, num - key_last);
				*(fork->child[mid]->val) = 0;
				val = fork->child[mid]->val;
				return fork;
			}
		if (ln_key > num - key_last){
			t_ptree_node *link;
			t_ptree_key *mem;
			mid = get_mid(tree->len, tree->alph, node->key[i], &err);
			if (err) return node;
			if ((link = new_node(tree->len)) == NULL)
				return node;
			if ((link->key = (t_ptree_key *)malloc((num - key_last)*sizeof(t_ptree_key))) == NULL){
				free(link->child);
				free(link);
				return node;
			}
			memcpy(link->key, key + key_last, num - key_last);
			link->key[num - key_last - 1] = tree->end;
			link->child[mid] = node;
			mem = (t_ptree_key *)malloc((ln_key - num + key_last)*sizeof(t_ptree_key));
			memcpy(mem, node->key + num - key_last - 1, ln_key - num + key_last);
			node->key = (t_ptree_key *)realloc(node->key, ln_key - num + key_last + 1);
			memcpy(node->key, mem, ln_key - num + key_last);
			node->key[ln_key - num + key_last] = tree->end;
			if ((link->val = (t_ptree_val *)malloc(sizeof(t_ptree_val))) == NULL){
				free(link->key);
				free(link->child);
				free(link);
				return node;
			}
			*(link->val) = 0;
			val = link->val;
			++(tree->size);
			return link;
		}
		if (ln_key == num - key_last){
			if (node->val == NULL)
				node->val = (t_ptree_val *)malloc(sizeof(t_ptree_val));
            *(node->val) = 0;
			val = node->val;
			return node;
		}
		mid = get_mid(tree->len, tree->alph, key[i + key_last], &err);
		if (err) return node;
		key_last += get_len(tree->end, node->key);
		node->child[mid] = ptree_add_node(node->child[mid]);
		return node;
	}
	tree->root[mid] = ptree_add_node(tree->root[mid]);
	return val;
}

t_ptree_val *ptree_get_elem(t_ptree ptree, const t_ptree_key *key){
	t_utree *tree = (t_utree *)ptree;
	t_ptree_val *val = NULL;
	size_t mid, num, key_last;
	key_last = 1;
	char err = 0;
	if ((tree == NULL)||(key == NULL))
		return NULL;
	num = get_len(tree->end, key);
	mid = get_mid(tree->len, tree->alph, key[0], &err);
	if (err) return NULL;
	void ptree_get_node(t_ptree_node *node){
		size_t mid, i, ln_key;
		if (node == NULL)
			return;
		ln_key = get_len(tree->end, node->key);
		for (i = 0; (i < (ln_key - 1))&&(i < (num - key_last - 1)); ++i)
			if (node->key[i] != key[i + key_last])
				return;
		if ((i == (num - key_last - 1))&&(node->val != NULL)){
			val = node->val;
			return;
		}
		mid = get_mid(tree->len, tree->alph, key[i + key_last], &err);
		if (err) return;
		key_last += i;
		ptree_get_node(node->child[mid]);
	}
	ptree_get_node(tree->root[mid]);
	return val;
}

void ptree_del_elem(t_ptree ptree, const t_ptree_key *key){
	t_utree *tree = (t_utree *)ptree;
	size_t mid, num, key_last;
	key_last = 1;
	char err = 0, info = 0;
	if ((tree == NULL)||(key == NULL))
		return;
	num = get_len(tree->end, key);
	mid = get_mid(tree->len, tree->alph, key[0], &err);
	if (err) return;
	t_ptree_node *ptree_del_node(t_ptree_node *node){
		size_t mid, i, ln_key;
		if (node == NULL)
			return node;
		ln_key = get_len(tree->end, node->key);
		for (i = key_last; (i < (ln_key - 1))&&(i < (num - key_last - 1)); ++i)
			if (node->key[i] != key[i + key_last])
				return node;
		if (i == (num - key_last - 1)){
			size_t k, count, ind;
			if (node->val == NULL)
				return node;
			for (k = 0; k < tree->len; ++k)
				if (node->child[k] != NULL){
					++count;
					ind = k;
				}
			if (count > 1){
				free(node->val);
				node->val = NULL;
				return node;
			}
			if (count == 1){
				t_ptree_node *child;
				t_ptree_key *key;
				child = node->child[ind];
				key = (t_ptree_key *)malloc(get_len(tree->end, child->key)*sizeof(t_ptree_key));
				memcpy(key, child->key, get_len(tree->end, child->key));
				child->key = realloc(child->key, get_len(tree->end, node->key) + get_len(tree->end, child->key));
				memcpy(child->key, key, get_len(tree->end, key));
				memcpy(child->key + get_len(tree->end, key) - 2, node->key, get_len(tree->end, node->key));
				free(node->key);
				free(node->val);
				free(node);
				return child;
			}
			info = 1;
			free(node->key);
			free(node->val);
			free(node);
			return NULL;
		}
		mid = get_mid(tree->len, tree->alph, key[i], &err);
		if (err) return node;
		key_last = i;
		node->child[mid] = ptree_del_node(node->child[mid]);
		if (info){
			size_t k, count, ind;
			t_ptree_node *child;
			t_ptree_key *key;
			info = 0;
			if (node->val != NULL)
				return node;
			for (k = 0; k < tree->len; ++k)
				if (node->child[k] != NULL){
					++count;
					ind = k;
				}
			if (count > 1)
				return node;
			child = node->child[ind];
//
			key = (t_ptree_key *)malloc(get_len(tree->end, child->key)*sizeof(t_ptree_key));
			memcpy(key, child->key, get_len(tree->end, child->key));
			child->key = realloc(child->key, get_len(tree->end, node->key) + get_len(tree->end, child->key));
			memcpy(child->key, key, get_len(tree->end, key));
			memcpy(child->key + get_len(tree->end, key) - 2, node->key, get_len(tree->end, node->key));
//
			free(node->key);
			free(node);
			return child;
		}
		return node;
	}
	tree->root[mid] = ptree_del_node(tree->root[mid]);
}

void ptree_out(t_ptree ptree, FILE *fout){
	t_utree *tree = (t_utree *)ptree;
	t_ptree_key *key;
	size_t i, ind;
	ind = 1;
	if ((key = (t_ptree_key *)malloc((tree->height)*sizeof(t_ptree_key))) == NULL)
        return;
    key[tree->height - 1] = tree->end;
	if ((tree == NULL)||(fout == NULL))
		return;
	void ptree_out_node(t_ptree_node *node){
		size_t i;
		if (node == NULL) return;
        memcpy((key + ind), node->key, get_len(tree->end, node->key));
        key[ind + get_len(tree->end, node->key) - 1] = tree->end;
//
		if (node->val != NULL)
			fprintf(fout, "%s - %u\n", key, *(node->val));
//
        ind += get_len(tree->end, node->key);
 /*
        printf("%i!\n", ind);
        printf("%i - height\n", get_len(tree->end, key));
        printf("%s\n", node->key);
        printf("%i - length of postfix of key_node %s\n", get_len(tree->end, node->key), node->key);
*/
		for (i = 0; i < tree->len; ++i){
            key[ind - 1] = tree->alph[i];
			ptree_out_node(node->child[i]);
		}
		ind -= get_len(tree->end, node->key);
	}
	for (i = 0; i < tree->len; ++i){
        key[0] = tree->alph[i];
		ptree_out_node(tree->root[i]);
	}
}
