#include "lab-2.4.h"

#include <ctype.h>

t_ptree_key low_case(const t_ptree_key sym)
{
	if (((sym > 64)&&(sym <= 90))||((sym >127)&&(sym < 160)))
		return sym + 32;
	return sym;
}

int main(){
	FILE *fp;
	t_ptree tree;
	size_t i, m, len = 26;
    static t_ptree_key buff[33];
	t_ptree_key alph[] = {"zaqwsxcderfvbgtyhnmjuiklop"};
	tree = PTREE_NULL;
	tree = ptree_new(len, alph, '\0');
	if ((fp = fopen("The Hitchhiker’s Guide to the Galaxy.txt", "rt")) == NULL)
        return 1;
    while (!feof(fp)){
        for (i = 0; i < 32; ++i){
            buff[i] = fgetc(fp);
            if (iscntrl(buff[i]))
                break;
            if (!isalpha(buff[i]))
                break;
            buff[i] = low_case(buff[i]);
        }
        if (i == 0)
            continue;
        buff[i] = '\0';
//        printf("!!\n");
//        if (ptree_get_elem(tree, buff) == NULL){
            *ptree_add_elem(tree, buff) = 1;
//        }
//        else
//            ++(*ptree_get_elem(tree, buff));
    }
    fclose(fp);
    if ((fp = fopen("output.txt", "wt")) == NULL){
        ptree_del(tree);
        return 1;
    }
	ptree_out(tree, fp);
	fclose(fp);
	ptree_del(tree);
	return 0;
}
