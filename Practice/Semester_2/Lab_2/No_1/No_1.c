#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <limits.h>

typedef unsigned char SYM;
SYM low_case(const SYM sym)
{
	//if (((sym > 64)&&(sym <= 90))||((sym >127)&&(sym < 160)))
	//	return sym + 32;
	return sym;
}
typedef uint32_t *t_mass;
t_mass pi_func(const SYM* tmp)
{
	t_mass pi_mass;

	uint32_t k, i, tmp_size;

		tmp_size = strlen(tmp);
		pi_mass = (uint32_t*)malloc(tmp_size*sizeof(uint32_t));
		pi_mass[0] = 0;
		pi_mass[1] = 0;
		k = 0;
		for (i = 2; i < tmp_size; ++i)
			{
				while ((k > 0)&&((low_case(tmp[k])) != low_case(tmp[i])))
					k = pi_mass[k - 1];
				if (low_case(tmp[k]) == low_case(tmp[i]))
					++k;
				pi_mass[i] = k;
			}

	return	pi_mass;
}

uint32_t KMP(const SYM* T, const SYM* S)
{
	uint32_t len_S, len_T , m, k, i;
	t_mass F;

	if ((S == NULL)||(T == NULL))
		return -1;
	F = pi_func(T);
	len_S = strlen(S);
	len_T = strlen(T);
	k = 0;
	i = 0;

	while (i<=len_S)
		{
			while ((k > 0)&&((T[k]) != (S[i])))
				k = F[k];
			if ((T[k]) == (S[i]))
				++k;
			if (k == len_T)
				return (i - len_T + 1);
			++i;
		}
	return -1;
}

uint32_t BMH(const SYM* T, const SYM* S)
{
	uint32_t i, j, k, len_T, len_S;
	SYM alph_table[256];

	if ((S == NULL)||(T == NULL))
		return -1;

	len_T = strlen(T);
	len_S = strlen(S);

	if (len_T<=len_S)
	{
		for (i = 0; i<256; ++i)
			alph_table[i] = len_T;
		for (i = 1; i<len_T; ++i)
			alph_table[(T[i])] = len_T - i;

		i = len_T;
		do{
				j = len_T;
				k = i;
				while ((j>0)&&((S[k-1]) == (T[j-1])))
					{
						printf("%c\n", S[k-1]);
						--k;
						--j;
					}
				i = i + alph_table[S[i]];
			}while ((j>0)&&(i<=len_S));
		if (j != 0)
			return -1;
		else
			return k;
	}

	return -1;
}

uint32_t fNAIV(const SYM* tmp, FILE* fin)
{
	uint32_t len = strlen(tmp), l = 0, t = 1;
	uint32_t count = 0, k, m;
	SYM *buff;
	char flag;

	buff = (SYM *)malloc(len);

	m = fread(buff, 1, len, fin);

	while (m == len)
		{
		flag = 1;
		
		for (k = l; k < len; ++k)
		{
			if (low_case(tmp[k]) != low_case(buff[k]))
			{
				flag = 0;
				break;
			}
		}

		if (flag)
		{
			++count;
			m = fread(buff, 1, len, fin);
		}
		else
		{
			memmove(buff, buff+t, len-t);
			--m;
			m += fread(buff+m, 1, t, fin);
		}
	}

	free(buff);
	return count;
}
uint32_t fKMP(const SYM* tmp, FILE* fin)
{
	uint32_t i = 0 , l = 0, t = 1, k, len, m, count, *pi;
	SYM *buff;
	char flag;

	if ((tmp == NULL)||(fin == NULL))
		return -1;

	count = 0;
	len = strlen(tmp);
	pi = pi_func(tmp);
	buff = (SYM *)malloc(len);
	m = fread(buff, 1, len, fin);
	
	while (m == len)
		{
			flag = 1;

			for (k = l; k<len; ++k)
			{
				if (low_case(buff[k]) != low_case(tmp[k]))
				{
					flag = 0;
					break;
				}
			}
			if (flag)
			{
				++count;
				m = fread(buff, 1, len, fin);
				l = 0;
				t = 1;
			}
			else
			{
				if (k == 0)
					k = 1;
				t = k - pi[k-1];
				l = pi[k];
				memmove(buff, buff+t, len - t);
				m -= t;
				m += fread(buff+m, 1, t, fin);
			}
		}

	free(pi);
	free(buff);
	return count;	
}

/**
 * ...
 */
uint32_t fBMH(const SYM* tmp, FILE* fin)
{
	uint32_t k, l = 0, t = 1, len, m, count;
	SYM *buff, alph_table[256];
	char flag;

	if ((tmp == NULL)||(fin == NULL))
		return -1;//UINT32_MAX;

	count = 0;
	len = strlen(tmp);
	buff = (SYM *)malloc(len);
	m = fread(buff, 1, len, fin);
	for (k = 0; k<256; ++k)
		alph_table[low_case(k)] = len;
	for (k = 0; k < len - 1; ++k)
		alph_table[low_case(tmp[k])] = len - k - 1;
	while (m == len)
	{
		flag = 1;
		for (k = l; k<len; ++k)
		{
			//printf("%c\n", buff[k]);
			if (low_case(buff[k]) != low_case(tmp[k]))
			{
				//printf("!%c!\n", buff[m-1]);
				flag = 0;
				break;
			}
		}
		if (flag)
			{
				++count;
				m = fread(buff, 1, len, fin);		
			}
		else
			{
				t = alph_table[low_case(buff[len - 1])];
				//printf("%u", t);
				memmove(buff, buff+t, len - t);
				m -= t;
				m += fread(buff+m, 1, t, fin);
			};
	}
	//printf("\n");
	free(buff);
	return count;
}

SYM main()
{
	FILE *fp;
	uint64_t i;
	time_t timing[2];
	SYM T[]="Бог";

/*
	if ((fp = fopen("/home/kofes/C_BAR/Lab_2/input.txt", "wb")) == NULL)
		return 1;
	srand(time(NULL));

	for (i = 0; i<(1024*1024); ++i)
	{
		fputc('H', fp); fputc('a', fp); fputc('r', fp); fputc('r', fp);
		fputc('y', fp); fputc(' ', fp); fputc('P', fp); fputc('o', fp);
		fputc('t', fp); fputc('t', fp); fputc('e', fp); fputc('r', fp);
		fputc('\n', fp);
	}
	fclose(fp);
*/

	//if ((fp = fopen("/home/kofes/C_BAR/Lab_2/Bible-single.txt", "rb")) == NULL)
	if ((fp = fopen("/home/kofes/C_BAR/Lab_2/Bible-single.txt", "rb")) == NULL)
		return 1;

	timing[0] = clock();
	printf("NAIV:\t%llu - ", fNAIV(T, fp));
	timing[1] = clock();
	printf("%llu\n", (timing[1] - timing[0]));

	fseek(fp, 0, SEEK_SET);
	timing[0] = clock();
	printf("KMP:\t%llu - ", fKMP(T, fp));
	timing[1] = clock();
	printf("%llu\n", (timing[1] - timing[0]));

	fseek(fp, 0, SEEK_SET);
	timing[0] = clock();
	printf("BMH:\t%llu - ", fBMH(T, fp));
	timing[1] = clock();
	printf("%llu\n", (timing[1] - timing[0]));

	fclose(fp);
	return 0;
}