Java - 1995 (Sun Microsystems)
JVM(виртуальная машина):
  Hotspot - Oracle
  Open JDK - с открытым исх. кодом(примерно в 2 раза медленнее)
  Dalvik VM(для ARM архитектур) - Android - сильно перелопачен

Исходный код -> байт-код(похож на ассемблер(1 байт на кодирование команды))
байт-код ->:
  * JIT
  * Интерпретатор

ReHection(метапрограммирование)(фича языка)(рефлексия - способность языка влиять на программу) - слишком долго для интепретатора, поэтому появился JIT
JIT <=> Just in time compilation(прирост раз в 10, поэтому сейчас почти на всех интерпетируемых языках эта фича используется)

Client(малое количество оптимизаций) и Server(максимум оптимизаций(отжирает гигабайт памяти под линем)) режимы JVM

1. Инкапсуляция
  public, protected, private
2. Наследование
  Интерфейс
  Comparable
  List<Integer>L = new ArrayList<Integer>();
3. Полиморфизм
  final - запрещает наследовать класс или перегружать вирт. функцию
/*/..../*/
Class.java -> Class.class -> (+)
OtherClass.java -> OtherClass.class -> (+)
(+) Package.jar

//
import java.io.*; - /* так подключается вся библитека
import java.io.PrintWriter; - /*/или так/*/

package program;
/*/public - только он доступен для других классов/*/
public class Class {

}
//
Java гарантирует решение циклических зависимостей и то, что все объекты будут инициализированы
Гарантии компилятора:
1. Инициализация любых переменных
2. Выход за границы массива -> exception
3. Гарантия - объект не разрушится, пока где-то используется (формально - нет деструктора и нет указателей)

Память контролируется Garbage Collector
RAII здесь нет
вместо него используется
//
try
catch
finally
Формально java не использует практически стэк(в не частых случаях инициализации) - только кучу

Стирание типов - формально нельзя использовать фичи переданных в шаблон классов, кроме фич общего Object (нельзя уже использовать простые типы в качестве параметров шаблона)
**//
public class Class<T> {//Словно передали Object
  T getVaue();
  List<T> getList();
};
//**
Но можно сделать так:
**//
public class Class<T extends Comparable> {
  T value = ...
    value.compareTo(...)
};
//**
, тогда будет откатываться не до Object, а до Comparable...

Нельзя перегружать операторы

JDK (Java Development Kit) - библиотеки
JRE (Java Runtime Eviroment) - то, на чем можно запускать то, что написал
JSE (Java Standart Edition) (Уже есть Java 2D, Swing)
JEE (Java Enterprise Edition) (Уже есть либы для HTTP серверов, СУБД, Framework'и,...)

Потоки есть (и даже проще, чем на плюсах)
Thread -> MyThread
есть join, fork(detach), run

Class extends MyClass implementation MyInterface

Методы синхронизации потоков:
1. public void synchronized inerement() {
  //потоки уснут *полусистемным* вызовом
}

2. synchronized statement
  synchronized (array) {
    array.add(...);//только один поток сможет что-то добавить
  }
Любой тип данных можно объявить с модификатором *volatile* - гарантирует синхронизированый доступ к объекту

Есть Atomic Flag
 ComoareAndSet
 CompareAndSnap

Есть lambda-выражения
