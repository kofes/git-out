Const correctness

const int N;
int const N;
const int* p;//указатель на константу, но указатель можно менять
int * const p;//Костантный указатель
const int& z = x;//ссылка сама по себе уже константна - только объект нельзя будет изменять
mutable - игнорирует константность

Обработка исключений
1.
#include <cerrno>
extern int errno;
int do_task (...) {
  ...
  return err;
}
2.
#include <stdexcept>
int do_task(...) {
  ...
  throw std::runtime_error();
  throw 4;//
  throw "hello";//
}
try {
  do_task();
} catch (std::runtime_error& e) {
//Если нужно очистить память и продоложить эксепшн далее тот же
  delete ...;
  throw;
}
#include <exception>
std::terminate();
std::set_terminate(void (*p)());
std::get_terminate();
//Exception safety
1. Basic guarantee
  no leaks - никакие ресурсы не "вытекают"
2. Strong guarantee
  no leaks
  rollback - откат действий(изменится состояние...)
3. No throw guarantee
  no exception
...
catch (...) {//Ловит все эксепшены!}
...
1.
//RAII (Resource Acquistion Is Initialization)
//stack umwinding (раскрутка стэка)
class Ptr {
  int *p;
  Ptr () {p = new int[100];}
  ~Ptr () {delete[] p;}//<-!!!no leaks
}
2.
//Используют обычно через try, catch(rollback)
bool std::uncaught_exception();//был ли эксепшн вызван, обработан? (во время stack umwinding true)...
int uncaught_exceptions();//C++17 - подсчитывает количество эксепшнов.
