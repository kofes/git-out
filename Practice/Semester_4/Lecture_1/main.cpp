#include <iostream>
#include "Num.hpp"

using namespace Number;

int main(int argc, char const *argv[]) {
  Real<int> num;
  Real<int> n1 = num + 20 - 45;
  n1 /= 33;
  std::cout << n1.Re << std::endl;
  return 0;
}
