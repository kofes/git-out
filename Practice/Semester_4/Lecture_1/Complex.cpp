/*В декартовых - для модуля и для степени; и полярных координатах класс комплексных чисел*/
/*+, -, *, /, ^, ==, =*/

#include <iostream>
#include <fstream>
#include <cmath>


class Complex { /*Для декартовых координат*/
public:
  enum errors {
    ERR_NORM,
    ERR_DIV0,
  };
private:
  double _real;
  double _image;
  static errors error;
  static double EPS;
public:
  Complex () : _real(0), _image(0) {};
  Complex (double real) : _real(real), _image(0) {};
  Complex (double real, double image) : _real(real), _image(image) {};
  Complex (const Complex & c) : _real(c._real), _image(c._image) {};

  inline double real()  const {return _real;}
  inline double image() const {return _image;}
  inline double real(double src) {
    _real = src;
    return _real;
  }
  inline double image(double src) {
    _image = src;
    return _image;
  }
  inline errors get_error() {return error;}
  inline void   off_error() {error = ERR_NORM;}
  inline virtual double Abs () {
    return std::sqrt(_real * _real + _image * _image);
  }
  inline virtual Complex Pow (int p) {
    Complex result;
    double delta = std::sqrt(_real * _real + _image * _image);
    delta = std::pow(delta, p);
    double arg = std::atan(_image / _real);
    result._real = delta * std::cos(arg * p);
    result._image = delta * std::sin(arg * p);
    return result;
  }
  inline Complex operator + (const Complex & rh) {
    Complex result;
    result._image = this->_image + rh._image;
    result._real  = this->_real + rh._real;
    return result;
  }
  inline Complex operator - (const Complex & rh) {
    Complex result;
    result._image = this->_image - rh._image;
    result._real  = this->_real - rh._real;
    return result;
  }
  inline Complex operator * (const Complex & rh) {
    Complex result;
    result._real  = this->_real * rh._real - this->_image * rh._image;
    result._image = this->_real * rh._image + this->_image * rh._real;
    return result;
  }
  /*(a + i * b) / (c + i * d) = (a + i * b) * (c - i * d) / (c^2 + d^2)
  = (ac + i * (b * c - a * d) + b * d) / (c^2 + d^2)*/
  inline Complex operator / (const Complex & rh) {
    Complex result;
    double delta = rh._real * rh._real + rh._image * rh._image;
    if ((delta + Complex::EPS > 0)&&(delta - Complex::EPS < 0)){
      error = ERR_DIV0;
      return 0;
    }
    result._real = (this->_real * rh._real + this->_image * rh._image) / delta;
    result._image = (this->_image * rh._real - this->_real * rh._image) / delta;
    return result;
  }
  inline Complex & operator = (const Complex & rh) {
    this->_real = rh._real;
    this->_image = rh._image;
    return *this;
  }
  inline bool operator == (const Complex & rh){
    return (this->_image == rh._image && this->_real == rh._real) ? true : false;
  }
  friend std::ofstream & operator << (std::ofstream & out, const Complex & src) {
    out << src._real << ' ' << ((src._image < 0) ? '-' : '+') << " i * " << src._image << std::endl;
    return out;
  }
};

Complex::errors Complex::error = Complex::ERR_NORM;
double Complex::EPS = 1e-5;

class ComplexPolar : private Complex {
private:
  double _p, _phi;
public:
  ComplexPolar () : _p(0), _phi(0) {};
  ComplexPolar (double p, double phi) : _p(p), _phi(phi) {};
  ComplexPolar (const ComplexPolar &cp) : _p(cp._p), _phi(cp._phi) {};
  ComplexPolar (const Complex &c) {
    this->_p = std::sqrt(c.real() * c.real() + c.image() * c.image());
    this->_phi = std::atan(c.image() / c.real());
  }
  inline Complex s_Complex () {
    Complex result;
    result.real(this->Abs() * std::cos(this->_phi));
    result.image(this->Abs() * std::sin(this->_phi));
    return result;
  }
  inline double phi () {return _phi;}
  inline Complex Pow (int p) {
    Complex result;
    result.real(std::pow(this->_p, p) * std::cos(this->_phi * p));
    result.image(std::pow(this->_p, p) * std::cos(this->_phi * p));
    return result;
  }
  inline double Abs () {return _p;}
  inline ComplexPolar operator + (ComplexPolar &rh) {
    return ComplexPolar(this->s_Complex() + rh.s_Complex());
  }
  inline ComplexPolar operator - (ComplexPolar &rh) {
    return ComplexPolar(this->s_Complex() - rh.s_Complex());
  };
  /*(cos a + isin a) * (cos b + isin b)
  = cos a * cos b + i * (cos a * sin b + sin a * cos b) - sin a * sin b = cos(a + b) + i * sin(a + b)*/
  inline ComplexPolar operator * (ComplexPolar &rh) {
    ComplexPolar result;
    result._p = this->_p * rh._p;
    result._phi = this->_phi * rh._phi;
    return result;
  };
  /*(cos a + isin a) / (cos b + isin b)
  = (cos a + isin a) * (cos b - isin b)
  / (cos^2 b + sin ^ b) = cos(a - b) + isin(a - b)*/
  inline ComplexPolar operator / (ComplexPolar &rh) {
    ComplexPolar result;
    result._p = this->_p / rh._p;
    result._phi = this->_phi - rh._phi;
    return result;
  };
  inline ComplexPolar & operator = (const ComplexPolar &rh){
    this->_p = rh._p;
    this->_phi = rh._phi;
  };
  inline bool operator ==(const ComplexPolar &rh){
    return (this->_p == rh._p && this->_phi == rh._p) ? true : false;
  };
};

void test1() {
  Complex obj;
  std::cout << "Test \"Empty input\":\t";
  std::cout << ((obj.real() == 0 && obj.image() == 0) ? "OK" : "BAD") << std::endl;
}

void test2() {
  Complex obj(20);
  std::cout << "Test \"Real input\":\t";
  std::cout << ((obj.real() == 20 && !obj.image()) ? "OK" : "BAD") << std::endl;
}

void test3() {
  Complex obj(4.5, -3.7);
  std::cout << "Test \"Full input\":\t";
  std::cout << ((obj.real() == 4.5 && obj.image() == -3.7) ? "OK" : "BAD") << std::endl;
}

void test4() {
  Complex obj(21.3, -0.5);
  Complex res(obj);
  std::cout << "Test \"Copy\":\t\t";
  std::cout << ((obj.real() == 21.3 && obj.image() == -0.5) ? "OK" : "BAD") << std::endl;
}

void test5() {
  Complex obj(0.7, 5.2);
  std::cout << "Test \"Abs\":\t\t";
  double delta = std::sqrt(obj.real() * obj.real() + obj.image() * obj.image());
  std::cout << ((delta == obj.Abs()) ? "OK" : "BAD") << std::endl;
}

void test6() {
  Complex obj(3, 4.0);
  Complex result;
  std::cout << "Test \"Pow\":\t\t";
  result = obj.Pow(4);
  double Arg = std::atan(4.0/3);
  double _image = std::pow(obj.Abs(), 4) * std::sin(Arg * 4);
  double _real  = std::pow(obj.Abs(), 4) * std::cos(Arg * 4);
  std::cout << ((result.real() == _real && result.image() == _image) ? "OK" : "BAD") << std::endl;
}

void test7() {
  Complex obj;
  std::cout << "Test \"+\":\t\t";
  obj = Complex(22.3, 1.2) + Complex(2.6, 2.3);
  std::cout << ((obj.real() == (22.3 + 2.6) && obj.image() == (1.2 + 2.3)) ? "OK" : "BAD") << std::endl;
}

void test8() {
  Complex obj;
  std::cout << "Test \"-\":\t\t";
  obj = Complex(3.6, 222.3) - Complex(11, 45.1);
  std::cout << ((obj.real() == (3.6 - 11) && obj.image() == (222.3 - 45.1)) ? "OK" : "BAD") << std::endl;
}

void test9() {
  Complex obj;
  obj = Complex(72.32, 23.123) * Complex(1.32, 5.51);
  std::cout << "Test \"*\":\t\t";
  std::cout << ((obj.real() == (72.32 * 1.32 - 23.123 * 5.51) && obj.image() == (72.32 * 5.51 + 23.123 * 1.32)) ? "OK" : "BAD") << std::endl;
}

void test10() {
  Complex obj;
  std::cout << "Test \"/\":\t\t";
  double delta = 13.2 * 1.2 + 22.3 * 2;
  obj = Complex(13.2, 22.3) / Complex(1.2, 2);
  std::cout << ((obj.real() == (13.2 * 1.2 + 22.3 * 2) / delta && obj.image() == (22.3 * 1.2 - 1.32 * 2) / delta) ? "OK" : "BAD") << std::endl;
}

void test11() {
  Complex obj(1.3, 22.1);
  Complex res = obj;
  std::cout << "Test \"=\":\t\t";
  std::cout << ((res.real() == 1.3 && res.image() == 22.1) ? "OK" : "BAD") << std::endl;
}

void test12() {
  Complex obj1(1.22, 23.3);
  Complex obj2(12.3, -22.3), obj3 = obj1;
  std::cout << "Test \"==\":\t\t";
  std::cout << ((!(obj1 == obj2) && (obj3 == obj1)) ? "OK" : "BAD") << std::endl;
}

void test13() {
  ComplexPolar obj;
  std::cout << "Test \"Empty input\":\t";
  std::cout << ((!obj.Abs() && !obj.phi()) ? "OK" : "BAD") << std::endl;
}

void test14(){
  ComplexPolar obj(9, 3.14);
  std::cout << "Test \"Full input\":\t";
  std::cout << ((obj.Abs() == 9 && obj.phi() == 3.14) ? "OK" : "BAD") << std::endl;
}

void test15(){
  ComplexPolar obj(11.2, 233.2);
  ComplexPolar res(obj);
  std::cout << "Test \"Copy\":\t\t";
  std::cout << ((obj.Abs() == 11.2 && obj.phi() == 233.2) ? "OK" : "BAD") << std::endl;
}

void test16(){
  Complex obj(2.2, 23.1);
  ComplexPolar res(obj);
  std::cout << "Test \"Copy Dekart\":\t";
  std::cout << ((res.Abs() == std::sqrt(2.2 * 2.2 + 23.1 * 23.1) && res.phi() == ) ? "OK" : "BAD") << std::endl;
}

int main() {
  std::cout << "Complex class" << std::endl;
  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  test7();
  test8();
  test9();
  test10();
  test11();
  test12();
  std::cout << "ComplexPolar class:" << std::endl;
  test13();
  test14();
  test15();
  return 0;
}
