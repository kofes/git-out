#include <cmath>

namespace Number {
  enum ERR {
    NORM,
    DIV0,
  };

  static ERR ERROR;

  template <typename REAL>
  class Real {
  public:
    double EPS = 1e-5;
    REAL Re;

    Real () : Re(0) {}
    Real (REAL Re) : Re(Re) {}
/**/
    inline Real operator +(const Real &src) {
      return Real(Re + src.Re);
    }
    inline Real operator -(const Real &src) {
      return Real(Re - src.Re);
    }
    inline Real operator *(const Real &src) {
      return Real(Re * src.Re);
    }
    inline Real operator /(const Real &src) {
      if ((src.Re + EPS > 0)&&(src.Re - EPS < 0)) {
        Number::ERROR = Number::ERR::DIV0;
        return Real(0);
      }
      return Real(Re / src.Re);
    }
/**/
  inline Real &operator +=(const Real &src) {
    Re += src.Re;
    return *this;
  }
  inline Real &operator -=(const Real &src) {
    Re -= src.Re;
    return *this;
  }
  inline Real &operator *=(const Real &src) {
    Re *= src.Re;
    return *this;
  }
  inline Real &operator /=(const Real &src) {
    if ((src.Re + EPS > 0)&&(src.Re - EPS < 0)) {
      Number::ERROR = Number::ERR::DIV0;
      return *this;
    }
    Re /= src.Re;
    return *this;
  }
/**/
    inline virtual REAL Abs() {
      return ((Re < 0) ? -Re : Re);
    }
    inline Real operator ^(int pow) {
      return std::pow(Re, pow);
    }
  };
}
