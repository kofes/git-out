#include <iostream>

/*Abstract class - like interface at Java*/
class iOpenable {
public:
  virtual void Open  () = NULL;//<- имеея такой метод, класс называется абстрактным
  virtual void Close () = NULL;
};

class Door : iOpenable {
public:
  void Open   () {/**/}
  void Close  () {/**/}
  void Squeak () {/**/}
};

class Book : iOpenable {
public:
  void Open  () {/**/}
  void Close () {/**/}
  void Burn  () {/**/}
};

void F(iOpenable & op) {
  op.Open();
  /**/
  op.Close();
}

/*Реализовать класс комплексных чисел*/

int main(){
  iOpenable door = Door();
  iOpenable book = Book();

  F(door);
  F(book);

  return 0;
}
