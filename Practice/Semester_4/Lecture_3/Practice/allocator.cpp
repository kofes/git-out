#include <iostream>
#include <memory>

using namespace std;

struct base {
  base () {
    cout << "adawf" << endl;
  }
  ~base () {
    cout << "destr" << endl;
  }
};

int main () {
  {
    base *b = new base;
  }
  shared_ptr<int> iptr(new int(10));//Не нужно вызывать больше оператор delete
  unique_ptr<int> global(new int(123));//Уникальный - нельзя копировать
  weak_ptr<int> wptr = iptr;//Слабый указатель - не владеет указателем и имеет метод lock() <- преобразование в shared_ptr. Благодаря этому можно избавиться от циклической зависимостью, ибо weak_ptr не вызывает shared_ptr
  iptr = make_shared<int>(10);//вызывает конструктор shared_ptr с new...
  
  cout << "the end" << endl;
}
