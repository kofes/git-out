#include <iostream>

using namespace std;

class base {

};

volatile int a = 1;//Переменная может поменяться неочивидным способом. Важно, если при оптимизации не должны отбрасываться процессы связанные с этой переменной.

int main () {
	register int a = 1;//Рекомендация компилятору хранить переменную в регистрах.
	int *intptr = new int[100];
	base* baseptr = new base;
	
	intptr[10] = 123;
	cout << intptr[10] << endl;
	int *elem_ptr = intptr + 10;
	
	cout << *elem_ptr << endl;
	
	delete[] intptr;
	delete baseptr;
}
