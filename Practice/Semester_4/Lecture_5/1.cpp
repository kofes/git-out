//Templates
/**
* Полиморфизм
*   Статический - на этапе компиляции (перегрузка функций)
*   Динамический - на этапе выполнения (виртуальные методы)
*
*
* template <typename/class T> - разницы между typename и class никакой.
* void func(T arg) {...}
* ...
* func<int>(1); - инстанция шаблона
* func<char>('a');//Дольше компиляция, ибо каждый раз заново создается функция при компиляции... Плюс ошибки после компиляции...
* func(1); - неявное...
*/
#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
class Base {
private:
  T a, b, c;
public:
  Base (T a, T b, T c): a(a), b(b), c(c) {};
  Base () {};
  template<typename U>
  void foo (U a);
};

template<typename T>
template<typename U>
void Base<T>::foo(U a) {
  std::cout << a << std::endl;
}
//Специализация шаблона
template<typename T>
void print (T a) {
  std::cout << a << std::endl;
}

template<>
void print (bool a) {
  std::cout << "!!!" << std::endl;
}
//Для классов аналогично
// template <>
// class Base<bool> {...};
// Если нужно целый блок занести "в особый случай"
template <typename T>
class test{/*...*/};
template <typename T>
class test<std::vector<T> >{/*...*/};
//Чтобы несколько раз не инстанцировались функции и классы
// extern
// template class std::vector<int>;
// extern void print<int>();

// variadic templates
template<typename ... Args>
void out (Args ... args) {
  bar(args ...);
}
//
// Mixin - "примешивание" разных классов к текущему
template <typename ... Mixins>
class derived: public Mixins...{
/*...*/
};
//
template <typename T>
void print_out (T arg) {
  std::cout << arg << std::endl;
}
template <typename T, typename... Args>
void print_out (T arg1, Args... args) {
  std::cout << arg1 << " ";
  print_out(args...);
}
//
template <typename T, typename... Args>
void PRINT (T arg1, Args... args) {
  std::cout << arg1 << " ";
  PRINT(func(args)...);
  PRINT((args += 2, i++)...);
}
//int a[] = {args...}; - Можно!
//int q[] = {(std::cout << args < " ", 0)...}; - Можно!
//sizeof...Args - возвращает количество аргументов
//
// Tuple
// tuple<int, char, int, Obj> t;
template <class T>
struct tuple<T> {
  T value;
}
template <class T, class...Types>
struct tuple : public tuple<Types...> {
  T value;
}
//
//SFINAE - Substitution failure is not an error
// Значит, что подставляется в тот шаблон, где не будет ошибки...
class Val {
  static  int val;
};
class Value {
  static  int value;
}
//
//Метафункция
is_int_or_char<T>::value
//
template <typename T>
decltype(T::value) foo () {};
template <typename T>
decltype(T::val) foo () {};
//

int main(int argc, char const *argv[]) {
  //
  std::cout << std::max<int>({1, 2, 3, 4, 5 ,10 ,1 ,2, -1, 20, 1, 2}) << std::endl;
  //
  Base<int> p;
  print(true);
  print_out("!!!@", "@");
  return 0;
}
