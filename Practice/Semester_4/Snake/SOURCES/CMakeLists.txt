cmake_minimum_required(VERSION 3.0)

project(Classes)

set(CMAKE_CXX_STANDARD 11)

file(GLOB SOURCE_LIB "*.cpp")

add_library(Classes SHARED ${SOURCE_LIB})
