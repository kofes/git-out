#include "../HEADERS/Map.hpp"

void Map::display () {
  for (auto iter = food.begin(); iter != food.end(); ++iter)
    (*iter)->randomSet(*this);
  if (_buff != nullptr)
    for (int j = 0; j < _height; ++j) {
      for (int i = 0; i < _width; ++i) {
          if (_buff[i][j]->symbol() == Symbol::SNAKE_BLOCK)
              addch((char)_buff[i][j]->symbol() | A_BOLD | COLOR_PAIR(Color::SNAKE));
          else if (_buff[i][j]->symbol() == Symbol::FOOD)
              addch((char)_buff[i][j]->symbol() | A_BOLD | COLOR_PAIR(Color::APPLE));
          else
            addch((char)_buff[i][j]->symbol() | A_NORMAL);
        }
        addch('\n' | A_NORMAL);
    }
}

Map::Map (int width, int height) {
  if (width < 20) width = 20;
  if (height < 20) height = 20;

  _buff = new Actor**[width];
  for (int i = 0; i < width; ++i)
    _buff[i] = new Actor*[height];
  for (int i = 0; i < width; ++i)
    for (int j = 0; j < height; ++j)
      _buff[i][j] = new Floor(i, j);
  _width = width;
  _height = height;
};

Map::~Map () {
  if (_buff != nullptr) {
    for (int i = 0; i < _width; ++i)
      for (int j = 0; j < _height; ++j) {
        delete _buff[i][j];
        _buff[i][j] = nullptr;
      }
    for (int i = 0; i < _width; ++i) {
      delete[] _buff[i];
      _buff[i] = nullptr;
    }
    delete[] _buff;
    _buff = nullptr;
  }
  _width = _height = 0;
  food.clear();
};

void Map::clear () {
  for (int i = 0; i < _width; ++i)
    for (int j = 0; j < _height; ++j) {
      if (_buff[i][j]->symbol() != Symbol::SNAKE_BLOCK)
      delete _buff[i][j];
      _buff[i][j] = new Floor(i, j);
    }
  food.clear();
};

void Map::clean () {
  if (_buff != nullptr) {
    for (int i = 0; i < _width; ++i)
      for (int j = 0; j < _height; ++j) {
        if (_buff[i][j]->symbol() != Symbol::SNAKE_BLOCK)
        delete _buff[i][j];
        _buff[i][j] = nullptr;
      }
    for (int i = 0; i < _width; ++i) {
      delete[] _buff[i];
      _buff[i] = nullptr;
    }
    delete[] _buff;
    _buff = nullptr;
  }
  _width = _height = 0;
  food.clear();
};

bool Map::setChar (Actor* obj, bool PTR_TYPE) {
  if (obj == nullptr)
    return false;
  IntVector2 pos(obj->position());
  if (pos.x() > 0 && pos.y() > 0 && pos.x() < _width && pos.y() < _height && _buff[pos.x()][pos.y()]->symbol() == Symbol::FLOOR) {
      delete _buff[pos.x()][pos.y()];
      _buff[pos.x()][pos.y()] = obj;
      return true;
  }
  if (PTR_TYPE == _DEF_SHARED_PTR_)
    delete obj;
  return false;
};

bool Map::memChar (Actor* obj, bool PTR_TYPE) {
  if (obj == nullptr)
    return false;

  if (obj->symbol() == Symbol::FOOD) {
    food.push_front((Food*)obj);
    return true;
  }

  if (PTR_TYPE == _DEF_SHARED_PTR_)
    delete obj;

  return false;
};

bool Map::removeChar (Actor *obj) {
  IntVector2 pos(obj->position());
  if (obj != nullptr && pos.x() > 0 && pos.y() > 0 && pos.x() < _width && pos.y() < _height && _buff[pos.x()][pos.y()] == obj) {
      _buff[pos.x()][pos.y()] = new Floor(pos.x(), pos.y());
      return true;
    }
  return false;
};
