#include "../HEADERS/Snake.hpp"
#include "../HEADERS/Map.hpp"

bool Snake::_alive = _DEF_ALIVE_;

Snake::Snake (Map& map) {
  _alive = _DEF_ALIVE_;
  blocks.push_back(new Snake::block(map.width() / 2, map.height() / 2));
  blocks.push_back(new Snake::block(map.width() / 2 - 1, map.height() / 2));
  blocks.push_back(new Snake::block(map.width() / 2 - 2, map.height() / 2));
};

void Snake::move (Map& map, Target target) {
  int tar[4][2] = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
  auto head = blocks.begin();
  IntVector2 pos;

  pos = (*head)->position();

  if (pos.x() + tar[target][0] <= 0 || pos.y() + tar[target][1] <= 0) {
    _alive = _DEF_NOT_ALIVE_;
    return;
  }
  if (pos.x() + tar[target][0] >= map.width() || pos.y() + tar[target][1] >= map.height()) {
    _alive = _DEF_NOT_ALIVE_;
    return;
  }

  if ((map._buff[pos.x() + tar[target][0]][pos.y() + tar[target][1]])->symbol() == Symbol::SNAKE_BLOCK) {
    _alive = _DEF_NOT_ALIVE_;
    return;
  }

  if ((map._buff[pos.x() + tar[target][0]][pos.y() + tar[target][1]])->symbol() == Symbol::FOOD) {
    blocks.push_front(new Snake::block(pos.x() + tar[target][0], pos.y() + tar[target][1]));
    Food* food = (Food*)(map._buff[pos.x() + tar[target][0]][pos.y() + tar[target][1]]);
    map.removeChar(food);
    food->effect();
    map.setChar(*(blocks.begin()));
    return;
  }

  IntVector2 oldPos(blocks.back()->position());
  map.removeChar(blocks.back());
  blocks.pop_back();
  Snake::block* blk = new Snake::block(pos.x() + tar[target][0], pos.y() + tar[target][1]);
  blocks.push_front(blk);
  map.setChar(blk);
};
