#pragma once

#include <list>
#include "Actor.hpp"
#include "Snake.hpp"
#include "Food.hpp"
#include "Floor.hpp"
#include <ncurses.h>

#pragma pack(push, 1)
class Map {
public:
  Map (int width, int height);
  ~Map ();
  inline int width () {return _width;};
  inline int height () {return _height;};

  void clear ();
  void clean ();

  bool setChar (Actor *, bool PTR_TYPE = _DEF_WEAK_PTR_);
  bool memChar (Actor *, bool PTR_TYPE = _DEF_WEAK_PTR_);
  bool removeChar (Actor *);

  void display();

private:
  friend class Snake;
  std::list<Food*> food;
  int _width, _height;
  Actor ***_buff;
};
#pragma pack(pop)
