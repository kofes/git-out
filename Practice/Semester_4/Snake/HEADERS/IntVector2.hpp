#pragma once

#pragma pack(push, 1)
class IntVector2 {
public:
  IntVector2 (int x = 0, int y = 0) : _x(x), _y(y) {};
  IntVector2 (const IntVector2& src) {
    _x = src._x;
    _y = src._y;
  };
  ~IntVector2 () {_x = 0, _y = 0;};
  IntVector2 &operator= (const IntVector2& src) {
    _x = src._x;
    _y = src._y;
    return *this;
  };

  inline int x () {return _x;};
  inline int x (int x) {return (_x = x);};
  inline int y () {return _y;};
  inline int y (int y) {return (_y = y);};

  IntVector2 operator+ (const IntVector2& src) {
    _x += src._x;
    _y += src._y;
    return *this;
  };
  IntVector2 operator- (const IntVector2& src) {
    _x -= src._x;
    _y -= src._y;
    return *this;
  };
  IntVector2 operator* (long long src) {
    _x *= src;
    _y *= src;
    return *this;
  };
  IntVector2 operator/ (long long src) {
    if (src <= 0)
      return IntVector2();
    _x /= src;
    _y /= src;
    return *this;
  };

  bool operator== (const IntVector2& src) {
    return (_x == src._x)&&(_y == src._y);
  };
  bool operator!= (const IntVector2& src) {
    return (_x != src._x)||(_y != src._y);
  };
private:
  int _x, _y;
};
#pragma pack(pop)
