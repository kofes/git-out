#pragma once

#include "IntVector2.hpp"

/*Characters's symbols*/
enum class Symbol : char {
  FLOOR = '.',
  FOOD = '@',
  SNAKE_BLOCK = 'o',
};

/*TYPE_PTR (in Map)*/
#define _DEF_SHARED_PTR_      true
#define _DEF_WEAK_PTR_        false

/*COLOR's ids*/
enum class Color {
  SNAKE = 1,
  APPLE = 2,
};

#pragma pack(push, 1)
class Actor {
public:
  virtual Symbol symbol () = 0;
  IntVector2 position () {return _position;};
  IntVector2 position (IntVector2&& pos) {return (_position = pos);}
  virtual ~Actor () {};
protected:
  IntVector2 _position;
};
#pragma pack(pop)
