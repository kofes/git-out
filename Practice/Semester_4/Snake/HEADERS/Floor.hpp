#pragma once

#include "Actor.hpp"

#pragma pack(push, 1)
class Floor : public Actor {
public:
  Floor (int x = 0, int y = 0) {
    _position.x(x);
    _position.y(y);
  };
  Symbol symbol () {return Symbol::FLOOR;};
  virtual ~Floor () {};
};
#pragma pack(pop)
