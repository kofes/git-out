#include <ncurses.h>
#include <unistd.h>
#include <ctime>
#include <cstdlib>
#include "Map.hpp"
#include "Snake.hpp"
#include "Food.hpp"

int engine (int _width_, int _height_, int _count_food_ = 0) {
  std::srand(std::time(NULL));

  Map MAP(_width_, _height_);

  Snake *snake = new Snake(MAP);
  for (auto i = snake->head(); i != snake->endTail(); ++i)
    MAP.setChar(*i);

  if (_count_food_ > 0)
    for (size_t i = 0; i < _count_food_; ++i)
      MAP.memChar(new Food(_DEF_SHARED_PTR_));
  else
    MAP.memChar(new Food(_DEF_SHARED_PTR_));

  while (Snake::alive() == _DEF_ALIVE_) {
    clear();
    MAP.display();
    printw("HEIGHT: %d, WIDTH: %d, COUNT: %d", MAP.height(), MAP.width(), snake->countPoints());
    char sym = getch();
    Target turn;
    usleep(10 * 10000.0);
    if (sym == 'w' || sym == 'W')
      if (turn != Target::bottom)
        turn = Target::top;
    if (sym == 's' || sym == 'S')
      if (turn != Target::top)
        turn = Target::bottom;
    if (sym == 'd' || sym == 'D')
      if (turn != Target::left)
        turn = Target::right;
    if (sym == 'a' || sym == 'A')
      if (turn != Target::right)
        turn = Target::left;
    if (sym == 'q' || sym == 'Q') break;
    snake->move(MAP, turn);
  }
  clear();
  MAP.clean();
  return snake->countPoints();
};
