#pragma once

#include "Actor.hpp"
#include <cstdlib>

/*is object used*/
#define _DEF_EXIST_ true
#define _DEF_NOT_EXIST_ false

class Map;

#pragma pack(push, 1)
class Food : public Actor {
public:
  Food (int x = -1, int y = -1, int overClock = 3) : _overClock(overClock), _clock(0) {
    _position.x(x);
    _position.y(y);
    _exist = _DEF_EXIST_;
    if (x < 0 || y < 0)
      _exist = _DEF_NOT_EXIST_;
  };
  Food (IntVector2& src) {
    _position.x(src.x());
    _position.y(src.y());
    if (src.x() < 0 || src.y() < 0)
      _exist = _DEF_NOT_EXIST_;
  }
  bool exist () {return _exist;};
  Symbol symbol () {return Symbol::FOOD;};
  void randomSet (Map&);
  void effect () {
    _exist = _DEF_NOT_EXIST_;
    _clock = 0;
  };
  virtual ~Food () {};
private:
  bool _exist;
  int _clock, _overClock;
};
#pragma pack(pop)
