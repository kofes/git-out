#include "Engine.hpp"

/*Main menu*/
char menu (int _width_, int _height_) {
  for (unsigned int i = 0; i < _height_; ++i) {
    for (unsigned int j = 0; j < _width_; ++j) {
      if ((i % (_height_ - 1)) == 0 || (j % (_width_ - 1) == 0))
      addch('X' | A_BOLD);
      else
      addch(' ');
    }
    addch('\n');
  }
  mvwprintw(stdscr, _height_/4, (_width_ - 12) / 2, "SNAKE");
  mvwprintw(stdscr, _height_/4 + 2, (_width_ - 12) / 2, "Start");
  mvwprintw(stdscr, _height_/4 + 3, (_width_ - 12) / 2, "Apple orchard mod");
  mvwprintw(stdscr, _height_/4 + 4, (_width_ - 12) / 2, "Quit");
  char sym,
  option = 'S',
  pos = 0;
  mvwprintw(stdscr, _height_/4 + 2, (_width_ - 12) / 2 - 1, "!");
  mvwprintw(stdscr, _height_/4 + 3, (_width_ - 12) / 2 - 1, " ");
  do {
    sym = '\0';
    sym = getch();

    if (sym == 'w' || sym == 'W')
      pos = (pos > 0) ? --pos : 0;
    if (sym == 's' || sym == 'S')
      pos = (pos < 2) ? ++pos : 2;

    for (int i = 0; i < 3; ++i)
      mvwprintw(stdscr, _height_/4 + 2 + i, (_width_ - 12) / 2 - 1, " ");

    mvwprintw(stdscr, _height_/4 + 2 + pos, (_width_ - 12) / 2 - 1, "!");

    switch (pos) {
      case 0 : option = 'S'; break;
      case 1 : option = 'M'; break;
      case 2 : option = 'Q'; break;
    }
  } while (sym != '\n');

  return option;
};

void game (int _width_, int _height_) {
  //Init ncurses
  initscr();
  if (has_colors()) {
    start_color();
    init_pair((char)Color::SNAKE, COLOR_GREEN, COLOR_BLACK);
    init_pair((char)Color::APPLE, COLOR_RED, COLOR_BLACK);
  }
  clear();
  noecho();
  cbreak();
  keypad(stdscr, TRUE);
  curs_set(0);
  if (_width_ > getmaxx(stdscr)) _width_ = getmaxx(stdscr);
  if (_height_ > getmaxy(stdscr) - 1) _height_ = getmaxy(stdscr) - 1;
  if (_width_ < 20) _width_ = 20;
  if (_height_ < 20) _height_ = 20;

  char option = menu(_width_, _height_);
  if (option == 'Q') {
    endwin();
    return;
  }

  while (option != 'Q') {
    timeout(0);
    int countPoints = engine(_width_, _height_, (option == 'M') ? 100 : 1);
    timeout(-1);
    mvwprintw(stdscr, _height_ / 4, (_width_ - 10) / 2, "GAME OVER!");
    mvwprintw(stdscr, _height_ / 4 + 1, (_width_ - 9) / 2, "COUNT: %d", countPoints);
    getch();
    clear();
    option = menu(_width_, _height_);
  };
  endwin();
}
