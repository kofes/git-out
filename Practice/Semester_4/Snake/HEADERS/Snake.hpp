#pragma once

#include "Actor.hpp"
#include <list>

#define _DEF_ALIVE_     true
#define _DEF_NOT_ALIVE_ false

class Map;

enum Target {
  top    = 0,
  right  = 1,
  bottom = 2,
  left   = 3,
};

class Snake {
public:

class block : public Actor {
public:
  block (int x = 0, int y = 0) {
    _position.x(x);
    _position.y(y);
  }
  virtual Symbol symbol () {return Symbol::SNAKE_BLOCK;};
};
  Snake (Map& );
  inline std::list<Snake::block *>::iterator head () {
    return blocks.begin();
  };
  inline std::list<Snake::block *>::iterator endTail () {
    return blocks.end();
  }
  inline std::size_t countPoints () {
    return blocks.size();
  };
  void move (Map& map, Target target);
  ~Snake () {
    for (auto iter = blocks.begin(); iter != blocks.end(); ++iter) {
      delete *iter;
      *iter = nullptr;
    }
    blocks.clear();
    _alive = _DEF_NOT_ALIVE_;
  };
  static bool alive () {return _alive;}
private:
  static bool _alive;
  std::list<Snake::block*> blocks;
};
