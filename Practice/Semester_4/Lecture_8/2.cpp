#include <iostream>
#include <functional>

using namespace std;
using namespace std::placeholders;

void groovy () {
  cout << "Groovy!" << endl;
};

struct Notifier {
  void subscribe (function<void ()> func) {
    callbacks.push_back(func);
  };

  void notify () {
    for (auto callback : callbacks) {
        callback();
      }
  };

  vector<funciton<void ()> > callbacks;
};

/**
* = - всё по значению
* & - всё по ссылке
* [используемые_значения] (аргументы_функции) -> возвращаемое_значение
*/

int main () {
  int version = 333;
  function<void ()> std_func2 = [version] () mutable -> void {
    version = 3;
    cout << "Lamda function in c++11 <=> Half-Life " << version << " approved!" << endl;
  };
  function<void ()> std_func = [=] () -> void {
    cout << "Lamda function in c++11 <=> Half-Life " << version << " approved!" << endl;
  };
  std_func();
  std_func2();
  Notifier notifier;
  Say what;
  notifier.subscribe(bind(&Say::what, &what));
  notifier.subscribe(&groovy);

  norifier.notify();
}
