#include <iostream>
#include <vector>

class Base {
public:
  Base (int value = 0) : value(value) {};
  void out () {std::cout << value << std::endl;};
private:
  int value;
};

void swap (std::vector<int>& a, std::vector<int>& b) {
  std::vector<int> tmp = std::move(a);
  a = std::move(b);
  b = std::move(tmp);
}

void foo (int& a) {
  std::cout << "l-value" << std::endl;
  std::cout << a << std::endl;
};

void foo (const int& a) {
  std::cout << "const-value" << std::endl;
  std::cout << a << std::endl;
};

void foo (int&& a) {
  std::cout << "r-value" << std::endl;
  std::cout << a << std::endl;
};

int main () {
  // int& lval = Base(); //<- Error!
  // Base&& rval = Base(); //<- OK!
  int a = 10;
  const int b = 2;
  // ++a = 1; <- l-value(возвращает ссылку на конечный объект)
  // a++ = 2; <- r-value(возвращает ссылку на временный объект(предыдущий))
  foo(1);
  foo(a+1);
  foo(a);
  foo(b);
};

/**
* has identity: xvalue, lvalue
* can be 'moved from': xvalue
*
* gvalue -> xvalue, lvalue
* rvalue -> prvalue, xvalue
*/
