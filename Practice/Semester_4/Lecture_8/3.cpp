/**
* Процессы - изолированны друг от друга,
* Потоки - общее всё.
*/
#include <thread>
#include <iostream>
#include <deque>

using namespace std;

deque<string> container;

void groovy (int count) {
  for (int i = 0; i < count; ++i)
    cout << "Groovy!" << endl;
};

void what (int n) {
  for (int i = 0; i < n; ++i) {
      container.push_back("What?");
  }
};

void say () {
  if (!container.empty()) {
    cout << container.front() << endl;
    container.pop_back();
  }
};

int main () {
  // thread th(groovy, 1);
  // th.join();
  thread what_s(what, 1000);
  vector<thread> speakers;
  for (int i = 0; i < 4; i++)
  return 0;
};
