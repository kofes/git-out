#include <iostream>
#include <vector>
#include <functional>
#include <string>

std::function<void ()> std_func;



using namespace std;
using namespace std::placeholders;

void groovy () {
  cout << "Groovy!" << endl;
};

struct Welcome {
  void f () {
    cout << "Welcome" << endl;
  };

  int a = 0;
};

template <typename Obj>
struct Callback {
  typedef void (Obj::*FuncPtr)();

  Callback (Obj* inst, FuncPtr func): inst(inst), func(func) {};

  void operator() () {
    (inst->*func)();
  }

  Obj* inst;
  FuncPtr func;
};

/*alias*/
typedef void (*func_ptr_t)();

/*
struct Notifier {
  void subscribe (func_ptr_t func, Welcome* inst) {
    callbacks.push_back(func);
    instances.push_back(inst);
  };

  void notify () {
    for (int i = 0; i < callbacks.size(); ++i) {
      (instances[i]->*callbacks[i])();
    }
  };

  vector<func_ptr_t> callbacks;
  vector<Welcome*> instances;
};
*/

/*
struct Notifier2 {
  void subscribe (Callback callback) {
    callbacks.push_back(callback);
  };

  void notify () {
    for (int i = 0; i < callbacks.size(); ++i) {
      (instances[i]->*callbacks[i])();
    }
  };

  vector<Callback> callbacks;
};
*/

void f1 (const string& name, int age, int index) {
  cout << "Name: " << name << '\n'
       << "Age: " << age << '\n'
       << "Index: " << index << endl;
};

int main () {
  Welcome welcome;
  // Notifier notifier;
  // notifier.subscribe(groovy);
  // notifier.subscribe(groovy);
  // notifier.subscribe(&Welcome::f, &welcome);
  // notifier.notify();

  // Callback<Welcome> callback(&welcome, &Welcome::f);
  // callback();

  // std_func = std::function<void ()>(groovy);
  // std_func = std::bind(&Welcome::f, &welcome);
  // std_func = bind(f1, "Pol", 20, 2231);
  // function<void (const string&, int)> my_func1 = bind(f1, _1, 20, _2);
  // my_func1("Tom", 313);
  // std_func();
  return 0;
};
