#include "Server/Server.hpp"

using namespace snake;

int main () {
  int ServerPort = 25225;
  int width = 70, height = 20;
  std::cout << "Local host: " << sf::IpAddress::LocalHost << std::endl;
  Server server(ServerPort);
  server.Run(width, height, 20);
  return 0;
}
