#include "Map.hpp"

Map::Map (int width, int height) {
  if (width < 20) width = 20;
  if (height < 20) height = 20;
  map = new char*[width];
  for (int i = 0; i < width; ++i)
    map[i] = new char[height];
  for (int i = 0; i < width; ++i)
    for (int j = 0; j < height; ++j)
      map[i][j] = (char)CharacterId::Floor;
  _width_ = width;
  _height_ = height;
};

void Map::clean () {
  if (map == nullptr) return;
  for (int i = 0; i < _width_; ++i)
    delete[] map[i];
  delete[] map;
  map = nullptr;
  _width_ = _height_ = 0;
  food.clear();
};

void Map::clear () {
  if (map == nullptr) return;
  for (int i = 0; i < _width_; ++i)
    for (int j = 0; j < _height_; ++j)
      map[i][j] = (char)CharacterId::Floor;
  food.clear();
};

void Map::resize (int width, int height) {
  clean();
  if (width < 20) width = 20;
  if (height < 20) height = 20;
  map = new char*[width];
  for (int i = 0; i < width; ++i)
    map[i] = new char[height];
  for (int i = 0; i < width; ++i)
    for (int j = 0; j < height; ++j)
      map[i][j] = (char)CharacterId::Floor;
  _width_ = width;
  _height_ = height;
};

bool Map::setChar (const sf::Vector2i& pos, CharacterId id) {
  if (map == nullptr || pos.x > _width_ || pos.x < 0 || pos.y > _height_ || pos.y < 0)
    return false;
  if (map[pos.x][pos.y] == (char)CharacterId::Floor) {
    map[pos.x][pos.y] = (char)id;
    return true;
  }
  return false;
};

bool Map::resetChar (const sf::Vector2i& pos, CharacterId currentId) {
  if (map == nullptr || pos.x > _width_ || pos.x < 0 || pos.y > _height_ || pos.y < 0 || map[pos.x][pos.y] != (char)currentId)
    return false;
  map[pos.x][pos.y] = (char)CharacterId::Floor;
  return true;
};

void Map::addFood (sf::Vector2i pos) {
  food.push_back(Food(pos.x,pos.y));
  setChar(pos, CharacterId::Food);
};

void Map::removeFood (sf::Vector2i pos) {
  food.remove(Food(pos.x, pos.y));
  food.push_back(Food());
  resetChar(pos, CharacterId::Food);
};
