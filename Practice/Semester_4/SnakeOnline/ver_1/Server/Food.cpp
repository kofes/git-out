#include "Food.hpp"
#include "Map.hpp"

void Food::genFood (Map& map) {
  if (exist) return;
  for (int i = 0; i < 1000; ++i) {
    pos.x = std::rand() % map.width();
    pos.y = std::rand() % map.height();
    if (map[pos.x][pos.y] == (char)CharacterId::Floor) {
      exist = map.setChar(pos, CharacterId::Food);
      return;
    }
  }
};
