#include "Server.hpp"

snake::Server::Server (unsigned short port) {
  socket.listen(port);
  socket.setBlocking(false);
};

void snake::Server::ReceivePackets () {
  for (auto itUser = users.begin(); itUser != users.end(); ++itUser) {
    sf::Packet inPacket;
    sf::Socket::Status status = itUser->first->receive(inPacket);
    if (status == sf::Socket::Status::Done) {
      sf::Int8 type;
      inPacket >> type;
      std::string name;
      sf::Packet outPacket;
      switch ((TypePacket)type) {
        case TypePacket::Init :
          inPacket >> name;
          itUser->second.nickname = name;
          itUser->second.id       = colors.get();
          itUser->second.snake.reset(map);
          itUser->second.target   = Target::top;
          std::cout << name << " connected." << std::endl;
          outPacket << (sf::Int8)TypePacket::Init << map.width() << map.height();
          itUser->first->send(outPacket);
        break;
        case TypePacket::Update :
          if (!itUser->second.snake.isAlive()) continue;
          std::cout << itUser->second.nickname << " update received." << std::endl;
          sf::Int8 tar;
          inPacket >> tar;
          itUser->second.target = (Target)tar;
        break;
      }
    } else if (status == sf::Socket::Status::Disconnected) {
      std::cout << itUser->second.nickname << " disconnected." << std::endl;
      colors.off(itUser->second.id);
      itUser->second.snake.transformToFood(map);
      auto sock = itUser->first;
      itUser = users.erase(itUser);
      delete sock;
      if (itUser == users.end()) return;
    }
  }
};

void snake::Server::SendPackets() {
  for (auto iter = users.begin(); iter != users.end(); ++iter) {
    sf::Packet packet;
    std::cout << "sending packet to " << iter->second.nickname << ": ";
    packet << (sf::Int8)TypePacket::Update;
    if (!iter->second.snake.isAlive()) {
      packet << (sf::Uint32)0;
      iter->first->send(packet);
      std::cout << "DEAD." << std::endl;
      colors.off(iter->second.id);
      auto sock = iter->first;
      iter = users.erase(iter);
      delete sock;
      if (iter == users.end()) return;
      continue;
    }
    /*Users*/
    sf::Uint32 size = users.size();
    for (auto i = users.begin(); i != users.end(); ++i)
      if (!i->second.snake.isAlive())
        --size;
    packet << size;
    for (auto i = users.begin(); i != users.end(); ++i) {
      if (!i->second.snake.isAlive()) continue;
      packet << i->second.nickname;
      packet << (sf::Int8)(i->second.id);
      packet << (int)(i->second.snake.size());
      // packet << (sf::Uint32)(i->second.snake.size());
      for (auto itBlock = i->second.snake.begin(); itBlock != i->second.snake.end(); ++itBlock) {
        packet << (sf::Uint16)((*itBlock).pos.x);
        packet << (sf::Uint16)((*itBlock).pos.y);
      }
    }
    /*Food*/
    packet << (sf::Uint32)(map.countFood());
    for (auto i = map.beginFood(); i != map.endFood(); ++i) {
      if (!i->isExist()) continue;
      packet << (*i).position().x;
      packet << (*i).position().y;
    }
    iter->first->send(packet);
    std::cout << "OK." << std::endl;
  }
};

void snake::Server::Run (int width, int height, int maxFood) {
  map.resize(width, height);
  users.clear();
  for (auto i = 0; i < maxFood; ++i)
    map.addFood(sf::Vector2i(-1, -1));
  for (auto iter = map.beginFood(); iter != map.endFood(); ++iter)
    iter->genFood(map);
  sf::Thread update([&]() {
    while (true) {
      buffMutex.lock();
        for (auto iter = users.begin(); iter != users.end(); ++iter) {
          if (!iter->second.snake.isAlive()) continue;
          iter->second.snake.move(map, iter->second.target);
        }
        for (auto iter = map.beginFood(); iter != map.endFood(); ++iter) {
          iter->genFood(map);
        }
      buffMutex.unlock();
      sf::sleep(sf::milliseconds(MIN_MILLISECONDS * 0.8));
    }
  });
  update.launch();
  while (true) {
    buffMutex.lock();
      SendPackets();
    buffMutex.unlock();
    sf::TcpSocket* client = new sf::TcpSocket();
    client->setBlocking(false);
    if (socket.accept(*client) == sf::Socket::Done) {
      if (users.find(client) == users.end())
        users.insert(std::make_pair(client, ClientOption()));
      else delete client;
    } else delete client;

    ReceivePackets();

    sf::sleep(sf::milliseconds(MIN_MILLISECONDS/10));
  }
};
