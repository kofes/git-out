#pragma once

#include "Resources.hpp"
class Map;

#pragma pack(push, 1)
class Food {
public:
  Food (int x = -1, int y = -1) {
    pos.x = x;
    pos.y = y;
    exist = true;
    if (x < 0 || y < 0)
      exist = false;
  };
  bool operator== (const Food& food) {
    if (pos.x == food.pos.x && pos.y == food.pos.y)
      return true;
    return false;
  };
  void genFood (Map&);
  inline const bool isExist () const {return exist;};
  inline const sf::Vector2i position() const {return pos;};
private:
  bool exist;
  sf::Vector2i pos;
};
#pragma pack(pop)
