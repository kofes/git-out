#pragma once

#include <atomic>
#include <SFML/System.hpp>
#pragma pack(push, 1)
class Mutex {
public:
  void lock () {
    while (flag.test_and_set()) {
      sf::sleep(sf::milliseconds(10));
  }
  };
  void unlock () {flag.clear();};
private:
  std::atomic_flag flag = ATOMIC_FLAG_INIT;
};
#pragma pack(pop)

static Mutex controlMutex;
static Mutex buffMutex;
