#pragma once

#include <string>
#include <SFML/System/Vector2.hpp>
#include <SFML/Network.hpp>

static int MIN_MILLISECONDS = 150;

enum class CharacterId : char {
  Floor =  '.',
  Food  =  '@',
  Snake =  'S',
};

enum class SnakeId : char {
  GreenSnake   = '1',
  BlueSnake    = '2',
  YellowSnake  = '3',
  MagentaSnake = '4',
  CyanSnake    = '5',
  Count        = 6,
};

enum class TypePacket : char {
  Init = 0,//Client just joined/Server sent mapOptions
  Update = 1,//Client just move/Server sent users, food positions
};

enum class Target: char {
  top    = 0,
  right  = 1,
  bottom = 2,
  left   = 3,
};

std::string snakeColor (SnakeId id);

class SnakeColors {
public:
  SnakeColors () {};
  inline SnakeId get () {
    for (auto i = 0; i < (char)SnakeId::Count; ++i)
      if (!color[i]) {
        color[i] = true;
        return (SnakeId)(i + (char)SnakeId::GreenSnake);
      }
  };
  inline void off (SnakeId id) {
    color[(char)((char)id - (char)SnakeId::GreenSnake)] = false;
  };
  inline void clear () {
    for (auto i = 0; i < (char)SnakeId::Count; ++i)
      color[i] = false;
  }
private:
  bool color[(char)SnakeId::Count];
};
