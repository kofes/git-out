#include "Snake.hpp"

void Snake::transformToFood (Map& map) {
  int distance = 2, count = 0;
  for (auto block : blocks) {
    map.resetChar(block.pos, CharacterId::Snake);
    if (count) --count;
    else {
      count = distance;
      map.addFood(block.pos);
    }
  }
  alive = false;
  blocks.clear();
};

void Snake::reset (Map& map) {
  alive = true;
  for (auto block: blocks)
    map.resetChar(block.pos, CharacterId::Snake);
  blocks.clear();

  while (true) {
    int x, y;
    x = 5 + std::rand() % (map.width() - 10);
    y = 5 + std::rand() % (map.height() - 10);
    if (map[x][y] == (char)CharacterId::Floor &&
        map[x+1][y] == (char)CharacterId::Floor &&
        map[x+2][y] == (char)CharacterId::Floor) {
          blocks.push_back(block(x,y));
          map.setChar(blocks.back().pos, CharacterId::Snake);
          blocks.push_back(block(x+1,y));
          map.setChar(blocks.back().pos, CharacterId::Snake);
          blocks.push_back(block(x+2,y));
          map.setChar(blocks.back().pos, CharacterId::Snake);
          break;
        }
  }
};

void Snake::move (Map& map, Target target) {
  int tar[4][2] = {{ 0,-1},
                   { 1, 0},
                   { 0, 1},
                   {-1, 0}};
  sf::Vector2i pos = blocks.begin()->pos;
  pos.x += tar[(char)target][0];
  pos.y += tar[(char)target][1];
  //Если выходим за пределы карты, то змея умирет, при этом нужно сделать замену змейки на еду.
  if (pos.x < 0 || pos.y < 0 || pos.x >= map.width() || pos.y >= map.height()) {
    this->transformToFood(map);
    return;
  }
  //Если в новом поле еда, то заменяем её на блок змеи, при этом нужно сделать проверку в классе food (что не съедена).
  if (map[pos.x][pos.y] == (char)CharacterId::Food) {
    blocks.push_front(block(pos.x, pos.y));
    map.removeFood(pos);
    map.setChar(pos, CharacterId::Snake);
    return;
  }
  //Если на своем пути встречаем что-то отличное от пола, то змея умерает, при этом нужно сделать замену змейки на еду.
  if (map[pos.x][pos.y] == (char)CharacterId::Snake) {
    this->transformToFood(map);
    return;
  }
  //Если на своем пути встречаем пол, то переносим блок с хвоста змеи на новое место в качестве головы.
  sf::Vector2i oldPos = blocks.back().pos;
  map.resetChar(oldPos, CharacterId::Snake);
  blocks.pop_back();
  blocks.push_front(block(pos.x, pos.y));
  map.setChar(pos, CharacterId::Snake);
};
