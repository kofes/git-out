#pragma once

#include <SFML/System/Vector2.hpp>
#include <list>
#include "Resources.hpp"
#include "Map.hpp"
#include <cstdlib>
#include <ctime>

#pragma pack(push, 1)
class Snake {
public:
//Функция замены змеи на еду (не проверяет "жива" ли змея или нет - "убивает" её).
  void transformToFood (Map& map);
  struct block {
    block (int x, int y) {pos.x = x; pos.y = y;};
    sf::Vector2i pos;
  };
  Snake () : alive(false) {std::srand(std::time(NULL));};
  ~Snake () {blocks.clear(); alive = false;};
  void reset (Map& map);
  inline std::list<Snake::block>::const_iterator begin () {return blocks.cbegin();};
  inline std::list<Snake::block>::const_iterator end () {return blocks.cend();};
  inline std::list<block>::size_type size() {return blocks.size();};
  void move (Map& map, Target target);
  inline bool isAlive () {return alive;};
private:
  std::list<block> blocks;
  bool alive;
};
#pragma pack(pop)
