#pragma once

#include <list>
#include "Resources.hpp"
#include "Food.hpp"

#pragma pack(push, 1)
class Map {
public:
  Map (int width = 20, int height = 20);
  ~Map () {clean();};
  void clean ();
  void clear ();
  void resize (int width, int height);
  inline const int width () const {return _width_;};
  inline const int height () const {return _height_;};
  inline const char* operator[] (int index) const {
    if (index < 0 || index > _width_)
      return nullptr;
    return map[index];
  };
  bool setChar (const sf::Vector2i& pos, CharacterId id);
  bool resetChar (const sf::Vector2i& pos, CharacterId currentId);
  void addFood (sf::Vector2i pos);
  void removeFood (sf::Vector2i pos);
  inline std::list<Food>::iterator beginFood () {
    return food.begin();
  };
  inline std::list<Food>::iterator endFood () {
    return food.end();
  };
  inline std::list<Food>::size_type countFood () {
    std::list<Food>::size_type count = 0;
    for (auto food : food)
      if (food.isExist()) ++count;
    return count;
  };
private:
  char** map;
  std::list<Food> food;
  int _width_, _height_;
};
#pragma pack(pop)
