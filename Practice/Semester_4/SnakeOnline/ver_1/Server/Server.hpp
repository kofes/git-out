#pragma once

#include <SFML/Network.hpp>
#include <thread>
#include <iostream>
#include "Resources.hpp"
#include "Map.hpp"
#include "Snake.hpp"
#include "Food.hpp"
#include "Mutex.hpp"
#include <unordered_map>
namespace snake {
#pragma pack(push, 1)
class Server {
public:
  Server (unsigned short port);
  void Run (int width, int height, int maxFood) ;
  void ReceivePackets ();
  void SendPackets ();
private:
  struct ClientOption {
    ClientOption () {};
    ClientOption (const std::string& nickname, int score, SnakeId id) : nickname(nickname), id(id) {};
    std::string nickname;
    SnakeId id;
    Snake snake;
    Target target;
  };
  sf::TcpListener socket;
  Map map;
  SnakeColors colors;
  std::unordered_map<sf::TcpSocket*, ClientOption> users;
};
#pragma pack(pop)
};
