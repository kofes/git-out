#include "Client.hpp"
//-------------------------------MapOption-------------------------------
snake::Client::MapOption::~MapOption () {
  if (buff == nullptr) return;
  for (int i = 0; i < width; ++i)
    delete[] buff[i];
  delete[] buff;
  buff = nullptr;
};

void snake::Client::MapOption::resize (int w, int h) {
  if (buff != nullptr) {
    for (int i = 0; i < width; ++i)
      delete[] buff[i];
    delete[] buff;
  }
  width = w; height = h;
  buff = new char*[width];
  for (int i = 0; i < width; ++i)
    buff[i] = new char[height];
};

void snake::Client::MapOption::clear () {
  if (buff == nullptr) return;
  for (int i = 0; i < width; ++i)
    for (int j = 0; j < height; ++j)
      buff[i][j] = (char)CharacterId::Floor;
};
//--------------------------------Client---------------------------------
sf::Socket::Status snake::Client::Connect (const sf::IpAddress& IP, unsigned short port) {
  sf::Socket::Status status;
  status = socket.connect(IP, port);
  socket.setBlocking(false);
  return status;
};

inline void snake::Client::Disconnect () {
  socket.disconnect();
};

sf::Socket::Status snake::Client::SendUpdate (TypePacket type, char target) {
  sf::Socket::Status status;
  sf::Packet packet;
  packet << (sf::Int8)type;
  packet << (sf::Int8)target;
  status = socket.send(packet);
  return status;
};

sf::Socket::Status snake::Client::SendInit (TypePacket type, const std::string& nickname) {
  sf::Socket::Status status;
  sf::Packet packet;
  packet << (sf::Int8)type << nickname.c_str();
  status = socket.send(packet);
  return status;
};

sf::Socket::Status snake::Client::ReceivePacket () {
  sf::Socket::Status status;
  sf::Packet packet;
  status = socket.receive(packet);
  if (status != sf::Socket::Done) return status;
  sf::Int8 type;
  packet >> type;
  switch ((TypePacket)type) {
    case TypePacket::Init :
      packet >> map.width;
      packet >> map.height;
      map.resize(map.width, map.height);
    break;
    case TypePacket::Update :
      sf::Uint32 count;
      packet >> count;
      //
      if (!count) {Disconnect(); return sf::Socket::Disconnected;}
      //
      users.clear(); map.clear();
      /*Users*/
      for (auto i = 0; i < count; ++i) {
        //nickname, id, score//
        std::string nickname;
        packet >> nickname;
        sf::Int8 id;
        packet >> id;
        int score;
        packet >> score;
        users.push_back(SnakeOption(nickname, (SnakeId)id, score));
        for (auto i = 0; i < score; ++i) {
          sf::Uint16 x, y;
          packet >> x >> y;
          map.buff[x][y] = id;
        }
      }
      /*Food*/
      packet >> count;
      for (auto i = 0; i < count; ++i) {
        sf::Vector2i pos;
        packet >> pos.x >> pos.y;
        map.buff[pos.x][pos.y] = (char)CharacterId::Food;
      }
    break;
  }
  return status;
};

void snake::Client::ConsoleDisplay () {
  clear();
  for (int j = 0; j < map.height; ++j) {
    for (int i = 0; i < map.width; ++i)
        addch(map.buff[i][j] | A_BOLD | COLOR_PAIR(map.buff[i][j]));
    addch('\n' | A_NORMAL);
  }
  for (auto user : users)
    printw("Nickname: %s| Score: %d| id: %c.\n", user.nickname.c_str(), user.score, user.id);
};

void snake::Client::Run (const sf::IpAddress& IP, unsigned short port, const std::string& nickname) {
  std::queue<char> keys;
  keys.push((char)Target::top);
  sf::Socket::Status status;

  status = Connect(IP, port);
  if (status != sf::Socket::Done) {
    std::cout << "Can't connect to server" << std::endl;
    return;
  }

  status = SendInit(TypePacket::Init, nickname);
  if (status != sf::Socket::Done) {
    std::cout << "Can't send packets";
    return;
  }
  //Отрисовка карты
  while (status != sf::Socket::Done) {
    status = ReceivePacket();
    if (status == sf::Socket::Disconnected) {
      std::cout << "Can't receive packets";
      return;
    }
  }
  initscr();
  if (has_colors()) {
    start_color();
    init_pair((char)SnakeId::GreenSnake, COLOR_GREEN, COLOR_BLACK);
    init_pair((char)SnakeId::BlueSnake, COLOR_BLUE, COLOR_BLACK);
    init_pair((char)SnakeId::YellowSnake, COLOR_YELLOW, COLOR_BLACK);
    init_pair((char)SnakeId::MagentaSnake, COLOR_MAGENTA, COLOR_BLACK);
    init_pair((char)SnakeId::CyanSnake, COLOR_CYAN, COLOR_BLACK);
    init_pair((char)CharacterId::Food, COLOR_RED, COLOR_BLACK);
  }
  clear();
  cbreak();
  curs_set(0);
  noecho();
  keypad(stdscr, TRUE);
  timeout(0);
  sf::Thread controller ([&]() {
    while (status != sf::Socket::Status::Disconnected) {
      //CONTROLLER
      char sym = getch();
      if (sym == 'w' || sym == 'W')
      if (keys.back() != (char)Target::bottom)
      keys.push((char)Target::top);
      if (sym == 's' || sym == 'S')
      if (keys.back() != (char)Target::top)
      keys.push((char)Target::bottom);
      if (sym == 'd' || sym == 'D')
      if (keys.back() != (char)Target::left)
      keys.push((char)Target::right);
      if (sym == 'a' || sym == 'A')
      if (keys.back() != (char)Target::right)
      keys.push((char)Target::left);
      if (sym == 'q' || sym == 'Q') {
        keys.push('Q');
        break;
      }
      //
    }
  });
  controller.launch();

  char key;
  while (status != sf::Socket::Status::Disconnected && keys.back() != 'Q') {
    key = (char)keys.back();
    status = SendUpdate(TypePacket::Update, key);
    if (keys.size() > 1) keys.pop();
    while (key == keys.back() && keys.size() > 1) keys.pop();
    sf::sleep(sf::milliseconds(MIN_MILLISECONDS/10));
    status = ReceivePacket();
    if (status == sf::Socket::Status::Done)
      ConsoleDisplay();
  }
  int score = 0;
  for (auto iter = users.begin(); iter != users.end(); ++iter)
    if (iter->nickname == nickname) {
      score = iter->score;
      break;
    }
  clear();
  timeout(-1);
  mvwprintw(stdscr, map.height / 4, (map.width - 10) / 2, "GAME OVER!");
  mvwprintw(stdscr, map.height / 4 + 1, (map.width - 9) / 2, "COUNT: %d", score);
  getch();
  endwin();
}
