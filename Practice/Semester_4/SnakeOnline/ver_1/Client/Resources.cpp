#include "Resources.hpp"

std::string snakeColor (SnakeId id) {
  switch (id) {
    case SnakeId::GreenSnake  : return "Green";
    case SnakeId::BlueSnake   : return "Blue";
    case SnakeId::YellowSnake : return "Yellow";
    case SnakeId::MagentaSnake: return "Magenta";
    case SnakeId::CyanSnake   : return "Cyan";
  }
};
