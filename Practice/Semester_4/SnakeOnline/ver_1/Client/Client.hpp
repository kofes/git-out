#pragma once

#include <SFML/Graphics.hpp>
#include <ncurses.h>
#include <list>
#include <iostream>
#include <queue>
#include <thread>
#include "Resources.hpp"
#include "Mutex.hpp"

namespace snake {
#pragma pack(push, 1)
class Client {
public:
  Client () : score(0) {};
  void Disconnect ();
  sf::Socket::Status Connect (const sf::IpAddress& IP, unsigned short port);
  sf::Socket::Status ReceivePacket ();
  sf::Socket::Status SendUpdate (TypePacket type, char target);
  sf::Socket::Status SendInit (TypePacket type, const std::string& nickname);
  void ConsoleDisplay ();
  void Run (const sf::IpAddress& IP, unsigned short port, const std::string& nickname);
  struct SnakeOption {
    SnakeOption(const std::string& nickname, const SnakeId id, int score) : nickname(nickname), id(id), score(score) {};
    std::string nickname;
    SnakeId id;
    int score;
  };
  struct MapOption {
    MapOption () : buff(nullptr), width(0), height(0) {};
    ~MapOption ();
    void resize (int w, int h);
    void clear ();
    int width, height;
    char** buff;
  };
private:
  sf::TcpSocket socket;
  MapOption map;
  std::list<SnakeOption> users;
  SnakeId id;
  int score;
  sf::Socket::Status status;
};
#pragma pack(pop)
};
