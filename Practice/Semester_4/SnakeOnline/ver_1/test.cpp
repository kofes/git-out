#include "Resources.hpp"
#include "Map.hpp"
#include "Snake.hpp"
#include <iostream>
//Controller added
#include <SFML/System.hpp>
#include <queue>
#include <thread>
#include "Mutex.hpp"

#define BAD {std::cout << "BAD!" << std::endl; return;}
#define OK {std::cout << "OK!" << std::endl; return;}

namespace map {
//init
void test1 () {
  std::cout << "Test init:\t";
  Map map1, map2(5,7), map3(25, 40);
  if (map1.width() != 20 || map1.height() != 20) BAD;
  if (map2.width() != 20 || map2.height() != 20) BAD;
  if (map3.width() != 25 || map3.height() != 40) BAD;
  OK;
}
//set char
void test2 () {
  std::cout << "Test setChar:\t";
  Map map;
  for (int i = 0; i < map.height(); ++i)
    map.setChar(sf::Vector2i(i, i), CharacterId::Snake);
  for (int i = 0; i < map.width(); ++i)
    for (int j = 0; j < map.height(); ++j)
      if (i != j && map[i][j] != (char)CharacterId::Floor || i == j && map[i][j] != (char)CharacterId::Snake) BAD;
  OK;
}
//reset char
void test3 () {
  std::cout << "Test resetChar:\t";
  Map map;
  for (int i = 0; i < map.width(); ++i)
    for (int j = 0; j < map.height(); ++j)
      map.setChar(sf::Vector2i(i, j), CharacterId::Snake);
  for (int i = 0; i < map.height(); ++i)
    map.resetChar(sf::Vector2i(i, i), CharacterId::Snake);
  for (int i = 0; i < map.width(); ++i)
    for (int j = 0; j < map.height(); ++j)
      if (i != j && map[i][j] != (char)CharacterId::Snake || i == j && map[i][j] != (char)CharacterId::Floor) BAD;
  OK;
}
//add food
void test4 () {
  std::cout << "Test addFood:\t";
  Map map;
  for (int i = 0; i < 100; ++i)
    map.addFood(sf::Vector2i(std::rand() % map.width(), std::rand() % map.height()));
  if (map.countFood() != 100) BAD;
  for (auto iter = map.beginFood(); iter != map.endFood(); ++iter)
    if (map[iter->position().x][iter->position().y] != (char)CharacterId::Food) BAD;
  OK;
}
//remove food
void test5 () {
  std::cout << "Test removeFood:";
  Map map;
  for (int i = 0; i < map.width(); ++i)
    map.addFood(sf::Vector2i(i, i));
  if (map.countFood() != map.width()) BAD;
  for (int i = 0; i < map.width(); ++i) {
    if (map[i][i] != (char)CharacterId::Food) BAD;
    map.removeFood(sf::Vector2i(i, i));
  }
  for (int i = 0; i < map.width(); ++i)
    if (map[i][i] != (char)CharacterId::Floor) BAD;
  OK;
}
//clean
void test6 () {
  std::cout << "Test clean:\t";
  Map map;
  for (int i = 0; i < 100; ++i)
    map.addFood(sf::Vector2i(std::rand() % map.width(), std::rand() % map.height()));
  map.clean();
  if (map.countFood()) BAD;
  if (map.width() || map.height()) BAD;
  OK;
}
//clear
void test7 () {
  std::cout << "Test clear:\t";
  Map map;
  for (int i = 0; i < 100; ++i)
    map.addFood(sf::Vector2i(std::rand() % map.width(), std::rand() % map.height()));
  map.clear();
  if (map.countFood()) BAD;
  if (!map.width() || !map.height()) BAD;
  for (int i = 0; i < map.width(); ++i)
    for (int j = 0; j < map.height(); ++j)
      if (map[i][j] != (char)CharacterId::Floor) BAD;
  OK;
}
//resize
void test8 () {
  std::cout << "Test resize:\t";
  Map map;
  for (int i = 0; i < 100; ++i)
    map.addFood(sf::Vector2i(std::rand() % map.width(), std::rand() % map.height()));
  map.resize(240, 25);
  if (map.countFood()) BAD;
  if (map.width() != 240 || map.height() != 25) BAD;
  for (int i = 0; i < map.width(); ++i)
    for (int j = 0; j < map.height(); ++j)
      if (map[i][j] != (char)CharacterId::Floor) BAD;
  OK;
}
}

namespace snake {
//init
void test1 () {
  std::cout << "Test init:\t";
  Snake snake;
  if (snake.isAlive()) BAD;
  OK;
}
//reset
void test2 () {
  std::cout << "Test reset:\t";
  Map map;
  Snake snake;
  snake.reset(map);
  for (auto iter = snake.begin(); iter != snake.end(); ++iter)
    if (map[iter->pos.x][iter->pos.y] != (char)CharacterId::Snake) BAD;
  OK;
}
//move
void test3 () {
  Map map;
  Snake snake;
  snake.reset(map);
  system("clear");
  std::cout << "Test move:";
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: top";
  snake.move(map, Target::top);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: left";
  snake.move(map, Target::left);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: bottom";
  snake.move(map, Target::bottom);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: right";
  snake.move(map, Target::right);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  while (snake.isAlive()) {
    system("clear");
    std::cout << "Test move: bottom";
    snake.move(map, Target::bottom);
    std::cout << std::endl;
    for (int j = 0; j < map.height(); ++j) {
      for (int i = 0; i < map.width(); ++i)
        std::cout << (int)map[i][j];
      std::cout << std::endl;
    }
    sf::sleep(sf::milliseconds(200.f));
  }
  std::cout << "Snake dead" << std::endl;
}
//eat
void test4 () {
  Map map(40, 20);
  Snake snake;
  for (int i = 0; i < 100; ++i)
    map.addFood(sf::Vector2i(std::rand() % map.width(), std::rand() % map.height()));
  snake.reset(map);
  system("clear");
  std::cout << "Test move:";
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: top";
  snake.move(map, Target::top);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: left";
  snake.move(map, Target::left);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: left";
  snake.move(map, Target::left);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: bottom";
  snake.move(map, Target::bottom);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  system("clear");
  std::cout << "Test move: right";
  snake.move(map, Target::right);
  std::cout << std::endl;
  for (int j = 0; j < map.height(); ++j) {
    for (int i = 0; i < map.width(); ++i)
      std::cout << (int)map[i][j];
    std::cout << std::endl;
  }
  sf::sleep(sf::milliseconds(350.f));
  while (snake.isAlive()) {
    system("clear");
    std::cout << "Test move: bottom";
    snake.move(map, Target::bottom);
    std::cout << std::endl;
    for (int j = 0; j < map.height(); ++j) {
      for (int i = 0; i < map.width(); ++i)
        std::cout << (int)map[i][j];
      std::cout << std::endl;
    }
    sf::sleep(sf::milliseconds(200.f));
  }
  std::cout << "Snake dead" << std::endl;
}
}

int main () {
  map::test1();
  map::test2();
  map::test3();
  map::test4();
  map::test5();
  map::test6();
  map::test7();
  map::test8();
  snake::test1();
  snake::test2();
  getchar();
  snake::test3();
  getchar();
  snake::test4();
  getchar();
  return 0;
}
