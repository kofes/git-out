#include "Client/Client.hpp"
#include <iostream>

using namespace snake;

int main () {
  std::string name;
  std::cout << "Nickname: ";
  std::cin >> name;
  Client client;
  client.Run(sf::IpAddress::LocalHost, 25225, name);
  return 0;
}
