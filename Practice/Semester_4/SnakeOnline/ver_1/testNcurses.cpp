#include <ncurses.h>
#include "Resources.hpp"
#include "Map.hpp"
#include "Snake.hpp"
#include <queue>
#include <thread>
#include "Mutex.hpp"

#include "Server.hpp"
#include "Client.hpp"

int testGame () {
  initscr();
  if (has_colors()) {
    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
  }
  clear();
  noecho();
  cbreak();
  keypad(stdscr, TRUE);
  curs_set(0);
  std::queue<char> keys;
  keys.push((char)Target::top);
  Map map(40, 20);
  Snake snake;
  for (int i = 0; i < 100; ++i)
    map.addFood(sf::Vector2i(std::rand() % map.width(), std::rand() % map.height()));
  snake.reset(map);
  timeout(0);
  std::thread update([&]() {
    while (snake.isAlive()) {
      sf::sleep(sf::milliseconds(MIN_MILLISECONDS));
      myMutex.lock();
        if (keys.back() == 'Q')
          break;
        snake.move(map, (Target)keys.back());
        if (keys.size() > 1)
          keys.pop();
        for (auto iter = map.beginFood(); iter != map.endFood(); ++iter)
          iter->genFood(map);
      myMutex.unlock();
    }
  });
  update.detach();
  std::thread display([&]() {
    while (keys.back() != 'Q') {
      sf::sleep(sf::milliseconds(MIN_MILLISECONDS));
      myMutex.lock();
        clear();
        for (int j = 0; j < map.height(); ++j) {
          for (int i = 0; i < map.width(); ++i) {
            if (map[i][j] == (char)CharacterId::Snake) {
                addch('S' | A_BOLD | COLOR_PAIR(1));
              }
            else if (map[i][j] == (char)CharacterId::Food)
                addch('@' | A_BOLD | COLOR_PAIR(2));
            else
              addch('.' | A_NORMAL);
          }
          addch('\n' | A_NORMAL);
        }
        printw("HEIGHT: %d, WIDTH: %d, COUNT: %d", map.height(), map.width(), snake.size());
      myMutex.unlock();
    }
  });
  display.detach();
  while (snake.isAlive()) {
    char sym = getch();
    sf::sleep(sf::milliseconds(MIN_MILLISECONDS/4));
    if (sym == 'w' || sym == 'W')
      if (keys.back() != (char)Target::bottom)
        keys.push((char)Target::top);
    if (sym == 's' || sym == 'S')
      if (keys.back() != (char)Target::top)
        keys.push((char)Target::bottom);
    if (sym == 'd' || sym == 'D')
      if (keys.back() != (char)Target::left)
        keys.push((char)Target::right);
    if (sym == 'a' || sym == 'A')
      if (keys.back() != (char)Target::right)
        keys.push((char)Target::left);
    if (sym == 'q' || sym == 'Q') {
      keys.push('Q');
      break;
    }
  }
  endwin();
}

int main () {
  testGame();
  return 0;
}
