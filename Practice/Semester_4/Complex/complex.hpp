#pragma once

#include <iostream>
#include <fstream>
#include <cmath>

namespace Complex {
enum class Error {
	NORM,
	DIV0,
};
static double EPS = 1e-5;
static Error error = Error::NORM;

template <typename TypeNum>
class Complex {
public:
	TypeNum Re, Im;
/**/
	Complex () : Re(0), Im(0) {}
	Complex (TypeNum Re, TypeNum Im = 0) : Re(Re), Im(Im) {}
  Complex (const Complex &src) : Re(src.Re), Im(src.Im) {}
  template <typename newType>
	inline Complex<TypeNum> operator + (const Complex<newType> &src) {
		Complex<TypeNum> result;
		result.Re = Re + src.Re;
		result.Im = Im + src.Im;
		return result;
	}
  template <typename newType>
	inline Complex<TypeNum> operator - (const Complex<newType> &src) {
		Complex<TypeNum> result;
		result.Re = Re - src.Re;
		result.Im = Im - src.Im;
		return result;
	}
  template <typename newType>
	inline Complex<TypeNum> operator * (const Complex<newType> &src) {
		Complex<TypeNum> result;
		result.Re = Re * src.Re - Im * src.Im;
		result.Im = Re * src.Im + Im * src.Re;
		return result;
	}
  template <typename newType>
	inline Complex<TypeNum> operator / (const Complex<newType> &src) {
		Complex<TypeNum> result;
		if (src.Re + EPS > 0 && src.Re - EPS < 0 && src.Im + EPS > 0 && src.Im - EPS < 0) {
			error = Error::DIV0;
			return Complex();
		}
		result.Re = (Re * src.Re + Im * src.Im) / (src.Re * src.Re + src.Im * src.Im);
		result.Im = (- Re * src.Im + Im * src.Re) / (src.Re * src.Re + src.Im * src.Im);
		return result;
	}
  template <typename newType>
	inline Complex<TypeNum> &operator = (const Complex<newType> &src) {
		Re = src.Re;
		Im = src.Im;
		return *this;
	}
	inline bool operator == (const Complex &src) {
		return (Re == src.Re && Im == src.Im);
	}
	inline virtual TypeNum Abs() {
		return std::sqrt(Re * Re + Im * Im);
	}
	inline virtual Complex Pow(int power) {
		Complex result;
		double delta = std::sqrt(Re * Re + Im * Im);
		delta = std::pow(delta, power);
		double arg = std::atan(Im / Re);
		result.Re = delta * std::cos(arg * power);
		result.Im = delta * std::sin(arg * power);
		return result;
	}
	friend std::ostream &operator <<(std::ostream &out, const Complex &src) {
		out << src.Re;
    if (EPS + src.Im > 0 && EPS - src.Im < 0) {
      out << (src.Im < 0 ? " - i * " : " + i * ") << (src.Im < 0 ? -src.Im : src.Im);
    }
    		return out;
	}
	friend std::ofstream &operator <<(std::ofstream &out, const Complex &src) {
    out << src.Re;
    if (EPS + src.Im > 0 && EPS - src.Im < 0) {
      out << (src.Im < 0 ? " - i * " : " + i * ") << (src.Im < 0 ? -src.Im : src.Im);
    }
    		return out;
	}
};

template <typename TypeNum>
class Polar : public Complex<TypeNum> {
public:
  TypeNum p, phi;
/**/
  Polar () : p(0), phi(0) {}
  Polar (TypeNum p, TypeNum phi = 0) : p(p), phi(phi) {}
  template <typename newType>
  Polar (const Polar<newType> &src) : p(src.p), phi(src.phi) {}
  template <typename newType>
  Polar (const Complex<newType> &src) {
    p = std::sqrt(src.Re * src.Re + src.Im * src.Im);
    phi = std::atan(src.Im / src.Re);
  }
  inline Complex<TypeNum> s_Complex () {
    Complex<TypeNum> result;
    result.Re = this->p * std::cos(phi);
    result.Im = this->p * std::sin(phi);
    return result;
  }
  inline Complex<TypeNum> Pow (int power) {
    Complex<TypeNum> result;
    result.Re = std::pow(p, power) * std::cos(phi * power);
    result.Im = std::pow(p, power) * std::sin(phi * power);
    return result;
  }
  inline TypeNum Abs () {
    return std::abs(p);
  }
  template <typename newType>
  inline Polar<TypeNum> operator + (Polar<newType> &src) {
    return Polar<TypeNum>(this->s_Complex() + src.s_Complex());
  }
  template <typename newType>
  inline Polar<TypeNum> operator - (Polar<newType> &src) {
    return Polar<TypeNum>(this->s_Complex() - src.s_Complex());
  }
  template <typename newType>
  inline Polar<TypeNum> operator * (const Polar<newType> &src) {
    Polar<TypeNum> result;
    result.p = p * src.p;
    result.phi = phi + src.phi;
    return result;
  }
  template <typename newType>
  inline Polar<TypeNum> operator / (const Polar<newType> &src) {
    Polar<TypeNum> result;
    if (src.p + EPS < 0 && src.p - EPS > 0) {
      error = Error::DIV0;
      return result;
    }
    result.p = p / src.p;
    result.phi = phi - src.phi;
    return result;
  }
  template <typename newType>
  inline Polar &operator = (const Polar<newType> &src) {
    p = src.p;
    phi = src.phi;
    return *this;
  }
  inline bool operator ==(const Polar &src) {
    return (p == src.p && phi == src.phi);
  }
  friend std::ostream &operator <<(std::ostream &out, const Polar &src) {
		out << src.p << "*e^(" << (src.phi < 0 ? "-i*" : "i*") << (src.phi < 0 ? src.phi : -src.phi);
    return out;
	}
  friend std::ofstream &operator <<(std::ofstream &out, const Polar &src) {
    out << src.p << "*e^(" << (src.phi < 0 ? "-i*" : "i*") << (src.phi < 0 ? src.phi : -src.phi);
    return out;
  }
};
}
