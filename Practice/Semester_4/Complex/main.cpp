#include <iostream>
#include "complex.hpp"
#include <complex>
// using namespace Complex;

static double STATIC_EPS = 1e-8;

void test1() {
  std::cout << "Test \"Empty input\":\t";
  Complex::Complex<double> obj;
  std::cout << ((obj.Re == 0 && obj.Im == 0) ? "OK" : "BAD") << std::endl;
}

void test2() {
  std::cout << "Test \"Real input\":\t";
  Complex::Complex<double> obj(20);
  std::cout << ((obj.Re == 20 && !obj.Im) ? "OK" : "BAD") << std::endl;
}

void test3() {
  std::cout << "Test \"Full input\":\t";
  Complex::Complex<double> obj(4.5, -3.7);
  std::cout << ((obj.Re == 4.5 && obj.Im == -3.7) ? "OK" : "BAD") << std::endl;
}

void test4() {
  std::cout << "Test \"Copy\":\t\t";
  Complex::Complex<double> obj(21.3, -0.5);
  Complex::Complex<double> res(obj);
  std::cout << ((obj.Re == 21.3 && obj.Im == -0.5) ? "OK" : "BAD") << std::endl;
}

void test5() {
  std::cout << "Test \"Abs\":\t\t";
  Complex::Complex<double> obj(0.7, 5.2);
  std::complex<double> obj_std(0.7, 5.2);
  std::cout << ((std::abs(obj_std) == obj.Abs()) ? "OK" : "BAD") << std::endl;
}

void test6() {
  std::cout << "Test \"Pow\":\t\t";
  Complex::Complex<double> obj(3, 4.0);
  Complex::Complex<double> result;
  result = obj.Pow(4);
  double Arg = std::atan(4.0/3);
  double _image = std::pow(obj.Abs(), 4) * std::sin(Arg * 4);
  double _real  = std::pow(obj.Abs(), 4) * std::cos(Arg * 4);
  std::cout << ((result.Re == _real && result.Im == _image) ? "OK" : "BAD") << std::endl;
}

void test7() {
  Complex::Complex<double> obj;
  std::cout << "Test \"+\":\t\t";
  obj = Complex::Complex<double>(22.3, 1.2) + Complex::Complex<double>(2.6, 2.3);
  std::complex<double> obj_std(22.3 + 2.6, 1.2 + 2.3);
  std::cout << (((obj.Re == obj_std.real()) && (obj.Im == obj_std.imag())) ? "OK" : "BAD") << std::endl;
}

void test8() {
  Complex::Complex<double> obj;
  std::cout << "Test \"-\":\t\t";
  obj = Complex::Complex<double>(3.6, 222.3) - Complex::Complex<double>(11, 45.1);
  std::complex<double> obj_std(3.6 - 11, 222.3 - 45.1);
  std::cout << (((obj.Re == obj_std.real()) && (obj.Im == obj_std.imag())) ? "OK" : "BAD") << std::endl;
}

void test9() {
  Complex::Complex<double> obj;
  obj = Complex::Complex<double>(72.32, 23.123) * Complex::Complex<double>(1.32, 5.51);
  std::cout << "Test \"*\":\t\t";
  std::complex<double> obj_std = std::complex<double>(72.32, 23.123) * std::complex<double>(1.32, 5.51);
  std::cout << (((obj.Re == obj_std.real()) && (obj.Im == obj_std.imag())) ? "OK" : "BAD") << std::endl;
}

void test10() {
  Complex::Complex<double> obj;
  std::cout << "Test \"/\":\t\t";

  obj = Complex::Complex<double>(13.2, 22.3) / Complex::Complex<double>(1.2, 2);
  std::complex<double> obj_std = std::complex<double>(13.2, 22.3) / std::complex<double>(1.2, 2);
  std::cout << ((obj.Re == (obj_std.real()) && obj.Im == (obj_std.imag())) ? "OK" : "BAD") << std::endl;
}

void test11() {
  Complex::Complex<double> obj(1.3, 22.1);
  Complex::Complex<double> res = obj;
  std::cout << "Test \"=\":\t\t";
  std::cout << ((res.Re == obj.Re && res.Im == obj.Im) ? "OK" : "BAD") << std::endl;
}

void test12() {
  Complex::Complex<double> obj1(1.22, 23.3);
  Complex::Complex<double> obj2(12.3, -22.3), obj3 = obj1;
  std::cout << "Test \"==\":\t\t";
  std::cout << ((!(obj1 == obj2) && (obj3 == obj1)) ? "OK" : "BAD") << std::endl;
}
/*Complex::Polar coords*/
void test13() {
  Complex::Polar<double>  obj;
  std::cout << "Test \"Empty input\":\t";
  std::cout << ((!obj.Abs() && !obj.phi) ? "OK" : "BAD") << std::endl;
}

void test14(){
  Complex::Polar<double>  obj(9, 3.14);
  std::cout << "Test \"Full input\":\t";
  std::cout << ((obj.Abs() == 9 && obj.phi == 3.14) ? "OK" : "BAD") << std::endl;
}

void test15(){
  Complex::Polar<double>  obj(11.2, 233.2);
  Complex::Polar<double>  res(obj);
  std::cout << "Test \"Copy\":\t\t";
  std::cout << ((obj.Abs() == 11.2 && obj.phi == 233.2) ? "OK" : "BAD") << std::endl;
}

void test16_1() {
  std::cout << "Test \"s_Complex\": \t";
  Complex::Polar<double> obj(1.324, 2312);
  double Re = 1.324 * std::cos(2312);
  double Im = 1.324 * std::sin(2312);
  std::cout << (std::abs(obj.s_Complex().Re - Re) < STATIC_EPS && std::abs(obj.s_Complex().Im - Im) < STATIC_EPS ? "OK" : "BAD") << std::endl;
}

void test16(){
  std::cout << "Test \"Copy Dekart\":\t";
  Complex::Complex<double> obj(2.2, 23.1);
  Complex::Polar<double>  res(obj);
  std::cout << ((std::abs(res.s_Complex().Re - obj.Re) < STATIC_EPS && std::abs(res.s_Complex().Im - obj.Im) < STATIC_EPS) ? "OK" : "BAD") << std::endl;
}

void test17(){
  std::cout << "Test \"Abs\":\t\t";
  Complex::Polar<double> obj(1.2, 332.2);
  std::complex<double> obj_std(obj.s_Complex().Re, obj.s_Complex().Im);
  std::cout << ((std::abs(obj.Abs() - std::abs(obj_std)) < STATIC_EPS) ? "OK" : "BAD") << std::endl;
}

void test18() {
  std::cout << "Test \"Pow\":\t\t";
  Complex::Polar<double> obj(3, 4.0);
  std::complex<double> obj_std(obj.s_Complex().Re, obj.s_Complex().Im);
  Complex::Complex<double> result;
  result = obj.Pow(4);
  obj_std = std::pow(obj_std, 4);
  std::cout << (((result.Re - obj_std.real()) < STATIC_EPS && (result.Im == obj_std.imag()) < STATIC_EPS) ? "OK" : "BAD") << std::endl;
}

void test19() {
  std::cout << "Test \"+\":\t\t";
  Complex::Polar<double> obj;
  Complex::Polar<double> obj_t(2.6, 2.3);
  Complex::Polar<double> obj_2t(22.3, 1.2);

  obj = obj_2t + obj_t;

  std::cout << ((obj.s_Complex().Re - (obj_2t.s_Complex().Re + obj_t.s_Complex().Re) < STATIC_EPS && obj.s_Complex().Im - (obj_2t.s_Complex().Im + obj_t.s_Complex().Im) < STATIC_EPS) ? "OK" : "BAD") << std::endl;
}

void test20() {
  std::cout << "Test \"-\":\t\t";
  Complex::Polar<double> obj;
  Complex::Polar<double> obj_t(11, 3.1);
  Complex::Polar<double> obj_2t(3.6, 22.3);

  obj = obj_2t - obj_t;

  std::cout << ((obj.s_Complex().Re - (obj_2t.s_Complex().Re - obj_t.s_Complex().Re) < STATIC_EPS && obj.s_Complex().Im - (obj_2t.s_Complex().Im - obj_t.s_Complex().Im) < STATIC_EPS) ? "OK" : "BAD") << std::endl;
}

void test21() {
  std::cout << "Test \"*\":\t\t";
  Complex::Polar<double> obj_t(2, 3);
  Complex::Polar<double> obj_2t(3.2, 11.22);
  Complex::Polar<double> obj = obj_t * obj_2t;
  std::cout << ((obj.p == (2 * 3.2) && obj.phi == (3 + 11.22)) ? "OK" : "BAD") << std::endl;
}

void test22() {
  std::cout << "Test \"/\":\t\t";
  Complex::Polar<double> obj;
  obj = Complex::Polar<double>(13.2, 22.3) / Complex::Polar<double>(1.2, 2);
  std::cout << ((obj.p == (13.2 / 1.2) && obj.phi == (22.3 - 2)) ? "OK" : "BAD") << std::endl;
}

void test23() {
  std::cout << "Test \"=\":\t\t";
  Complex::Polar<double> obj(1.3, 22.1);
  Complex::Polar<double> res = obj;
  std::cout << ((res.p == 1.3 && res.phi == 22.1) ? "OK" : "BAD") << std::endl;
}

void test24() {
  std::cout << "Test \"==\":\t\t";
  Complex::Polar<double> obj1(1.22, 23.3);
  Complex::Polar<double> obj2(12.3, -22.3), obj3 = obj1;
  std::cout << ((!(obj1 == obj2) && (obj3 == obj1)) ? "OK" : "BAD") << std::endl;
}


int main() {
  std::cout << "class Complex::Complex" << std::endl;
  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  test7();
  test8();
  test9();
  test10();
  test11();
  test12();
  std::cout << "class Complex::Polar" << std::endl;
  test13();
  test14();
  test15();
  test16_1();
  test16();
  test17();
  test18();
  test19();
  test20();
  test21();
  test22();
  test23();
  test24();
  return 0;
}
