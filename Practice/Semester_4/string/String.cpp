#include "String.hpp"

//
//copy std::string
my::String::String (const std::string& src) {
  _first = (char*)::operator new[](src.size() + 1);
  for (my::size_type i = 0; i < src.size(); ++i)
    _first[i] = src[i];
  _last = _first + src.size() - 1;
  _end = _last + 1;
};
//fill
my::String::String (my::size_type n, char val) {
  _first = (char*)::operator new[](n + 1);
  for (my::size_type i = 0; i < n; ++i)
    _first[i] = val;
  _last = _first + n - 1;
  _end = _last + 1;
};
//copy
my::String::String (const my::String& src) {
  if (src._first == src._end) {
    _first = _last = _end = nullptr;
    return;
  }
  _first = (char*)::operator new[](src._end - src._first + 1);
  for (my::size_type i = 0; i < src._last - src._first + 1; ++i)
    _first[i] = src._first[i];
  _last = _first + (src._last - src._first);
  _end = _first + (src._end - src._first);
};
//initializer list
my::String::String (std::initializer_list<char> il) {
  _first = _last = _end = nullptr;
  if (!il.size())
    return;
  _first = (char*)::operator new[](il.size() + 1);
  _last = _first;
  _end = _first + il.size();
  auto i = il.begin();
  _first[0] = *i;
  ++i;
  for (; i != il.end(); ++i)
    push_back(*i);
};
my::String::~String () {
  if (_first != nullptr) {
    delete[] _first;
    _first = _last = _end = nullptr;
  }
};
//String
my::String& my::String::operator= (const my::String& src) {
  if (_first == src._first)
    return *this;
  if (_first != nullptr)
    delete[] _first;
  if (src._first == src._end) {
    _first = _last = _end = nullptr;
    return *this;
  }
  _first = (char*)::operator new[](src._end - src._first + 1);
  for (my::size_type i = 0; i < src._last - src._first + 1; ++i)
    _first[i] = src._first[i];
  _last = _first + (src._last - src._first);
  _end = _first + (src._end - src._first);
  return *this;
};
//std::string
my::String& my::String::operator= (const std::string& src) {
  if (_first != nullptr)
    delete[] _first;
  _first = _last = _end = nullptr;
  if (!src.size())
    return *this;
  _first = (char*)::operator new[](src.size() + 1);
  _last = _first;
  _end = _first + src.size();
  auto i = src.begin();
  _first[0] = *i;
  ++i;
  for (; i != src.end(); ++i)
    push_back(*i);
  return *this;
};
//initialiler list
my::String& my::String::operator= (std::initializer_list<char> il) {
  if (_first != nullptr)
    delete[] _first;
  _first = _last = _end = nullptr;
  if (!il.size())
    return *this;
  _first = (char*)::operator new[](il.size() + 1);
  _last = _first;
  _end = _first + il.size();
  auto i = il.begin();
  _first[0] = *i;
  ++i;
  for (; i != il.end(); ++i)
    push_back(*i);
  return *this;
};
/*my::String::iterators*/
my::String::iterator my::String::begin () {
  my::String::iterator it;
  it._container = this;
  it._pointer = _first;
  return it;
};
my::String::iterator my::String::end () {
  my::String::iterator it;
  it._container = this;
  it._pointer = _last + 1;
  return it;
};
/*Capacity*/
my::size_type my::String::size () const {
  if (_first == nullptr)
    return 0;
  return (_last - _first + 1);
};
my::size_type my::String::length () const {
  if (_first == nullptr)
    return 0;
  return (_last - _first + 1);
};
my::size_type my::String::max_size () const {
  return (1 << sizeof(my::size_type)*CHAR_BIT) - 1;
};
void my::String::resize (my::size_type n) {
  if (_first == nullptr) {
    _first = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < n; ++i)
      _first[i] = '\0';
    _last = _first + n - 1;
    _end = _last + 1;
    return;
  }
  if (n >= _end - _first + 1) {
    char *buff = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < _last - _first + 1; ++i)
      buff[i] = _first[i];
    for (my::size_type i = _last - _first + 1; i < n; ++i)
      buff[i] = '\0';
    delete[] _first;
    _first = buff;
    _last = _first + n - 1;
    _end = _last + 1;
  } else {
    if (n > _last - _first)
      for (my::size_type i = 1; i < n - (_last - _first); ++i)
        _last[i] = '\0';
    _last = _first + n - 1;
  }
};
void my::String::resize (my::size_type n, char val) {
  if (_first == nullptr) {
    _first = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < n; ++i)
      _first[i] = '\0';
    _last = _first + n - 1;
    _end = _last + 1;
    return;
  }
  if (n >= _end - _first + 1) {
    char *buff = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < _last - _first + 1; ++i)
      buff[i] = _first[i];
    for (my::size_type i = _last - _first + 1; i < n; ++i)
      buff[i] = val;
    delete[] _first;
    _first = buff;
    _last = _first + n - 1;
    _end = _last + 1;
  } else {
    if (n > _last - _first)
      for (my::size_type i = 1; i < n - (_last - _first); ++i)
        _last[i] =  val;
    _last = _first + n - 1;
  }
};
my::size_type my::String::capacity () const {
  if (_end == _first)
    return 0;
  return (_end - _first);
};
bool my::String::empty () const {
  return (_last - _first == 0);
};
void my::String::reserve (my::size_type n) {
  char *buff = (char*)::operator new[](n + 1);
  for (my::size_type i = 0; (i < _last - _first + 1)&&(i < n); ++i)
    buff[i] = _first[i];
  if (n > _last - _first + 1)
    for (my::size_type i = _last - _first + 1; i < n; ++i)
      buff[i] = '\0';
  delete[] _first;
  _first = buff;
  _last = _first + n - 1;
  _end = _last + 1;
};
void my::String::shrink_to_fit () {
  my::size_type n = _last - _first + 1;
  char* buff = (char*)::operator new[](n + 1);
  for (my::size_type i = 0; i < n; ++i)
    buff[i] = _first[i];
  delete[] _first;
  _first = buff;
  _last = _first + n - 1;
  _end = _last + 1;
};
void my::String::clear () {
  for (auto it = iter_list.begin(); it != iter_list.end(); ++it)
    (*it)->_pointer = nullptr;
  delete[] _first;
  _first = _last = _end = nullptr;
};
/*Element access*/
char& my::String::operator[] (my::size_type n) {
  return _first[n];
};
const char& my::String::operator[] (my::size_type n) const {
  return _first[n];
};
char& my::String::at (my::size_type n) {
  if (n > _last - _first)
    throw std::out_of_range("non-const at");
  return _first[n];
};
const char& my::String::at (my::size_type n) const {
  if (n > _last - _first)
    throw std::out_of_range("const at");
  return _first[n];
};
char& my::String::front () {
  if (_first == nullptr)
    throw my::invalid_pointer("Обращение к нулевому указателю в non-const front()");
  return *_first;
};
const char& my::String::front () const {
  if (_first == nullptr)
    throw my::invalid_pointer("Обращение к нулевому указателю в const front()");
  return *_first;
};
char& my::String::back () {
  if (_first == nullptr)
    throw my::invalid_pointer("Обращение к нулевому указателю в non-const back()");
  return *_last;
};
const char& my::String::back () const {
  if (_first == nullptr)
    throw my::invalid_pointer("Обращение к нулевому указателю в non-const back()");
  return *_last;
};
/*Modifiers*/
const char* my::String::data () const {
  return _first;
};
const char* my::String::c_str () const {
  return _first;
};
//String
my::String& my::String::operator+= (const my::String& src) {
  if (_first == nullptr) {
    if (src._first == src._end) {
      _first = _last = _end = nullptr;
      return *this;
    }
    _first = (char*)::operator new[](src._end - src._first + 1);
    for (my::size_type i = 0; i < src._last - src._first + 1; ++i)
      _first[i] = src._first[i];
    _last = _first + (src._last - src._first);
    _end = _first + (src._end - src._first);
    _end = '\0';
    return *this;
  }
  if (_end - _last - 1 < src.size()) {
    my::size_type n = size() + src.size();
    char* buff = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < size(); ++i)
      buff[i] = _first[i];
    _first = buff;
    _last = _first + n - 1 - src.size();
    _end = _first + n + 1;
    *(_last + 1) = '\0';
  }
  for (auto i = src._first; i != src._end; ++i)
    push_back(*i);
};
//std::string
my::String& my::String::operator+= (const std::string& src) {
  if (_first == nullptr) {
    if (!src.size()) {
      _first = _last = _end = nullptr;
      return *this;
    }
    _first = (char*)::operator new[](src.size() + 1);
    for (my::size_type i = 0; i < src.size(); ++i)
      _first[i] = src[i];
    _last = _first + src.size();
    _end = _first + src.size() + 1;
    _end = '\0';
    return *this;
  }
  if (_end - _last - 1 < src.size()) {
    my::size_type n = this->size() + src.size();
    char* buff = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < this->size(); ++i)
      buff[i] = _first[i];
    _first = buff;
    _last = _first + n - 1 - src.size();
    _end = _first + n + 1;
    *(_last + 1) = '\0';
  }
  for (auto i = src.begin(); i != src.end(); ++i)
    push_back(*i);
};
//initializer list
my::String& my::String::operator+= (std::initializer_list<char> il) {
  if (_first == nullptr) {
    if (!il.size())
      return *this;
    _first = (char*)::operator new[](il.size() + 1);
    _last = _first;
    _end = _first + il.size();
    _end = '\0';
    auto i = il.begin();
    _first[0] = *i;
    ++i;
    for (; i != il.end(); ++i)
      push_back(*i);
  }
  if (_end - _last - 1 < il.size()) {
    my::size_type n = this->size() + il.size();
    char* buff = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < this->size(); ++i)
      buff[i] = _first[i];
    _first = buff;
    _last = _first + n - 1 - il.size();
    _end = _first + n + 1;
    *(_last + 1) = '\0';
  }
  for (auto i = il.begin(); i != il.end(); ++i)
    push_back(*i);
};
//char
my::String& my::String::operator+= (char val) {
  push_back(val);
  return *this;
};
//String
my::String my::String::operator+ (const my::String& src) {
  my::String str(*this);
  str += src;
  return str;
};
//std::string
my::String my::String::operator+ (const std::string& src) {
  my::String str(*this);
  str += src;
  return str;
};
//char
my::String my::String::operator+ (char val) {
  my::String str(*this);
  str += val;
  return str;
};
void my::String::assign (my::String::iterator first, my::String::iterator last) {
  delete[] _first;
  for (my::String::iterator i = first; i != last; ++i)
    push_back(*i);
};
void my::String::assign (my::size_type n, const char& val) {
  if (_first == nullptr) {
      _first = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < n; ++i)
      _first[i] = val;
    _last = _first + n - 1;
    _end = _last + 1;
    return;
  }
  if (n >= _end - _first + 1) {
    char *buff = (char*)::operator new[](n + 1);
    for (my::size_type i = 0; i < _last - _first + 1; ++i)
      buff[i] = _first[i];
    for (my::size_type i = _last - _first + 1; i < n; ++i)
      buff[i] = val;
    delete[] _first;
    _first = buff;
    _last = _first + n - 1;
    _end = _last + 1;
  } else {
    _last = _first + n - 1;
    for (my::size_type i = 0; i < _last - _first + 1; ++i)
      _first[i] = val;
    }
};
void my::String::assign (std::initializer_list<char> il) {
  if (_first != nullptr)
    delete[] _first;
  _first = _last = _end = nullptr;
  if (!il.size())
    return;
  _first = (char*)::operator new[](il.size() + 1);
  _last = _first;
  _end = _first + il.size();
  auto i = il.begin();
  _first[0] = *i;
  ++i;
  for (; i != il.end(); ++i)
    push_back(*i);
};
void my::String::push_back (const char& val) {
  if (_first == nullptr) {
    _first = (char*)::operator new[](2);
    _first[0] = val;
    _last = _first;
    _end = _last + 1;
    *(_last + 1) = '\0';
    return;
  }
  if (_end - _last == 1) {
    my::size_type n = _last - _first + 1;
    char* buff = (char*)::operator new[](2*n + 1);
    for (my::size_type i = 0; i < n; ++i)
      buff[i] = _first[i];
    delete[] _first;
    _first = buff;
    _last = _first + n - 1;
    _end = _first + 2*n + 1;
  }
  ++_last;
  *_last = val;
  *(_last + 1) = '\0';
};
void my::String::pop_back () {
  if (_last == _first) {
    _last = '\0';
    return;
  }
  --_last;
};
my::String::iterator my::String::insert (my::String::iterator position, char val) {
  my::String::iterator it;
  my::size_type n = position - begin();
  if (n > _end - _first + 1)
    return end();
  if (n == _end - _first + 1) {
    push_back(val);
  } else
    _first[n] = val;
  it._container = this;
  it._pointer = _first + n;
  return it;
};
void my::String::insert (my::String::iterator position, my::size_type n, char val) {
  my::size_type pos = position - begin();
  if (n + 1 > _end - _first + 1)
    return;
  if (n + pos + 1 >= _end - _first + 1) {
    char *buff = (char*)::operator new[](n + pos + 2);
    for (my::size_type i = 0; i < pos; ++i)
      buff[i] = _first[i];
    for (my::size_type i = pos; i < n + pos + 1; ++i)
      buff[i] = val;
    delete[] _first;
    _first = buff;
    _last = _first + n + pos;
    _end = _first + n + pos + 1;
  } else {
    for (my::size_type i = pos; i < n + pos + 1; ++i)
      _first[i] = val;
    if (n + pos > _last - _first)
      _last = _first + n + pos;
  }
};
my::String::iterator my::String::erase (my::String::iterator position) {
  my::String::iterator it;
  my::size_type pos = position - begin();
  if (pos > _last - _first)
    return end();
  if (_last == _first) {
    *_first = '\0';
    return end();
  }
  _last = _first + pos - 1;
  for (my::size_type i = pos; i < _last - _first + 1; ++i)
    _first[i] = '\0';
  it._container = this;
  it._pointer = _last;
  return it;
};
my::String::iterator my::String::erase (my::String::iterator first, my::String::iterator last) {
  my::String::iterator it;
  my::size_type pos = first - begin(),
            count = last - first;
  if (pos > _last - _first)
    return end();
  if (count + pos >= _last - _first)
    return erase(first);
  for (my::size_type i = pos; i < pos + count; ++i)
    _first[i] = '\0';
  for (my::size_type i = pos + count; i < _last - _first + 1; ++i)
    _first[i - count] = _first[i];
  _last = _last - count;

  it._container = this;
  it._pointer = _last;
  return it;
};
bool my::String::operator== (const my::String& src) {
  if (size() - src.size()) return false;
  for (my::size_type i = 0; i < size(); ++i)
    if (_first[i] - src._first[i]) return false;
  return true;
};
bool my::String::operator!= (const my::String& src) {
  return !(*this == src);
};
//std::string
bool my::String::operator== (const std::string& src) {
  if (size() - src.size()) return false;
  for (my::size_type i = 0; i < size(); ++i)
    if (_first[i] - src[i]) return false;
  return true;
};
bool my::String::operator!= (const std::string& src) {
  return !(*this == src);
};
/*BMH*/
//std::string
my::size_type my::String::find(const std::string& src) {
  char alph_table[256];
  if (!size() || !src.size()) return npos;
  if (size() < src.size()) return npos;

  my::size_type i, j, k;
  for (i = 0; i < 256; ++i)
    alph_table[i] = src.size();
  for (i = 1; i < src.size(); ++i)
    alph_table[src[i]] = src.size() - i;
  i = src.size();
  do {
    j = src.size();
    k = i;
    while (j > 0 && _first[k - 1] == src[j - 1]) {--k; --j;}
    i = i + alph_table[_first[i]];
  } while (j > 0 && i < size());
  if (j != 0) return npos;
  return k;
};
//String
my::size_type my::String::find(const my::String& src) {
  char alph_table[256];
  if (!size() || !src.size()) return npos;
  if (size() < src.size()) return npos;

  my::size_type i, j, k;
  for (i = 0; i < 256; ++i)
    alph_table[i] = src.size();
  for (i = 1; i < src.size(); ++i)
    alph_table[src._first[i]] = src.size() - i;
  i = src.size();
  do {
    j = src.size();
    k = i;
    while (j > 0 && _first[k - 1] == src._first[j - 1]) {--k; --j;}
    i = i + alph_table[_first[i]];
  } while (j > 0 && i < size());
  if (j != 0) return npos;
  return k;
};
//
my::String operator+ (char val, const my::String& src) {
  my::String str(1, val);
  str += src;
  return str;
};

my::String operator+ (const std::string& src0, const my::String& src1) {
  my::String str(src0);
  str += src1;
  return str;
};
//...............................................................
std::ofstream& operator<< (std::ofstream &out, const my::String& src) {
	if (src.data() != nullptr)
		out << src.data();
	return out;
};

std::ostream& operator<< (std::ostream& out, const my::String& src) {
  if (src.data() != nullptr)
		out << src.data();
	return out;
};
