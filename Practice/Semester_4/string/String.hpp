#pragma once

#include <exception>
#include <cstddef>
#include <climits>
#include <iostream>
#include <initializer_list>
#include <list>
#include <ostream>
#include <fstream>

namespace my {

typedef std::size_t size_type;

struct invalid_pointer : public std::exception {
  invalid_pointer() : err("invalid pointer") {};
  invalid_pointer(const std::string& str) : err(str) {};
  const char* what() const noexcept {return err.c_str();};
private:
  std::string err;
};

#pragma pack(push, 1)
class String {
public:
  static const size_type npos = -1;
  //
  class iterator {
  public:
    iterator () : _pointer(nullptr), _container(nullptr) {};
    ~iterator () {
      _pointer = nullptr;
      if (_container != nullptr)
        _container->iter_list.remove(this);
      _container = nullptr;
    };
    iterator& operator= (const iterator &it) {
      _pointer = it._pointer;
      if (_container != nullptr)
        _container->iter_list.remove(this);
      _container = it._container;
      if (_container != nullptr)
        _container->iter_list.push_back(this);
      return *this;
    };
    iterator& operator++ () {
      if (_pointer != _container->_end)
        _pointer++;
      return *this;
    }
    iterator& operator-- () {
      if (_pointer != _container->_first)
        _pointer--;
      return *this;
    }
    bool operator== (const iterator& it) {
      if (_pointer == it._pointer)
        return true;
      return false;
    }
    bool operator!= (const iterator& it) {
      return !(*this == it);
    }
    size_type operator- (const iterator& it) {
      if (_container - it._container)
        return 0;
      if (_container == nullptr || it._container == nullptr)
        return 0;
      return _pointer - it._pointer;
    }
    char& operator* () {
      return *_pointer;
    }
    const char& operator* () const {
      return *_pointer;
    }
  private:
    friend class my::String;
    my::String* _container;
    char* _pointer;
  };
  //
  //default
  String () : _first(nullptr), _last(nullptr), _end(nullptr) {};
  //copy std::string
  String (const std::string& src);
  //fill
  String (size_type n, char val = '\0');
  //copy
  String (const my::String& src);
  //initializer list
  String (std::initializer_list<char> il);
  ~String ();
  //String
  String& operator= (const String& src);
  //std::string
  String& operator= (const std::string& src);
  //initialiler list
  String& operator= (std::initializer_list<char> il);
  /*Iterators*/
  iterator begin ();
  iterator end ();
  /*Capacity*/
  size_type size () const;
  size_type length () const;
  size_type max_size () const;
  void resize (size_type n);
  void resize (size_type n, char val);
  size_type capacity () const;
  bool empty () const;
  void reserve (size_type n);
  void shrink_to_fit ();
  void clear ();
  /*Element access*/
  char& operator[] (size_type n);
  const char& operator[] (size_type n) const;
  char& at (size_type n);
  const char& at (size_type n) const;
  char& front ();
  const char& front () const;
  char& back ();
  const char& back () const;
  /*Modifiers*/
  const char* data () const;
  const char* c_str () const;
  //String
  String& operator+= (const my::String& src);
  //std::string
  String& operator+= (const std::string& src);
  //initializer list
  String& operator+= (std::initializer_list<char> il);
  //char
  String& operator+= (char val);
  //String
  String operator+ (const my::String& src);
  //std::string
  String operator+ (const std::string& src);
  //char
  String operator+ (char val);
  void assign (iterator first, iterator last);
  void assign (size_type n, const char& val);
  void assign (std::initializer_list<char> il);
  void push_back (const char& val);
  void pop_back ();
  iterator insert (iterator position, char val);
  void insert (iterator position, size_type n, char val);
  iterator erase (iterator position);
  iterator erase (iterator first, iterator last);
  bool operator== (const my::String& src);
  bool operator!= (const my::String& src);
  //std::string
  bool operator== (const std::string& src);
  bool operator!= (const std::string& src);
  //std::string
  size_type find(const std::string& src);
  //String
  size_type find(const my::String& src);
private:
  char *_first, *_last, *_end;
  std::list<my::String::iterator*> iter_list;
};
#pragma pack(pop)
};

my::String operator+ (char val, const my::String& src);
my::String operator+ (const std::string& src0, const my::String& src1);
std::ofstream& operator<< (std::ofstream &out, const my::String& src);
std::ostream& operator<< (std::ostream& out, const my::String& src);
