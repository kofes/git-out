#include "String.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

#define BAD {std::cout << "BAD!" << std::endl;return;}
#define OK {std::cout << "OK!" << std::endl;return;}

void test01 () {
  my::String str1, str2(10, 'c'), str3("string"), str4 = {'H', 'e', 'l', 'l'};
}

void test1 () {
  std::cout << "Test init № 1:\t\t";
  test01();
  OK;
}

void test2 () {
  std::cout << "Test init № 2:\t\t";
  my::String str(5, 's');
  for (unsigned char i = 0; i < 5; ++i)
    if ('s' - str[i]) BAD;
  OK;
}

void test3 () {
  std::cout << "Test scobes:\t\t";
  my::String str(11, 4);
  str[7] -= 2;
  for (unsigned char i = 0; i < 11; ++i)
    if (std::abs(3 - str[i]) != 1) BAD;
  OK;
}

void test4 () {
  std::cout << "Test size()/length():\t";
  my::String str(1);
  if (1 - str.size()) BAD;
  OK;
}

void test5 () {
  std::cout << "Test capacity():\t";
  my::String str(12);
  if (12 - str.capacity()) BAD;
  OK;
}

void test6 () {
  std::cout << "Test copy():\t\t";
  my::String str;
  std::srand(std::time(NULL));
  for (unsigned char i = 0; i < str.size(); ++i)
    str[i] = std::rand() % 256;
  my::String strCopy(str);
  for (unsigned char i = 0; i < str.size(); ++i)
    if (str[i] - strCopy[i]) BAD;
  OK;
}

void test7 () {
  std::cout << "Test resize():\t\t";
  my::String str(5, 4);
  str.resize(10, 4);
  if (str.size() - 10) BAD;
  for (unsigned char i = 0; i < str.size(); ++i)
    if (str[i] - 4) BAD;
  OK;
}

void test8 () {
  std::cout << "Test empty():\t\t";
  my::String str1, str2(8);
  if (!str1.empty() || str2.empty()) BAD;
  OK;
}

void test9 () {
  std::cout << "Test reserve():\t\t";
  my::String str(2, 1);
  str.reserve(5);
  if (str.capacity() - 5) BAD;
  for (unsigned char i = 0; i < 2; ++i)
    if (str[i] - 1) BAD;
  for (unsigned char i = 2; i < str.size(); ++i)
    if (str[i] != '\0') BAD;
  OK;
}

void test10 () {
  std::cout << "Test shrink_to_fit():\t";
  my::String str;
  str.resize(20);
  str.resize(10);
  if (str.capacity() - 20) BAD;
  str.shrink_to_fit();
  if (str.capacity() - 10) BAD;
  OK;
}

void test11 () {
  std::cout << "Test at():\t\t";
  my::String str(2, 11);
  str.at(0) = 5;
  if (str.at(0) - 5) BAD;
  try {
    str.at(3) = 2;
  } catch (std::out_of_range& err) {
    OK;
  }
}

void test12 () {
  std::cout << "Test front():\t\t";
  my::String str;
  try {
    str.front() = 10;
  } catch (std::exception& err) {}
  str.resize(2);
  str.front() = 4;
  if (str.front() - 4) BAD;
  OK;
}

void test13 () {
  std::cout << "Test back():\t\t";
  my::String str;
  try {
    str.back() = 10;
  } catch (std::exception& err) {}
  str.resize(2);
  str.back() = 11;
  if (str.back() - 11) BAD;
  OK;
}

void test14 () {
  std::cout << "Test data():\t\t";
  my::String str;
  if (str.data() != nullptr) BAD;
  OK;
}

void test15 () {
  std::cout << "Test assign():\t\t";
  my::String str;
  str.assign(20, 2);
  if (str.capacity() != 20) BAD;
  for (unsigned char i = 0; i < str.size(); ++i)
    if (str[i] - 2) BAD;
  str.assign(10, 1);
  if (str.capacity() != 20 || str.size() != 10) BAD;
  str.resize(20);
  for (unsigned char i = 0; i < 10; ++i)
    if (str[i] - 1) BAD;
  for (unsigned char i = 10; i < str.size(); ++i)
    if (str[i]) BAD;
  str.assign({1, 2, 3, 4 ,5});
  for (unsigned char i = 0; i < str.size(); ++i)
    if (str[i] - i - 1) BAD;
  OK;
}

void test16 () {
  std::cout << "Test push_back():\t";
  my::String str;
  str.push_back(10);
  if (str.capacity() != 1) BAD;
  if (str.back() - 10) BAD;
  OK;
}

void test17 () {
  std::cout << "Test pop_back():\t";
  my::String str;
  str.push_back(10);
  str.push_back(20);
  str.pop_back();
  if (str.back() - 10) BAD;
  OK;
}

void test18 () {
  std::cout << "Test iterator:\t\t";
  my::String str(4, 20);
  for (my::String::iterator it = str.begin(); it != str.end(); ++it)
    if (*it - 20) BAD;
  for (my::String::iterator it = str.begin(); it != str.end(); ++it)
    *it -= 15;
  for (my::String::iterator it = str.end(); it != str.begin();) {
    --it;
    if (*it - 5) BAD;
  }
  OK;
}

void test19 () {
  std::cout << "Test insert():\t\t";
  my::String str(10, 1);
  my::String::iterator it = str.begin();
  ++it; ++it; ++it;
  str.insert(it, 10);
  if (str[3] != 10) BAD;

  str.insert(it, 5, 10);
  for (unsigned char i = 3; i < 8; ++i)
    if (str[i] - 10) {
      std::cout << (int)i << std::endl;
      BAD;
    }
  OK;
}

void test20 () {
  std::cout << "Test clear():\t\t";
  my::String str(22, 3);
  str.clear();
  if (!str.empty()) BAD;
  OK;
}

void test21 () {
  std::cout << "Test erase():\t\t";
  my::String str(10, 2);
  my::String::iterator it = str.begin();
  ++it; ++it;
  str.erase(it);
  if (str.size() - 2) BAD;
  str = my::String(10, 3);
  my::String::iterator iter = str.begin();
  ++iter;
  str.erase(iter, it);
  if (str.size() - 9) BAD;
  OK;
}

void test22 () {
  std::cout << "Test operator==:\t";
  my::String str("World");
  if (str != "World") BAD;
  if (str == "world") BAD;
  OK;
}

void test23 () {
  std::cout << "Test operator+=:\t";
  my::String str("Hello ");
  str += "world!";
  if (str != "Hello world!") BAD;
  str.clear();
  str += 'A';
  str += " little string";
  if (str != "A little string") BAD;
  str += {' ', 'i', 's', ' ', 'v', 'e', 'r', 'y', ' ', 'h', 'u', 'g', 'e', '!'};
  if (str != "A little string is very huge!") BAD;
  OK;
}

void test24 () {
  std::cout << "Test find():\t\t";
  my::String str("A little string is very huge!");
  my::size_type pos = str.find("little");
  if (pos != 2) BAD;
  OK;
}

void test25 () {
  std::cout << "Test operator+:\t\t";
  my::String str0("Hello "), str1("world!");
  if ((str0 + str1) != "Hello world!") BAD;
  if ((str0 + "buddy") != "Hello buddy") BAD;
  if ((str0 + "mr. " + 'S') != "Hello mr. S") BAD;
  if (("Other " + str1) != "Other world!") BAD;
  OK;
}

int main () {
  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  test7();
  test8();
  test9();
  test10();
  test11();
  test12();
  test13();
  test14();
  test15();
  test16();
  test17();
  test18();
  test19();
  test20();
  test21();
  test22();
  test23();
  test24();
  test25();
  return 0;
}
