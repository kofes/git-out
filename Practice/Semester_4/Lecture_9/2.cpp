#include <thread>
#include <mutex>
#include <iostream>
#include <atomic>
#include <future>
/*
is_lock_free() - блокирующий или нет?
atomic_flag - не блокирующий(по стандарту!)
*/
using namespace std;
//spinlock

mutex m;
atomic_int counter;
const int n = 10000000;

/*
операции с atomic_"type" дольше, чем с "type"...
*/

class spinlock_mutex {//активное ожидание
public:
  void lock () {
    while (flag.test_and_set());
  }
  void unlock () {
    flag.clear();
  }
private:
  atomic_flag flag = ATOMIC_FLAG_INIT;
};

spinlock_mutex sm;
void increment () {
  m.lock();
  for (int i = 0; i < n; ++i)
    ++counter;
  m.unlock();
}
/*
int main () {
  thread t1(increment);
  thread t2(increment);
  t1.join();
  t2.join();
  cout << counter << endl;
  return 0;
}
*/
/*
lock_free - структура, в которой потоки не портят данные, но при этом нет lock'ов

stack push:
  node_t* node = new node_t(val);
  do {
    old_top = top;
    node->next = old_top;
  }while (!top.compare_exchange_weak(old_top, node));
stack pop:
  do {
    if (top == nullptr)
      return;
    old_top = top;
    new_top = top->next;
  } while (!top.compare_exchange_weak(old_topm new_top));

*/

int foo () {
  int x = 10;
  return x;
}

int main () {
  future<int> f = asyac(foo);
  cout << f.get() << endl;
  return 0;
}
