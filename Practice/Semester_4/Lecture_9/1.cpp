#include <thread>
#include <iostream>
#include <cstdlib>
#include <mutex>//для синхронизации

using namespace std;

int counter = 0;

const int n = 1000000;

void increment1 () {
  while (true) {
    ++counter;
  }
}
void increment2 () {
  for (int i = 0; i < n; ++i)
    ++counter;//++ - не является отдельной ассемблерной инструкцией, поэтому в промежуточном состоянии возможно изменение counter'a другим потоком(-ами)
}

mutex m;//доступ к ней - атомарный
//mutex - функционал операционной системы...
void increment3 () {
  for (int i = 0; i < n; ++i) {
    m.lock();//устанавливает флаг, что текущий поток заблокирован - останавливаются лишь те потоки, которые заходять "сюда"; вызывает lock распределителя потоков (усыпляет те, которые сюда заходят); Однако, происходит очень много чего, что система перевела поток в lock(), поэтому делать так mutex нельзя(долго)
    ++counter;
    m.unlock();
  }
}

void increment4 () {
  m.lock();
  for (int i = 0; i < n; ++i)
    ++counter;
  m.unlock();
}

void incrementDeadLock () {
  m.lock();
  m.lock();//Он блокирует сам себя
  for (int i = 0; i < n; ++i)
    ++counter;
  m.unlock();
}

void incrementGuard () {
  lock_guard<mutex> guard(m);//при вызове деструктора разблокирует "поток"
  //lock вызывается в конструкторе, а unlock в деструкторе...(принцип RAII)
  for (int i = 0; i < n; ++i)
    ++counter;
}
/*

recursive_mutex rm;

...
if (rm.try_lock()) {//для одного и того же потока возвращает true
...//опасные операции
}
...
*/
/*
Есть ещё livelock - когда потоки заблокировались без явной на то причины.

Thread pool - когда каждый поток создается зарание и имеет свою задачу

Пример:
  while (!exit_flag) {
    if (!q.empty) {
      q -> task
      process(task);
    }
}

Можно указать потокам сколько спать:
1. this_thread::sleep(или как-то так)
2:
while (!exit_flag) {
  if (!q.empty) {
    q -> task
    process(task);
  }
push(Task) {}
................................
conditional_variable cond;
worker:
while (...) {
  while (q.empty()) {cond.wait(mut);}//усыпляет текущий поток
  q -> task
  process(task);
}
push(Task t) {
  q <- t;
  cond.notify_one();
}
*/
/*
thread.detach - запуск потока в фоновом режиме
thread.join - ждать, когда поток завершится
Поток начинает выполняться сразу после вызова конструктора (вне зависимости от detach/join) - последние лишь говорят: какой ждать поток (или очередь их ожидания) для окончания (или очередность окончания), либо отпустить в фоновый режим.
Потоки обладают общим адресным пространством...
Race condition - когда потоки изменяют за одни и тоже данные

Способы избегания ошибок:
1. Потоки работают с разными данными(хоть и в одном адресном пространстве), либо данными лежащими в разных "местах"
*/
/*
int main () {
  thread t(increment1);
  t.detach();
  for (int i = 0; i < 1000000; ++i);
  cout << counter << endl;
  return 0;
}
*/
/*
int main () {
  thread t1(increment2);
  thread t2(increment2);
  t1.join();
  t2.join();
  cout << counter << endl;
  return 0;
}
*/

int main () {
/*
  thread t1(increment4);//даже если потоки вызваны так "последовательно", никто не гарантирует, что они начнут работать в такой последовательности
  thread t2(increment4);
*/
  thread t1(incrementGuard);
  thread t2(incrementGuard);
  t1.join();
  t2.join();
  cout << counter << endl;
  return 0;
}
