#include "../Log.hpp"
#include <vector>
#include <iostream>
#include <string>
// Спорышев Максим Сергеевич
// Бьерн Страуструп < Simula
// Совместим на этапе линковки с Си-кодом

/** -std=c++14
* 1. Инкапсуляция - писать сначала public, потом private, protected
* конструктор по-умолчанию класса вызывает конструкторы всех внутренних классов. Если есть объекты простых типов, то для них ничего не делается
*
* Class obj = a; <=> obj(a); <=/=> obj = a;
* non-copyable <- constructorCopy in private;
*
* 2. Наследование
  Class Derived : {default - public} Base{};
* Полиморфизм
*/
std::vector<int> nums(100);
/*
void l1() {
  int a[100];
  int *b = new int[100];
  b = a; //OK!
  a = b; //BAD!
}
*/
/*
class Base {
public:
  /*explicit*//* Base(char *s) : s(s) {};
  Base(int a);

  std::string s;
};
*/
/*
void print(Base b) {
  std::cout << b.s << std::endl;
}
*/
#pragma pack(push ,1)
/*То, что сверху делает упаковку(выравнивание) побайтово, а не по 4(или 8) байт*/
class Base {
public:
  Base(int a) {};
  int a, b;
  void print(int a) {} //<- не весит ничего(не в объекте)
  virtual void print(char a) {} //<- появляется указатель в объекте на таблицу виртуальных функций
private:
  char c;
}

class Derived : public Base {
  char cc;
}

int main(int argc, char const *argv[]) {
  // Base b("gfafs");
  // print("fafs"); <- работает, если не указать explicit.
  /*
  auto a = 1;//синтаксический сахар.

  Log::err().warning("Work!");
  for (const int &n : nums) {//foreach
    std::cout << n << std::endl;
  }
  */
  return 0;
}
