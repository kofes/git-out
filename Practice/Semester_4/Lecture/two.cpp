#include <iostream>

class Base {
public:
  long long b;
  virtual void print(int a) {};
};

class Derived : public Base {
  char cc;
};

int main(int argc, char const *argv[]) {
  const Base* ptr;
  int a;
  Base *bptr = const_cast<Base *>(ptr);
  Base *bbptr = new Derived();
  Derived *dptr = static_cast<Derived*>(bbptr);

  long long ddptr = reinterpret_cast<long long>(bbptr); //Чистое копирование значения

  return 0;
}
