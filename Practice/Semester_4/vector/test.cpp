#include "vector.hpp"
#include "sort.hpp"
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <exception>

void test01 () {
  my::vector<int> vec1, vec2(10), vec3(3, 3);
}

void test1 () {
  std::cout << "Test init № 1:\t\t";
  test01();
  std::cout << "OK!" << std::endl;
}

void test2 () {
  std::cout << "Test init № 2:\t\t";
  my::vector<int> vec(5, 4);
  for (unsigned char i = 0; i < 5; ++i)
    if (4 - vec[i]) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test3 () {
  std::cout << "Test scobes:\t\t";
  my::vector<int> vec(11, 4);
  vec[7] -= 2;
  for (unsigned char i = 0; i < 11; ++i)
    if (std::abs(3 - vec[i]) != 1) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test4 () {
  std::cout << "Test size():\t\t";
  my::vector<int> vec(1);
  if (1 - vec.size()) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test5 () {
  std::cout << "Test capacity():\t";
  my::vector<int> vec(12);
  if (12 - vec.capacity()) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}
// PROBLEMS!!!
void test6 () {
  std::cout << "Test copy():\t\t";
  my::vector<int> vec;
  std::srand(std::time(NULL));
  for (unsigned char i = 0; i < vec.size(); ++i)
    vec[i] = std::rand() % 100;
  my::vector<int> vecCopy(vec);
  for (unsigned char i = 0; i < vec.size(); ++i)
    if (vec[i] - vecCopy[i]) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test7 () {
  std::cout << "Test resize():\t\t";
  my::vector<int> vec(5, 4);
  vec.resize(10, 4);
  if (vec.size() - 10) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  for (unsigned char i = 0; i < vec.size(); ++i)
    if (vec[i] - 4) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test8 () {
  std::cout << "Test empty():\t\t";
  my::vector<int> vec1, vec2(8);
  if (!vec1.empty() || vec2.empty()) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test9 () {
  std::cout << "Test reserve():\t\t";
  my::vector<int> vec(2, 1);
  vec.reserve(5);
  if (vec.capacity() - 5) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  for (unsigned char i = 0; i < 2; ++i)
    if (vec[i] - 1) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  for (unsigned char i = 2; i < vec.size(); ++i)
    if (vec[i]) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test10 () {
  std::cout << "Test shrink_to_fit():\t";
  my::vector<int> vec;
  vec.resize(20);
  vec.resize(10);
  if (vec.capacity() - 20) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  vec.shrink_to_fit();
  if (vec.capacity() - 10) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test11 () {
  std::cout << "Test at():\t\t";
  my::vector<int> vec(2, 11);
  vec.at(0) = 5;
  if (vec.at(0) - 5) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  try {
    vec.at(3) = 2;
  } catch (std::out_of_range& err) {
    std::cout << "OK!" << std::endl;
  }
}

void test12 () {
  std::cout << "Test front():\t\t";
  my::vector<int> vec;
  try {
    vec.front() = 10;
  } catch (std::exception& err) {}
  vec.resize(2);
  vec.front() = 4;
  if (vec.front() - 4) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test13 () {
  std::cout << "Test back():\t\t";
  my::vector<int> vec;
  try {
    vec.back() = 10;
  } catch (std::exception& err) {}
  vec.resize(2);
  vec.back() = 11;
  if (vec.back() - 11) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test14 () {
  std::cout << "Test data():\t\t";
  my::vector<int> vec;
  int *data = vec.data();
  if (data != nullptr) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test15 () {
  std::cout << "Test assign():\t\t";
  my::vector<int> vec;
  vec.assign(20, 2);
  if (vec.capacity() != 20) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  for (unsigned char i = 0; i < vec.size(); ++i)
    if (vec[i] - 2) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  vec.assign(10, 1);
  if (vec.capacity() != 20 || vec.size() != 10) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  vec.resize(20);
  for (unsigned char i = 0; i < 10; ++i)
    if (vec[i] - 1) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  for (unsigned char i = 10; i < vec.size(); ++i)
    if (vec[i]) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  vec.assign({1, 2, 3, 4 ,5});
  for (unsigned char i = 0; i < vec.size(); ++i)
    if (vec[i] - i - 1) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test16 () {
  std::cout << "Test push_back():\t";
  my::vector<int> vec;
  vec.push_back(10);
  if (vec.capacity() != 1) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  if (vec.back() - 10) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test17 () {
  std::cout << "Test pop_back():\t";
  my::vector<int> vec;
  vec.push_back(10);
  vec.push_back(20);
  vec.pop_back();
  if (vec.back() - 10) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test18 () {
  std::cout << "Test iterator:\t\t";
  my::vector<int> vec(4, 20);
  for (my::vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
    if (*it - 20) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  for (my::vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
    *it -= 15;
  for (my::vector<int>::iterator it = vec.end(); it != vec.begin();) {
    --it;
    if (*it - 5) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  }
  std::cout << "OK!" << std::endl;
}

void test19 () {
  std::cout << "Test insert():\t\t";
  my::vector<int> vec(10, 1);
  my::vector<int>::iterator it = vec.begin();
  ++it; ++it; ++it;
  vec.insert(it, 10);
  if (vec[3] != 10) {
    std::cout << "BAD!" << std::endl;
    return;
  }

  vec.insert(it, 5, 10);
  for (unsigned char i = 3; i < 8; ++i)
    if (vec[i] - 10) {
      std::cout << (int)i << std::endl;
      std::cout << "BAD!" << std::endl;
      return;
    }

  std::cout << "OK!" << std::endl;
}

void test20 () {
  std::cout << "Test clear():\t\t";
  my::vector<int> vec(22, 3);
  vec.clear();
  if (!vec.empty()) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

void test21 () {
  std::cout << "Test erase():\t\t";
  my::vector<int> vec(10, 2);
  my::vector<int>::iterator it = vec.begin();
  ++it; ++it;
  vec.erase(it);
  if (vec.size() - 2) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  vec = my::vector<int>(10, 3);
  my::vector<int>::iterator iter = vec.begin();
  ++iter;
  vec.erase(iter, it);
  if (vec.size() - 9) {
    std::cout << "BAD!" << std::endl;
    return;
  }
  std::cout << "OK!" << std::endl;
}

template <class value_type>
struct cmp {
  bool operator() (const value_type& left, const value_type& right) {
    return (left < right);
  };
};

void test22 () {
  std::cout << "Test sort():\t\t";
  my::vector<int> vec = {1, 20, 22, 13, 2, -1, 7, 49, 3, 0, 1};
  my::sort(vec.begin(), vec.end());
  auto it1 = vec.begin(),
       it2 = vec.begin();
  ++it2;
  for (; it2 != vec.end(); ++it1, ++it2)
    if (*it1 < *it2) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  my::sort(vec.begin(), vec.end(), cmp<int>());
  it1 = vec.begin(),
  it2 = vec.begin();
  ++it2;
  for (; it2 != vec.end(); ++it1, ++it2)
    if (*it1 > *it2) {
      std::cout << "BAD!" << std::endl;
      return;
    }
  std::cout << "OK!" << std::endl;
}

void test23 () {
  std::clock_t clock;
  std::vector<int> std_vec;
  my::vector<int> my_vec;
  size_t n = 1000000;
  for (unsigned int i = 0; i < n; ++i) {
    int elem = std::rand();
    std_vec.push_back(elem);
    my_vec.push_back(elem);
  }
  std::cout << "Test time:" << std::endl;
  std::cout << "stl:\t";
  clock = std::clock();
  my::sort(std_vec.begin(), std_vec.end());
  clock = std::clock() - clock;
  std::cout << ((float)clock)/CLOCKS_PER_SEC << " sec" << std::endl;
  std::cout << "my:\t";
  my::sort(my_vec.begin(), my_vec.end());
  clock = std::clock() - clock;
  std::cout << ((float)clock)/CLOCKS_PER_SEC << " sec" << std::endl;
}

int main () {
  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  test7();
  test8();
  test9();
  test10();
  test11();
  test12();
  test13();
  test14();
  test15();
  test16();
  test17();
  test18();
  test19();
  test20();
  test21();
  test22();
  test23();
  return 0;
}
