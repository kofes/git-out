#pragma once

#include <memory>
#include <exception>
#include <cstddef>
#include <climits>
#include <iostream>
#include <initializer_list>
#include <list>
#include <new>

namespace my {

typedef std::size_t size_type;

template <class value_type>
class allocator {
public:
  value_type* allocate (size_type count) {
    value_type* buff;
    try {
      buff = (value_type*)(::operator new(sizeof(value_type) * count));
    } catch (std::bad_alloc& err) {
      throw err;
    }
    return buff;
  };
  void construct (value_type* pointer, const value_type& val) {
    if (pointer == nullptr)
      return;
    *pointer = val;
  };
  void deallocate (value_type* pointer, size_type count) {
    if (pointer == nullptr)
      return;
    delete[] pointer;
  };
  void destroy (value_type* pointer) {
    if (pointer == nullptr)
      return;
    *pointer = value_type();
  };
};

struct invalid_pointer : public std::exception {
  invalid_pointer() : err("invalid pointer") {};
  invalid_pointer(const std::string& str) : err(str) {};
  const char* what() const noexcept {return err.c_str();};
private:
  std::string err;
};

#pragma pack(push, 1)
template <class value_type, class allocator_type = my::allocator<value_type> /*std::allocator<value_type>*/ >
class vector {
public:
  //
  class iterator {
  public:
    iterator () : _pointer(nullptr), _container(nullptr) {};
    ~iterator () {
      _pointer = nullptr;
      if (_container != nullptr)
        _container->iter_list.remove(this);
      _container = nullptr;
    };
    iterator& operator= (const iterator &it) {
      _pointer = it._pointer;
      _container = it._container;
      if (_container != nullptr)
        _container->iter_list.push_back(this);
      return *this;
    };
    iterator& operator++ () {
      if (_pointer != _container->_end)
        _pointer++;
      return *this;
    }
    iterator& operator-- () {
      if (_pointer != _container->_first)
        _pointer--;
      return *this;
    }
    bool operator== (const iterator& it) {
      if (_pointer == it._pointer)
        return true;
      return false;
    }
    bool operator!= (const iterator& it) {
      return !(*this == it);
    }
    size_type operator- (const iterator& it) {
      if (_container - it._container)
        return 0;
      if (_container == nullptr || it._container == nullptr)
        return 0;
      return _pointer - it._pointer;
    }
    value_type& operator* () {
      return *_pointer;
    }
    const value_type& operator* () const {
      return *_pointer;
    }
  private:
    friend class my::vector<value_type>;
    my::vector<value_type>* _container;
    value_type* _pointer;
  };
  //
  vector (const allocator_type &alloc = allocator_type()) : _alloc(alloc), _first(nullptr), _last(nullptr), _end(nullptr) {};
  vector (size_type n, const allocator_type &alloc = allocator_type()) : _alloc(alloc) {
    _first = _alloc.allocate(n + 1);
    _alloc.construct(_first, value_type());
    _last = _first + n - 1;
    _end = _last + 1;
  };
  vector (size_type n, const value_type& val, const allocator_type& alloc = allocator_type()) : _alloc(alloc) {
    _first = _alloc.allocate(n + 1);
    for (size_type i = 0; i < n; ++i)
      _alloc.construct(_first + i, val);
    _last = _first + n - 1;
    _end = _last + 1;
  };
  vector (const my::vector<value_type>& src) {
    if (src._first == src._end) {
      _first = _last = _end = nullptr;
      return;
    }
    _first = _alloc.allocate(src._end - src._first + 1);
    for (size_type i = 0; i < src._last - src._first + 1; ++i)
      _alloc.construct(_first + i, src._first[i]);
    _last = _first + (src._last - src._first);
    _end = _first + (src._end - src._first);
  };
  vector (std::initializer_list<value_type> il) {
    _first = _last = _end = nullptr;
    if (!il.size())
      return;
    _first = _alloc.allocate(il.size() + 1);
    _last = _first;
    _end = _first + il.size();
    auto i = il.begin();
    _alloc.construct(_first, *i);
    ++i;
    for (; i != il.end(); ++i)
      push_back(*i);
  };
  ~vector () {
    if (_first != nullptr) {
      _alloc.deallocate(_first, _end - _first + 1);
      _first = _last = _end = nullptr;
    }
  };
  vector& operator= (const vector& src) {
    if (_first == src._first)
      return *this;
    if (_first != nullptr)
      _alloc.deallocate(_first, _end - _first + 1);
    if (src._first == src._end) {
      _first = _last = _end = nullptr;
      return *this;
    }
    _first = _alloc.allocate(src._end - src._first + 1);
    for (size_type i = 0; i < src._last - src._first + 1; ++i)
      _alloc.construct(_first + i, src._first[i]);
    _last = _first + (src._last - src._first);
    _end = _first + (src._end - src._first);
    return *this;
  };
  vector& operator= (std::initializer_list<value_type> il) {
    if (_first != nullptr)
      _alloc.deallocate(_first, _end - _first + 1);
    _first = _last = _end = nullptr;
    if (!il.size())
      return *this;
    _first = _alloc.allocate(il.size() + 1);
    _last = _first;
    _end = _first + il.size();
    auto i = il.begin();
    _alloc.construct(_first, *i);
    ++i;
    for (; i != il.end(); ++i)
      push_back(*i);
    return *this;
  };
  /*Iterators*/
  iterator begin () {
    iterator it;
    it._container = this;
    it._pointer = _first;
    return it;
  };
  iterator end () {
    iterator it;
    it._container = this;
    it._pointer = _last + 1;
    return it;
  };
  /*Capacity*/
  size_type size () const {
    if (_first == nullptr)
      return 0;
    return (_last - _first + 1);
  };
  size_type max_size () const {
    return (1 << sizeof(size_type)*CHAR_BIT) - 1;
  };
  void resize (size_type n) {
    if (_first == nullptr) {
      _first = _alloc.allocate(n + 1);
      for (size_type i = 0; i < n; ++i)
        _alloc.construct(_first + i, value_type());
      _last = _first + n - 1;
      _end = _last + 1;
      return;
    }
    if (n >= _end - _first + 1) {
      value_type *buff = _alloc.allocate(n + 1);
      for (size_type i = 0; i < _last - _first + 1; ++i)
        _alloc.construct(buff + i, _first[i]);
      for (size_type i = _last - _first + 1; i < n; ++i)
        _alloc.construct(buff + i, value_type());
      _alloc.deallocate(_first, _end - _first + 1);
      _first = buff;
      _last = _first + n - 1;
      _end = _last + 1;
    } else {
      if (n > _last - _first)
        for (size_type i = 1; i < n - (_last - _first); ++i)
          _alloc.construct(_last + i, value_type());
      _last = _first + n - 1;
    }
  };
  void resize (size_type n, const value_type& val) {
    if (_first == nullptr) {
      _first = _alloc.allocate(n + 1);
      for (size_type i = 0; i < n; ++i)
        _alloc.construct(_first + i, value_type());
      _last = _first + n - 1;
      _end = _last + 1;
      return;
    }
    if (n >= _end - _first + 1) {
      value_type *buff = _alloc.allocate(n + 1);
      for (size_type i = 0; i < _last - _first + 1; ++i)
        _alloc.construct(buff + i, _first[i]);
      for (size_type i = _last - _first + 1; i < n; ++i)
        _alloc.construct(buff + i, val);
      _alloc.deallocate(_first, _end - _first + 1);
      _first = buff;
      _last = _first + n - 1;
      _end = _last + 1;
    } else {
      if (n > _last - _first)
        for (size_type i = 1; i < n - (_last - _first); ++i)
          _alloc.construct(_last + i, value_type());
      _last = _first + n - 1;
    }
  };
  size_type capacity () const {
    if (_end == _first)
      return 0;
    return (_end - _first);
  };
  bool empty () const {
    return (_last - _first == 0);
  };
  void reserve (size_type n) {
    value_type *buff = _alloc.allocate(n + 1);
    for (size_type i = 0; (i < _last - _first + 1)&&(i < n); ++i)
      _alloc.construct(buff + i, _first[i]);
    if (n > _last - _first + 1)
      for (size_type i = _last - _first + 1; i < n; ++i)
        _alloc.construct(buff + i, value_type());
    _alloc.deallocate(_first, _end - _first + 1);
    _first = buff;
    _last = _first + n - 1;
    _end = _last + 1;
  };
  void shrink_to_fit () {
    size_type n = _last - _first + 1;
    value_type* buff = _alloc.allocate(n + 1);
    for (size_type i = 0; i < n; ++i)
      _alloc.construct(buff + i, _first[i]);
    _alloc.deallocate(_first, _end - _first + 1);
    _first = buff;
    _last = _first + n - 1;
    _end = _last + 1;
  };
  /*Element access*/
  value_type& operator[] (size_type n) {
    return _first[n];
  };
  const value_type& operator[] (size_type n) const {
    return _first[n];
  };
  value_type& at (size_type n) {
    if (n > _last - _first)
      throw std::out_of_range("non-const at");
    return _first[n];
  };
  const value_type& at (size_type n) const {
    if (n > _last - _first)
      throw std::out_of_range("const at");
    return _first[n];
  };
  value_type& front () {
    if (_first == nullptr)
      throw my::invalid_pointer("Обращение к нулевому указателю в non-const front()");
    return *_first;
  };
  const value_type& front () const {
    if (_first == nullptr)
      throw my::invalid_pointer("Обращение к нулевому указателю в const front()");
    return *_first;
  };
  value_type& back () {
    if (_first == nullptr)
      throw my::invalid_pointer("Обращение к нулевому указателю в non-const back()");
    return *_last;
  };
  const value_type& back () const {
    if (_first == nullptr)
      throw my::invalid_pointer("Обращение к нулевому указателю в non-const back()");
    return *_last;
  };
  value_type* data () {
    return _first;
  };
  const value_type* data () const {
    return _first;
  };
  /*Modifiers*/
  void assign (iterator first, iterator last) {
    _alloc.deallocate(_first, _end - _first + 1);
    for (iterator i = first; i != last; ++i)
      push_back(*i);
  };
  void assign (size_type n, const value_type& val) {
    if (_first == nullptr) {
        _first = _alloc.allocate(n + 1);
      for (size_type i = 0; i < n; ++i)
        _alloc.construct(_first + i, val);
      _last = _first + n - 1;
      _end = _last + 1;
      return;
    }
    if (n >= _end - _first + 1) {
      value_type *buff = _alloc.allocate(n + 1);
      for (size_type i = 0; i < _last - _first + 1; ++i)
        _alloc.construct(buff + i, _first[i]);
      for (size_type i = _last - _first + 1; i < n; ++i)
        _alloc.construct(buff + i, val);
      _alloc.deallocate(_first, _end - _first + 1);
      _first = buff;
      _last = _first + n - 1;
      _end = _last + 1;
    } else {
      _last = _first + n - 1;
      for (size_type i = 0; i < _last - _first + 1; ++i)
        _alloc.construct(_first + i, val);
      }
  };
  void assign (std::initializer_list<value_type> il) {
    if (_first != nullptr)
      _alloc.deallocate(_first, _end - _first + 1);
    _first = _last = _end = nullptr;
    if (!il.size())
      return;
    _first = _alloc.allocate(il.size() + 1);
    _last = _first;
    _end = _first + il.size();
    auto i = il.begin();
    _alloc.construct(_first, *i);
    ++i;
    for (; i != il.end(); ++i)
      push_back(*i);
  };
  void push_back (const value_type& val) {
    if (_first == nullptr) {
      _first = _alloc.allocate(2);
      _alloc.construct(_first, val);
      _last = _first;
      _end = _last + 1;
      _alloc.construct(_last, val);
      return;
    }
    if (_end - _last == 1) {
      size_type n = _last - _first + 1;
      value_type* buff = _alloc.allocate(2*n + 1);
      for (size_type i = 0; i < n; ++i)
        _alloc.construct(buff + i, _first[i]);
      _alloc.deallocate(_first, _end - _first + 1);
      _first = buff;
      _last = _first + n - 1;
      _end = _first + 2*n + 1;
    }
    ++_last;
    _alloc.construct(_last, val);
  };
  void pop_back () {
    if (_last == _first) {
      _last = value_type();
      return;
    }
    --_last;
  };
  iterator insert (iterator position, const value_type& val) {
    iterator it;
    size_type n = position - begin();
    if (n > _end - _first + 1)
      return end();
    if (n == _end - _first + 1) {
      push_back(val);
    } else
      _alloc.construct(_first + n, val);
    it._container = this;
    it._pointer = _first + n;
    return it;
  };
  void insert (iterator position, size_type n, const value_type& val) {
    size_type pos = position - begin();
    if (n + 1 > _end - _first + 1)
      return;
    if (n + pos + 1 >= _end - _first + 1) {
      value_type *buff = _alloc.allocate(n + pos + 2);
      for (size_type i = 0; i < pos; ++i)
        _alloc.construct(buff + i, _first[i]);
      for (size_type i = pos; i < n + pos + 1; ++i)
        _alloc.construct(buff + i, val);
      _alloc.deallocate(_first, _end - _first + 1);
      _first = buff;
      _last = _first + n + pos;
      _end = _first + n + pos + 1;
    } else {
      for (size_type i = pos; i < n + pos + 1; ++i)
        _alloc.construct(_first + i, val);
      if (n + pos > _last - _first)
        _last = _first + n + pos;
    }
  };
  void clear () {
    for (auto it = iter_list.begin(); it != iter_list.end(); ++it)
      (*it)->_pointer = nullptr;
    _alloc.deallocate(_first, _end - _first + 1);
    _first = _last = _end = nullptr;
  };
  iterator erase (iterator position) {
    iterator it;
    size_type pos = position - begin();
    if (pos > _last - _first)
      return end();
    if (_last == _first) {
      _alloc.destroy(_first);
      return end();
    }
    _last = _first + pos - 1;
    for (size_type i = pos; i < _last - _first + 1; ++i)
      _alloc.destroy(_first + i);
    it._container = this;
    it._pointer = _last;
    return it;
  };
  iterator erase (iterator first, iterator last) {
    iterator it;
    size_type pos = first - begin(),
              count = last - first;
    if (pos > _last - _first)
      return end();
    if (count + pos >= _last - _first)
      return erase(first);
    for (size_type i = pos; i < pos + count; ++i)
      _alloc.destroy(_first + i);
    for (size_type i = pos + count; i < _last - _first + 1; ++i)
      _alloc.construct(_first + i - count, _first[i]);
    _last = _last - count;

    it._container = this;
    it._pointer = _last;
    return it;
  };
private:
  value_type *_first, *_last, *_end;
  allocator_type _alloc;
  std::list<iterator*> iter_list;
};
#pragma pack(pop)


};
