/**
* Quick sort
*/
#pragma once

#include <functional>
#include <iterator>
#include <algorithm>
namespace my {
template <class Iterator, class Compare>
  void sort (Iterator first, Iterator last, Compare cmp) {
  if (first != last) {
    Iterator left  = first;
    Iterator right = last;
    Iterator pivot = left;
    ++left;
    while (left != right) {
      if (cmp(*left, *pivot)) {
         ++left;
      } else {
         while ((left != --right) && cmp(*pivot, *right));
         std::iter_swap(left, right);
      }
    }
    --left;
    std::iter_swap(first, left);
    my::sort(first, left, cmp);
    my::sort(right, last, cmp);
  }
};

template <class Iterator>
void sort (Iterator first, Iterator last) {
  if (first != last) {
    Iterator left  = first;
    Iterator right = last;
    Iterator pivot = left;
    ++left;
    while (left != right) {
      if (*left > *pivot) {
         ++left;
      } else {
         while ((left != --right) && (*pivot > *right));
         std::iter_swap(left, right);
      }
    }
    --left;
    std::iter_swap(first, left);
    my::sort(first, left);
    my::sort(right, last);
  }
};

};
