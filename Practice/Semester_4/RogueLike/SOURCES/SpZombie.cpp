#include "../HEADERS/Map.hpp"
#include "../HEADERS/SpZombie.hpp"
#include <cstdlib>

void SpZombie::spawn(Map &map) {
  int direction[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
  if (_overClock - _clock) {
    ++_clock;
    return;
  } else
    _clock = 0;

  char target = std::rand() % 4;
  for (char k = 0; k < 4; ++k) {
    Zombie *obj = new Zombie(_position.x() + direction[(target + k)%4][0], _position.y() + direction[(target + k)%4][1]);//<=
    if (!map.setChar((Actor *)obj)) delete obj;
    else {map.memChar(obj); break;}
  }
}
