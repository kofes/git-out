#include "../HEADERS/Zombie.hpp"
#include "../HEADERS/Map.hpp"
#include "../HEADERS/Stuff.hpp"

void Zombie::move (Map &map) {
  int direction[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
  if (_overClock - _clock) {
    ++_clock;
    return;
  } else
    _clock = 0;

  for (char target = 0; target < 4; ++target)
    if (map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol() == _DEF_KNIGHT_) {
    Character *ptr = (Character *)map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]];
    ptr->hit(_damage);
    return;
  }
  for (char target = 0; target < 4; ++target)
    if (map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol() == _DEF_MEDKIT_) {
    Stuff *ptr = (Stuff *)map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]];
    ptr->effect(this);
    return;
  }
  map.removeChar(this);

  char target = std::rand() % 4;
  for (int diff = 0; diff < _distance; ++diff) {
    _position.x(_position.x() + direction[target][0]);
    _position.y(_position.y() + direction[target][1]);
    if (map._buff[_position.x()][_position.y()]->symbol() != _DEF_FLOOR_) {
      _position.x(_position.x() - direction[target][0]);
      _position.y(_position.y() - direction[target][1]);
      break;
    }
  }

  map.setChar(this);
};
