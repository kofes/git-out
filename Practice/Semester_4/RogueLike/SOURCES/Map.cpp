#include "../HEADERS/Map.hpp"
#include "../HEADERS/IntVector2.hpp"
#include "../HEADERS/Fireball.hpp"
#include "../HEADERS/Monster.hpp"
#include "../HEADERS/Spawn.hpp"
#include <cstdlib>
#include <iostream>

void Map::display () {
  if (_buff != nullptr)
    for (int j = 0; j < _height; ++j) {
      for (int i = 0; i < _width; ++i) {
          if (_buff[i][j]->symbol() == _DEF_FIREBALL_BOTTOM_ ||
              _buff[i][j]->symbol() == _DEF_FIREBALL_TOP_ ||
              _buff[i][j]->symbol() == _DEF_FIREBALL_LEFT_ ||
              _buff[i][j]->symbol() == _DEF_FIREBALL_RIGHT_)
              addch(_buff[i][j]->symbol() | A_BOLD | COLOR_PAIR(1));
          else if (_buff[i][j]->symbol() == _DEF_MEDKIT_)
              addch(_buff[i][j]->symbol() | A_BOLD | COLOR_PAIR(2));
          else
            addch(_buff[i][j]->symbol() | A_NORMAL);
        }
        addch('\n' | A_NORMAL);
    }
};

bool Map::setChar (Actor *obj, bool PTR_TYPE) {
  if (obj == nullptr)
    return false;
  IntVector2 pos(obj->position());
  if (pos.x() > 0 && pos.y() > 0 && pos.x() < _width && pos.y() < _height &&
      (_buff[pos.x()][pos.y()]->symbol() == _DEF_FLOOR_ ||
      (_buff[pos.x()][pos.y()]->symbol() == _DEF_WALL_ &&
      (obj->symbol() == _DEF_KNIGHT_ || obj->symbol() == _DEF_PRINCESS_)))) {
      delete _buff[pos.x()][pos.y()];
      _buff[pos.x()][pos.y()] = obj;
      return true;
    }
  else if (PTR_TYPE == _DEF_SHARED_PTR_)
    delete obj;
  return false;
}

bool Map::memChar (Actor *obj, bool PTR_TYPE) {
  if (obj == nullptr)
    return false;

  if (obj->symbol() == _DEF_SP_DRAGON_ || obj->symbol() == _DEF_SP_ZOMBIE_) {
    spawns.push_front((Spawn *)obj);
    return true;
  }

  if (obj->symbol() == _DEF_DRAGON_ || obj->symbol() == _DEF_ZOMBIE_ ||
      obj->symbol() == _DEF_SORCERER_ || obj->symbol() == _DEF_KNIGHT_ ||
      obj->symbol() == _DEF_PRINCESS_) {
    characters.push_front((Character *)obj);
    return true;
  }

  if (obj->symbol() == _DEF_FIREBALL_BOTTOM_ || obj->symbol() == _DEF_FIREBALL_LEFT_ ||
      obj->symbol() == _DEF_FIREBALL_RIGHT_ || obj->symbol() == _DEF_FIREBALL_TOP_ ||
      obj->symbol() == _DEF_MEDKIT_) {
    stuff.push_front((Stuff *)obj);
    return true;
  }

  if (PTR_TYPE == _DEF_SHARED_PTR_)
    delete obj;

  return false;
}


bool Map::removeChar (Actor *obj) {
  IntVector2 pos(obj->position());
  if (obj != nullptr && pos.x() > 0 && pos.y() > 0 && pos.x() < _width && pos.y() < _height &&
      _buff[pos.x()][pos.y()] == obj) {
      _buff[pos.x()][pos.y()] = new Floor(pos.x(), pos.y());
      return true;
    }
  return false;
}

bool Map::Room::intersect(const Map::Room &src) const {
  return !(src.x >= (x + w) || x >= (src.x + src.w) ||
           src.y >= (y + h) || y >= (src.y + src.h));
}

void Map::clear () {
  for (int i = 0; i < _width; ++i)
    for (int j = 0; j < _height; ++j) {
      delete _buff[i][j];
      _buff[i][j] = new Wall(i, j);
    }
  stuff.clear();
  characters.clear();
  spawns.clear();
}

void Map::clean () {
  for (auto iter = characters.begin(); iter != characters.end(); ++iter)
    *iter = nullptr;
  for (auto iter = spawns.begin(); iter != spawns.end(); ++iter)
    *iter = nullptr;
  for (auto iter = stuff.begin(); iter != stuff.end(); ++iter)
    *iter = nullptr;
  for (int i = 0; i < _width; ++i){
    for (int j = 0; j < _height; ++j) {
      // if (_buff[i][j]->symbol() != _DEF_DRAGON_ || _buff[i][j]->symbol() != _DEF_ZOMBIE_)
      delete _buff[i][j];
      _buff[i][j] = nullptr;
    }
    delete[] _buff[i];
    _buff[i] = nullptr;
  }
  delete[] _buff;
  _buff = nullptr;
  _width = _height = 0;
  stuff.clear();
  characters.clear();
  spawns.clear();
}

void Map::resize (int width, int height) {
  if (_buff != nullptr) {
    for (int i = 0; i < _width; ++i) {
      for (int j = 0; j < _height; ++j)
        delete _buff[i][j];
      delete[] _buff[i];
    }
    delete[] _buff;
  }
  _buff = new Actor **[width];
  for (int i = 0; i < width; ++i)
    _buff[i] = new Actor *[height];
  _width = width;
  _height = height;
}

void Map::generateRooms (int countRooms) {
  this->clear();
  std::list<Room> rooms;
  for (int i = 0; i < countRooms; ++i)
    for (int j = 0; j < 100; ++j) {
      int w = 3 + std::rand() % 12;
      int h = 3 + std::rand() % 12;
      Room room;
      room.x = 1 + std::rand() % (_width - w - 6 > 0 ? _width - w - 6 : 1);
      room.y = 1 + std::rand() % (_height - h - 6 > 0 ? _height - h - 6 : 1);
      room.w = w;
      room.h = h;
      bool end = false;
      for (auto iter : rooms)
        if (room.intersect(iter)) {
          end = true;
          break;
        }
      if (!end) {
        rooms.push_back(room);
        break;
      }
    }
  for (auto room : rooms)
    for (int x = 0; (x < room.w)&&(x < _width - room.x - 1); ++x)
      for (int y = 0; (y < room.h)&&(y < _height - room.y - 1); ++y) {
        delete _buff[room.x + x][room.y + y];
        _buff[room.x + x][room.y + y] = new Floor(room.x + x, room.y + y);
      }
  rooms.clear();
}

void Map::generatePassage(IntVector2 start, IntVector2 finish) {
  int buff[_width][_height];
  int direction[4][2] = {{1,0}, {0,1}, {-1,0}, {0,-1}};
  int new_x = start.x(), new_y = start.y();

  for (int i = 0; i < _width; ++i)
    for (int j = 0; j < _height; ++j)
      buff[i][j] = -1;

  int d = buff[start.x()][start.y()] = 0;
  bool find = false;

  while (!find) {
    for (int i = 1; i < (_width - 2); ++i) {
      for (int j = 1; j < (_height - 2); ++j) {
        if (buff[i][j] == d) {
          new_x = i; new_y = j;
          for (char k = 0; k < 4; ++k) {
            if (buff[new_x + direction[k][0]][new_y + direction[k][1]] == -1) {
              buff[new_x + direction[k][0]][new_y + direction[k][1]] = d + 1;
              //Если пришли в конечную точку
              if (((new_x + direction[k][0]) == finish.x()) && ((new_y + direction[k][1]) == finish.y())) {
                find = true;
                break;
              }
            }
          }
        }
        if (find) break;
      }
      if (find) break;
    }
    ++d;
  }
  // return;
  new_x = finish.x(); new_y = finish.y();
  d = buff[finish.x()][finish.y()];
  while (1) {
    if (!d) break;

    for (int k = 0; k < 4; ++k)
      if ((new_x + direction[k][0] > 0) &&
      (new_y + direction[k][1] > 0) &&
      (buff[new_x + direction[k][0]][new_y + direction[k][1]] == d - 1)) {
        --d;
        new_x += direction[k][0];
        new_y += direction[k][1];

        if (_buff[new_x][new_y]->symbol() == '#') {
          delete _buff[new_x][new_y];
          _buff[new_x][new_y] = new Floor(new_x, new_y);
        }
        continue;
      }
  }
}
