#include "../HEADERS/Map.hpp"
#include "../HEADERS/SpDragon.hpp"
#include <cstdlib>

void SpDragon::spawn(Map &map) {
  int direction[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
  if (_overClock - _clock) {
    ++_clock;
    return;
  } else
    _clock = 0;

  char target = std::rand() % 4;
  for (char k = 0; k < 4; ++k) {
    Dragon *obj = new Dragon(_position.x() + direction[(target + k)%4][0], _position.y() + direction[(target + k)%4][1]);//<=
    if (!map.setChar((Actor *)obj))delete obj;
    else {map.memChar(obj); break;}
  }
}
