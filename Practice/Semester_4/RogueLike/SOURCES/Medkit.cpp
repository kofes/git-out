#include "../HEADERS/Medkit.hpp"
#include "../HEADERS/Character.hpp"
#include <cstdlib>

void Medkit::effect (Character *src) {
  src->hit(-_hp);
  _exist = _DEF_NOT_EXIST_;
}

void Medkit::set (Map &map) {
  if (_exist == _DEF_NOT_EXIST_) {
    map.removeChar(this);
    if (_overClock - _clock)
      ++_clock;
    else
      for (int i = 0; i < 1000; ++i) {
        _position.x(std::rand() % map.width());
        _position.y(std::rand() % map.height());
        if (map.setChar(this)) {
          _exist = _DEF_EXIST_;
          _clock = 0;
          break;
        }
      }
  }
}
