#include "../HEADERS/Knight.hpp"
#include "../HEADERS/Stuff.hpp"
#include "../HEADERS/Princess.hpp"
#include "../HEADERS/Zombie.hpp"
#include "../HEADERS/Dragon.hpp"
#include "../HEADERS/Sorcerer.hpp"
#include "../HEADERS/Map.hpp"

bool Knight::_win = _DEF_NOT_WIN_;

void Knight::move (Map &map) {
  char movement = 0;
  bool active = false;
  if (_hitPoints <= 0) return;
  int direction[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
  int dist;
  char sym;
  char k;
  do {
    k = -1;
    sym = getch();
         if (sym == 'd' || sym == 'D') k = 0;
    else if (sym == 's' || sym == 'S') k = 1;
    else if (sym == 'a' || sym == 'A') k = 2;
    else if (sym == 'w' || sym == 'W') k = 3;
    else if (sym == 'q' || sym == 'Q') {_win = _DEF_LOSE_; active = true;}
    else if (sym == 'f' || sym == 'F') {
      for (char target = 0; target < 4; ++target)
        if (map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol() == _DEF_PRINCESS_)
          {_win = true; active = true; break;}
      if (!active)
        for (char target = 0; target < 4; ++target){
          char sym = map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol();
          if (sym == _DEF_ZOMBIE_ || sym == _DEF_DRAGON_ || sym == _DEF_SORCERER_) {
            Character *ptr =
            (Character *)map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]];
            ptr->hit(_damage);
            active = true;
            break;
          }
        }
      if (!active)
        for (char target = 0; target < 4; ++target)
          if (map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol() == _DEF_MEDKIT_) {
            Stuff *med = (Stuff *)map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]];
            med->effect(this);
          }
      active = true;
    }
    if (k >= 0) {
      map.removeChar(this);
      for (dist = 0; (dist < _distance)&&(_position.y() + dist < map._height - 1); ++dist)
        if (map._buff[_position.x() + (dist + 1)*direction[k][0]][_position.y() + (dist + 1)*direction[k][1]]->symbol() != _DEF_FLOOR_)
          break;
      _position.x(_position.x() + dist*direction[k][0]);
      _position.y(_position.y() + dist*direction[k][1]);
      active = true;
      map.setChar(this);
    }
  } while (!active);
};
