#include "../HEADERS/Sorcerer.hpp"
#include "../HEADERS/Map.hpp"
#include "../HEADERS/Stuff.hpp"

void Sorcerer::move (Map &map) {
  int direction[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
  if (_overClock - _clock) {
    ++_clock;
    return;
  } else
    _clock = 0;

  for (char target = 0; target < 4; ++target)
    if (map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol() == _DEF_KNIGHT_) {
      Character *ptr = (Character *)map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]];
      ptr->hit(_damage);
      return;
    }

  for (char target = 0; target < 4; ++target)
    for (int dist = 0; dist < _range; ++dist) {
      char sym = map._buff[_position.x() + (dist + 1)*direction[target][0]][_position.y() + (dist + 1)*direction[target][1]]->symbol();
      if (sym == _DEF_WALL_ || sym == _DEF_SP_DRAGON_ || sym == _DEF_SP_ZOMBIE_)
        break;
      if (sym == _DEF_KNIGHT_) {
        char TAR;
             if (target == 0) TAR = _DEF_FIREBALL_RIGHT_;
        else if (target == 1) TAR = _DEF_FIREBALL_BOTTOM_;
        else if (target == 2) TAR = _DEF_FIREBALL_LEFT_;
        else                  TAR = _DEF_FIREBALL_TOP_;
        Fireball *fb = new Fireball(_position.x() + direction[target][0], _position.y() + direction[target][1], TAR, _fireballDamage);
        if (!map.setChar(fb)) delete fb;
        else {
          map.memChar(fb);
          return;
        }
      }
    }
  for (char target = 0; target < 4; ++target)
    if (map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]]->symbol() == _DEF_MEDKIT_) {
    Stuff *ptr = (Stuff *)map._buff[_position.x() + direction[target][0]][_position.y() + direction[target][1]];
    ptr->effect(this);
    return;
  }

  map.removeChar(this);

  char target = std::rand() % 4;
  for (int diff = 0; diff < _distance; ++diff) {
    _position.x(_position.x() + direction[target][0]);
    _position.y(_position.y() + direction[target][1]);
    if (map._buff[_position.x()][_position.y()]->symbol() != _DEF_FLOOR_) {
      _position.x(_position.x() - direction[target][0]);
      _position.y(_position.y() - direction[target][1]);
      break;
    }
  }

  map.setChar(this);
};
