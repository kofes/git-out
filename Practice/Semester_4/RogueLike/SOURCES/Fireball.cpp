#include "../HEADERS/Fireball.hpp"
#include "../HEADERS/Character.hpp"

void Fireball::set (Map &map) {
  char TAR[2];
       if (_target == _DEF_FIREBALL_TOP_)   {TAR[0] =  0; TAR[1] = -1;}
  else if (_target == _DEF_FIREBALL_RIGHT_) {TAR[0] =  1; TAR[1] =  0;}
  else if (_target == _DEF_FIREBALL_BOTTOM_){TAR[0] =  0; TAR[1] =  1;}
  else if (_target == _DEF_FIREBALL_LEFT_)  {TAR[0] = -1; TAR[1] =  0;}

  map.removeChar(this);

  char sym = map._buff[_position.x() + TAR[0]][_position.y() + TAR[1]]->symbol();

  if (sym == _DEF_WALL_ || sym == _DEF_MEDKIT_ || sym == _DEF_SP_DRAGON_ || sym == _DEF_SP_ZOMBIE_) {
    _exist = _DEF_NOT_EXIST_;
    return;
  }

  if (sym == _DEF_KNIGHT_ || sym == _DEF_ZOMBIE_ || sym == _DEF_DRAGON_ || sym == _DEF_SORCERER_) {
    effect((Character *)map._buff[_position.x() + TAR[0]][_position.y() + TAR[1]]);
    _exist = _DEF_NOT_EXIST_;
    return;
  }
  _position.x(_position.x() + TAR[0]);
  _position.y(_position.y() + TAR[1]);

  map.setChar(this);
}

void Fireball::effect (Character *src) {
  src->hit(_hit);
}
