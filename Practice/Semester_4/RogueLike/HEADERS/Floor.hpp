#pragma once

#include "Actor.hpp"

#pragma pack(push, 1)
class Floor : public Actor {
public:
  Floor (int X = 0, int Y = 0) {
    _position.x(X);
    _position.y(Y);
  };
  char symbol () {return _DEF_FLOOR_;};
  virtual ~Floor () {};
};
#pragma pack(pop)
