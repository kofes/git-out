#pragma once

#include "Spawn.hpp"
#include "Dragon.hpp"

#pragma pack(push, 1)
class SpDragon : public Spawn {
public:
  SpDragon (int X = 0, int Y = 0, int overClock = 10) {
    _position.x(X);
    _position.y(Y);
    _overClock = overClock;
    _clock = 0;
  };
  char symbol () {return _DEF_SP_DRAGON_;};
  void spawn(Map &map);
  virtual ~SpDragon () {};
};
#pragma pack(pop)
