#pragma once

#include "Monster.hpp"

#pragma pack(push, 1)
class Zombie : public Monster {
public:
  Zombie (int X = 0, int Y = 0, int hitPoints = 2, int damage = 1, int distance = 1, int overClock = 1, int range = 2) {
    _position.x(X);
    _position.y(Y);
    _hitPoints = hitPoints;
    _damage = damage;
    _distance = distance;
    _overClock = overClock;
    _range = range;
    _clock = 0;
  };
  char symbol () override {return _DEF_ZOMBIE_;};
  void move (Map &map) override;
  virtual ~Zombie () {}
};
#pragma pack(pop)
