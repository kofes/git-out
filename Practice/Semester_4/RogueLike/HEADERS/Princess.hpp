#pragma once

#include "Character.hpp"

#pragma pack(push, 1)
class Princess : public Character {
public:
  Princess(int X = 0, int Y = 0, int hitPoints = 1, int damage = 0, int distance = 0) {
    _position.x(X);
    _position.y(Y);
    _hitPoints = hitPoints;
    _damage = damage;
    _distance = distance;
  };
  char symbol () override {return _DEF_PRINCESS_;};
  void move (Map &map) override {};
  virtual ~Princess () {}
};
#pragma pack(pop)
