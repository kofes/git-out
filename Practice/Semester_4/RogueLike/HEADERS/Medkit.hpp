#pragma once

#include "Stuff.hpp"
#include "Map.hpp"

#pragma pack(push, 1)
class Medkit : public Stuff {
public:
  Medkit (int X = 0, int Y = 0, int hp = 1, int overClock = 3) : _hp(hp) {
    _position.x(X);
    _position.y(Y);
    if (X == Y && X == 0)
      _exist = _DEF_NOT_EXIST_;
    _overClock = overClock;
    _clock = 0;
  };
  Medkit (int hp, IntVector2 &src) : _hp(hp) {
    _position = src;
  };
  void set (Map &map);
  bool exist () {return _DEF_EXIST_;};
  char symbol () {return _DEF_MEDKIT_;};
  void effect (Character *src);
  virtual ~Medkit () {};
private:
  int _hp;
  int _clock, _overClock;
};
#pragma pack(pop)
