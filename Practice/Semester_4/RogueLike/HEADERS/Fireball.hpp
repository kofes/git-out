#pragma once

#include "Stuff.hpp"
#include "Map.hpp"

#pragma pack(push, 1)
class Fireball : public Stuff {
public:
  Fireball (int X = 0, int Y = 0, char target = '<', int hit = 1) {
    _hit = hit;
    _target = target;
    _position.x(X);
    _position.y(Y);
  };
  void target(char target) {_target = target;};
  char symbol () {return _target;};
  void effect (Character *);
  void set (Map &map);
  ~Fireball () {};
private:
  char _target;
  int _hit;
};
#pragma pack(pop)
