#pragma once

#include "Actor.hpp"
class Map;

#pragma pack(push, 1)
class Spawn : public Actor {
public:
  virtual void spawn(Map &map) = 0;
  virtual ~Spawn () {};
protected:
  int _clock;
  int _overClock;
};
#pragma pack(pop)
