#pragma once

#include "Actor.hpp"

#pragma pack(push, 1)
class Unknown : public Actor {
public:
  Unknown (int X = 0, int Y = 0) {
    _position.x(X);
    _position.y(Y);
  };
  char symbol () {return _DEF_UNKNOWN_;};
  virtual ~Unknown () {};
};
#pragma pack(pop)
