#pragma once

#include "Actor.hpp"

#pragma pack(push, 1)
class Wall : public Actor {
public:
  Wall (int X = 0, int Y = 0) {
    _position.x(X);
    _position.y(Y);
  };
  char symbol () {return _DEF_WALL_;};
  virtual ~Wall () {};
};
#pragma pack(pop)
