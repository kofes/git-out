#pragma once

#include "Actor.hpp"

class Map;
class Character;

/*is killed/used object*/
#define _DEF_EXIST_           true
#define _DEF_NOT_EXIST_       false


#pragma pack(push, 1)
class Stuff : public Actor {
public:
  Stuff () : _exist(_DEF_EXIST_) {};
  virtual void effect (Character *) = 0;
  virtual void set (Map &map) = 0;
  virtual bool exist () {return _exist;};
  virtual ~Stuff () {};
protected:
  bool _exist;
};
#pragma pack(pop)
