#pragma once

#include "Spawn.hpp"
#include "Zombie.hpp"

#pragma pack(push, 1)
class SpZombie : public Spawn {
public:
  SpZombie (int X = 0, int Y = 0, int overClock = 8) {
    _position.x(X);
    _position.y(Y);
    _overClock = overClock;
    _clock = 0;
  };
  char symbol () {return _DEF_SP_ZOMBIE_;};
  void spawn(Map &map);
  virtual ~SpZombie () {};
};
#pragma pack(pop)
