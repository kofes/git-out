#pragma once

#include "Monster.hpp"
#include "Fireball.hpp"

#pragma pack(push, 1)
class Sorcerer : public Monster {
public:
  Sorcerer (int X = 0, int Y = 0, int hitPoints = 1, int damage = 1, int distance = 1, int overClock = 1, int fireballDamage = 1, int range = 5) {
    _position.x(X);
    _position.y(Y);
    _hitPoints = hitPoints;
    _damage = damage;
    _distance = distance;
    _overClock = overClock;
    _range = range;
    _clock = 0;
    _fireballDamage = fireballDamage;
  };
  char symbol () override {return _DEF_SORCERER_;};
  void move (Map &map) override;
  virtual ~Sorcerer () {};
private:
  int _fireballDamage;
};
#pragma pack(pop)
