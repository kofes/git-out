#pragma once
//Class of all object's classes

#include "IntVector2.hpp"

/*BASIC*/
#define _DEF_UNKNOWN_         '"'
#define _DEF_FLOOR_           '.'
#define _DEF_WALL_            '#'

/*STUFF*/
#define _DEF_MEDKIT_          '+'
#define _DEF_FIREBALL_TOP_    '^'
#define _DEF_FIREBALL_LEFT_   '<'
#define _DEF_FIREBALL_RIGHT_  '>'
#define _DEF_FIREBALL_BOTTOM_ 'v'

/*CHARACTERS*/
#define _DEF_KNIGHT_          'K'
#define _DEF_PRINCESS_        'P'
#define _DEF_ZOMBIE_          'Z'
#define _DEF_DRAGON_          'D'
#define _DEF_SORCERER_        'S'

/*SPAWNS*/
#define _DEF_SP_DRAGON_       '$'
#define _DEF_SP_ZOMBIE_       '@'

/*TYPE_PTR (in Map)*/
#define _DEF_SHARED_PTR_      true
#define _DEF_WEAK_PTR_        false

/*TYPE_WIN (in Knight)*/
#define _DEF_NOT_WIN_         0
#define _DEF_WIN_             1
#define _DEF_LOSE_            2

#pragma pack(push, 1)
class Actor {
public:
  virtual char symbol () = 0;
  IntVector2 position () {return _position;};
  IntVector2 position (IntVector2 &pos) {return (_position = pos);}
  virtual ~Actor () {};
protected:
  IntVector2 _position;
};
#pragma pack(pop)
