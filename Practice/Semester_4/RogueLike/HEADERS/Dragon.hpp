#pragma once

#include "Monster.hpp"

#pragma pack(push, 1)
class Dragon : public Monster {
public:
  Dragon (int X = 0, int Y = 0, int hitPoints = 6, int damage = 3, int distance = 2, int overClock = 1, int range = 4) {
    _position.x(X);
    _position.y(Y);
    _hitPoints = hitPoints;
    _damage = damage;
    _distance = distance;
    _overClock = overClock;
    _range = range;
    _clock = 0;
  };
  char symbol () {return _DEF_DRAGON_;};
  void move (Map &map);
  virtual ~Dragon () {};
};
#pragma pack(pop)
