#pragma once

#include "Actor.hpp"

class Map;
class Medkit;

#pragma pack(push, 1)
class Character : public Actor {
public:
  virtual void move (Map &map) = 0;                                       //Действия персонажа
  virtual int hitPoints () {return ((_hitPoints > 0) ? _hitPoints : 0);}; //Размер здоровья
  virtual int damage () {return _damage;};                                //Размер урона
  virtual void hit (int hit) {_hitPoints -= hit;};                        //Получение урона
  virtual ~Character () {};
protected:
  int _hitPoints;                                                         //Количество здоровья
  int _damage;                                                            //Размер урона
  int _distance;                                                          //Расстояние за ход
  int _range;                                                             //Дальность видимости
};
#pragma pack(pop)
