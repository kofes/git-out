#pragma once

#include "Character.hpp"
#include <cstdlib>

#pragma pack(push, 1)
class Monster : public Character {
public:
  virtual ~Monster() {};
protected:
  int _overClock;   //Ограничение на ход
  int _clock;       //Сколько ходов пропущено
};
#pragma pack(pop)
