#pragma once

#include <ncurses.h>
#include <list>
#include "Actor.hpp"
#include "Wall.hpp"
#include "Floor.hpp"
#include "Unknown.hpp"
#include <iostream>//<<<<<<<<<<<<<<<<<<<<<<<<<
class Dragon;
class Zombie;
class Knight;
class Princess;
class Spawn;
class Monster;
class Sorcerer;
class Fireball;
class Character;
class Stuff;
/**
* true - успешное выполнение функции
* false - !"true"
*/

#pragma pack(push, 1)
class Map {
public:
  struct Room {
    int x, y, w, h;
    bool intersect (const Room &src) const;
  };
  struct Point {
    int x, y;
    Point (int x, int y) : x(x), y(y) {};
    bool operator== (const Point &p) const {
        return x == p.x && y == p.y;
    }
  };
  Map (int width, int height, int countRooms = 300) : _width(width), _height(height) {
    stuff.clear();
    characters.clear();
    spawns.clear();
    _buff = new Actor **[width];
    for (int i = 0; i < width; ++i)
      _buff[i] = new Actor *[height];
    for (int i = 0; i < width; ++i)
      for (int j = 0; j < height; ++j) {
        _buff[i][j] = new Wall(i, j);
      }
    generateRooms(countRooms);
  };
  void clear();
  void clean();
  void resize(int width, int height);
  void generateRooms (int countRooms);
  void generatePassage (IntVector2 start, IntVector2 finish);
  void display ();
  inline int width () {return _width; }
  inline int height () {return _height; }

  bool setChar (Actor *, bool PTR_TYPE = _DEF_WEAK_PTR_);
  bool memChar (Actor *, bool PTR_TYPE = _DEF_WEAK_PTR_);
  bool removeChar (Actor *);

  std::list<Character *> characters;
  std::list<Spawn *> spawns;
  std::list<Stuff *> stuff;
private:
  friend class Dragon;
  friend class Zombie;
  friend class Knight;
  friend class Princess;
  friend class Sorcerer;
  friend class Fireball;
  int _width, _height;
  Actor ***_buff;
};
#pragma pack(pop)
