#include "Map.hpp"
#include "Knight.hpp"
#include "Princess.hpp"
#include "Zombie.hpp"
#include "Dragon.hpp"
#include "Medkit.hpp"
#include "SpZombie.hpp"
#include "SpDragon.hpp"
#include "Sorcerer.hpp"
#include <ncurses.h>
#include <list>
#include <ctime>
#include <cstdlib>
#include "../Parser/Config.hpp"


int game (const std::string& configName) {
// /*
  //Init ncurses
  initscr();
  if (has_colors()) {
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_MAGENTA, COLOR_BLACK);
  }
  clear();
  noecho();
  cbreak();
  keypad(stdscr, TRUE);
  curs_set(0);
  //
// */
  std::srand(std::time(NULL));
  config::Container src(configName);
  if (config::Container::getErrMsg() != "") {
    printw((config::Container::getErrMsg() + '\n').c_str());
    printw((config::Container::getLastLine() + '\n').c_str());
    printw("%d\n", config::Container::getLastNumLine());
    getch();
    clear();
  }
  auto w = src["Map"]["width"];
  auto h = src["Map"]["height"];

  if (w < 20) w = 20;
  if (h < 20) h = 20;
  if (w > getmaxx(stdscr)) w = getmaxx(stdscr);
  if (h > getmaxy(stdscr) - 1) h = getmaxy(stdscr) - 1;
  Map MAP(w, h);

  for (int i = 0; i < src["Zombie"]["count"]; ++i) {
    Monster *monster;
    monster = new Zombie(std::rand() % MAP.width(), std::rand() % MAP.height(), src["Zombie"]["hitPoints"], src["Zombie"]["damage"], src["Zombie"]["distance"], src["Zombie"]["overClock"]);
    if (!MAP.setChar(monster)) delete monster;
    else MAP.memChar(monster);
  }

  for (int i = 0; i < src["Dragon"]["count"]; ++i) {
    Monster *monster;
    monster = new Dragon(std::rand() % MAP.width(), std::rand() % MAP.height(), src["Dragon"]["hitPoints"], src["Dragon"]["damage"], src["Dragon"]["distance"], src["Dragon"]["overClock"]);
    if (!MAP.setChar(monster)) delete monster;
    else MAP.memChar(monster);
  }

  for (int i = 0; i < src["Sorcerer"]["count"]; ++i) {
    Monster *monster;
    monster = new Sorcerer(std::rand() % MAP.width(), std::rand() % MAP.height(), src["Sorcerer"]["hitPoints"], src["Sorcerer"]["damage"], src["Sorcerer"]["distance"], src["Sorcerer"]["overClock"], src["Sorcerer"]["fireballDamage"]);
    if (!MAP.setChar(monster)) delete monster;
    else MAP.memChar(monster);
  }

  for (int i = 0; i < src["SpawnZombie"]["count"]; ++i) {
    Spawn *spawn;
      spawn = new SpZombie(std::rand() % MAP.width(), std::rand() % MAP.height(), src["SpawnZombie"]["overClock"]);
    if (!MAP.setChar(spawn)) delete spawn;
    else MAP.memChar(spawn);
  }

  for (int i = 0; i < src["SpawnDragon"]["count"]; ++i) {
    Spawn *spawn;
      spawn = new SpDragon(std::rand() % MAP.width(), std::rand() % MAP.height(), src["SpawnDragon"]["overClock"]);
    if (!MAP.setChar(spawn)) delete spawn;
    else MAP.memChar(spawn);
  }

  Princess *princess = new Princess(MAP.width() - 5, MAP.height() - 5, src["Princess"]["hitPoints"]);
    while (!MAP.setChar(princess)) {
      princess->position().x(std::rand() % MAP.width());
      princess->position().y(std::rand() % MAP.height());
    }
  MAP.memChar(princess);

  Knight *knight = new Knight(3, 3, src["Knight"]["hitPoints"], src["Knight"]["damage"], src["Knight"]["distance"]);
    while (!MAP.setChar(knight)) {
      knight->position().x(std::rand() % MAP.width());
      knight->position().y(std::rand() % MAP.height());
    }
  MAP.memChar(knight);

  MAP.memChar(new Medkit(0, 0, src["Medkit"]["hp"], src["Medkit"]["overClock"]), _DEF_SHARED_PTR_);

  MAP.generatePassage(knight->position(), princess->position());

  printw("Let's begin...");
  getch();
  while (Knight::win() == _DEF_NOT_WIN_) {
    clear();
    for (auto iter = MAP.stuff.begin(); iter != MAP.stuff.end(); ++iter) {
      (*iter)->set(MAP);
      if ((*iter)->exist() == _DEF_NOT_EXIST_) {
        MAP.removeChar(*iter);
        Stuff *ptr = *iter;
        --iter;
        MAP.stuff.remove(ptr);
      }
    }
    MAP.display();
    printw("HEIGHT: %d, WIDTH: %d. HP: %d", MAP.height(), MAP.width(), knight->hitPoints());
    for (auto iter = MAP.characters.begin(); iter != MAP.characters.end(); ++iter) {
      if (!(*iter)->hitPoints()) {
        MAP.removeChar(*iter);
        Character *ptr = *iter;
        --iter;
        MAP.characters.remove(ptr);
        continue;
      }
      (*iter)->move(MAP);
    }
    for (auto iter = MAP.spawns.begin(); iter != MAP.spawns.end(); ++iter)
      (*iter)->spawn(MAP);
  }
  clear();
  if (Knight::win() == _DEF_WIN_)
    mvwprintw(stdscr, MAP.height() / 2, (MAP.width() - 8) / 2, "YOU WIN!");
  else
    mvwprintw(stdscr, MAP.height() / 2, (MAP.width() - 10) / 2, "GAME OVER!");
  getch();
  endwin();
  MAP.clean();
  return 0;
}
