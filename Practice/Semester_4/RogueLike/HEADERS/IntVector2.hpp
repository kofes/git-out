#pragma once

#pragma pack(push, 1)
class IntVector2 {
public:
  IntVector2 (int X = 0, int Y = 0) : _x(X), _y(Y) {};
  IntVector2 (const IntVector2 &src) {
    _x = src._x;
    _y = src._y;
  }
  IntVector2 &operator= (const IntVector2 &src) {
    _x = src._x;
    _y = src._y;
    return *this;
  }
  int x () {return _x;};
  int x (int X) {return (_x = X);};
  int y () {return _y;};
  int y (int Y) {return (_y = Y);};
  IntVector2 operator+ (const IntVector2 &src) {
    _x += src._x;
    _y += src._y;
    return *this;
  }
  IntVector2 operator- (const IntVector2 &src) {
    _x -= src._x;
    _y -= src._y;
    return *this;
  }

  IntVector2 operator* (long long src) {
    _x *= src;
    _y *= src;
    return *this;
  }

  IntVector2 operator/ (long long src) {
    if (src <= 0)
      return IntVector2();
    _x /= src;
    _y /= src;
    return *this;
  }

  bool operator== (const IntVector2 &src) {
    return (_x == src._x)&&(_y == src._y);
  }

  bool operator!= (const IntVector2 &src) {
    return (_x != src._x)||(_y != src._y);
  }

  ~IntVector2 () {_x = 0, _y = 0;};
private:
  int _x, _y;
};
#pragma pack(pop)
