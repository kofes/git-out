#pragma once

#include "Character.hpp"

#pragma pack(push, 1)
class Knight : public Character {
public:
  Knight (int X = 0, int Y = 0, int hitPoints = 6, int damage = 2, int distance = 1, int range = 3) {
    _position.x(X);
    _position.y(Y);
    _hitPoints = hitPoints;
    _damage = damage;
    _distance = distance;
    _range = range;
  };
  char symbol () override {return _DEF_KNIGHT_;};
  void move (Map &map) override;
  int hitPoints () override {
    if (_hitPoints > 0)
      return _hitPoints;
    _win = _DEF_LOSE_;
    return 0;
  };
  static bool win () {return _win;};
  virtual ~Knight () {};
private:
  static bool _win;
};
#pragma pack(pop)
/**
int hitPoints; //Количество здоровья
int damage;    //Размер урона
int _distance;  //Расстояние за ход
int range;     //Дальность видимости
*/
