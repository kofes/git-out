#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <climits>

namespace config {
class Container {
public:
  enum class State {
    ERROR = -1,
    START_READ = 0,
    READ_HEAD = 1,
    READ_OPTION = 2,
  };
  Container (const std::string& fileName) {
    std::fstream file(fileName.c_str());
    initContainer();
    if (!file.is_open()) {
      errorLine = "Can't open " + fileName;
      lastLine = "";
      lastNumLine = 0;
      return;
    }
    State q = State::START_READ;
    size_t pos = 0;
    std::string name;
    lastLine = "";
    while (true) {
      switch (q) {
      case (State::START_READ) ://Start read
        if (!std::getline(file, lastLine)) return;
        ++lastNumLine;
        q = State::READ_HEAD;
        break;
      case (State::READ_HEAD) ://Main Name
        q = readMainName(lastLine, name);
        break;
      case (State::READ_OPTION) ://Options
        if (!std::getline(file, lastLine)) return;
        ++lastNumLine;
        q = readOption(lastLine, name, pos);
        break;
      default:
        initContainer();
        return;
      }
    }
  };
  std::map<std::string, int>& operator[] (const std::string& line) {
    if (container.find(line) == container.end())
      return container["Empty"];
    return container[line];
  }
  void clean () {
    container.clear();
  }
  static const std::string& getErrMsg () {
    return errorLine;
  }
  static const std::string& getLastLine () {
    return lastLine;
  }
  static unsigned int getLastNumLine () {
    return lastNumLine;
  }
private:
  void initContainer () {
    /*Map*/
    container["Map"]["width"] = 20;
    container["Map"]["height"] = 20;
    /*Knight*/
    container["Knight"]["hitPoints"] = 6;
    container["Knight"]["damage"] = 2;
    container["Knight"]["distance"] = 1;
    /*Princess*/
    container["Princess"]["hitPoints"] = 1;
    /*Zombie*/
    container["Zombie"]["count"] = 2;
    container["Zombie"]["hitPoints"] = 4;
    container["Zombie"]["damage"] = 2;
    container["Zombie"]["distance"] = 2;
    container["Zombie"]["overClock"] = 1;
    /*Dragon*/
    container["Dragon"]["count"] = 4;
    container["Dragon"]["hitPoints"] = 2;
    container["Dragon"]["damage"] = 1;
    container["Dragon"]["distance"] = 1;
    container["Dragon"]["overClock"] = 0;
    /*Sorcerer*/
    container["Sorcerer"]["count"] = 2;
    container["Sorcerer"]["hitPoints"] = 1;
    container["Sorcerer"]["damage"] = 1;
    container["Sorcerer"]["distance"] = 1;
    container["Sorcerer"]["overClock"] = 0;
    container["Sorcerer"]["fireballDamage"] = 1;
    /*SpawnZombies*/
    container["SpawnZombie"]["count"] = 2;
    container["SpawnZombie"]["overClock"] = 7;
    /*SpawnDragons*/
    container["SpawnDragon"]["count"] = 1;
    container["SpawnDragon"]["overClock"] = 15;
    /*Medkit*/
    container["Medkit"]["hp"] = 1;
    container["Medkit"]["overClock"] = 3;
  }
  State readMainName (const std::string& line, std::string& str) {
    if (line[0] == '#' || line[0] == '\n' || line[0] == '\0')
      return State::START_READ;
    str = "";
    str.append(line.c_str(), (line.find(':') == std::string::npos) ? line.size() : line.find(':'));
    if (container.find(str) == container.end()) {
      errorLine = "Wrong string";
      return State::ERROR;
    }
    return State::READ_OPTION;
  }
  State readOption (const std::string& line, const std::string& name, size_t& pos) {
    if (line == "\n") return State::START_READ;
    if (line[0] == '#') return State::READ_OPTION;
    if (line[0] == '\t' || line[0] == ' ') {
      for (auto iter = container[name].begin(); iter != container[name].end(); ++iter) {
        int i = 0;
        for (; ((i + 1) < line.size()) && (i < (*iter).first.size()); ++i)
          if (line[i + 1] != (*iter).first[i]) {
            break;
          }
        if (i == (*iter).first.size()) {
          pos = i + 1;
          int num = 0;
          readNum(line, pos, num);
          if (num >= 0) {
            (*iter).second = num;
            return State::READ_OPTION;
          } else
            return State::ERROR;
        }
      }
    }
    return State::READ_HEAD;
  }
  void readNum(const std::string& str, size_t pos, int& num) {
    for (; (pos < str.size()); ++pos)
      if (str[pos] == '=') {
        ++pos;
        break;
      } else if (str[pos] != ' ' && str[pos] != '\t') {
        errorLine = std::string("Wrong symbol: ") + str[pos];
        num = -1;
        return;
      }
    if (pos == str.size()) {
      errorLine = "Unexpected end of line";
      num = -1;
      return;
    }
    for (; pos < str.size(); ++pos)
      if (str[pos] == ' ' || str[pos] == '\t')
        continue;
      else if ('0' <= str[pos] && str[pos] <= '9')
        break;
      else {
        errorLine = std::string("Wrong symbol: ") + str[pos];
        num = -1;
        return;
      }
    if (pos == str.size()) {
      errorLine = "Unexpected end of line";
      num = -1;
      return;
    }
    num = 0;
    for (; (pos < str.size() && str[pos] != '#'); ++pos)
      if (str[pos] == ' ') break;
      else if ('0' > str[pos] || str[pos] > '9') {
        errorLine = std::string("Wrong symbol: ") + str[pos];
        num = -1;
        return;
      } else if (num != INT_MAX) {
        num *= 10;
        num += str[pos] - '0';
      	if (num < 0)
      		num = INT_MAX;
      }
    if (str[pos] == ' ')
      for (; pos < str.size() && str[pos] != '#'; ++pos)
        if (str[pos] == ' ' || str[pos] == '\t')
          continue;
        else {
          errorLine = std::string("Wrong symbol: ") + str[pos];
          num = -1;
          return;
        }
  }
  Container ();
  Container (const config::Container&);
  Container& operator= (const config::Container&);
  std::map< std::string, std::map<std::string, int> > container;
  static std::string lastLine;
  static std::string errorLine;
  static unsigned int lastNumLine;
};

std::string Container::lastLine = "";
std::string Container::errorLine = "";
unsigned int Container::lastNumLine = 0;
};
