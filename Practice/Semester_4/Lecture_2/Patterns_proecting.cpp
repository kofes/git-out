/**
* Паттерны проектирования банда четрыех - норм книжка
*/
#include <iostream>
#include <vector>

// Double dispacth

struct Medkit;
struct Character;

struct Actor {
  virtual void collide(Actor *a) = 0;
  virtual void collide(Medkit *a) = 0;
  virtual void collide(Character *a) = 0;
};

struct Character : Actor {
  int hp;
  void collide(Actor *m) override {
    hp += m->val;
  }
};

struct Monster : Character {

};


struct Medkit : Actor {
  int val;
};

vector<Actor*> actors;

void collide(Actor *first, Actor *second) {

}

/*Singleton*/
