#include <iostream>


class Complex {
public:
  int real = 0;
  int image = 0;
  Complex operator+ (Complex &a) {
    Complex num;
    num.real = real + a.real;
    num.image = image + a.image;
    return num;
  }
  Complex operator+ (int a) {
    Complex num;
    num.real = real + a;
    num.image = image;
    return num;
  }
  /**
  * Так делать не стоит, т.к. нужно будет вызывать оператор + как статическую функцию...
  static operator+ (int a, Complex &b) {
    Complex res;
    res.real = a + b.real;
    res.image = b.image;
    return res;
  }
  */
};
// Вот так можно сделать!
static operator+ (int a, Complex &b) {
  Complex res;
  res.real = a + b.real;
  res.image = b.image;
  return res;
}

int main () {
  std::cout << (Complex() + 10) << std::endl;
  return 0;
}

/**
static - проинициализирует сразу
extern (по умолчанию) - будет искать в других файлах его.
Пример:
file1.cpp
START
extern int a = 10;
END
file2.cpp
START
extern int a; <- даже без инклуда будет искать в
файлах переданных компилятору для компиляции.
END
*/

/**
Можно создавать внутри функции статический объект и он будет хранится всё время работы программы - хоть можешь счетчик создать - он не будете переинициализирован или "разрушен" - он будет работать

Большая магия будет, если создашь внешний объект с конструктором, в котором будет инициализация статических объектов, даже если они находятся внутри функции внутри конструктора - то это заработает раньше main - даже можно будет что-то вывести в консоль
*/
/**
* class Cl {
* ...
* mutable int var; - если создать константный объект, то можем изменять var.
* ...
* };
*/
