#include <iostream>
#include <list>



class Stack : private std::list<int> {
public:
  using std::list<int>::push_back; //<- Дает возможность пользоваться методом, не смотря на привтаное объявление класса std::list<int>
	void push(int a) {
		push_back(a);
	}
};




int main () {
	Stack s;
	//s.push_back(1); <- Error - private...
	s.push(1); //<- OK!
	return 0;
}
