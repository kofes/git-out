#include <iostream>
#include <list>

using namespace std;

// Полиморфизм
// Статический - тип объектов известен на этапе компиляции:
void print(int a);
void print(double a);
//
// Полиморфизм в объектах
class base {
  virtual void print() {
    cout << "I'm base" << endl;
  }
};

class derived : public base {
  virtual void print() {
    cout << "I'm derived: " << endl;
  }
};

class second : public base {
  virtual void print(int a) override {
    cout << "I'm derived: " << endl;
  }
};
// c++ 11: overrride сигнатура - позволяет явно указать, что идет перегрузка заданной в родительском классе метода.

base *bases[100];
//Словво virtual можно не писать в дочерних классах, если уже указан в классе родителя.
int main() {
  // base *bptr = new derived;
  // cout << sizeof(base) << endl; -> 8 - если не будет виртуальных методов, то будет размер зависеть лишь от "объектных" полей. Т.к. при объявлении виртуальных функций неявно создается указатель на "таблицу" виртуальных функций.
  return 0;
}
/**
* Статические поля хранятся вне объекта
*/
