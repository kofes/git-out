#ifndef __INCLUDE_LOG_
#define __INCLUDE_LOG_

#include <fstream>
#include <string>

namespace Log {
  class err {
  private:
    std::fstream log;
  public:
    err () : log("./err.log", std::ios::out) {};
    err (const std::string &file) : log(file.c_str(), std::ios::out) {};
    err (const char *file) : log(file, std::ios::out) {};
    ~err () { log.close();}
    inline void warning(const std::string &line) {
      log << "Warning: " << line << std::endl;
    }
    inline void error(const std::string &line) {
      log << "Error: " << line << std::endl;
    }
  };
};

#endif //__INCLUDE_LOG_
