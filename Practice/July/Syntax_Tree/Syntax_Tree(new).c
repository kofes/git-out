#include "Syntax_Tree.h"
/*	39 : -'-'
**	40 : '('
**	41 : Нет id.
**	42 : '*'
**	43 : '+'
**	44 : "cos"
**	45 : '-'
**	46 : "log"
**	47 : '/'
**	48 : "exp"
**	49 : "sqrt"
**	50 : "sin"
*/

int SYN_TREE_set_tree(t_Syn_Tree *, char *);

int SYN_TREE_del(t_Syn_Tree );

int SYN_TREE_get_answer(t_Syn_Tree , double *, FILE *in, FILE *out);

int SYN_TREE_diff(t_Syn_Tree *, char *);

int SYN_TREE_out(t_Syn_Tree , FILE *);

typedef int (*t_func)(char** pos);
typedef char *tb_key;
typedef unsigned char tb_id;


/*-------------------------------------------*/
/*Глобальные переменные для работы с деревом*/
static void *NUMB, *SIGN;			/*Стэки для деревьев с числами и с функциями/операциями*/
static void *MAP;

/*-----------------------------*/
/*Функции и макросы для работы с парсером*/
#define STACK_NULL (NULL)
#define MAPTREE_NULL (NULL)
#define ALL_ID (UCHAR_MAX + 1)
#define RU_TO_RN (5)
#define GET_NUM(NUM) {							\
  NUM = stack_get_elem(NUMB);					\
  stack_off_elem(NUMB);							\
}												\


typedef struct tb_node{
	struct tb_node *child[2];
	tb_key key;
	double val;
	char mark;
	char diff;
}tb_node;
/*--------------------------*/
/*Тип элемента в узле дерева*/
typedef struct{
	char flag;		/* 0 - число, 1 - переменная, 2 - операция, 3 - функция*/
	union{
		double val;
		tb_node *pointer;
		unsigned char sign;	/*Переменная, хранящая в себе уникальный id каждой функции/операции/переменной*/
	};
}t_elem;

/*---------------*/
/*Тип узла синтаксического дерева*/
typedef struct tt_node{
	struct tt_node *left, *right;
	t_elem elem;
}tt_node;

/*Тип конечного синтаксического дерева*/
typedef struct{
	tt_node *head;
	 void *MAB;
}the_tree;

/*------------------------*/
/*Тип узла списка деревьев*/
typedef struct tl_node{
	struct tl_node *next;	/*Указатель на следующий узел в списке*/
	tt_node *tree;	/*Указатель на вершину текущего дерева*/
}tl_node;

/*----------------*/
/*Структура списка*/
typedef struct{
	tl_node *head;
	size_t height;
}t_stack;


typedef struct{
	tb_node *root;
}t_tree;


/*list - список указателей на вершины синтаксического дерева*/
void list_del(void *);
tt_node **list_add_elem(void **);
void list_fill(void *, double);
void list_out(void *, FILE *);
/*Mtree - словарь*/
void *Mtree_new();
void Mtree_del(void *);
tb_node *Mtree_add_elem(void *, tb_key);
char fill_var(void *, FILE *, FILE *);
void tree_out_tree(void *, FILE *);
void Mtree_out(void *, FILE *);
/*-------------------------------------*/
/*Функции для работы со стеком деревьев*/
int fill_book(tt_node *, void *);
tt_node *Optimize(tt_node *);

void *stack_new();
void stack_del(void *);
size_t stack_hht(void *);
tt_node *stack_add_elem(void *);
tt_node *stack_get_elem(void *);
void stack_off_elem(void *);
void stack_del_elem(void *);

tt_node *copy_tree(tt_node *);
/*--------------------------------------------------------------*/
int diff_tree(tt_node **tree, char *str);
/*--------------------------------------------------------------*/
/*--------------------------------------------------------------*/
void tree_out(tt_node *, FILE *);
/*--------------------------------------------------------------*/
/*--------------------------------------------------------------*/
double tree_result(tt_node *, int *err);
/*--------------------------------------------------------------*/
/*--------------------------------------------------------------*/
void tree_del(tt_node *tree);
/*--------------------------------------------------------------*/
int get_type(char pos);
int RS (char **pos);
int RN (char **pos);
int RU (char **pos);
int RB (char **pos);

void *Mtree_new(){

	t_tree *tree;

	if ((tree = (t_tree *)malloc(sizeof(t_tree))) == NULL)
		return NULL;

	tree->root = NULL;

	return tree;
}

void Mtree_del(void *vtree){

	t_tree *tree;
	tree = (t_tree *)vtree;

	void tree_del_node(tb_node *node){

		if (node == NULL)
			return;

		tree_del_node(node->child[0]);
		tree_del_node(node->child[1]);
		free(node->key);
		free(node);
	}

	if (tree == NULL)
		return;
	tree_del_node(tree->root);
}

tb_node *little_turn(tb_node *root, char inv){
	tb_node *node = root->child[(unsigned char)inv];

	root->child[(unsigned char)inv] = node->child[(unsigned char)(inv + 1)%2];
	node->child[(unsigned char)(inv + 1)%2] = root;
	root = node;
	return root;
}

tb_node *turns(tb_node *node, char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[(unsigned char)inv]->diff);
		diff[2] = NULL;

		if (node->child[(unsigned char)inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[(unsigned char)inv]->child[(unsigned char)(inv + 1)%2]->diff);
			node->child[(unsigned char)inv] = little_turn(node->child[(unsigned char)inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return little_turn(node, inv);
	}
	return node;
}

tb_node *Mtree_add_elem(void *vtree, tb_key key){
	char info = 0;
	t_tree *tree;
	tb_node *vx = NULL;
	tree = (t_tree *)vtree;
	tb_node *tree_add_node(tb_node *node){
		int i;
		if (node == NULL){
			size_t len;
			if ((node = (tb_node *)malloc(sizeof(tb_node))) == NULL){
				info = 1;
				return NULL;
			}
			vx = node;
			node->val = NULL;
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			node->mark = 1;
			len = strlen(key) + 1;
			if ((node->key = (tb_key )malloc(len)) == NULL){
				free(node);
				vx = NULL;
				info = 0;
				return NULL;
			}
			memcpy(node->key, key, len);
			

			return node;
		}

		i = strcmp(key, node->key);
		if (i == 0){
			vx = node;
			node->mark = 1;
			info = 1;
			return node;
		}
		i = (i > 0);

		node->child[(unsigned int)i] = tree_add_node(node->child[(unsigned int)i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turns(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = tree_add_node(tree->root);
	return vx;
}

char fill_var(void *vtree, FILE *in, FILE *out){
	t_tree *tree = (t_tree *)vtree;

	void tree_fill_node(tb_node *node){
		if (node == NULL)
			return;
		tree_fill_node(node->child[0]);
		tree_fill_node(node->child[1]);
		if (!node->mark)
			continue;
		fprintf(out, "INPUT %s : ", node->key);
		fscanf(in, "%lf", &(node->pointer->val));
		node->mark = 2;
	}

	if ((tree == NULL)||((tree->root != NULL)&&((in == NULL)||(out == NULL))))
		return ERR_DATA;

	tree_fill_node(tree->root);
	return ERR_NORM;
}

void Mtree_out(void *vtree, FILE *out){

	t_tree *tree = (t_tree *)vtree;
	void btree_out_node(tb_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		fprintf(out, "key: %s, val: %lf, mark: %i\n", node->key, node->val, node->mark);
		btree_out_node(node->child[1]);
	}

	if ((tree == NULL)||(out == NULL))
		return;

	btree_out_node(tree->root);
}

void *stack_new(){
	t_stack *stack;

	if ((stack = (t_stack *)malloc(sizeof(t_stack))) == NULL)
		return NULL;
	stack->height = 0;
	stack->head = NULL;

	return stack;
}

void stack_del(void *stack){

	tl_node *curr, *node;

	if (stack == NULL)
		return;
	curr = ((t_stack *)stack)->head;

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		tree_del(node->tree);
		free(node);
	}
	((t_stack *)stack)->head = NULL;
	((t_stack *)stack)->height = 0;
}

size_t stack_hht(void *stack){

	if (stack == NULL)
		return 0;

	return ((t_stack *)stack)->height;
}

tt_node *stack_add_elem(void *stack){

	tl_node *node;

	if (stack == NULL)
		return NULL;
	if ((node = (tl_node *)malloc(sizeof(tl_node))) == NULL)
		return NULL;
	if ((node->tree = (tt_node *)malloc(sizeof(tt_node))) == NULL){
		free(node);
		return NULL;
	}
	node->next = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node;
	++((t_stack *)stack)->height;
	node->tree->left = NULL;
	node->tree->right = NULL;
	return node->tree;
}

tt_node *stack_get_elem(void *stack){

	if ((stack == NULL)||(((t_stack *)stack)->head == NULL))
		return NULL;

	return ((t_stack *)stack)->head->tree;
}

void stack_off_elem(void *stack){

	tl_node *node;

	if ((stack == NULL)||((t_stack *)stack)->head == NULL)
		return;
	node = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node->next;
	free(node);
	--((t_stack *)stack)->height;
}

void stack_del_elem(void *stack){

	tl_node *node;

	if ((stack == NULL)||((t_stack *)stack)->head == NULL)
		return;
	node = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node->next;
	tree_del(node->tree);
	free(node);
	--((t_stack *)stack)->height;
}

void tree_del(tt_node *tree){

	if (tree == NULL)
		return;

	void del_node(tt_node *node){

		if (node == NULL)
			return;
		del_node(node->left);
		del_node(node->right);
		free(node);
	}

	del_node((tt_node *)tree);
}

void tree_out(tt_node *tree, FILE *out){
	if ((tree == NULL)||(out == NULL))
		return;

	void tree_out_node(tt_node *node, size_t count){
		size_t i;

		if (node == NULL)
			return;
		tree_out_node(node->left, count + 1);
		for (i = 0; i < 2*count; ++i)
			fprintf(out, "  ");
		switch (node->elem.flag){
			case 0 : fprintf(out, "%lf\n", node->elem.val); break;
			case 1 :
				if (node->elem.pointer->mark == 1) fprintf(out, "%s\n", node->elem.pointer->key);
				else fprintf(out, "%lf\n", node->elem.pointer->val); break;
			case 2 : fprintf(out, "%c\n", node->elem.sign); break;
			case 3 : fprintf(out, "%i\n", node->elem.sign); break;
		}

			
		tree_out_node(node->right, count + 1);
	}

	tree_out_node(tree, 0);
}

double tree_result(tt_node *tree, int *err){		//ПОЛНОСТЬЮ ПЕРЕДЕЛАТЬ!!!
	void node_result(tt_node *node){

		if ((node == NULL)||(!node->elem.flag))
			return;

		node_result(node->right);
		node_result(node->left);
		node->elem.flag = 0;
		switch (node->elem.sign){
			case 39 :
				if (node->left->elem.flag) node->elem.val = - node->left->elem.pointer->val;
				else node->elem.val = - node->left->elem.val; break;
			case 42 :
				if (node->left->elem.flag){	
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val * node->left->elem.pointer->val;
					else node->elem.val = node->right->elem.val * node->left->elem.pointer->val;
				} else{
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val * node->left->elem.val;
					else node->elem.val = node->right->elem.val * node->left->elem.val;
				}
				break;
			case 43 :
				if (node->left->elem.flag){	
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val + node->left->elem.pointer->val;
					else node->elem.val = node->right->elem.val + node->left->elem.pointer->val;
				} else{
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val + node->left->elem.val;
					else node->elem.val = node->right->elem.val + node->left->elem.val;
				}
				break;
			case 44 :
				if (node->left->elem.flag) node->elem.val = cos(node->left->elem.pointer->val);
				else node->elem.val = cos(node->left->elem.val); break;
			case 45 :
				if (node->left->elem.flag){	
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val - node->left->elem.pointer->val;
					else node->elem.val = node->right->elem.val - node->left->elem.pointer->val;
				} else{
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val - node->left->elem.val;
					else node->elem.val = node->right->elem.val - node->left->elem.val;
				}
				break;
			case 46 :
				if (node->left->elem.flag) node->elem.val = log(node->left->elem.pointer->val);
				else node->elem.val = log(node->left->elem.val); break;
			case 47 :
				if (node->left->elem.flag){
					if ((node->left->elem.pointer->val - EPS < 0)&&(node->left->elem.pointer->val + EPS > 0)){ *err = 1; return;}
					//
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val / node->left->elem.pointer->val;
					else node->elem.val = node->right->elem.val / node->left->elem.pointer->val;
					//
				} else{
					if ((node->left->elem.val - EPS < 0)&&(node->left->elem.val + EPS > 0)){ *err = 1; return;}
					//
					if (node->right->elem.flag) node->elem.val = node->right->elem.pointer->val / node->left->elem.val;
					else node->elem.val = node->right->elem.val / node->left->elem.val;
					//
				}
				break;
			case 48 :
				if (node->left->elem.flag) node->elem.val = exp(node->left->elem.pointer->val);
				else node->elem.val = exp(node->left->elem.val); break;
			case 49 :
				if (node->left->elem.flag) node->elem.val = sqrt(node->left->elem.pointer->val);
				else node->elem.val = sqrt(node->left->elem.val); break;
			case 50 :
				if (node->left->elem.flag) node->elem.val = sin(node->left->elem.pointer->val);
				else node->elem.val = sin(node->left->elem.val); break;
		}
	}
	
	if ((tree == NULL)||(err == NULL))
		return 1/(+0.0);
	err = 0;
	node_result(tree);

	return tree->elem.pointer->val;
}

tt_node *copy_tree(tt_node *head){
	if (head == NULL)
		return NULL;
	tt_node* copy_node(tt_node *node){
		tt_node *copy;
		if (node == NULL)
			return NULL;
		copy = (tt_node *)malloc(sizeof(tt_node));
		copy->elem = node->elem;
		copy->left = copy_node(node->left);
		copy->right = copy_node(node->right);
		return copy;
	}
	return copy_node(head);
}

int priority(t_elem ch){
	switch (ch.sign){
		case '(': return -1;
		case '\0': return 0;
		case '\n': return 0;
		case ')': return 1;
		case '+': return 2;
		case '-': return 2;
		case '*': return 3;
		case '/': return 3;
		case 39 : return 4;
	}
	return -1;
}

const static char STAT [3][8] = {
	{0, 1, 2, -1, 0, -1, -1, -1},
	{1, -2, 2, 2, -1, 1, 3, -1},
	{2, 1, -2, -2, 0, -1, -1, -1}
};

const static t_func FUNC [3][8] = {
	{&RS, &RN, &RU, NULL, &RU, NULL, NULL, NULL},
	{&RS, &RN, &RB, &RB, NULL, &RB, NULL, NULL},
	{&RS, &RN, NULL, NULL, &RU, NULL, NULL, NULL}
};

int SYN_TREE_set_tree(t_Syn_Tree *tree, char *str){
	char alph_table[256], *pos = str;
	the_tree *TREE;
	int flag, t, g, err;
	if ((tree == NULL)||(str == NULL)||(*tree != NULL))
		return ERR_DATA;
	memset(alph_table, 7, 256);
	alph_table[' '] = 0;
	alph_table['-'] = 2;
	memset(alph_table + 48, 1, 10);/*0-9*/
	memset(alph_table + 65, 1, 26);
	memset(alph_table + 97, 1, 26);
	alph_table['+'] = 3; alph_table['*'] = 3; alph_table['/'] = 3;
	alph_table['('] = 4; alph_table[')'] = 5;
	alph_table['s'] = 4; alph_table['c'] = 4;
	alph_table['e'] = 4; alph_table['l'] = 4;
	alph_table['\0'] = 6; alph_table['\n'] = 6;
	flag = 0;
	id = 50;
	err = ERR_NORM;
	TREE = NULL;
	if ((TREE = (the_tree *)malloc(sizeof(the_tree))) == NULL)
		return ERR_NULL;
	MAP = MAPTREE_NULL;
	MAP = Mtree_new();
	NUMB = stack_new();
	SIGN = stack_new();
	while (1){
		t = alph_table[(unsigned char)(*pos)];
		g = flag;
		flag = STAT[g][t];
		if (flag < 0){
			err = (flag) == (-2) ? ERR_SIGN : ERR_CHAR;
			break;
		}
		if (flag == 3){
			err = RB(&pos);
			break;
		}
		err = FUNC [g][t](&pos);
		if (err == RU_TO_RN){
			flag = 1;
			err = ERR_NORM;
		}
		if (err != ERR_NORM){
			break;
		}
	}
	if (err == ERR_NORM){
		tt_node *node;
		unsigned short i;
		node = stack_get_elem(NUMB);
		stack_off_elem(NUMB);
		stack_del(SIGN);
		stack_del(NUMB);
		node = Optimize(node);						//========================= ПЕРЕДЕЛАТЬ OPTIMIZE - НЕ УДАЛЯТЬ СЛОВАРЬ!
		fill_book(node, MAP);						//========================= ПЕРЕДЕЛАТЬ fill_book - ПРИНЕМАЕТ СЛОВАРЬ И МАРКЕРУЕТ УЗЛЫ!
		TREE->head = node;
		TREE->MAP = MAP;
		MAP = NULL;
		*tree = (t_Syn_Tree)TREE;
	}
	return err;
}

int SYN_TREE_del(t_Syn_Tree tree){
	tree_del(((the_tree *)tree)->head);
	Mtree_del(((the_tree *)tree)->MAP);
	((the_tree *)tree)->head = NULL;
	((the_tree *)tree)->MAP = NULL;
	return ERR_NORM;
}
//Переделать с get_id;
int SYN_TREE_get_answer(t_Syn_Tree tree, double *val, FILE *in, FILE *out){
	tt_node *node;
	int err;
	if ((tree == NULL)||(val == NULL))
		return ERR_DATA;
	if ((node = copy_tree(((the_tree *)tree)->head)) == NULL)
		return ERR_NULL;
	err = fill_var(((the_tree *)tree)->MAP, in, out);
	if (err)	return err;
	*val = tree_result(((the_tree *)tree)->head, &err);
	tree_del(((the_tree *)tree)->head);
	((the_tree *)tree)->head = node;
	((the_tree *)tree)->MAP = MAP;
	return err;
}

int SYN_TREE_diff(t_Syn_Tree tree, t_Syn_Tree *out_tree, char *key){
	int err;
	tt_node *node;
	if ((tree == NULL)||(key == NULL)||(*tree == NULL)||(out_tree == NULL)||(((the_tree *)(*tree))->head == NULL))
		return ERR_DATA;
	//Дописать!
	node = ((the_tree *)(*tree))->head;
	err = diff_tree(&node, key);
	MAP = NULL;
	node = Optimize(node);
	((the_tree *)tree)->MAP = MAP;
	SYN_TREE_del(*out_tree);
	return ERR_NORM;
}

int SYN_TREE_out(t_Syn_Tree tree, FILE *out){
	if ((tree == NULL)||(out == NULL)||(((the_tree *)tree)->head == NULL))
		return ERR_DATA;
	tree_out(((the_tree *)tree)->head, out);
	return ERR_NORM;
}

int RS(char **pos){
	for (; **pos == ' '; ++(*pos));
	return 0;
}

int RN(char **pos){
	t_elem elem;
	unsigned char *val;
	tb_key str;
	tt_node *node;
	if (**pos < 60){
		elem.flag = 0;	
		elem.pointer->val = strtod(*pos, pos);
		stack_add_elem(NUMB)->elem = elem;
	} else{
		unsigned char i;
		for (i = 0; isalpha((*pos)[i]); ++i);
		str = (char *)malloc(i*sizeof(char));
		memcpy(str, *pos, i);
		str[i] = '\0';
		node = stack_add_elem(NUMB); ////////------------------------------
		node->elem.flag = 1;
		val = tree_add_elem(MAB, str, node);
		if (*val == 41){
			id = (id + 1) % ALL_ID;
			*val = id;
			node->elem.sign = id;
		} else
			node->elem.sign = *val;
		*pos = *pos + i;
	}
	return 0;
}

int RU(char **pos){
	char str[5];
	t_elem elem;
	unsigned char i;
	if ((**pos == '-')||(**pos == '(')){
		elem.flag = 2;
		elem.sign = (**pos) == ('-') ? (39) : (**pos);
		stack_add_elem(SIGN)->elem = elem;
		++(*pos);
		return 0;
	}
	for (i = 0; (*(*pos + i) != '\0')&&(*(*pos + i) != '\n')&&(i < 4); ++i)
		str[i] = *(*pos + i);
	str[4] = '\0';
	elem.flag = 3;
	if (!strcmp(str, "sqrt")){
		elem.sign = 49;
		++(*pos);
	} else{
		str[3] = '\0';
		if (!strcmp(str, "cos"))
			elem.sign = 44;
		else if (!strcmp(str, "log"))
				elem.sign = 46;
			else if (!strcmp(str, "exp"))
					elem.sign = 48;
				else if (!strcmp(str, "sin"))
						elem.sign = 50;
					else{
						RN(pos);
						return RU_TO_RN;
					}
	}
	stack_add_elem(SIGN)->elem = elem;
	*pos = *pos + 3;
	if (**pos != '(')
		return ERR_CHAR;
	++(*pos);
	return ERR_NORM;
}

int RB(char** pos){
	t_elem elem;
	char ch = **pos;
	elem.flag = 2;
	elem.sign = ch;
	int p = priority(elem);
	while ((stack_get_elem(SIGN) != NULL) && (priority(stack_get_elem(SIGN)->elem) >= p)){
		t_elem elem = stack_get_elem(SIGN)->elem;
		char sym = elem.sign;
		stack_del_elem(SIGN);
		tt_node *num1, *num2, *head;
		switch (sym){
			case 39 :
				GET_NUM(num1);
				if (!num1->elem.flag){
					num1->elem.pointer->val = - num1->elem.pointer->val;
					stack_add_elem(NUMB)->elem = num1->elem;
				} else{
					head = stack_add_elem(NUMB);
					head->elem.flag = 2;
					head->left = num1;
					head->right = NULL;
					head->elem.sign = '-';
				}
				break;
			case '+':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.pointer->val = num2->elem.pointer->val + num1->elem.pointer->val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '-':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.pointer->val = num2->elem.pointer->val - num1->elem.pointer->val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '/':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					if ((num1->elem.pointer->val - EPS < 0)&&(num1->elem.pointer->val + EPS > 0)) return 1;
					head->elem.pointer->val = num2->elem.pointer->val / num1->elem.pointer->val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '*':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.pointer->val = num2->elem.pointer->val * num1->elem.pointer->val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			}
	}
	if (ch == ')'){
		if (stack_get_elem(SIGN) == NULL)
			return 2;
		if (stack_get_elem(SIGN)->elem.flag == 3){
			tt_node *node, *head;
			node = stack_get_elem(NUMB);
			if (node->elem.flag == 0){
				head = stack_get_elem(SIGN);
				switch (head->elem.sign){
					case 44 :	node->elem.pointer->val = cos(node->elem.pointer->val);	break;
					case 46 :	node->elem.pointer->val = log(node->elem.pointer->val);	break;
					case 48 :	node->elem.pointer->val = exp(node->elem.pointer->val);	break;
					case 49 :	node->elem.pointer->val = sqrt(node->elem.pointer->val);	break;
					case 50 :	node->elem.pointer->val = sin(node->elem.pointer->val);	break;
				}
			} else{
				stack_off_elem(NUMB);
				head = stack_add_elem(NUMB);
				head->left = node;
				head->right = NULL;
				head->elem.flag = 3;
				head->elem.sign = stack_get_elem(SIGN)->elem.sign;
			}
		}
		stack_del_elem(SIGN);
		++(*pos);
		return ERR_NORM;
	}
	if ((ch == '\0')||(ch == '\n')){
		if (stack_get_elem(SIGN) != NULL)
			return ERR_OPEN;
		return ERR_NORM;
	}
	elem.flag = 2;
	elem.sign = ch;
	stack_add_elem(SIGN)->elem = elem;
	++(*pos);
	return 0;
}

char **diff_tree(tt_node **tree, char *str){
	unsigned char *ind;
	char **Mabs;
	if ((tree == NULL)||(str == NULL))
		return NULL;
	ind = tree_get_elem(MAB, str);
	if (ind == NULL){
		tree_del((*tree)->left);
		tree_del((*tree)->right);
		(*tree)->elem.flag = 0;
		(*tree)->elem.pointer->val = 0;
		return NULL;
	}
	id = 50;
	Mabs = tree_id(MAB);//Нужно будет перезаполнять дерево переменных
	MAB = NULL;
	tt_node *diff_node(tt_node *node){
		if (node == NULL)
			return node;
		if (node->elem.flag > 1){
			tt_node *head, *jnode, *shead, *rnode, *lnode;
			switch (node->elem.sign){
				case 39 :
					node->left = diff_node(node->left);
					return node;
				case 42 :	/*Умножение*/
					jnode = copy_tree(node);
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = node;
					head->left = jnode;
					head->elem.flag = 2;
					head->elem.sign = '+';
					head->right->right = diff_node(head->right->right);
					head->left->left = diff_node(head->left->left);
					return head;
				case 43 :	/*Сложение*/
					node->left = diff_node(node->left);
					node->right = diff_node(node->right);
					return node;
				case 44 :	/*Косинус*/
					//
					head = (tt_node *)malloc(sizeof(tt_node));
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->left = copy_tree(node->left);
					//
					shead = (tt_node *)malloc(sizeof(tt_node));
					shead->elem.flag = 2;
					shead->elem.sign = 39;
					//
					node->elem.sign = 50;
					head->right = shead;
					shead->left = node;
					shead->right = NULL;
					head->left = diff_node(head->left);
					return head;
				case 45 :	/*Вычитание*/
					node->left = diff_node(node->left);
					node->right = diff_node(node->right);
					return node;
				case 46 :	/*Логарифм*/
					jnode = copy_tree(node->left);
					node->right = jnode;
					node->elem.flag = 2;
					node->elem.sign = '/';
					node->right = diff_node(node->right);
					return node;
				case 47 :	/*Деление*/
					rnode = (tt_node *)malloc(sizeof(tt_node));
					lnode = (tt_node *)malloc(sizeof(tt_node));
					rnode->left = copy_tree(node);
					rnode->right = copy_tree(node);
					lnode->left = copy_tree(node->left);
					lnode->right = copy_tree(node->left);
					node->right = rnode;
					node->left = lnode;
					rnode->elem.flag = 2;
					lnode->elem.flag = 2;
					rnode->left->elem.flag = 2;
					rnode->right->elem.flag = 2;
					rnode->elem.sign = '-';
					lnode->elem.sign = '*';
					rnode->left->elem.sign = '*';
					rnode->right->elem.sign = '*';
					rnode->right->right = diff_node(rnode->right->right);
					rnode->left->left = diff_node(rnode->left->left);
					return node;
				case 48 : /*Экспонента*/
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = node;
					head->left = copy_tree(node->left);
					head->left = diff_node(head->left);
					return head;
				case 49 : /*Квадратный корень*/
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = (tt_node *)malloc(sizeof(tt_node));
					head->left = copy_tree(node);
					head->right->elem.flag = 0;
					head->right->elem.pointer->val = 0.5;
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->left->elem.flag = 2;
					head->left->elem.sign = '/';
					head->left->right = head->left->left;
					head->left->left = node;
					head->left->right = diff_node(head->left->right);
					return head;
				case 50 : /*Синус*/
					head = copy_tree(node);
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->right = node;
					node->elem.sign = 44;
					head->left = diff_node(head->left);
					return head;
			}
		}
		if ((node->elem.flag == 0)||(node->elem.sign != *ind)){
			node->elem.flag = 0;
			node->elem.pointer->val = 0;
		} else{
			node->elem.flag = 0;
			node->elem.pointer->val = 1;
		}
		return node;
	}
	*tree = diff_node(*tree);
	return Mabs;
}

tt_node *Optimize(tt_node *head){
	if (head == NULL)
		return NULL;
	tt_node *node_opt(tt_node *node){
		if ((node == NULL)||(node->elem.flag < 1))
			return node;

		node->left = node_opt(node->left);
		node->right = node_opt(node->right);

		if (node->elem.flag == 2){
			if (node->elem.sign == '*'){
				if (((node->right->elem.flag == 0)&&(node->right->elem.pointer->val == 0))||
					((node->left->elem.flag == 0)&&(node->left->elem.pointer->val == 0))){
					tree_del(node->left);
					tree_del(node->right);
					node->left = NULL;
					node->right = NULL;
					node->elem.flag = 0;
					node->elem.pointer->val = 0;
					return node;
				} else if ((node->right->elem.flag == 0)&&(node->right->elem.pointer->val == 1)){
					tt_node *jnode;
					tree_del(node->right);
					node->right = NULL;
					jnode = node->left;
					free(node);
					return jnode;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.pointer->val == 1)){
					tt_node *jnode;
					tree_del(node->left);
					node->left = NULL;
					jnode = node->right;
					free(node);
					return jnode;
				}
			} else if (node->elem.sign == '+'){
				if ((node->right->elem.flag == 0)&&(node->right->elem.pointer->val == 0)){
					tt_node *jnode;
					tree_del(node->right);
					node->right = NULL;
					jnode = node->left;
					free(node);
					return jnode;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.pointer->val == 0)){
					tt_node *jnode;
					tree_del(node->left);
					node->left = NULL;
					jnode = node->right;
					free(node);
					return jnode;
				}
			} else if (node->elem.sign == '-'){
				if ((node->right->elem.flag == 0)&&(node->right->elem.pointer->val == 0)){
					tree_del(node->right);
					node->right = NULL;
					node->elem.sign = 39;
					return node;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.pointer->val == 0)){
					tt_node *jnode;
					tree_del(node->left);
					node->left = NULL;
					jnode = node->right;
					free(node);
					return jnode;
				}
			}
		}

		if ((node->elem.flag > 1)&&(node->left->elem.flag == 0)&&
			((node->right == NULL)||(node->right->elem.flag == 0)))
			switch (node->elem.sign){
				case 39 :	//-'-'
					node->elem.flag = 0;
					node->elem.pointer->val = - node->left->elem.pointer->val;
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 42 :	//'*'
					node->elem.flag = 0;
					node->elem.pointer->val = node->right->elem.pointer->val * node->left->elem.pointer->val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 43 :	//'+'
					node->elem.flag = 0;
					node->elem.pointer->val = node->right->elem.pointer->val + node->left->elem.pointer->val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 44 :	//"cos"
					node->elem.flag = 0;
					node->elem.pointer->val = cos(node->left->elem.pointer->val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 45 :	//'-'
					node->elem.flag = 0;
					node->elem.pointer->val = node->right->elem.pointer->val - node->left->elem.pointer->val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 46 :	//"log"
					node->elem.flag = 0;
					node->elem.pointer->val = log(node->left->elem.pointer->val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 47 :	//'/'
					node->elem.flag = 0;
					node->elem.pointer->val = node->right->elem.pointer->val / node->left->elem.pointer->val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 48 :	//"exp"
					node->elem.flag = 0;
					node->elem.pointer->val = exp(node->left->elem.pointer->val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 49 :	//"sqrt"
					node->elem.flag = 0;
					node->elem.pointer->val = sqrt(node->left->elem.pointer->val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 50 :	//"sin"
					node->elem.flag = 0;
					node->elem.pointer->val = sin(node->left->elem.pointer->val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
			}
		return node;
	}

	return node_opt(head);
}

int fill_book(tt_node *head){
	unsigned char *val;
	if ((head == NULL)||(Mabs == NULL))
		return ERR_DATA;
	void fill_node(tt_node *node){
		if (node == NULL)
			return;
		fill_node(node->left);
		fill_node(node->right);
		if (node->elem.flag == 1){
			val = tree_mark_elem(MAB, node->elem.pointer->key);
			if (*val == 41){
				id = (id + 1) % ALL_ID;
				*val = id;
				node->elem.sign = id;
			} else
				node->elem.sign = *val;
		}
	} 
	fill_node(head);
	return 0;
}