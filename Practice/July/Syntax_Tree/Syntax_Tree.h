#ifndef __INCLUDE_SYNTAX_TREE_
#define __INCLUDE_SYNTAX_TREE_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>

#define SYN_TREE_NULL (NULL)
#define ERR_NORM (0)
#define ERR_DIV0 (1)
#define ERR_OPEN (2)
#define ERR_SIGN (3)
#define ERR_CHAR (4)
#define ERR_DATA (6)
#define ERR_NULL (7)
#define EPS (1e-5)
#define SIGNAL(ERR) {								\
	switch(ERR){									\
		case (0): printf("ERROR: NO ERR\n"); break;	\
		case (1): printf("ERROR: DIV 0\n"); break;	\
		case (2): printf("ERROR: OPEN\n"); break;	\
		case (3): printf("ERROR: SIGN\n"); break;	\
		case (4): printf("ERROR: CHAR\n"); break;	\
	}												\
}													\

typedef void *t_Syn_Tree;

/*Записывает выражение в форме дерева в структуру t_Syn_Tree*/
extern int SYN_TREE_set_tree(t_Syn_Tree *, char *);

/*Освобождает память из-под структуры*/
extern int SYN_TREE_del(t_Syn_Tree );

/*Вычисляет значение выражения, хранящегося в структуре t_Syn_Tree*/
extern int SYN_TREE_get_answer(t_Syn_Tree , double *, FILE *, FILE *);

/*Дифференцирует выражение(1) по переменной (char) и сохраняет результат(2)*/
extern int SYN_TREE_diff(t_Syn_Tree , t_Syn_Tree *, char *);

/*Выводит дерево на печать*/
extern int SYN_TREE_out(t_Syn_Tree , FILE *);

#endif