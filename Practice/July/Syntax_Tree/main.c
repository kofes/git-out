#include "Syntax_Tree.h"
int main(){
	t_Syn_Tree book, next;
	double val = 0;
	char err;
	book = SYN_TREE_NULL;
	next = SYN_TREE_NULL;
	err = SYN_TREE_set_tree(&book, "x*x");
	SIGNAL(err);
	SYN_TREE_out(book, stdout);
	err = SYN_TREE_diff(book, &next, "x");
	SIGNAL(err);
	SYN_TREE_out(next, stdout);
	err = SYN_TREE_get_answer(book, &val, stdin, stdout);
	SIGNAL(err);
	printf("Функция: %lf\n", val);
	err = SYN_TREE_get_answer(next, &(val), stdin, stdout);
	SIGNAL(err);
	printf("Продифференцированная функция: %lf\n", val);
	SYN_TREE_del(book);
	SYN_TREE_del(next);
	return 0;
}