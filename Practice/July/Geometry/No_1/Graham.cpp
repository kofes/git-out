#include "Graham.hpp"

bool cmp(pt a, pt b){
	return (a.x < b.x) || ((a.x == b.x) && (a.y < b.y));
}
/*Функция проверки на правый поворот*/
bool right(pt a, pt b, pt c){
	return ((
			a.x * (b.y - c.y)
			+
			b.x * (c.y - a.y)
			+
			c.x * (a.y - b.y)
			) < 0);
}
/*Функция проверки на левый поворот*/
bool left(pt a, pt b, pt c){
	return ((
			a.x * (b.y - c.y)
			+
			b.x * (c.y - a.y)
			+
			c.x * (a.y - b.y)
			) > 0);
}

std::vector<unsigned long long> convex_hull(std::vector<pt> & src){
	if (src.size() < 2)
		return std::vector<unsigned long long>();
	std::vector<pt> local(src);
	std::sort(local.begin(), local.end(), &cmp);

	std::vector<pt> up, down;
	pt first, last;

	first = local[0];
	last = local.back();

	up.push_back(first);
	down.push_back(first);

	for (unsigned long long i = 1; i < local.size(); ++i){
		if ((i == local.size() - 1) || right(first, local[i], last)){
			while ((up.size() >= 2) && !right(up[up.size() - 2], up[up.size() - 1], local[i]))
				up.pop_back();
			up.push_back(local[i]);
		}
		if ((i == local.size() - 1) || left(first, local[i], last)){
			while ((down.size() >= 2) && !left(down[down.size() - 2], down[down.size() - 1], local[i]))
				down.pop_back();
			down.push_back(local[i]);
		}
	}
	local.clear();
	std::vector<unsigned long long> result;
	unsigned long long i, j;
	j = 0;
	i = 0;


	while (j < up.size()){
			if ((up[j].x == src[i].x)&&(up[j].y == src[i].y)){
				result.push_back(i);
				++j;
			}
			i = (i + 1) % src.size();
	}
	j = down.size() - 2;
	i = 0;
	while (j > 0){
		if ((down[j].x == src[i].x)&&(down[j].y == src[i].y)){
			result.push_back(i);
			--j;
		}
		i = (i + 1) % src.size();
	}
	return result;
}
