#include "GLUT.hpp"

#include <iostream>
void GLUT::Default(std::string label, int *argc, char **argv, int width, int height){
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(1000, 100);
	glutInitContextVersion(3, 3);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutCreateWindow(label.c_str());
}

void GLUT::Init(int *argc, char **argv){
	glutInit(argc, argv);
}

void GLUT::DisplayMode(unsigned int mode){
	glutInitDisplayMode(mode);
}

void GLUT::WindowSize(int width, int height){
	glutInitWindowSize(width, height);
}

void GLUT::WindowPos(int x, int y){
	glutInitWindowPosition(x, y);
}

void GLUT::ContextVersion(int major, int minor){
	glutInitContextVersion(major, minor);
}

void GLUT::ContextProfile(int profile){
	glutInitContextProfile(profile);
}

void GLUT::CreateWindow(std::string label){
	glutCreateWindow(label.c_str());
}

void GLUT::Display(void (*render)(void)){
	glutDisplayFunc(render);
}

void GLUT::Reshape(void (*reshape)(int w, int h)){
	glutReshapeFunc(reshape);
}

void GLUT::MainLoop(){
	glutMainLoop();
}

void GLUT::Timer(unsigned int msecs, void (*update)(int value), int value){
	glutTimerFunc(msecs, update, value);
}

void GLUT::PostRedisplay(){
	glutPostRedisplay();
}
void GLUT::Keyboard(void (*keyboard)(unsigned char button, int x, int y)){
	glutKeyboardFunc(keyboard);
}
