#ifndef __INCLUDE_MY_GRAHAM_

#define __INCLUDE_MY_GRAHAM_

#include <vector>
#include <algorithm>

struct pt{
	double x, y;
};

/*Возврашает вектор с индексами вершин, входящих в выпукулую оболочку
**в порядке обхода(правый поворот)
*/
std::vector<unsigned long long> convex_hull (std::vector<pt> & src);

#endif
