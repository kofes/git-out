#include "shader.hpp"

shader::shader(GLuint _TYPE){
	ID = glCreateShader(_TYPE);
	TYPE = _TYPE;
	ERR = 0;
}

shader::shader(GLuint _TYPE, std::ifstream &file){
	ID = glCreateShader(_TYPE);
	TYPE = _TYPE;
	std::string source;
	if (file.is_open()){
		std::string str = "";
		while (getline(file, str))
			source += str + "\n";
	}
	const char *str = source.c_str();
	glShaderSource(ID, 1, &str, NULL);
}

shader::shader(GLuint _TYPE, std::string code){
	ID = glCreateShader(_TYPE);
	TYPE = _TYPE;
	const char *str = code.c_str();
	glShaderSource(ID, 1, &str, NULL);
}

shader::~shader(){
	glDeleteShader(ID);
}

void shader::exec(std::ifstream &file){
	std::string source;
	if (file.is_open()){
		std::string str = "";
		while (getline(file, str))
			source += str + "\n";
	}
	const char *str = source.c_str();
	glShaderSource(ID, 1, &str, NULL);
}

void shader::exec(std::string code){
	const char *str = code.c_str();
	glShaderSource(ID, 1, &str, NULL);
}

void shader::compile(){
	glCompileShader(ID);
}

GLuint shader::GET_TYPE(){
	return TYPE;
}

GLint shader::ERR_COMP(){
	glGetShaderiv(ID, GL_COMPILE_STATUS, &ERR);
	return ERR;
}

std::string shader::ERR_STR(){
	GLsizei len;
	GLchar *err;
	glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &len);
	glGetShaderInfoLog(ID, len, NULL, err);
	std::string result(err);
	delete[] err;
	return result;
}

shader_program::shader_program(){
	ID = glCreateProgram();
	count_pts = new unsigned int;
	*count_pts = 0;
	ERR = 0;
}

shader_program::shader_program(const shader_program &src){
	ID = src.ID;
	count_pts = src.count_pts;
	++*count_pts;
	ERR = src.ERR;
}

shader_program::~shader_program(){
	if (!*count_pts){
		glDeleteProgram(ID);
		delete count_pts;
	}
	else
		--*count_pts;
}

shader_program shader_program::operator =(const shader_program &src){
	if (!*count_pts){
		glDeleteProgram(ID);
		delete count_pts;
	}else
		--*count_pts;
	ID = src.ID;
	ERR = src.ERR;
	count_pts = src.count_pts;
	++*count_pts;
	return *this;
}

void shader_program::add(shader &src){
	glAttachShader(ID, src.ID);
}

void shader_program::link(){
	glLinkProgram(ID);
}

GLint shader_program::ERR_LINK(){
	glGetProgramiv(ID, GL_LINK_STATUS, &ERR);
	return ERR;
}

std::string shader_program::ERR_STR(){
	GLsizei len;
	GLchar *err;
	glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &len);
	glGetProgramInfoLog(ID, len, NULL, err);
	std::string result(err);
	delete[] err;
	return result;
}

GLint shader_program::get_attribute(const std::string &attribute){
	return glGetAttribLocation(ID, attribute.c_str());
}

GLint shader_program::get_uniform(const std::string &uniform){
	return glGetUniformLocation(ID, uniform.c_str());
}
