#include "shader.hpp"
#include "GLUT.hpp"
#include "Graham.hpp"
#include <cmath>
#include <ctime>

/**/
std::ostream &operator <<(std::ostream &out, std::vector<pt> vec){
	for (unsigned long long i = 0; i < vec.size(); ++i)
		out << i + 1 << ": " << '(' << vec[i].x << ", " << vec[i].y << ')' << std::endl;
	return out;
}
/**/

using namespace std;

GLuint VAO_ID;
vector<pt> VEC;
vector<pt> VEC_INIT;
GLuint ShPr;
GLint VERTEX_ID;
GLint COLOR_ID;


shader_program init_shader_program(){
	shader_program sp;
	shader sh_vert(GL_VERTEX_SHADER), sh_frag(GL_FRAGMENT_SHADER);
	ifstream from("fragment.glsl", ios::in);
	sh_frag.exec(from);
	sh_frag.compile();
	if (sh_frag.ERR_COMP() == GL_FALSE){
		cerr << "Error: " << sh_frag.ERR_STR() << endl;
		return shader_program();
	}else
		cerr << "Fragment shader: compiled" << endl;
	from.close();
	from.open("vertex.glsl", ios::in);
	sh_vert.exec(from);
	sh_vert.compile();
	if (sh_vert.ERR_COMP() == GL_FALSE){
		cerr << "Error: " << sh_vert.ERR_STR() << endl;
		return shader_program();
	}else
		cerr << "Vertex shader: compiled" << endl;
	sp.add(sh_frag);
	sp.add(sh_vert);
	sp.link();
	if (sp.ERR_LINK() == GL_FALSE){
		cerr << "Error: " << sp.ERR_STR() << endl;
		return shader_program();
	}
	const char VERTEX[] = "pos";
	VERTEX_ID = sp.get_attribute(VERTEX);
	if (VERTEX_ID == -1){
		cout << "could not bind " << VERTEX << endl;
		return shader_program();
	}else
		cout << VERTEX << ": " << VERTEX_ID << endl;
	const char COLOR[] = "line_color";
	COLOR_ID = sp.get_uniform(COLOR);
	if (COLOR_ID == -1){
		cout << "could not bind " << COLOR << endl;
		return shader_program();
	}else
		cout << COLOR << ": " << COLOR_ID << endl;
	return sp;
}

void init_VERTEX_V_O(){
	float buff[3 * 2 * VEC.size()];
	float max_x, max_y;
	max_x = 0.0f; max_y = 0.0f;
	for (unsigned long long i = 0; i < VEC_INIT.size(); ++i)
		if (max_x < fabs(VEC_INIT[i].x))
			max_x = VEC_INIT[i].x;
	for (unsigned long long i = 0; i < VEC_INIT.size(); ++i)
		if (max_y < fabs(VEC_INIT[i].y))
			max_y = VEC_INIT[i].y;
	for (unsigned long long i = 0; i < VEC.size() - 1; ++i){
		buff[6 * i] = VEC[i].x / (max_x + max_x / 2);
		buff[6 * i + 1] = VEC[i].y / (max_y + max_y / 2);
		buff[6 * i + 2] = -0.5f;
		buff[6 * i + 3] = VEC[i + 1].x / (max_x + max_x / 2);
		buff[6 * i + 4] = VEC[i + 1].y / (max_y + max_y / 2);
		buff[6 * i + 5] = -0.5f;
	}
		buff[6 * (VEC.size() - 1)] = VEC[VEC.size() - 1].x / (max_x + max_x / 2);
		buff[6 * (VEC.size() - 1) + 1] = VEC[VEC.size() - 1].y / (max_y + max_y / 2);
		buff[6 * (VEC.size() - 1) + 2] = -0.5f;
		buff[6 * (VEC.size() - 1) + 3] = VEC[0].x / (max_x + max_x / 2);
		buff[6 * (VEC.size() - 1) + 4] = VEC[0].y / (max_y + max_y / 2);
		buff[6 * (VEC.size() - 1) + 5] = -0.5f;
	GLuint VBO_ID;
	glGenBuffers(1, &VBO_ID);
	if (VBO_ID < 0){
		cout << "could not generate buffers" << endl;
		return;
	}
	glBindBuffer(GL_ARRAY_BUFFER, VBO_ID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * 2 * VEC.size(), buff, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO_ID);
	glBindVertexArray(VAO_ID);

	glEnableVertexAttribArray(VERTEX_ID);
	glVertexAttribPointer(VERTEX_ID, 3, GL_FLOAT, GL_FALSE, 0, NULL);

}

bool toggle;

void render(){
	if (!toggle)
		glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(ShPr);
	glBindVertexArray(VAO_ID);
	if (!toggle)
		glUniform4f(COLOR_ID, 0.0f, 0.5f, 0.7f, 1.0f);
	else
		glUniform4f(COLOR_ID, 0.5f, 0.1f, 0.1f, 1.0f);
	glDrawArrays(GL_LINES, 0, 3 * 2 * VEC.size());
	glutSwapBuffers();
	glBindVertexArray(0);
	glUseProgram(0);
}


void keys(unsigned char button, int x, int y){
	vector<unsigned long long> index;
	switch (button) {
		case 'y':
			VEC.clear();
			index = convex_hull(VEC_INIT);
			for (unsigned long long i = 0; i < index.size(); ++i)
				VEC.push_back(VEC_INIT[index[i]]);
			toggle = true;
			break;
		case 'n':
			VEC = VEC_INIT;
			toggle = false;
			break;
		default:
			break;
	}
	init_VERTEX_V_O();
	GLUT::PostRedisplay();
}

void update(int value){
	if (!toggle){
		vector<unsigned long long> index;
		VEC.clear();
		index = convex_hull(VEC_INIT);
		for (unsigned long long i = 0; i < index.size(); ++i)
			VEC.push_back(VEC_INIT[index[i]]);
	}else
		VEC = VEC_INIT;
	toggle = !toggle;
	init_VERTEX_V_O();
	GLUT::PostRedisplay();
	GLUT::Timer(600, update, 0);
}


int main(int argc, char *argv[]){
	fstream input("input.txt", ios::in);
	unsigned long long n;
	n = 0;
	input >> n;
	VEC_INIT.clear();
	pt elem;
	for (unsigned long long i = 0; i < n; ++i){
		input >> elem.x >> elem.y;
		VEC_INIT.push_back(elem);
	}
	VEC = VEC_INIT;
	int width, height;
	width = 600;
	height = 600;
	GLUT::Default("MAIN", &argc, argv, width, height);
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK){
		cerr << "Error: "<< glewGetErrorString(err) << endl;
		return 1;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0, width, height);
	toggle = false;
	shader_program sp = init_shader_program();
	/*На этом моменте мы имеем скомпилированную
	шейдерную программу, с перменными pos и mvp*/
	ShPr = sp.id();
	init_VERTEX_V_O();/*Идет закрепление переменных*/

	GLUT::Display(render);
	cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION) << endl;
	// GLUT::Keyboard(keys);
	GLUT::Timer(600, update, 0);
	GLUT::MainLoop();
	return 0;
}
