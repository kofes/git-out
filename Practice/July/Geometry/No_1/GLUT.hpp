#ifndef __INCLUDE_MY_GLUT_

#define __INCLUDE_MY_GLUT_

#include <GL/freeglut.h>
#include <string>

namespace GLUT{
	void Default(std::string, int *, char **, int width, int height);
	void Init(int *argc, char **argv);
	void DisplayMode(unsigned int);
	void WindowSize(int width, int height);
	void WindowPos(int x, int y);
	void ContextVersion(int, int);
	void ContextProfile(int);
	void CreateWindow(std::string);
	void Display(void (*render)(void));
	void Reshape(void (*reshape)(int w, int h));
	void MainLoop();
	void Keyboard(void (*keyboard)(unsigned char button, int x, int y));
	void Timer(unsigned int msecs, void (*update)(int value), int value);
	void PostRedisplay();
}

#endif
