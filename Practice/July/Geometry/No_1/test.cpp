#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;

int main(int argc, char *argv[]){
	fstream fp("input.txt", ios::out);
	unsigned int count;
	srand(time(NULL));
	if (argc <= 1)
		count = 1000;
	else
		count = strtof(argv[1], argv + 1);
	fp << count << '\n';
	for (unsigned int i = 0; i < count; ++i)
		fp << (float)rand() - RAND_MAX / 2.0f << ' ' << (float)rand() - RAND_MAX / 2.0f << '\n';
	fp.close();
	return 0;
}
