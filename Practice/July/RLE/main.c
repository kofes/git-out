#include "RLE.h"

int main(){
	FILE *in, *out;
	setlocale(LC_ALL, "");
	in = fopen("input.txt", "rb");
	out = fopen("file.RLE", "wb");
	RLE_code(in, out);
	fclose(in);
	fclose(out);
	in = fopen("file.RLE", "rb");
	out = fopen("output.txt", "wb");
	RLE_decode(in, out);
	fclose(in);
	fclose(out);
	return 0;
}