/*	RLE	*/
#include "RLE.h"
#define FALSE (0)
#define TRUE (1)
int RLE_code(FILE *fin, FILE *fout){
	wchar_t i = 0, j = 0, first = 1, ch[2], sym;
	if ((fin == NULL)||(fout == NULL))
		return ERR_DATA;
	while (!feof(fin)){
		ch[i] = fgetwc(fin);
		if (feof(fin))
			return 0;
		fputwc(ch[i], fout);
		i = (i + 1)%2;
		if (first)
			first = FALSE;
		else{
			if (ch[0] == ch[1]){
				while ((!feof(fin))&&(j < UCHAR_MAX)){
					sym = fgetwc(fin);
					if (feof(fin)){
						fputwc(j, fout);
						return 0;
					}
					if (sym == ch[0])
						++j;
					else{
						fputwc(j, fout);
						fputwc(sym, fout);
						break;
					}
				}
				if (j == UCHAR_MAX){
					fputwc(j, fout);
				}
				j = 0;
				ch[(i+1)%2] = sym;
			}
		}
	}
	return ERR_NORM;
}
int RLE_decode(FILE *fin, FILE *fout){
	wchar_t ch[2], i = 0, first = 1, count = 0;
	if ((fin == NULL)||(fout == NULL))
		return ERR_DATA;
	while (!feof(fin)){
		ch[i] = fgetwc(fin);
		if (feof(fin))
			return 0;
		fputwc(ch[i], fout);
		i++;
		i %= 2;
		if (first)
			first = FALSE;
		else if (ch[0] == ch[1]){
			count = fgetwc(fin);
			while (count != 0){
				fputwc(ch[0], fout);
				--count;
			}
		}
	}
	return ERR_NORM;
}