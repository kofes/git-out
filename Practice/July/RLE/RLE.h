#include <stdio.h>
#include <limits.h>
#include <wchar.h>
#include <locale.h>
/*Коды ошибок*/
#define ERR_NORM (0)/*Функция отработала без ошибок*/
#define ERR_DATA (1)/*Ошибка входных данных*/
/*Кодирование входных данных алгоритмом RLE*/
int RLE_code(FILE *, FILE *);
/*Декодирование входных данных*/
int RLE_decode(FILE *, FILE *);