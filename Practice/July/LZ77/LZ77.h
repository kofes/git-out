#ifndef __INCLUDE_LZ77_
#define __INCLUDE_LZ77_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

/*-----------*/
/*Коды ошибок*/
#define ERR_NORM	(0)
#define ERR_DATA	(1)
/*--------------------------------*/
/*Границы размеров окна*/
#define SIZE_MIN	(4)
#define SIZE_MAX	(256)

/*Функция кодирования файла*/
extern int LZ77_code(FILE *in, FILE *out, size_t WINDOW_SIZE);

/*Функция декодирования файла*/
extern int LZ77_decode(FILE *in, FILE *out, size_t WINDOW_SIZE);

#endif