#include "LZ77.h"
int main(int argv, char **argc){
	FILE *fp, *fLZ;
	unsigned short WINDOW_SIZE = (argv) > (1) ? (strtoul(*argc + 7, NULL, 0)) : 16;
	fp = fopen("10 Silver for Monsters....flac", "rb");
	fLZ = fopen("file.LZ", "wb");
	LZ77_code(fp, fLZ, WINDOW_SIZE);
	fclose(fp);
	fclose(fLZ);
	fLZ = fopen("file.LZ", "rb");
	fp = fopen("output.flac", "wb");
	LZ77_decode(fLZ, fp, WINDOW_SIZE);
	fclose(fLZ);
	fclose(fp);
	return 0;
}