#include "LZ77.h"
/*--------------------------------------------*/
/*Поиск в "словаре" текущей подстроки символов*/
long BMH(const unsigned char* S, const unsigned char* T, size_t len_S, size_t len_T){
	size_t i, j, k;
	unsigned char alph_table[256];

	if ((S == NULL)||(T == NULL))
		return -1;

	if (len_T <= len_S){
		for (i = 0; i < 256; ++i)
			alph_table[i] = len_T;
		for (i = 1; i < len_T; ++i)
			alph_table[T[i]] = len_T - i;
		i = len_T;
		do{
				j = len_T;
				k = i;
				while ((j > 0)&&((S[k-1]) == (T[j-1]))){
						--k;
						--j;
					}
				i = i + alph_table[S[i]];
			}while ((j > 0)&&(i <= len_S));
		if (j != 0)
			return -1;
		else
			return k;
	}
	return -1;
}
/*---------------------------------------------------*/
/*Заполнение двух байт 9 битовым кодом строки/символа*/
size_t LZ77_setelem(char sign, unsigned char pos, char count, unsigned char size){
	return (1 + (count << (size + 1)))*(sign != 0) + (pos<<1);
}

int LZ77_code(FILE *in, FILE *out, size_t WINDOW_SIZE, FILE *binout){
/*------------------------------------------------------------------*/
/*Инициализация всех параметров и проверка входных данных(ALL RIGHT)*/
	if ((in == NULL)||(out == NULL)||(WINDOW_SIZE < SIZE_MIN)||(WINDOW_SIZE > SIZE_MAX))
		return ERR_DATA;
	if (feof(in))
		return ERR_NORM;
	unsigned long long n;
	long long m;
	size_t elem, i, j;
	unsigned char buff, pos, k, size;
	unsigned char window[WINDOW_SIZE], buffer[WINDOW_SIZE];
	unsigned char ch;
	memset(buffer, 0, WINDOW_SIZE);
	for (size = 1; (1 << size) < WINDOW_SIZE; ++size);
	for (i = 0, ch = fgetc(in); (!feof(in))&&(i < WINDOW_SIZE); ch = fgetc(in), ++i)
		window[i] = ch;
	pos = 0;
	buff = 0;
/*-----------------------------------------------*/
/*Вывод самого первого блока на печать.(ALL WORK)*/
	for (j = 0; j < i; ++j){
		elem = LZ77_setelem(0, window[j], 0, 0);
		for (k = 0; k < (CHAR_BIT + 1); ++k){
			if (elem & (1<<k))
				buff |= 1 << pos;
			++pos;
			if (pos == CHAR_BIT){
				fputc(buff, out);
				pos = 0;
				buff = 0;
			}
		}
	}
/*----------------------------------------------------------------------*/
/*Если первые считанные символы последние, то завершаем программу.(WORK)*/
	if (feof(in)){
		if (pos != 0)
			fputc(buff, out);
		return ERR_NORM;
	}
/*--------------------------------------------*/
/*Основные приключения окна со словарем.(WORK)*/
	buffer[0] = ch;
	i = 1;
	while (!feof(in)){
		while ((!feof(in))&&((m = BMH(window, buffer, WINDOW_SIZE, i)) >= 0)&&(i < WINDOW_SIZE)){
			n = m;
			ch = fgetc(in);
			buffer[i++] = ch;
		}
		if ((m < 0)||(feof(in)))	--i;
		if ((i == 0)||(m >= 0)) ch = fgetc(in);
		if (!i)
			elem = LZ77_setelem(0, buffer[0], 0, 0);
		else
			elem = LZ77_setelem(1, n, i - 1, size);
		for (k = 0; k < ((i) == (0) ? (CHAR_BIT + 1) : (1 + (size << 1))); ++k){
			if (elem & (1<<k))
				buff |= 1 << pos;
			++pos;
			if (pos == CHAR_BIT){
				fputc(buff, out);
				pos = 0;
				buff = 0;
			}
		}
		if (i == 0) ++i;
		memmove(window, window + i, WINDOW_SIZE - i);
		memcpy(window + (WINDOW_SIZE - i), buffer, i);
		buffer[0] = ch;
		i = 1;
	}
	if (pos != 0)
		fputc(buff, out);
	return ERR_NORM;
}

int LZ77_decode(FILE *in, FILE *out, size_t WINDOW_SIZE, FILE *binout){
	if ((in == NULL)||(out == NULL)||(WINDOW_SIZE < SIZE_MIN)||(WINDOW_SIZE > SIZE_MAX))
		return ERR_DATA;
	size_t max_buff, pos, j, i;
	unsigned char buff, k, size;
	unsigned char window[WINDOW_SIZE], buffer[WINDOW_SIZE];
	unsigned char ch, flag, sign;
	if (feof(in))
		return ERR_NORM;
	for (size = 0; (1 << size) < WINDOW_SIZE; ++size);
	memset(buffer, 0, WINDOW_SIZE);
	buff = 0; pos = 0; j = 0; flag = 0;
/*Составление первоначального шаблона (window) и вывод его в файл.(ALL WORK)*/
	for (i = 0, ch = fgetc(in); (!feof(in))&&(j < WINDOW_SIZE); ch = fgetc(in), ++i){
		for (k = 0; k < CHAR_BIT; ++k){
			if (!flag){
				flag = 1;
				sign = 0;
				sign |= (ch & (1 << k));
				continue;
			}
			if (ch & (1 << k))
				buff |= 1 << pos;
			++pos;
			if (pos == CHAR_BIT){
				window[j++] = buff;
				fputc(buff, out);
				buff = 0;
				pos = 0;
				flag = 0;
			}
		}
	}
/*Если первые считанные символы последние, то завершаем программу.(WORK)*/
	if (feof(in))
		return ERR_NORM;
/*Основные приключения окна со словарем.(WORK)*/
	k = 0; max_buff = buff;
	if (sign)
		max_buff = buff;
	else
		max_buff = 0;
	while (!feof(in)){
		while (k < CHAR_BIT){
			if (!flag){
				flag = 1;
				sign = 0;
				sign |= (ch & (1 << k));
				++k;
			} else
				k = 0;
			if (!sign){
				for (; k < CHAR_BIT; ++k){
					if (!flag)	break;
					if (ch & (1 << k))
						buff |= 1 << pos;
					++pos;
					if (pos == CHAR_BIT){
						fputc(buff, out);
						memmove(window, window + 1, WINDOW_SIZE - 1);
						window[WINDOW_SIZE - 1] = buff;
						buff = 0;
						pos = 0;
						flag = 0;
					}
				}
			} else{
				for (; k < CHAR_BIT; ++k){
					if (!flag)	break;
					if (ch & (1 << k))
						max_buff |= 1 << pos;
					++pos;
					if (pos == (size << 1)){
						for (i = 0; i < (((max_buff >> size)&((1 << size) - 1)) + 1); ++i){
							buffer[i] = window[(max_buff & ((1 << size) - 1)) + i];
							fputc(buffer[i], out);
						}
						memmove(window, window + i, WINDOW_SIZE - i);
						memcpy(window + (WINDOW_SIZE - i), buffer, i);
						max_buff = 0;
						pos = 0;
						flag = 0;
					}
				}
			}
		}
		k = 0;
		ch = fgetc(in);
	}
	return ERR_NORM;
}
