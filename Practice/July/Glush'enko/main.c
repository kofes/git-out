#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define ERR_NORM (+0)
#define ERR_DIV0 (+1)
#define ERR_OPEN (+2)
#define ERR_SIGN (+3)
#define ERR_CHAR (+4)

int get_type (char pos);
int RS (char **pos);
int RN (char **pos);
int RU (char **pos);
int RB (char **pos);
int RCON(char **pos);

typedef union t_lexem
{
    double val;
    char sign;
    size_t ind;
} t_lexem;

typedef struct t_btree_node  //���� ������
{
    struct t_btree_node* child[2];
    t_lexem lex;
    char type;
//    int height; //������ ������ - ��� ������� AVL
} t_btree_node;

typedef struct //���� ��������� ������
{
    t_btree_node *root;
} t_btree_struct;

typedef struct t_node
{
    t_btree_node* lex;
    struct t_node* next;
} t_node;

typedef struct
{
    int val;
    t_node* head;
} t_stack;

typedef struct t_con
{
    char* str;
    size_t id;
    double val;
}t_con;

static t_stack *NUMB, *SIGN;
t_con *cons;
//cons = (t_con*)malloc(sizeof(t_con)*1);
int id = 0;


t_stack *stack_new ();
void stack_del (t_stack *st);
void stack_add (t_stack* st, t_btree_node* lex);
void stack_pop (t_stack *st);
t_btree_node* stack_top (t_stack* st);
int add_con(char *str);

const static char STAT [5][9] =
{
    {0, 1, 2, -1, 0, -1, 1, -1, -1},
    {1, -2, 2, 2, -1, 1, -1, 3, -1},
    {2, 1, -2, -2, 0, -1, 1, -1, -1},
    {0, -1, -1, -1, 0, -1, -1, -1, -1 },
    {1, -1, 2, 2, -1, 1, -1, 3, -1}
};

typedef int (*t_func)(char** pos); //��� ��������� �� �������

const static t_func FUNC [5][9] =
{
    {&RS, &RN, &RU, NULL, &RU, NULL, &RCON, NULL, NULL},
    {&RS, NULL, &RB, &RB, NULL, &RB, NULL, NULL, NULL},
    {&RS, &RN, NULL, NULL, &RU, NULL, &RCON, NULL, NULL},
    {&RS, NULL, NULL, NULL, &RU ,NULL, NULL, NULL},
    {&RS, NULL, &RB, &RB, NULL, &RB, NULL, NULL, NULL}
};

t_btree_struct* btree_new()//�����������
{
    t_btree_struct *tree;
    if((tree = (t_btree_struct *) malloc(sizeof(t_btree_struct))) == NULL)
        return NULL;//��������� ������
    tree -> root = NULL;
    return tree;
}


t_btree_node* btree_new_node()
{
    t_btree_node *node;
    if((node = (t_btree_node*)malloc(sizeof(t_btree_node)))==NULL)
    {
        return NULL;
    }
    //printf("val");
    node -> child[0] = NULL;
    node -> child[1] = NULL;
    node->type = -1;
    //node -> lex = 0;
    // node -> height = 1; //��� ���
    return node;
}

void btree_out (t_btree_struct* tree)
{
    static size_t level, ii;
    level = 0;
    void btree_print_node(t_btree_node *node)
    {
        if (node == NULL) {
            return;
        }
        ++ level;
        btree_print_node(node -> child[0]);
        for (ii = 0; ii < level; ++ ii, printf("\t"));
        if(node->type == 0)
            printf("%f \n", node -> lex.val);
        if(node->type == 1)
            printf("%i \n", node -> lex.ind);
        if(node->type == 2)
            printf("%c \n", node -> lex.sign);
        btree_print_node(node -> child[1]);
        -- level;
        //prtinf("%i \n", node -> key);
    }
    if (tree == NULL) {
        return;
    }
    printf("/////////////TREE///////////////\n");
    btree_print_node(tree -> root);
}

void give_const()
{
    int i;
    for(i=0; i<id; ++i)
    {
        //printf("%s", cons[i].str);
        printf("Enter: %s ", cons[i].str);
        scanf("%lf",&cons[i].val);
    }
    return;
}

double get_res(t_btree_struct *tree)
{
    double result;

    void get_res_node(t_btree_node *node)
    {
        if (node == NULL) {
            return;
        }
        int i;
        get_res_node(node -> child[1]);
        get_res_node(node -> child[0]);
        if(node->type == 0)
        {
            return;
        }
        if(node->type == 1)
        {
            for (i=0; i< id; ++i)
            {
                if(node->lex.ind == cons[i].id)
                {
                    node->lex.val = cons[i].val;
                    //printf("sus=%lf",node->lex.val);
                    node->type = 0;
                }
            }
            return;
        }
        if((node->child[1] != NULL) && (node->child[0] == NULL))
        {
            if(node->child[1]->type == 0)
            {
                switch (node->lex.sign)
                {
                    case 's':
                    {
                        node->lex.val=sin(node->child[1]->lex.val);
                        node->type = 0;
                        //printf("s %lf", node->lex.val);
                        break;
                    }
                    case 'c':
                    {
                        node->lex.val=cos(node->child[1]->lex.val);
                        node->type = 0;
                        //printf("c %lf", node->lex.val);
                        break;
                    }
                    case 'l':
                    {
                        node->lex.val=log(node->child[1]->lex.val);
                        node->type = 0;
                        //printf("l %lf", node->lex.val);
                        break;
                    }
                    case 'e':
                    {
                        node->lex.val=exp(node->child[1]->lex.val);
                        node->type = 0;
                        //printf("e %lf", node->lex.val);
                        break;
                    }
                    case 'q':
                    {
                        node->lex.val=sqrt(node->child[1]->lex.val);
                        node->type = 0;
                        //printf("q %lf", node->lex.val);
                        break;
                    }
                    case -'-':
                    {
                        node->lex.val=(-1)*(node->child[1]->lex.val);
                        node->type = 0;
                        //printf("-- %lf", node->lex.val);
                        break;
                    }
                }
            }
            return;
        }

        if((node->child[1] != NULL) && (node->child[0] != NULL))
        {
            if((node->child[1]->type == 0) && (node->child[0]->type == 0))
            {
                switch (node->lex.sign)
                {
                    case '+':
                    {
                        node->lex.val=node->child[1]->lex.val + node->child[0]->lex.val;
                        node->type = 0;
                        //printf("+ %lf", node->lex.val);
                        break;
                    }
                    case '*':
                    {
                        node->lex.val=node->child[1]->lex.val * node->child[0]->lex.val;
                        node->type = 0;
                        //printf("* %lf", node->lex.val);
                        break;
                    }
                    case '-':
                    {
                        node->lex.val=node->child[0]->lex.val - node->child[1]->lex.val;
                        node->type = 0;
                        //printf("- %lf", node->lex.val);
                        break;
                    }
                    case '/':
                    {
                        node->lex.val=node->child[0]->lex.val / node->child[1]->lex.val;
                        node -> type = 0;
                        //printf("/ %lf", node->lex.val);
                        break;
                    }
                    case '^':
                    {
                        node -> lex.val = pow(node -> child[0] -> lex.val , node -> child[1] -> lex.val);
                        node -> type = 0;
                        //printf("^ %lf", node->lex.val);
                        break;
                    }
                }
            }
            return;
        }
        //get_res_node(node -> child[1]);

        //prtinf("%i \n", node -> key);
    }
    if (tree == NULL) {
        return 0;
    }
    get_res_node(tree -> root);
    //printf("%i\n", tree->root->type);
    result = tree->root->lex.val;
    printf("RESULR =  %lf\n",tree->root->lex.val);

    return result;
}

int parser (const char* str, double* val)
{
    int get_type (char buf)
    {
        if (buf == ' ')
            return 0;
        if ((buf >= '0') && (buf <= '9'))
            return 1;
        if (buf == '-')
            return 2;
        if ((buf == '+') || (buf =='*') || (buf == '/') || (buf == '^'))
            return 3;
        if (buf == '(')
            return 4;
        if (buf == ')')
            return 5;
        if ((buf >= 'A') && (buf <= 'z'))
            return 6;
        if ((buf == '\n') || (buf == '\0'))
            return 7;
        return 8;
    }
    int result;
    cons = (t_con*)malloc(sizeof(t_con)*1);
    char *pos = str;
    int flag, t, g, err;
    flag = 0;
    err = ERR_NORM;
    NUMB = stack_new();
    SIGN = stack_new();
    while (1)
    {
        //printf("%c\n", *pos);
        t = get_type(*pos);
        g = flag;
        flag = STAT[g][t];
        if (flag<0)
        {
            if (flag == -2)
                err = ERR_SIGN;
            else
                err = ERR_CHAR;//������������ ������
            break;
        }
        if (flag>2)
        {
            err = RB(&pos);//����������� ��� ���������� �������� �� �����!
            break;//��� ��������� ����� ������!
        }
        //printf("Func %i %i", g, t);
        err = FUNC [g][t](&pos);
        if(err == 30)
        {
            flag = 3;
            err = 0;
        }
        if(err == 40)
        {
            flag = 4;
            err = 0;
        }
        if (err != ERR_NORM)
        {
            break;//������ ����������
        }
    }
    t_btree_struct *tree = btree_new();
    if (err == ERR_NORM)
    {
        tree->root = stack_top(NUMB);
        //stack_pop(NUMB);
    }
    else
    {
        return err;
    }

    btree_out (tree);

    int key;
    printf("ENTER A KEY : 0 - calculate ; !=0 - dif\n");
    //scanf("key = %i\n", &key);
    //printf("key = %i", key);
    key = 1;
    if(key == 1)
    {
        if(id > 0)
        {
            give_const();
        }
        result = get_res(tree);
        //printf("result = %.25lf\n", result);
    }
    else
    {

    }

    //������� ������!
    stack_del(SIGN);
    stack_del(NUMB);
    return err;
}

t_stack* stack_new ()
{
    t_stack *st = (t_stack*) malloc(sizeof(t_stack));
    st -> head = NULL;
    st -> val = 0;
    return st;
}

void stack_del (t_stack* st)
{
    t_node* curr;
    while (st -> head != NULL)
    {
        curr = st -> head;
        st -> head = curr -> next;
        free (curr);
    }
    free (st);
}

void stack_add (t_stack* st, t_btree_node* lex)
{
    t_node* node;
    node = (t_node*)malloc(sizeof(t_node));
    node->lex = lex;
    /*if(node->lex->type == 0)
        printf("add_lex_val %f \n", node ->lex -> lex.val);
    if(node->lex->type == 1)
        printf("add_lex_ind %i \n", node->lex -> lex.ind);
    if(node->lex->type == 2)
        printf("add_lex_sign %c \n", node->lex -> lex.sign);*/
    //node -> lex = lex;
    node -> next = st -> head;
    st -> head = node;
    st -> val++;
}

void stack_pop (t_stack* st)
{
    t_node* node = st -> head;
    /*if(node->lex->type == 0)
        printf("del_lex %i \n", node->lex -> lex.val);
    if(node->lex->type == 1)
        printf("del_lex %i \n", node->lex -> lex.ind);
    if(node->lex->type == 2)
        printf("del_lex %c \n", node->lex -> lex.sign);*/
    if (node != NULL)
        st -> head = node -> next;
    free (node);
    st->val--;
}

t_btree_node* stack_top (t_stack* st)
{
    //printf("st size %i\n", st->val);
    return st->head->lex;
}

int priority (char ch)
{
    switch (ch)
    {
        case '(': return -1;
        case '\0': return 0;
        case '\n': return 0;
        case ')': return 1;
        case '+': return 2;
        case '-': return 2;
        case '*': return 3;
        case '/': return 3;
        case -'-': return 4;
        case 's': return 4;
        case 'c': return 4;
        case 'l': return 4;
        case 'e': return 4;
        case '^': return 5;
    }
}

void get_numbs(t_stack* NUMB, t_btree_node** num1, t_btree_node** num2)
{
    *num2 = stack_top(NUMB);
    stack_pop(NUMB);
    *num1 = stack_top(NUMB);
    stack_pop(NUMB);
}

int RS (char **pos)
{
    if (pos == NULL)
        return -1;
    while (**pos == ' ')
    ++ *pos;
    return 0;
}

int RN (char** pos)
{
    t_btree_node* node = btree_new_node();
    node->lex.val = strtof(*pos, pos);
    node->type = 0;
    stack_add(NUMB, node);
    //printf("!!!!!!!");
    //t_lexem elem;
    //elem.val = strtof(*pos, pos);
    //stack_add(NUMB, elem);
    return 0;
}

int RU (char** pos)
{
    t_btree_node* node = btree_new_node();
    //t_lexem elem;
    char ch = **pos;
    if (ch == '-')
        node->lex.sign = - ch;
    else
        node->lex.sign = ch;
    node->type = 2;
    stack_add(SIGN, node);
    ++ *pos;
    return 0;
}

int RCON(char** pos)
{
    t_btree_node* node = btree_new_node();
    char str[100];
    int i = 0;
    for(i=0; i< 100; ++i)
    {
        str[i]=0;
    }
    i=0;
    while((**pos <= 'z') && (**pos >= 'A'))
    {
        //strcpy( str, pos );
        str[i] = **pos;
        (*pos)++;
        i++;
    }
    //printf("STR :: %s\n", str);
    if(strcmp(str, "cos") == 0)
    {
        node->lex.sign = 'c';
        node->type = 2;
        stack_add(SIGN, node);
        return 30;
    }
    if(strcmp(str, "sin") == 0)
    {
        node->lex.sign = 's';
        node->type = 2;
        stack_add(SIGN, node);
        //printf("add sin\n");
        return 30;
    }
    if(strcmp(str, "log") == 0)
    {
        node->lex.sign = 'l';
        node->type = 2;
        stack_add(SIGN, node);
        return 30;
    }
    if(strcmp(str, "sqrt") == 0)
    {
        node->lex.sign = 'q';
        node->type = 2;
        stack_add(SIGN, node);
        return 30;
    }
    if(strcmp(str, "exp") == 0)
    {
        node->lex.sign = 'e';
        node->type = 2;
        stack_add(SIGN, node);
        return 30;
    }
    else
    {
        node->lex.ind = id;
        node->type = 1;
        add_con(str);
        stack_add(NUMB, node);
        return 40;
    }
}

int add_con(char *str)
{
    //cons[id].str = str;
    cons[id].str = (char*)calloc(10, sizeof(char));
    int i;
    for(i=0;i<10;++i)
        cons[id].str[i] = str[i];
    cons[id].id = id;
    id++;
    cons = (t_con*)realloc(cons, sizeof(t_con)*(id+1));
    return 0;
}

int stack_clear(t_stack *st)
{
    if (st -> head == NULL)
        return 1;
    return 0;
}

int RB (char** pos)
{
    char ch = **pos;
    int p = priority (ch);

    while (!stack_clear(SIGN) && (priority(stack_top(SIGN)->lex.sign) >= p))
    {
        t_btree_node* ct = stack_top(SIGN);
        stack_pop(SIGN);
        //t_lexem lex;
        t_btree_node* node0;// = btree_new_node();
        t_btree_node* node1;// = btree_new_node();

        //double num1, num2;
        //printf("   %i", ct->lex.sign);
        switch (ct->lex.sign)
        {
            case - '-':
                //printf("!!!!!!!");
                node1 = stack_top(NUMB);
                stack_pop(NUMB);
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case 'c':
                node1 = stack_top(NUMB);
                stack_pop(NUMB);
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case 's':
                node1 = stack_top(NUMB);
                stack_pop(NUMB);
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case 'l':
                node1 = stack_top(NUMB);
                stack_pop(NUMB);
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case 'e':
                node1 = stack_top(NUMB);
                stack_pop(NUMB);
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case 'q':
                node1 = stack_top(NUMB);
                stack_pop(NUMB);
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case '+':
                get_numbs(NUMB, &node0, &node1);
                if((node0 == NULL) || (node1 == NULL))
                {
                   // printf("=((\n");
                }
                ct->child[0] = node0;
                ct->child[1] = node1;
                if((ct->child[0] == NULL) || (ct->child[0] == NULL))
                {
                    //printf("=++++++++++++++++++++++\n");
                }

                stack_add(NUMB, ct);
                if(stack_top(NUMB)->child[0]==NULL)
                {
                    //printf("QWERTYUYTDCV");
                }
            break;
            case '-':
                get_numbs(NUMB, &node0, &node1);
                if((node0 == NULL) || (node1 == NULL))
                {
                   // printf("=((((((((((((((((((((((((((((((((((\n");
                }
                ct->child[0] = node0;
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case '/':
                get_numbs(NUMB, &node0, &node1);
                if((node0 == NULL) || (node1 == NULL))
                {
                    printf("=((\n");
                }
                if (node1->lex.val == 0)
                {
                    return 1; // ��������� ������� �� 0!!
                }
                ct->child[0] = node0;
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case '*':
                get_numbs(NUMB, &node0, &node1);
                if((node0 == NULL) || (node1 == NULL))
                {
                    printf("=((\n");
                }
                ct->child[0] = node0;
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
            case '^':
                get_numbs(NUMB, &node0, &node1);
                ct->child[0] = node0;
                ct->child[1] = node1;
                stack_add(NUMB, ct);
            break;
        }
    }
    if (ch == ')')
    {
        if (stack_clear(SIGN))
        {
            return 2; // �������� ����������� ������! ������ �������������.
        }
        stack_pop(SIGN);
        ++ *pos;
        return 0;
    }
    if ((ch == '\0') || (ch == '\n'))
    {
        if (!stack_clear(SIGN))
        {
            return 2; // �������� ����������� ������! ������ �������������.
        }
        return 0;
    }
    t_btree_node* ct = btree_new_node();// = stack_top(SIGN);//??
    ct->lex.sign = ch;//??
    ct->type = 2;
    //t_lexem lex;
    //lex.sign = ch;
    stack_add(SIGN, ct);
    ++ *pos;
    return 0;
}

int main()
{
    double x;
    //char* str = "4.25*4+cos(4-x) + sus";//"2 + 3 * (3 / sin(cos(-1 + sus)) - 1) + 4 * (2 - 7 * cos(0))";// //"(1-sin(2)-3)"; //������ ������������� ������
    char* str = "4+2*5";
    int code = parser (str, &x);
    if ( code == 0)
    {
        //printf("Otvet: %lf\n", x);
    }
    else
    {
        printf("ERROR! - %i\n", code);
    }
    return 0;
}
