#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "AVL.h"

typedef struct t_btree_node  //узел дерева
{
    struct t_btree_node* child[2];
    t_btree_key key;
//    int height; //высота левого - выс правого AVL
} t_btree_node;

typedef struct //сама структура дерева
{
    t_btree_node *root;
} t_btree_struct;

#define BTREE_PTR(a) ((t_btree_struct *) a)

t_btree btree_new()//конструктор
{
    t_btree_struct *tree;
    if((tree = (t_btree_struct *) malloc(sizeof(t_btree_struct))) == NULL)
        return;//выделение памяти
    tree -> root = NULL;
    return tree;
}

void btree_del (t_btree tree)//диструктор
{
    void btree_del_node(t_btree_node *node)
    {
        if (node == NULL) {
            return;
        }
        btree_del_node(node -> child[0]);
        btree_del_node(node -> child[1]);
        free(node);
    }
    if (tree == NULL) {
        return;
    }
    btree_del_node(BTREE_PTR(tree) -> root);
}

int height(t_btree_node* node)
{
    if (node == NULL)
        return 0;
    else
        return node -> height;
}

int diff_height(t_btree_node* node)
{
    return height(node -> child[1]) - height(node -> child[0]);
}

void fixheight(t_btree_node* node)
{
    int hl = height(node -> child[0]);
    int hr = height(node -> child[1]);
    node -> height = (hl > hr ? hl : hr) + 1;
}

t_btree_node *right_turn(t_btree_node* node) // правый поворот
{
    t_btree_node *next = node -> child[0];
    node -> child[0] = next -> child[1];
    next -> child[1] = node;
    fixheight(node);
    fixheight(next);
    return next;
}

t_btree_node *left_turn(t_btree_node* node) // левый поворот
{
    t_btree_node *next = node -> child[1];
    node -> child[1] = next -> child[0];
    next -> child[0] = node;
    fixheight(next);
    fixheight(node);
    return next;
}

t_btree_node *double_turn(t_btree_node* node)
{
    fixheight(node);
    if( diff_height(node) == 2 )
    {
        if( diff_height(node -> child[1]) < 0 )
            node -> child[1] = right_turn(node -> child[1]);
        return left_turn(node);
    }
    if( diff_height(node) == -2 )
    {
        if( diff_height(node -> child[0]) > 0  )
            node -> child[0] = left_turn(node -> child[0]);
        return right_turn(node);
    }
    return node;
}

t_btree_val *btree_add_elem(t_btree tree, t_btree_key key)
{
    t_btree_val *val = NULL;
    t_btree_node* btree_new_node()
    {
        t_btree_node *node;
        if((node = (t_btree_node*)malloc(sizeof(t_btree_node)))==NULL)
        {
            return NULL;
        }
        //printf("val");
        node -> child[0] = NULL;
        node -> child[1] = NULL;
        node -> key = key;
        node -> height = 1; //для АВЛ
        val = &(node->key);
        return node;
    }
    t_btree_node *btree_add_node(t_btree_node *node) // вставка ключа
    {
        if(node == NULL)
        {
            //printf("val");
            return btree_new_node();
        }
        if( key < node -> key )
            node -> child[0] = btree_add_node(node -> child[0]);
        else
            node -> child[1] = btree_add_node(node -> child[1]);
        return (node);
    }
    BTREE_PTR(tree) ->root = btree_add_node(BTREE_PTR(tree) -> root);
    return val;
}

t_btree_node* min_node(t_btree_node* node)
{
    if (node -> child[0] == NULL)
    {
        return node;
    }
    else
    {
        return min_node(node -> child[0]);
    }
}

t_btree_node* min_node_del(t_btree_node* node)
{
    if( node -> child[0] == 0) {
        return node -> child[1];
    }
    node -> child[0] = min_node_del(node -> child[0]);
    return (node);
}

void btree_del_elem(t_btree tree, t_btree_key key) // удаление ключа
{
    t_btree_node *btree_del_node(t_btree_node *node)
    {
        if(node == NULL)
        {
            return 0;
        }
        if( key < node -> key )
            node -> child[0] = btree_del_node(node -> child[0]);
        else if( key > node->key )
            node -> child[1] = btree_del_node(node -> child[1]);
        else
        {
            t_btree_node* nodel = node -> child[0];
            t_btree_node* noder = node -> child[1];

            free(node);
            if( noder == NULL )
                return nodel;
            t_btree_node* min = min_node(noder);
            min -> child[1] = min_node_del(noder);
            min -> child[0] = nodel;
            return (min);
        }
        return (node);
    }
    BTREE_PTR(tree) -> root = btree_del_node(BTREE_PTR(tree) -> root);
}

t_btree_val *btree_get_elem(t_btree tree, t_btree_key key)
{
    t_btree_val *val = NULL;

    t_btree_node *btree_get_node(t_btree_node *node) // вставка ключа
    {
        if(node == NULL)
        {
            return NULL;
        }
        if(node -> key == key)
        {
            val = &(node -> key);
            return node;
        }
        if( key < node -> key )
            node -> child[0] = btree_get_node(node -> child[0]);
        else
            node -> child[1] = btree_get_node(node -> child[1]);
    }
    BTREE_PTR(tree) -> root = btree_get_node(BTREE_PTR(tree) -> root);
    return val;
}

void btree_out (t_btree tree, FILE *out)
{
    void btree_print_node(t_btree_node *node)
    {
        if (node == NULL) {
            return;
        }
        btree_print_node(node -> child[0]);
        fprintf(out, "%i \n", node -> key);
        btree_print_node(node -> child[1]);
        //prtinf("%i \n", node -> key);
    }
    if (tree == NULL) {
        return;
    }
    btree_print_node(BTREE_PTR(tree) -> root);
}
