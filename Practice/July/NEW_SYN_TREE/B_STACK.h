#ifndef __INCLUDE_B_STACK_H
#define __INCLUDE_B_STACK_H

#include <stdio.h>
#include <stdlib.h>
#include "Syn_STACK.h"

//Признак открепленного дескриптора:
#define STACK_NULL (NULL)

//Освобождает память из-под стека:
extern void list_del(void *);

//Выполняет вставку элемента в вершину стека:
extern tt_node **list_add_elem(void **);

//Заполняет переменную из списка значением double:
extern void list_fill(void *, double);

//Выводит содержимое стека в выходной поток:
extern void list_out(void *, FILE *);

#endif //__INCLUDE_STACK_H
