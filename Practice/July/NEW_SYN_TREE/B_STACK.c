#include "B_STACK.h"

typedef struct l_node{
	struct l_node* next;
	tt_node *point;
}l_node;

void list_del(void *head){

	l_node *curr, *node;

	if (head == NULL)
		return;
	curr = *((l_node **)head);

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		free(node);
	}
}

tt_node **list_add_elem(void **head){

	l_node *node;

	if ((node = (l_node *)malloc(sizeof(l_node))) == NULL)
		return NULL;
	node->point = NULL;
	node->next = (l_node *)*head;
	*head = (void *)node;
	return &(node->point);
}

void list_fill(void *head, double val){
	l_node *curr;

	if (head == NULL)
		return;
	curr = (l_node *)head;

	while (curr != NULL){
		curr->point->elem.flag = 0;
		curr->point->elem.val = val;
		curr = curr->next;
	}
}

void list_out(void *head, FILE *out){

	l_node *curr;

	if ((head == NULL)||(out == NULL))
		return;

	curr = (l_node *)head;

	while (curr != NULL){
		fprintf(out, "%lf ", curr->point->elem.val);
		curr = curr->next;
	}
}