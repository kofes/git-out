#include "Syn_STACK.h"
#include <stdlib.h>
#include <stdio.h>

/*-------------------------------------------*/
/*Функции для работы с синтаксическим деревом*/
void tree_del(tt_node *);												/*Освобождение памяти из-под дерева*/

void *stack_new(){
	t_stack *stack;

	if ((stack = (t_stack *)malloc(sizeof(t_stack))) == NULL)
		return NULL;
	stack->height = 0;
	stack->head = NULL;

	return stack;
}

void stack_del(void *stack){

	tl_node *curr, *node;

	if (stack == NULL)
		return;
	curr = ((t_stack *)stack)->head;

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		tree_del(node->tree);
		free(node);
	}
	((t_stack *)stack)->head = NULL;
	((t_stack *)stack)->height = 0;
}

size_t stack_hht(void *stack){

	if (stack == NULL)
		return 0;

	return ((t_stack *)stack)->height;
}

tt_node *stack_add_elem(void *stack){

	tl_node *node;

	if (stack == NULL)
		return NULL;
	if ((node = (tl_node *)malloc(sizeof(tl_node))) == NULL)
		return NULL;
	if ((node->tree = (tt_node *)malloc(sizeof(tt_node))) == NULL){
		free(node);
		return NULL;
	}
	node->next = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node;
	++((t_stack *)stack)->height;
	node->tree->left = NULL;
	node->tree->right = NULL;
	return node->tree;
}

tt_node *stack_get_elem(void *stack){

	if ((stack == NULL)||(((t_stack *)stack)->head == NULL))
		return NULL;

	return ((t_stack *)stack)->head->tree;
}

void stack_off_elem(void *stack){

	tl_node *node;

	if ((stack == NULL)||((t_stack *)stack)->head == NULL)
		return;
	node = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node->next;
	free(node);
	--((t_stack *)stack)->height;
}

void stack_del_elem(void *stack){

	tl_node *node;

	if ((stack == NULL)||((t_stack *)stack)->head == NULL)
		return;
	node = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node->next;
	tree_del(node->tree);
	free(node);
	--((t_stack *)stack)->height;
}

void tree_del(tt_node *tree){

	if (tree == NULL)
		return;

	void del_node(tt_node *node){

		if (node == NULL)
			return;
		del_node(node->left);
		del_node(node->right);
		free(node);
	}

	del_node((tt_node *)tree);
}

void tree_out(tt_node *tree, FILE *out){
	if ((tree == NULL)||(out == NULL))
		return;

	void tree_out_node(tt_node *node, size_t count){
		size_t i;

		if (node == NULL)
			return;
		tree_out_node(node->left, count + 1);
		for (i = 0; i < 2*count; ++i)
			fprintf(out, "  ");
		switch (node->elem.flag){
			case 0 : fprintf(out, "%lf\n", node->elem.val); break;
			case 1 : fprintf(out, "%i\n", node->elem.sign); break;
			case 2 : fprintf(out, "%c\n", node->elem.sign); break;
			case 3 : fprintf(out, "%i\n", node->elem.sign); break;
		}

			
		tree_out_node(node->right, count + 1);
	}

	tree_out_node(tree, 0);
}

double tree_result(tt_node *tree, int *err){
	void node_result(tt_node *node){

		if ((node == NULL)||(!node->elem.flag))
			return;

		node_result(node->right);
		node_result(node->left);
		node->elem.flag = 0;
		switch (node->elem.sign){
			case 39 : node->elem.val = - node->left->elem.val; break;
			case 42 : node->elem.val = node->right->elem.val * node->left->elem.val; break;
			case 43 : node->elem.val = node->right->elem.val + node->left->elem.val; break;
			case 44 : node->elem.val = cos(node->left->elem.val); break;
			case 45 : node->elem.val = node->right->elem.val - node->left->elem.val; break;
			case 46 : node->elem.val = log(node->left->elem.val); break;
			case 47 :
				if ((node->left->elem.val - EPS < 0)&&(node->left->elem.val + EPS > 0)){ *err = 1; return;}
				node->elem.val = node->right->elem.val / node->left->elem.val; break;
			case 48 : node->elem.val = exp(node->left->elem.val); break;
			case 49 : node->elem.val = sqrt(node->left->elem.val); break;
			case 50 : node->elem.val = sin(node->left->elem.val); break;
		}
	}
	
	if ((tree == NULL)||(err == NULL))
		return 1/(+0.0);
	err = 0;
	node_result(tree);

	return tree->elem.val;
}

tt_node *copy_tree(tt_node *head){
	if (head == NULL)
		return NULL;
	tt_node* copy_node(tt_node *node){
		tt_node *copy;
		if (node == NULL)
			return NULL;
		copy = (tt_node *)malloc(sizeof(tt_node));
		copy->elem = node->elem;
		copy->left = copy_node(node->left);
		copy->right = copy_node(node->right);
		return copy;
	}
	return copy_node(head);
}