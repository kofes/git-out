#include "MAP.h"
#include "MAB.h"
#include "Syn_STACK.h"
#include <ctype.h>
#include <math.h>
#include <limits.h>
/*	39 : -'-'
**	40 : '('
**	41 : Нет id.
**	42 : '*'
**	43 : '+'
**	44 : "cos"
**	45 : '-'
**	46 : "log"
**	47 : '/'
**	48 : "exp"
**	49 : "sqrt"
**	50 : "sin"
*/

typedef int (*t_func)(char** pos);


/*-------------------------------------------*/
/*Глобальные переменные для работы с деревом*/
static t_btree MAP;					/*Словарь для мат. функций*/
static void *NUMB, *SIGN;			/*Стэки для деревьев с числами и с функциями/операциями*/
static void *MAB;
static unsigned char id;

/*-----------------------------*/
/*Функции и макросы для работы с парсером*/
#define ALL_ID (UCHAR_MAX + 1)
#define ERR_NORM (0)
#define ERR_DIV0 (1)
#define ERR_OPEN (2)
#define ERR_SIGN (3)
#define ERR_CHAR (4)
#define RU_TO_RN (5)
#define GET_NUM(NUM) {							\
  NUM = stack_get_elem(NUMB);					\
  stack_off_elem(NUMB);							\
}												\

#define SIGNAL(ERR) {								\
	switch(ERR){									\
		case (0): printf("ERROR: NO ERR\n"); break;	\
		case (1): printf("ERROR: DIV 0\n"); break;	\
		case (2): printf("ERROR: OPEN\n"); break;	\
		case (3): printf("ERROR: SIGN\n"); break;	\
		case (4): printf("ERROR: CHAR\n"); break;	\
	}												\
}													\

int fill_book(tt_node *, char **);
char **diff_tree(tt_node **tree, char *str);
tt_node *Optimize(tt_node *);
int get_type(char pos);
int RS (char **pos);
int RN (char **pos);
int RU (char **pos);
int RB (char **pos);

int priority(t_elem ch){
	switch (ch.sign){
		case '(': return -1;
		case '\0': return 0;
		case '\n': return 0;
		case ')': return 1;
		case '+': return 2;
		case '-': return 2;
		case '*': return 3;
		case '/': return 3;
		case 39 : return 4;
	}
	return -1;
}

const static char STAT [3][8] = {
	{0, 1, 2, -1, 0, -1, -1, -1},
	{1, -2, 2, 2, -1, 1, 3, -1},
	{2, 1, -2, -2, 0, -1, -1, -1}
};

const static t_func FUNC [3][8] = {
	{&RS, &RN, &RU, NULL, &RU, NULL, NULL, NULL},
	{&RS, &RN, &RB, &RB, NULL, &RB, NULL, NULL},
	{&RS, &RN, NULL, NULL, &RU, NULL, NULL, NULL}
};

int parser(char *str, double *val){
	char alph_table[256], *pos = str;
	int flag, t, g, err;
	memset(alph_table, 7, 256);
	alph_table[' '] = 0;
	alph_table['-'] = 2;
	memset(alph_table + 48, 1, 10);/*0-9*/
	memset(alph_table + 65, 1, 26);
	memset(alph_table + 97, 1, 26);
	alph_table['+'] = 3; alph_table['*'] = 3; alph_table['/'] = 3;
	alph_table['('] = 4; alph_table[')'] = 5;
	alph_table['s'] = 4; alph_table['c'] = 4;
	alph_table['e'] = 4; alph_table['l'] = 4;
	alph_table['\0'] = 6; alph_table['\n'] = 6;
	flag = 0;
	id = 50;
	err = ERR_NORM;
	MAP = BTREE_NULL;
	MAP = btree_new();
	MAB = MABTREE_NULL;
	MAB = Mtree_new();
	NUMB = stack_new();
	SIGN = stack_new();
	btree_add_elem(MAP, "sqrt", 4);
	btree_add_elem(MAP, "sin", 3);
	btree_add_elem(MAP, "cos", 3);
	btree_add_elem(MAP, "exp", 3);
	btree_add_elem(MAP, "log", 3);
	while (1){
		t = alph_table[(unsigned char)(*pos)];
		g = flag;
		flag = STAT[g][t];
		if (flag < 0){
			err = (flag) == (-2) ? ERR_SIGN : ERR_CHAR;
			break;
		}
		if (flag == 3){
			err = RB(&pos);
			break;
		}
		err = FUNC [g][t](&pos);
		if (err == RU_TO_RN){
			flag = 1;
			err = ERR_NORM;
		}
		if (err != ERR_NORM){
			break;
		}
	}
	if (err == ERR_NORM){
		tt_node *node;
		char **Mabs;
		char diff = 0;
		node = stack_get_elem(NUMB);
		stack_off_elem(NUMB);
		stack_del(SIGN);
		stack_del(NUMB);
		while ((diff != '1')&&(diff != '4')){
			diff = 0;
			printf("Choose:\n 1)Answer;\n 2)Diff;\n 3)Show tree;\n 4)Exit.\n");
			scanf("%c\0", &(diff));
			switch (diff){
				case '1':
					fill_var(MAB, stdin, stdout);
					*val = tree_result(node, &err);
					printf("Answer: %lf\n", *val);
					break;
				case '2':
					Mabs = diff_tree(&node, "x");
					printf("%lf\n", node->elem.val);
					MAB = NULL;
					MAB = Mtree_new();
					node = Optimize(node);
					fill_book(node, Mabs);
					if (Mabs != NULL)
						free(*Mabs);
					free(Mabs);
					break;
				case '3':
					tree_out(node, stdout);
					break;
			}
		}
		tree_del(node);
	}
	btree_del(MAP);
	Mtree_del(MAB);
	return err;
}

int RS(char **pos){
	for (; **pos == ' '; ++(*pos));
	return 0;
}

int RN(char **pos){
	t_elem elem;
	unsigned char *val;
	tb_key str;
	tt_node *node;
	if (**pos < 60){
		elem.flag = 0;	
		elem.val = strtod(*pos, pos);
		stack_add_elem(NUMB)->elem = elem;
	} else{
		unsigned char i;
		for (i = 0; isalpha((*pos)[i]); ++i);
		str = (char *)malloc(i*sizeof(char));
		memcpy(str, *pos, i);
		str[i] = '\0';
		node = stack_add_elem(NUMB);
		node->elem.flag = 1;
		val = tree_add_elem(MAB, str, node);
		if (*val == 41){
			id = (id + 1) % ALL_ID;
			*val = id;
			node->elem.sign = id;
		} else
			node->elem.sign = *val;
		*pos = *pos + i;
	}
	return 0;
}

int RU(char **pos){
	char str[4];
	t_elem elem;
	if ((**pos == '-')||(**pos == '(')){
		elem.flag = 2;
		elem.sign = (**pos) == ('-') ? (39) : (**pos);
		stack_add_elem(SIGN)->elem = elem;
		++(*pos);
		return 0;
	}
	memcpy(str, *pos, 4);
	if (btree_search(MAP, str, 4) >= 0){
		elem.flag = 3;
		elem.sign = 49;
		stack_add_elem(SIGN)->elem = elem;
		*pos = *pos + 4;
	} else if (btree_search(MAP, str, 3) >= 0){
		elem.flag = 3;
		switch (str[0]){
			case 'c': elem.sign = 44; break;
			case 'l': elem.sign = 46; break;
			case 'e': elem.sign = 48; break;
			case 's': elem.sign = 50; break;
		}
		stack_add_elem(SIGN)->elem = elem;
		*pos = *pos + 3;
	} else{
		RN(pos);
		return RU_TO_RN;
	}
	if (**pos != '(')
		return ERR_CHAR;
	++(*pos);
	return ERR_NORM;
}

int RB(char** pos){
	t_elem elem;
	char ch = **pos;
	elem.flag = 2;
	elem.sign = ch;
	int p = priority(elem);
	while ((stack_get_elem(SIGN) != NULL) && (priority(stack_get_elem(SIGN)->elem) >= p)){
		t_elem elem = stack_get_elem(SIGN)->elem;
		char sym = elem.sign;
		stack_del_elem(SIGN);
		tt_node *num1, *num2, *head;
		switch (sym){
			case 39 :
				GET_NUM(num1);
				if (!num1->elem.flag){
					num1->elem.val = - num1->elem.val;
					stack_add_elem(NUMB)->elem = num1->elem;
				} else{
					head = stack_add_elem(NUMB);
					head->elem.flag = 2;
					head->left = num1;
					head->right = NULL;
					head->elem.sign = '-';
				}
				break;
			case '+':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.val = num2->elem.val + num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '-':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.val = num2->elem.val - num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '/':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					if ((num1->elem.val - EPS < 0)&&(num1->elem.val + EPS > 0)) return 1;
					head->elem.val = num2->elem.val / num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '*':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.val = num2->elem.val * num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			}
	}
	if (ch == ')'){
		if (stack_get_elem(SIGN) == NULL)
			return 2;
		if (stack_get_elem(SIGN)->elem.flag == 3){
			tt_node *node, *head;
			node = stack_get_elem(NUMB);
			if (node->elem.flag == 0){
				head = stack_get_elem(SIGN);
				switch (head->elem.sign){
					case 44 :	node->elem.val = cos(node->elem.val);	break;
					case 46 :	node->elem.val = log(node->elem.val);	break;
					case 48 :	node->elem.val = exp(node->elem.val);	break;
					case 49 :	node->elem.val = sqrt(node->elem.val);	break;
					case 50 :	node->elem.val = sin(node->elem.val);	break;
				}
			} else{
				stack_off_elem(NUMB);
				head = stack_add_elem(NUMB);
				head->left = node;
				head->right = NULL;
				head->elem.flag = 3;
				head->elem.sign = stack_get_elem(SIGN)->elem.sign;
			}
		}
		stack_del_elem(SIGN);
		++(*pos);
		return ERR_NORM;
	}
	if ((ch == '\0')||(ch == '\n')){
		if (stack_get_elem(SIGN) != NULL)
			return ERR_OPEN;
		return ERR_NORM;
	}
	elem.flag = 2;
	elem.sign = ch;
	stack_add_elem(SIGN)->elem = elem;
	++(*pos);
	return 0;
}
#define ERR_DATA (1)
char **diff_tree(tt_node **tree, char *str){
	unsigned char *ind;
	char **Mabs;
	if ((tree == NULL)||(str == NULL))
		return NULL;
	ind = tree_get_elem(MAB, str);
	if (ind == NULL){
		tree_del((*tree)->left);
		tree_del((*tree)->right);
		(*tree)->elem.flag = 0;
		(*tree)->elem.val = 0;
		return NULL;
	}
	id = 50;
	Mabs = tree_id(MAB);//Нужно будет перезаполнять дерево переменных
	MAB = NULL;
	tt_node *diff_node(tt_node *node){
		if (node == NULL)
			return node;
		if (node->elem.flag > 1){
			tt_node *head, *jnode, *shead, *rnode, *lnode;
			switch (node->elem.sign){
				case 39 :
					node->left = diff_node(node->left);
					return node;
				case 42 :	/*Умножение*/
					jnode = copy_tree(node);
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = node;
					head->left = jnode;
					head->elem.flag = 2;
					head->elem.sign = '+';
					head->right->right = diff_node(head->right->right);
					head->left->left = diff_node(head->left->left);
					return head;
				case 43 :	/*Сложение*/
					node->left = diff_node(node->left);
					node->right = diff_node(node->right);
					return node;
				case 44 :	/*Косинус*/
					//
					head = (tt_node *)malloc(sizeof(tt_node));
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->left = copy_tree(node->left);
					//
					shead = (tt_node *)malloc(sizeof(tt_node));
					shead->elem.flag = 2;
					shead->elem.sign = 39;
					//
					node->elem.sign = 50;
					head->right = shead;
					shead->left = node;
					shead->right = NULL;
					head->left = diff_node(head->left);
					return head;
				case 45 :	/*Вычитание*/
					node->left = diff_node(node->left);
					node->right = diff_node(node->right);
					return node;
				case 46 :	/*Логарифм*/
					jnode = copy_tree(node->left);
					node->right = jnode;
					node->elem.flag = 2;
					node->elem.sign = '/';
					node->right = diff_node(node->right);
					return node;
				case 47 :	/*Деление*/
					rnode = (tt_node *)malloc(sizeof(tt_node));
					lnode = (tt_node *)malloc(sizeof(tt_node));
					rnode->left = copy_tree(node);
					rnode->right = copy_tree(node);
					lnode->left = copy_tree(node->left);
					lnode->right = copy_tree(node->left);
					node->right = rnode;
					node->left = lnode;
					rnode->elem.flag = 2;
					lnode->elem.flag = 2;
					rnode->left->elem.flag = 2;
					rnode->right->elem.flag = 2;
					rnode->elem.sign = '-';
					lnode->elem.sign = '*';
					rnode->left->elem.sign = '*';
					rnode->right->elem.sign = '*';
					rnode->right->right = diff_node(rnode->right->right);
					rnode->left->left = diff_node(rnode->left->left);
					return node;
				case 48 : /*Экспонента*/
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = node;
					head->left = copy_tree(node->left);
					head->left = diff_node(head->left);
					return head;
				case 49 : /*Квадратный корень*/
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = (tt_node *)malloc(sizeof(tt_node));
					head->left = copy_tree(node);
					head->right->elem.flag = 0;
					head->right->elem.val = 0.5;
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->left->elem.flag = 2;
					head->left->elem.sign = '/';
					head->left->right = head->left->left;
					head->left->left = node;
					head->left->right = diff_node(head->left->right);
					return head;
				case 50 : /*Синус*/
					head = copy_tree(node);
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->right = node;
					node->elem.sign = 44;
					head->left = diff_node(head->left);
					return head;
			}
		}
		if ((node->elem.flag == 0)||(node->elem.sign != *ind)){
			node->elem.flag = 0;
			node->elem.val = 0;
		} else{
			node->elem.flag = 0;
			node->elem.val = 1;
		}
		return node;
	}
	*tree = diff_node(*tree);
	return Mabs;
}

tt_node *Optimize(tt_node *head){
	if (head == NULL)
		return NULL;
	tt_node *node_opt(tt_node *node){
		if ((node == NULL)||(node->elem.flag < 1))
			return node;

		node->left = node_opt(node->left);
		node->right = node_opt(node->right);

		if (node->elem.flag == 2){
			if (node->elem.sign == '*'){
				if (((node->right->elem.flag == 0)&&(node->right->elem.val == 0))||
					((node->left->elem.flag == 0)&&(node->left->elem.val == 0))){
					tree_del(node->left);
					tree_del(node->right);
					node->left = NULL;
					node->right = NULL;
					node->elem.flag = 0;
					node->elem.val = 0;
					return node;
				} else if ((node->right->elem.flag == 0)&&(node->right->elem.val == 1)){
					tt_node *jnode;
					tree_del(node->right);
					jnode = node->left;
					free(node);
					return jnode;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.val == 1)){
					tt_node *jnode;
					tree_del(node->left);
					jnode = node->right;
					free(node);
					return jnode;
				}
			} else if (node->elem.sign == '+'){
				if ((node->right->elem.flag == 0)&&(node->right->elem.val == 0)){
					tt_node *jnode;
					tree_del(node->right);
					jnode = node->left;
					free(node);
					return jnode;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.val == 0)){
					tt_node *jnode;
					tree_del(node->left);
					jnode = node->right;
					free(node);
					return jnode;
				}
			} else if (node->elem.sign == '-'){
				if ((node->right->elem.flag == 0)&&(node->right->elem.val == 0)){
					tree_del(node->right);
					node->elem.sign = 39;
					return node;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.val == 0)){
					tt_node *jnode;
					tree_del(node->left);
					jnode = node->right;
					free(node);
					return jnode;
				}
			}
		}

		if ((node->elem.flag > 1)&&(node->left->elem.flag == 0)&&
			((node->right == NULL)||(node->right->elem.flag == 0)))
			switch (node->elem.sign){
				case 39 :	//-'-'
					node->elem.flag = 0;
					node->elem.val = - node->left->elem.val;
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 42 :	//'*'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val * node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 43 :	//'+'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val + node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 44 :	//"cos"
					node->elem.flag = 0;
					node->elem.val = cos(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 45 :	//'-'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val - node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 46 :	//"log"
					node->elem.flag = 0;
					node->elem.val = log(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 47 :	//'/'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val / node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 48 :	//"exp"
					node->elem.flag = 0;
					node->elem.val = exp(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 49 :	//"sqrt"
					node->elem.flag = 0;
					node->elem.val = sqrt(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 50 :	//"sin"
					node->elem.flag = 0;
					node->elem.val = sin(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
			}
		return node;
	}

	return node_opt(head);
}

int fill_book(tt_node *head, char **Mabs){
	unsigned char *val;
	if ((head == NULL)||(Mabs == NULL))
		return ERR_DATA;
	void fill_node(tt_node *node){
		if (node == NULL)
			return;
		fill_node(node->left);
		fill_node(node->right);
		if (node->elem.flag == 1){
			val = tree_add_elem(MAB, Mabs[node->elem.sign], node);
			if (*val == 41){
				id = (id + 1) % ALL_ID;
				*val = id;
				node->elem.sign = id;
			} else
				node->elem.sign = *val;
		}
	} 
	fill_node(head);
	return 0;
}

int main(){
	double val = 0;
	char err;
	err = parser("2", &val);
	//if (err == ERR_NORM)
	//	printf("%lf\n", val);
	SIGNAL(err);
	return 0;
}