#include "MAP.h"

typedef struct t_btree_node{
	struct t_btree_node *child[2];
	t_btree_key key;
	t_btree_val val;
	char diff, len;
}t_btree_node;

typedef struct{
	t_btree_node *root;
}t_utree;

int cmp_buff(unsigned char *left, unsigned char *right, char lenl, char lenr){
	unsigned char i;
	char diff;
	if (left == NULL){	
		if (right == NULL)
			return 0;
		else
			return 1;
	}
	if (right == NULL)
		return -1;
	for (i = 0; (i < lenl)&&(i < lenr); i++){
		diff = left[i] - right[i];
		if (diff)
			return diff;
	}
	return (lenr - lenl);
}

t_btree btree_new(){

	t_utree *tree;

	if ((tree = (t_utree *)malloc(sizeof(t_utree))) == NULL)
		return NULL;
	tree->root = NULL;

	return (t_btree)tree;
}

void btree_del(t_btree btree){

	t_utree *tree;
	tree = (t_utree *)btree;

	void btree_del_node(t_btree_node *node){

		if (node == NULL)
			return;

		btree_del_node(node->child[0]);
		btree_del_node(node->child[1]);
		free(node->key);
		free(node);
	}

	if (tree == NULL)
		return;
	btree_del_node(tree->root);
}

t_btree_node *lit_turn(t_btree_node *root, unsigned char inv){
	t_btree_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

t_btree_node *turn(t_btree_node *node, unsigned char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

t_btree_val *btree_add_elem(t_btree btree, t_btree_key key, char len){
	char info = 0;
	t_utree *tree;
	t_btree_val *val = NULL;
	tree = (t_utree *)btree;
	t_btree_node *btree_add_node(t_btree_node *node){
		int i;
		if (node == NULL){
			if ((node = (t_btree_node *)malloc(sizeof(t_btree_node))) == NULL){
				info = 1;
				return NULL;
			}
			node->val = 0;
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			if ((node->key = (t_btree_key )malloc(sizeof(char)*len)) == NULL){
				free(node);
				info = 1;
				return NULL;
			}
			memcpy(node->key, key, len);
			val = &(node->val);
			node->val = 16 + len;
			node->len = len;
			return node;
		}

		i = cmp_buff(key, node->key, len, node->len);
		if (i == 0){val = &(node->val); info = 1; return node;}
		i = (i > 0);
		node->child[i] = btree_add_node(node->child[i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turn(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = btree_add_node(tree->root);
	return val;
}

long long btree_search(t_btree btree, char *buff, char len){
	t_utree *tree = (t_utree *)btree;
	t_btree_node *node;
	char diff, i;
	if (tree == NULL)
		return -1;
	node = tree->root;
	for (i = 0; i < len; ++i){
		if (node == NULL){	
			return -2;
		}
		if ((i > node->len)||(buff[i] != node->key[i])){
			diff = cmp_buff(buff, node->key, len, node->len);
			diff = (diff > 0);
			node = node->child[(unsigned char)diff];
			i = -1;
		}
	}
	return node->val;
}

void btree_out(t_btree btree, FILE *fout){
	t_utree *tree = (t_utree *)btree;
	unsigned char i;
	void btree_out_node(t_btree_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		for (i = 0; i < node->len; ++i)
			fprintf(fout, "%c ", node->key[i]);
		fprintf(fout, "%llu\n", node->val);
		btree_out_node(node->child[1]);
	}
	if ((tree == NULL)||(fout == NULL))
		return;
	btree_out_node(tree->root);
}