#ifndef __INCLUDE_MTREE_H
#define __INCLUDE_MTREE_H

#include "B_STACK.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "limits.h"
//Тип ключа:
typedef char *tb_key;

//Тип значения:
typedef unsigned char tb_id;

//Признак открепленного дескриптора:
#define MABTREE_NULL (NULL)

//Генерирует пустое дерево и возвращает его дескриптор:
extern void *Mtree_new();

//Освобождает память из-под дерева:
extern void Mtree_del(void *);

//Выполняет вставку элемента по ключу:
extern tb_id *tree_add_elem(void *, tb_key , tt_node *);

//Заполнение всех переменных в дереве:
extern void fill_var(void *, FILE *, FILE *);

//Выполняет поиск элемента по ключу:
extern tb_id *tree_get_elem(void *, tb_key );

//Выводит массив с индексами равными id всех переменных и удаляет дерево
char **tree_id(void *vtree);
/*
//Выводит содержимое дерева в виде дерева в выходной поток:
extern void tree_out_tree(void *, FILE *);
*/

//Выводит содержимое дерева в выходной поток:
extern void Mtree_out(void *, FILE *);

#endif //__INCLUDE_BTREE_H
