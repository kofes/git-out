#include "MAB.h"
typedef struct tb_node{
	struct tb_node *child[2];
	tb_key key;
	tb_id id;
	void *head;
	unsigned char count;
	char diff;
}tb_node;

typedef struct{
	tb_node *root;
}t_tree;

void *Mtree_new(){

	t_tree *tree;

	if ((tree = (t_tree *)malloc(sizeof(t_tree))) == NULL)
		return NULL;

	tree->root = NULL;

	return tree;
}

void Mtree_del(void *vtree){

	t_tree *tree;
	tree = (t_tree *)vtree;

	void tree_del_node(tb_node *node){

		if (node == NULL)
			return;

		tree_del_node(node->child[0]);
		tree_del_node(node->child[1]);
		list_del(node->head);
		free(node->key);
		free(node);
	}

	if (tree == NULL)
		return;
	tree_del_node(tree->root);
}

tb_node *little_turn(tb_node *root, char inv){
	tb_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

tb_node *turns(tb_node *node, char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = little_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return little_turn(node, inv);
	}
	return node;
}

tb_id *tree_add_elem(void *vtree, tb_key key, tt_node *elem){//Изменить, ибо нужно добавлять элементы в стек
	char info = 0;
	t_tree *tree;
	tb_id *id = NULL;
	tree = (t_tree *)vtree;
	tb_node *tree_add_node(tb_node *node){
		int i;
		if (node == NULL){
			size_t len;
			if ((node = (tb_node *)malloc(sizeof(tb_node))) == NULL){
				info = 1;
				return NULL;
			}
			node->id = 41;
			*list_add_elem(&(node->head)) = elem; //<------------------------
			node->count = 1;
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			len = strlen(key) + 1;
			if ((node->key = (tb_key )malloc(len)) == NULL){
				free(node);
				info = 1;
				return NULL;
			}
			memcpy(node->key, key, len);
			id = &(node->id);

			return node;
		}

		i = strcmp(key, node->key);
		if (i == 0){
			id = &(node->id);
			info = 1;
			*list_add_elem(&(node->head)) = elem; //<------------------------
			++(node->count);
			return node;
		}
		i = (i > 0);

		node->child[i] = tree_add_node(node->child[i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turns(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = tree_add_node(tree->root);
	return id;
}

void fill_var(void *vtree, FILE *in, FILE *out){
	double val;
	t_tree *tree = (t_tree *)vtree;

	void tree_fill_node(tb_node *node){
		if (node == NULL)
			return;
		tree_fill_node(node->child[0]);
		tree_fill_node(node->child[1]);
		fprintf(out, "INPUT %s : ", node->key);
		fscanf(in, "%lf", &val);
		list_fill(node->head, val);
	}

	if ((tree == NULL)||(in == NULL)||(out == NULL))
		return;

	tree_fill_node(tree->root);
}

char **tree_id(void *vtree){

	t_tree *tree;
	tree = (t_tree *)vtree;
	char **result;
	unsigned short i;
	void node_id(tb_node *node){

		if (node == NULL)
			return;

		node_id(node->child[0]);
		node_id(node->child[1]);
		list_del(node->head);
		node->head = NULL;
		result[node->id] = node->key;
		free(node);
	}

	if (tree == NULL)
		return NULL;

	result = (char **)malloc(sizeof(char *)*(UCHAR_MAX + 1));
	for (i = 0; i < UCHAR_MAX + 1; ++i)
		result[i] = NULL;
	node_id(tree->root);

	return result;
}

void Mtree_out(void *vtree, FILE *out){

	t_tree *tree = (t_tree *)vtree;
	void btree_out_node(tb_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		fprintf(out, "key: %s, id: %u, count: %u\n", node->key, node->id, node->count);
		btree_out_node(node->child[1]);
	}

	if ((tree == NULL)||(out == NULL))
		return;

	btree_out_node(tree->root);
}

tb_id *tree_get_elem(void *vtree, tb_key key){

	t_tree *tree = (t_tree *)vtree;
	tb_node *node;
	int i;
	if (tree == NULL)
		return NULL;
	node = tree->root;
	while (node != NULL){
		if ((i = strcmp(key, node->key)) == 0)
			return &(node->id);
		else{
			i = (i > 0);
			node = node->child[i];
		}
	}
	return NULL;
}

/*
void tree_out_tree(tb *btree, FILE *fout)
{
	t_utree *tree;
	tree = (t_utree *)btree;

	void btree_outb_node(tb_node *node, size_t count)
	{
		size_t i;

		if (node == NULL)
			return;
		btree_outb_node(node->child[1], count + 1);
		for (i = 0; i<2*count; ++i)
			fprintf(fout, "  ");
		fprintf(fout, "(%i-%i)\n", node->diff, node->val);
		btree_outb_node(node->child[0], count + 1);
	}

	if ((tree == NULL)||(fout == NULL))
		return;

	btree_outb_node(tree->root, 0);
}
*/