#ifndef __INCLUDE_BTREE_H
#define __INCLUDE_BTREE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//Тип ключа:
typedef char *t_btree_key;

//Тип значения:
typedef char t_btree_val;

//Тип дескриптора двоичного дерева:
typedef void *t_btree;

//Признак открепленного дескриптора:
#define BTREE_NULL (NULL)

//Генерирует пустое дерево и возвращает его дескриптор:
extern t_btree btree_new();

//Освобождает память из-под дерева:
extern void btree_del(t_btree);

//Выполняет вставку элемента по ключу:
extern t_btree_val *btree_add_elem(t_btree , t_btree_key , char);

//Выводит содержимое дерева в выходной поток:
extern void btree_out(t_btree , FILE *);

//Поиск в дереве узла, префикс ключа которого совпадает с текущим ключом
extern long long btree_search(t_btree , char *, char);

#endif //__INCLUDE_BTREE_H
