#include "MAP.h"
#include "MAB.h"
#include "Syn_STACK.h"
#include <ctype.h>
#include <math.h>
#include <limits.h>
/*	39 : -'-'
**	40 : '('
**	41 : Нет id.
**	42 : '*'
**	43 : '+'
**	44 : "cos"
**	45 : '-'
**	46 : "log"
**	47 : '/'
**	48 : "exp"
**	49 : "sqrt"
**	50 : "sin"
*/

typedef int (*t_func)(char** pos);


/*-------------------------------------------*/
/*Глобальные переменные для работы с деревом*/
static t_btree MAP;					/*Словарь для мат. функций*/
static void *NUMB, *SIGN;			/*Стэки для деревьев с числами и с функциями/операциями*/
static void *MAB;
static unsigned char id;

/*-----------------------------*/
/*Функции и макросы для работы с парсером*/
#define STACK_NULL (NULL)
#define BTREE_NULL (NULL)
#define MABTREE_NULL (NULL)
#define ALL_ID (UCHAR_MAX + 1)
#define ERR_NORM (0)
#define ERR_DIV0 (1)
#define ERR_OPEN (2)
#define ERR_SIGN (3)
#define ERR_CHAR (4)
#define RU_TO_RN (5)
#define EPS (1e-5)
#define GET_NUM(NUM) {							\
  NUM = stack_get_elem(NUMB);					\
  stack_off_elem(NUMB);							\
}												\

#define SIGNAL(ERR) {								\
	switch(ERR){									\
		case (0): printf("ERROR: NO ERR\n"); break;	\
		case (1): printf("ERROR: DIV 0\n"); break;	\
		case (2): printf("ERROR: OPEN\n"); break;	\
		case (3): printf("ERROR: SIGN\n"); break;	\
		case (4): printf("ERROR: CHAR\n"); break;	\
	}												\
}													\

/*--------------------------*/
/*Тип элемента в узле дерева*/
typedef struct{
	char flag;		/* 0 - число, 1 - переменная, 2 - операция, 3 - функция*/
	union{
		double val;
		unsigned char sign;	/*Переменная, хранящая в себе уникальный id каждой функции/операции/переменной*/
	};
}t_elem;

/*---------------*/
/*Тип узла дерева*/
typedef struct tt_node{
	struct tt_node *left, *right;
	t_elem elem;
}tt_node;

/*------------------------*/
/*Тип узла списка деревьев*/
typedef struct tl_node{
	struct tl_node *next;	/*Указатель на следующий узел в списке*/
	tt_node *tree;	/*Указатель на вершину текущего дерева*/
}tl_node;

/*----------------*/
/*Структура списка*/
typedef struct{
	tl_node *head;
	size_t height;
}t_stack;

typedef struct t_btree_node{
	struct t_btree_node *child[2];
	t_btree_key key;
	t_btree_val val;
	char diff, len;
}t_btree_node;

typedef struct{
	t_btree_node *root;
}t_utree;

typedef struct tb_node{
	struct tb_node *child[2];
	tb_key key;
	tb_id id;
	void *head;
	unsigned char count;
	char diff;
}tb_node;

typedef struct{
	tb_node *root;
}t_tree;

typedef struct l_node{
	struct l_node* next;
	tt_node *point;
}l_node;

typedef char *t_btree_key;
typedef char t_btree_val;
typedef void *t_btree;
typedef char *tb_key;
typedef unsigned char tb_id;

void list_del(void *);
tt_node **list_add_elem(void **);
void list_fill(void *, double);
void list_out(void *, FILE *);

void *Mtree_new();
void Mtree_del(void *);
tb_id *tree_add_elem(void *, tb_key , tt_node *);
void fill_var(void *, FILE *, FILE *);
tb_id *tree_get_elem(void *, tb_key );
char **tree_id(void *vtree);
void tree_out_tree(void *, FILE *);
void Mtree_out(void *, FILE *);

t_btree btree_new();
void btree_del(t_btree);
t_btree_val *btree_add_elem(t_btree , t_btree_key , char);
void btree_out(t_btree , FILE *);
long long btree_search(t_btree , char *, char);
/*-------------------------------------*/
/*Функции для работы со стеком деревьев*/
int fill_book(tt_node *, char **);
char **diff_tree(tt_node **tree, char *str);
tt_node *Optimize(tt_node *);
void *stack_new();
void stack_del(void *);
size_t stack_hht(void *);
tt_node *stack_add_elem(void *);
tt_node *stack_get_elem(void *);
void stack_off_elem(void *);
void stack_del_elem(void *);
void tree_out(tt_node *, FILE *);
double tree_result(tt_node *, int *);
tt_node *copy_tree(tt_node *);
void tree_del(tt_node *tree);
int get_type(char pos);
int RS (char **pos);
int RN (char **pos);
int RU (char **pos);
int RB (char **pos);


void list_del(void *head){

	l_node *curr, *node;

	if (head == NULL)
		return;
	curr = *((l_node **)head);

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		free(node);
	}
}

tt_node **list_add_elem(void **head){

	l_node *node;

	if ((node = (l_node *)malloc(sizeof(l_node))) == NULL)
		return NULL;
	node->point = NULL;
	node->next = (l_node *)*head;
	*head = (void *)node;
	return &(node->point);
}

void list_fill(void *head, double val){
	l_node *curr;

	if (head == NULL)
		return;
	curr = (l_node *)head;

	while (curr != NULL){
		curr->point->elem.flag = 0;
		curr->point->elem.val = val;
		curr = curr->next;
	}
}

void list_out(void *head, FILE *out){

	l_node *curr;

	if ((head == NULL)||(out == NULL))
		return;

	curr = (l_node *)head;

	while (curr != NULL){
		fprintf(out, "%lf ", curr->point->elem.val);
		curr = curr->next;
	}
}

void *Mtree_new(){

	t_tree *tree;

	if ((tree = (t_tree *)malloc(sizeof(t_tree))) == NULL)
		return NULL;

	tree->root = NULL;

	return tree;
}

void Mtree_del(void *vtree){

	t_tree *tree;
	tree = (t_tree *)vtree;

	void tree_del_node(tb_node *node){

		if (node == NULL)
			return;

		tree_del_node(node->child[0]);
		tree_del_node(node->child[1]);
		list_del(node->head);
		free(node->key);
		free(node);
	}

	if (tree == NULL)
		return;
	tree_del_node(tree->root);
}

tb_node *little_turn(tb_node *root, char inv){
	tb_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

tb_node *turns(tb_node *node, char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = little_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return little_turn(node, inv);
	}
	return node;
}

tb_id *tree_add_elem(void *vtree, tb_key key, tt_node *elem){//Изменить, ибо нужно добавлять элементы в стек
	char info = 0;
	t_tree *tree;
	tb_id *id = NULL;
	tree = (t_tree *)vtree;
	tb_node *tree_add_node(tb_node *node){
		int i;
		if (node == NULL){
			size_t len;
			if ((node = (tb_node *)malloc(sizeof(tb_node))) == NULL){
				info = 1;
				return NULL;
			}
			node->id = 41;
			*list_add_elem(&(node->head)) = elem; //<------------------------
			node->count = 1;
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			len = strlen(key) + 1;
			if ((node->key = (tb_key )malloc(len)) == NULL){
				free(node);
				info = 1;
				return NULL;
			}
			memcpy(node->key, key, len);
			id = &(node->id);

			return node;
		}

		i = strcmp(key, node->key);
		if (i == 0){
			id = &(node->id);
			info = 1;
			*list_add_elem(&(node->head)) = elem; //<------------------------
			++(node->count);
			return node;
		}
		i = (i > 0);

		node->child[i] = tree_add_node(node->child[i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turns(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = tree_add_node(tree->root);
	return id;
}

void fill_var(void *vtree, FILE *in, FILE *out){
	double val;
	t_tree *tree = (t_tree *)vtree;

	void tree_fill_node(tb_node *node){
		if (node == NULL)
			return;
		tree_fill_node(node->child[0]);
		tree_fill_node(node->child[1]);
		fprintf(out, "INPUT %s : ", node->key);
		fscanf(in, "%lf", &val);
		list_fill(node->head, val);
	}

	if ((tree == NULL)||(in == NULL)||(out == NULL))
		return;

	tree_fill_node(tree->root);
}

char **tree_id(void *vtree){

	t_tree *tree;
	tree = (t_tree *)vtree;
	char **result;
	unsigned short i;
	void node_id(tb_node *node){

		if (node == NULL)
			return;

		node_id(node->child[0]);
		node_id(node->child[1]);
		list_del(node->head);
		node->head = NULL;
		result[node->id] = node->key;
		free(node);
	}

	if (tree == NULL)
		return NULL;

	result = (char **)malloc(sizeof(char *)*(UCHAR_MAX + 1));
	for (i = 0; i < UCHAR_MAX + 1; ++i)
		result[i] = NULL;
	node_id(tree->root);

	return result;
}

void Mtree_out(void *vtree, FILE *out){

	t_tree *tree = (t_tree *)vtree;
	void btree_out_node(tb_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		fprintf(out, "key: %s, id: %u, count: %u\n", node->key, node->id, node->count);
		btree_out_node(node->child[1]);
	}

	if ((tree == NULL)||(out == NULL))
		return;

	btree_out_node(tree->root);
}

tb_id *tree_get_elem(void *vtree, tb_key key){

	t_tree *tree = (t_tree *)vtree;
	tb_node *node;
	int i;
	if (tree == NULL)
		return NULL;
	node = tree->root;
	while (node != NULL){
		if ((i = strcmp(key, node->key)) == 0)
			return &(node->id);
		else{
			i = (i > 0);
			node = node->child[i];
		}
	}
	return NULL;
}

int cmp_buff(unsigned char *left, unsigned char *right, char lenl, char lenr){
	unsigned char i;
	char diff;
	if (left == NULL){	
		if (right == NULL)
			return 0;
		else
			return 1;
	}
	if (right == NULL)
		return -1;
	for (i = 0; (i < lenl)&&(i < lenr); i++){
		diff = left[i] - right[i];
		if (diff)
			return diff;
	}
	return (lenr - lenl);
}

t_btree btree_new(){

	t_utree *tree;

	if ((tree = (t_utree *)malloc(sizeof(t_utree))) == NULL)
		return NULL;
	tree->root = NULL;

	return (t_btree)tree;
}

void btree_del(t_btree btree){

	t_utree *tree;
	tree = (t_utree *)btree;

	void btree_del_node(t_btree_node *node){

		if (node == NULL)
			return;

		btree_del_node(node->child[0]);
		btree_del_node(node->child[1]);
		free(node->key);
		free(node);
	}

	if (tree == NULL)
		return;
	btree_del_node(tree->root);
}

t_btree_node *lit_turn(t_btree_node *root, unsigned char inv){
	t_btree_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

t_btree_node *turn(t_btree_node *node, unsigned char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

t_btree_val *btree_add_elem(t_btree btree, t_btree_key key, char len){
	char info = 0;
	t_utree *tree;
	t_btree_val *val = NULL;
	tree = (t_utree *)btree;
	t_btree_node *btree_add_node(t_btree_node *node){
		int i;
		if (node == NULL){
			if ((node = (t_btree_node *)malloc(sizeof(t_btree_node))) == NULL){
				info = 1;
				return NULL;
			}
			node->val = 0;
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			if ((node->key = (t_btree_key )malloc(sizeof(char)*len)) == NULL){
				free(node);
				info = 1;
				return NULL;
			}
			memcpy(node->key, key, len);
			val = &(node->val);
			node->val = 16 + len;
			node->len = len;
			return node;
		}

		i = cmp_buff(key, node->key, len, node->len);
		if (i == 0){val = &(node->val); info = 1; return node;}
		i = (i > 0);
		node->child[i] = btree_add_node(node->child[i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turn(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = btree_add_node(tree->root);
	return val;
}

long long btree_search(t_btree btree, char *buff, char len){
	t_utree *tree = (t_utree *)btree;
	t_btree_node *node;
	char diff, i;
	if (tree == NULL)
		return -1;
	node = tree->root;
	for (i = 0; i < len; ++i){
		if (node == NULL){	
			return -2;
		}
		if ((i > node->len)||(buff[i] != node->key[i])){
			diff = cmp_buff(buff, node->key, len, node->len);
			diff = (diff > 0);
			node = node->child[(unsigned char)diff];
			i = -1;
		}
	}
	return node->val;
}

void btree_out(t_btree btree, FILE *fout){
	t_utree *tree = (t_utree *)btree;
	unsigned char i;
	void btree_out_node(t_btree_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		for (i = 0; i < node->len; ++i)
			fprintf(fout, "%c ", node->key[i]);
		fprintf(fout, "%llu\n", node->val);
		btree_out_node(node->child[1]);
	}
	if ((tree == NULL)||(fout == NULL))
		return;
	btree_out_node(tree->root);
}

void *stack_new(){
	t_stack *stack;

	if ((stack = (t_stack *)malloc(sizeof(t_stack))) == NULL)
		return NULL;
	stack->height = 0;
	stack->head = NULL;

	return stack;
}

void stack_del(void *stack){

	tl_node *curr, *node;

	if (stack == NULL)
		return;
	curr = ((t_stack *)stack)->head;

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		tree_del(node->tree);
		free(node);
	}
	((t_stack *)stack)->head = NULL;
	((t_stack *)stack)->height = 0;
}

size_t stack_hht(void *stack){

	if (stack == NULL)
		return 0;

	return ((t_stack *)stack)->height;
}

tt_node *stack_add_elem(void *stack){

	tl_node *node;

	if (stack == NULL)
		return NULL;
	if ((node = (tl_node *)malloc(sizeof(tl_node))) == NULL)
		return NULL;
	if ((node->tree = (tt_node *)malloc(sizeof(tt_node))) == NULL){
		free(node);
		return NULL;
	}
	node->next = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node;
	++((t_stack *)stack)->height;
	node->tree->left = NULL;
	node->tree->right = NULL;
	return node->tree;
}

tt_node *stack_get_elem(void *stack){

	if ((stack == NULL)||(((t_stack *)stack)->head == NULL))
		return NULL;

	return ((t_stack *)stack)->head->tree;
}

void stack_off_elem(void *stack){

	tl_node *node;

	if ((stack == NULL)||((t_stack *)stack)->head == NULL)
		return;
	node = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node->next;
	free(node);
	--((t_stack *)stack)->height;
}

void stack_del_elem(void *stack){

	tl_node *node;

	if ((stack == NULL)||((t_stack *)stack)->head == NULL)
		return;
	node = ((t_stack *)stack)->head;
	((t_stack *)stack)->head = node->next;
	tree_del(node->tree);
	free(node);
	--((t_stack *)stack)->height;
}

void tree_del(tt_node *tree){

	if (tree == NULL)
		return;

	void del_node(tt_node *node){

		if (node == NULL)
			return;
		del_node(node->left);
		del_node(node->right);
		free(node);
	}

	del_node((tt_node *)tree);
}

void tree_out(tt_node *tree, FILE *out){
	if ((tree == NULL)||(out == NULL))
		return;

	void tree_out_node(tt_node *node, size_t count){
		size_t i;

		if (node == NULL)
			return;
		tree_out_node(node->left, count + 1);
		for (i = 0; i < 2*count; ++i)
			fprintf(out, "  ");
		switch (node->elem.flag){
			case 0 : fprintf(out, "%lf\n", node->elem.val); break;
			case 1 : fprintf(out, "%i\n", node->elem.sign); break;
			case 2 : fprintf(out, "%c\n", node->elem.sign); break;
			case 3 : fprintf(out, "%i\n", node->elem.sign); break;
		}

			
		tree_out_node(node->right, count + 1);
	}

	tree_out_node(tree, 0);
}

double tree_result(tt_node *tree, int *err){
	void node_result(tt_node *node){

		if ((node == NULL)||(!node->elem.flag))
			return;

		node_result(node->right);
		node_result(node->left);
		node->elem.flag = 0;
		switch (node->elem.sign){
			case 39 : node->elem.val = - node->left->elem.val; break;
			case 42 : node->elem.val = node->right->elem.val * node->left->elem.val; break;
			case 43 : node->elem.val = node->right->elem.val + node->left->elem.val; break;
			case 44 : node->elem.val = cos(node->left->elem.val); break;
			case 45 : node->elem.val = node->right->elem.val - node->left->elem.val; break;
			case 46 : node->elem.val = log(node->left->elem.val); break;
			case 47 :
				if ((node->left->elem.val - EPS < 0)&&(node->left->elem.val + EPS > 0)){ *err = 1; return;}
				node->elem.val = node->right->elem.val / node->left->elem.val; break;
			case 48 : node->elem.val = exp(node->left->elem.val); break;
			case 49 : node->elem.val = sqrt(node->left->elem.val); break;
			case 50 : node->elem.val = sin(node->left->elem.val); break;
		}
	}
	
	if ((tree == NULL)||(err == NULL))
		return 1/(+0.0);
	err = 0;
	node_result(tree);

	return tree->elem.val;
}

tt_node *copy_tree(tt_node *head){
	if (head == NULL)
		return NULL;
	tt_node* copy_node(tt_node *node){
		tt_node *copy;
		if (node == NULL)
			return NULL;
		copy = (tt_node *)malloc(sizeof(tt_node));
		copy->elem = node->elem;
		copy->left = copy_node(node->left);
		copy->right = copy_node(node->right);
		return copy;
	}
	return copy_node(head);
}

int priority(t_elem ch){
	switch (ch.sign){
		case '(': return -1;
		case '\0': return 0;
		case '\n': return 0;
		case ')': return 1;
		case '+': return 2;
		case '-': return 2;
		case '*': return 3;
		case '/': return 3;
		case 39 : return 4;
	}
	return -1;
}

const static char STAT [3][8] = {
	{0, 1, 2, -1, 0, -1, -1, -1},
	{1, -2, 2, 2, -1, 1, 3, -1},
	{2, 1, -2, -2, 0, -1, -1, -1}
};

const static t_func FUNC [3][8] = {
	{&RS, &RN, &RU, NULL, &RU, NULL, NULL, NULL},
	{&RS, &RN, &RB, &RB, NULL, &RB, NULL, NULL},
	{&RS, &RN, NULL, NULL, &RU, NULL, NULL, NULL}
};

int parser(char *str, double *val){
	char alph_table[256], *pos = str;
	int flag, t, g, err;
	memset(alph_table, 7, 256);
	alph_table[' '] = 0;
	alph_table['-'] = 2;
	memset(alph_table + 48, 1, 10);/*0-9*/
	memset(alph_table + 65, 1, 26);
	memset(alph_table + 97, 1, 26);
	alph_table['+'] = 3; alph_table['*'] = 3; alph_table['/'] = 3;
	alph_table['('] = 4; alph_table[')'] = 5;
	alph_table['s'] = 4; alph_table['c'] = 4;
	alph_table['e'] = 4; alph_table['l'] = 4;
	alph_table['\0'] = 6; alph_table['\n'] = 6;
	flag = 0;
	id = 50;
	err = ERR_NORM;
	MAP = BTREE_NULL;
	MAP = btree_new();
	MAB = MABTREE_NULL;
	MAB = Mtree_new();
	NUMB = stack_new();
	SIGN = stack_new();
	btree_add_elem(MAP, "sqrt", 4);
	btree_add_elem(MAP, "sin", 3);
	btree_add_elem(MAP, "cos", 3);
	btree_add_elem(MAP, "exp", 3);
	btree_add_elem(MAP, "log", 3);
	while (1){
		t = alph_table[(unsigned char)(*pos)];
		g = flag;
		flag = STAT[g][t];
		if (flag < 0){
			err = (flag) == (-2) ? ERR_SIGN : ERR_CHAR;
			break;
		}
		if (flag == 3){
			err = RB(&pos);
			break;
		}
		err = FUNC [g][t](&pos);
		if (err == RU_TO_RN){
			flag = 1;
			err = ERR_NORM;
		}
		if (err != ERR_NORM){
			break;
		}
	}
	if (err == ERR_NORM){
		tt_node *node;
		char **Mabs;
		char diff = 0;
		node = stack_get_elem(NUMB);
		stack_off_elem(NUMB);
		stack_del(SIGN);
		stack_del(NUMB);
		while ((diff != '1')&&(diff != '4')){
			diff = 0;
			printf("Choose:\n 1)Answer;\n 2)Diff;\n 3)Show tree;\n 4)Exit.\n");
			scanf("%c\0", &(diff));
			switch (diff){
				case '1':
					fill_var(MAB, stdin, stdout);
					*val = tree_result(node, &err);
					printf("Answer: %lf\n", *val);
					break;
				case '2':
					Mabs = diff_tree(&node, "x");
					printf("%lf\n", node->elem.val);
					MAB = NULL;
					MAB = Mtree_new();
					node = Optimize(node);
					fill_book(node, Mabs);
					if (Mabs != NULL)
						free(*Mabs);
					free(Mabs);
					break;
				case '3':
					tree_out(node, stdout);
					break;
			}
		}
		tree_del(node);
	}
	btree_del(MAP);
	Mtree_del(MAB);
	return err;
}

int RS(char **pos){
	for (; **pos == ' '; ++(*pos));
	return 0;
}

int RN(char **pos){
	t_elem elem;
	unsigned char *val;
	tb_key str;
	tt_node *node;
	if (**pos < 60){
		elem.flag = 0;	
		elem.val = strtod(*pos, pos);
		stack_add_elem(NUMB)->elem = elem;
	} else{
		unsigned char i;
		for (i = 0; isalpha((*pos)[i]); ++i);
		str = (char *)malloc(i*sizeof(char));
		memcpy(str, *pos, i);
		str[i] = '\0';
		node = stack_add_elem(NUMB);
		node->elem.flag = 1;
		val = tree_add_elem(MAB, str, node);
		if (*val == 41){
			id = (id + 1) % ALL_ID;
			*val = id;
			node->elem.sign = id;
		} else
			node->elem.sign = *val;
		*pos = *pos + i;
	}
	return 0;
}

int RU(char **pos){
	char str[4];
	t_elem elem;
	if ((**pos == '-')||(**pos == '(')){
		elem.flag = 2;
		elem.sign = (**pos) == ('-') ? (39) : (**pos);
		stack_add_elem(SIGN)->elem = elem;
		++(*pos);
		return 0;
	}
	memcpy(str, *pos, 4);
	if (btree_search(MAP, str, 4) >= 0){
		elem.flag = 3;
		elem.sign = 49;
		stack_add_elem(SIGN)->elem = elem;
		*pos = *pos + 4;
	} else if (btree_search(MAP, str, 3) >= 0){
		elem.flag = 3;
		switch (str[0]){
			case 'c': elem.sign = 44; break;
			case 'l': elem.sign = 46; break;
			case 'e': elem.sign = 48; break;
			case 's': elem.sign = 50; break;
		}
		stack_add_elem(SIGN)->elem = elem;
		*pos = *pos + 3;
	} else{
		RN(pos);
		return RU_TO_RN;
	}
	if (**pos != '(')
		return ERR_CHAR;
	++(*pos);
	return ERR_NORM;
}

int RB(char** pos){
	t_elem elem;
	char ch = **pos;
	elem.flag = 2;
	elem.sign = ch;
	int p = priority(elem);
	while ((stack_get_elem(SIGN) != NULL) && (priority(stack_get_elem(SIGN)->elem) >= p)){
		t_elem elem = stack_get_elem(SIGN)->elem;
		char sym = elem.sign;
		stack_del_elem(SIGN);
		tt_node *num1, *num2, *head;
		switch (sym){
			case 39 :
				GET_NUM(num1);
				if (!num1->elem.flag){
					num1->elem.val = - num1->elem.val;
					stack_add_elem(NUMB)->elem = num1->elem;
				} else{
					head = stack_add_elem(NUMB);
					head->elem.flag = 2;
					head->left = num1;
					head->right = NULL;
					head->elem.sign = '-';
				}
				break;
			case '+':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.val = num2->elem.val + num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '-':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.val = num2->elem.val - num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '/':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					if ((num1->elem.val - EPS < 0)&&(num1->elem.val + EPS > 0)) return 1;
					head->elem.val = num2->elem.val / num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			case '*':
				GET_NUM(num1); GET_NUM(num2);
				head = stack_add_elem(NUMB);
				if ((num1->elem.flag == 0)&&(num2->elem.flag == 0)){
					head->elem.flag = 0;
					head->elem.val = num2->elem.val * num1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = num1;
					head->right = num2;
					head->elem.sign = sym;
				}
				break;
			}
	}
	if (ch == ')'){
		if (stack_get_elem(SIGN) == NULL)
			return 2;
		if (stack_get_elem(SIGN)->elem.flag == 3){
			tt_node *node, *head;
			node = stack_get_elem(NUMB);
			if (node->elem.flag == 0){
				head = stack_get_elem(SIGN);
				switch (head->elem.sign){
					case 44 :	node->elem.val = cos(node->elem.val);	break;
					case 46 :	node->elem.val = log(node->elem.val);	break;
					case 48 :	node->elem.val = exp(node->elem.val);	break;
					case 49 :	node->elem.val = sqrt(node->elem.val);	break;
					case 50 :	node->elem.val = sin(node->elem.val);	break;
				}
			} else{
				stack_off_elem(NUMB);
				head = stack_add_elem(NUMB);
				head->left = node;
				head->right = NULL;
				head->elem.flag = 3;
				head->elem.sign = stack_get_elem(SIGN)->elem.sign;
			}
		}
		stack_del_elem(SIGN);
		++(*pos);
		return ERR_NORM;
	}
	if ((ch == '\0')||(ch == '\n')){
		if (stack_get_elem(SIGN) != NULL)
			return ERR_OPEN;
		return ERR_NORM;
	}
	elem.flag = 2;
	elem.sign = ch;
	stack_add_elem(SIGN)->elem = elem;
	++(*pos);
	return 0;
}
#define ERR_DATA (1)
char **diff_tree(tt_node **tree, char *str){
	unsigned char *ind;
	char **Mabs;
	if ((tree == NULL)||(str == NULL))
		return NULL;
	ind = tree_get_elem(MAB, str);
	if (ind == NULL){
		tree_del((*tree)->left);
		tree_del((*tree)->right);
		(*tree)->elem.flag = 0;
		(*tree)->elem.val = 0;
		return NULL;
	}
	id = 50;
	Mabs = tree_id(MAB);//Нужно будет перезаполнять дерево переменных
	MAB = NULL;
	tt_node *diff_node(tt_node *node){
		if (node == NULL)
			return node;
		if (node->elem.flag > 1){
			tt_node *head, *jnode, *shead, *rnode, *lnode;
			switch (node->elem.sign){
				case 39 :
					node->left = diff_node(node->left);
					return node;
				case 42 :	/*Умножение*/
					jnode = copy_tree(node);
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = node;
					head->left = jnode;
					head->elem.flag = 2;
					head->elem.sign = '+';
					head->right->right = diff_node(head->right->right);
					head->left->left = diff_node(head->left->left);
					return head;
				case 43 :	/*Сложение*/
					node->left = diff_node(node->left);
					node->right = diff_node(node->right);
					return node;
				case 44 :	/*Косинус*/
					//
					head = (tt_node *)malloc(sizeof(tt_node));
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->left = copy_tree(node->left);
					//
					shead = (tt_node *)malloc(sizeof(tt_node));
					shead->elem.flag = 2;
					shead->elem.sign = 39;
					//
					node->elem.sign = 50;
					head->right = shead;
					shead->left = node;
					shead->right = NULL;
					head->left = diff_node(head->left);
					return head;
				case 45 :	/*Вычитание*/
					node->left = diff_node(node->left);
					node->right = diff_node(node->right);
					return node;
				case 46 :	/*Логарифм*/
					jnode = copy_tree(node->left);
					node->right = jnode;
					node->elem.flag = 2;
					node->elem.sign = '/';
					node->right = diff_node(node->right);
					return node;
				case 47 :	/*Деление*/
					rnode = (tt_node *)malloc(sizeof(tt_node));
					lnode = (tt_node *)malloc(sizeof(tt_node));
					rnode->left = copy_tree(node);
					rnode->right = copy_tree(node);
					lnode->left = copy_tree(node->left);
					lnode->right = copy_tree(node->left);
					node->right = rnode;
					node->left = lnode;
					rnode->elem.flag = 2;
					lnode->elem.flag = 2;
					rnode->left->elem.flag = 2;
					rnode->right->elem.flag = 2;
					rnode->elem.sign = '-';
					lnode->elem.sign = '*';
					rnode->left->elem.sign = '*';
					rnode->right->elem.sign = '*';
					rnode->right->right = diff_node(rnode->right->right);
					rnode->left->left = diff_node(rnode->left->left);
					return node;
				case 48 : /*Экспонента*/
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = node;
					head->left = copy_tree(node->left);
					head->left = diff_node(head->left);
					return head;
				case 49 : /*Квадратный корень*/
					head = (tt_node *)malloc(sizeof(tt_node));
					head->right = (tt_node *)malloc(sizeof(tt_node));
					head->left = copy_tree(node);
					head->right->elem.flag = 0;
					head->right->elem.val = 0.5;
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->left->elem.flag = 2;
					head->left->elem.sign = '/';
					head->left->right = head->left->left;
					head->left->left = node;
					head->left->right = diff_node(head->left->right);
					return head;
				case 50 : /*Синус*/
					head = copy_tree(node);
					head->elem.flag = 2;
					head->elem.sign = '*';
					head->right = node;
					node->elem.sign = 44;
					head->left = diff_node(head->left);
					return head;
			}
		}
		if ((node->elem.flag == 0)||(node->elem.sign != *ind)){
			node->elem.flag = 0;
			node->elem.val = 0;
		} else{
			node->elem.flag = 0;
			node->elem.val = 1;
		}
		return node;
	}
	*tree = diff_node(*tree);
	return Mabs;
}

tt_node *Optimize(tt_node *head){
	if (head == NULL)
		return NULL;
	tt_node *node_opt(tt_node *node){
		if ((node == NULL)||(node->elem.flag < 1))
			return node;

		node->left = node_opt(node->left);
		node->right = node_opt(node->right);

		if (node->elem.flag == 2){
			if (node->elem.sign == '*'){
				if (((node->right->elem.flag == 0)&&(node->right->elem.val == 0))||
					((node->left->elem.flag == 0)&&(node->left->elem.val == 0))){
					tree_del(node->left);
					tree_del(node->right);
					node->left = NULL;
					node->right = NULL;
					node->elem.flag = 0;
					node->elem.val = 0;
					return node;
				} else if ((node->right->elem.flag == 0)&&(node->right->elem.val == 1)){
					tt_node *jnode;
					tree_del(node->right);
					jnode = node->left;
					free(node);
					return jnode;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.val == 1)){
					tt_node *jnode;
					tree_del(node->left);
					jnode = node->right;
					free(node);
					return jnode;
				}
			} else if (node->elem.sign == '+'){
				if ((node->right->elem.flag == 0)&&(node->right->elem.val == 0)){
					tt_node *jnode;
					tree_del(node->right);
					jnode = node->left;
					free(node);
					return jnode;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.val == 0)){
					tt_node *jnode;
					tree_del(node->left);
					jnode = node->right;
					free(node);
					return jnode;
				}
			} else if (node->elem.sign == '-'){
				if ((node->right->elem.flag == 0)&&(node->right->elem.val == 0)){
					tree_del(node->right);
					node->elem.sign = 39;
					return node;
				} else if ((node->left->elem.flag == 0)&&(node->left->elem.val == 0)){
					tt_node *jnode;
					tree_del(node->left);
					jnode = node->right;
					free(node);
					return jnode;
				}
			}
		}

		if ((node->elem.flag > 1)&&(node->left->elem.flag == 0)&&
			((node->right == NULL)||(node->right->elem.flag == 0)))
			switch (node->elem.sign){
				case 39 :	//-'-'
					node->elem.flag = 0;
					node->elem.val = - node->left->elem.val;
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 42 :	//'*'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val * node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 43 :	//'+'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val + node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 44 :	//"cos"
					node->elem.flag = 0;
					node->elem.val = cos(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 45 :	//'-'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val - node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 46 :	//"log"
					node->elem.flag = 0;
					node->elem.val = log(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 47 :	//'/'
					node->elem.flag = 0;
					node->elem.val = node->right->elem.val / node->left->elem.val;
					tree_del(node->right);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 48 :	//"exp"
					node->elem.flag = 0;
					node->elem.val = exp(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 49 :	//"sqrt"
					node->elem.flag = 0;
					node->elem.val = sqrt(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
				case 50 :	//"sin"
					node->elem.flag = 0;
					node->elem.val = sin(node->left->elem.val);
					tree_del(node->left);
					node->right = NULL;
					node->left = NULL;
					break;
			}
		return node;
	}

	return node_opt(head);
}

int fill_book(tt_node *head, char **Mabs){
	unsigned char *val;
	if ((head == NULL)||(Mabs == NULL))
		return ERR_DATA;
	void fill_node(tt_node *node){
		if (node == NULL)
			return;
		fill_node(node->left);
		fill_node(node->right);
		if (node->elem.flag == 1){
			val = tree_add_elem(MAB, Mabs[node->elem.sign], node);
			if (*val == 41){
				id = (id + 1) % ALL_ID;
				*val = id;
				node->elem.sign = id;
			} else
				node->elem.sign = *val;
		}
	} 
	fill_node(head);
	return 0;
}

int main(){
	double val = 0;
	char err;
	err = parser("2", &val);
	//if (err == ERR_NORM)
	//	printf("%lf\n", val);
	SIGNAL(err);
	return 0;
}