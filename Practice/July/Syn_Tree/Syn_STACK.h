#ifndef __INCLUDE_SYN_STACK_H
#define __INCLUDE_SYN_STACK_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define EPS (1e-5)
/*--------------------------*/
/*Тип элемента в узле дерева*/
typedef struct{
	char flag;		/* 0 - число, 1 - переменная, 2 - операция, 3 - функция*/
	union{
		double val;
		unsigned char sign;	/*Переменная, хранящая в себе уникальный id каждой функции/операции/переменной*/
	};
}t_elem;

/*---------------*/
/*Тип узла дерева*/
typedef struct tt_node{
	struct tt_node *left, *right;
	t_elem elem;
}tt_node;

/*------------------------*/
/*Тип узла списка деревьев*/
typedef struct tl_node{
	struct tl_node *next;	/*Указатель на следующий узел в списке*/
	tt_node *tree;	/*Указатель на вершину текущего дерева*/
}tl_node;

/*----------------*/
/*Структура списка*/
typedef struct{
	tl_node *head;
	size_t height;
}t_stack;

/*-------------------------------------*/
/*Функции для работы со стеком деревьев*/

/*Инициализация нового стека*/
extern void *stack_new();

/*Освобождение памяти из-под стека*/
extern void stack_del(void *);

/*Вывод количества элементов в стеке*/
extern size_t stack_hht(void *);

/*Добавление элемента в вершину стека*/
extern tt_node *stack_add_elem(void *);

/*Получение элемента из вершины стека*/
extern tt_node *stack_get_elem(void *);

/*Удаление узла без освобождения памяти*/
extern void stack_off_elem(void *);

/*Удаление элемента с вершины стека*/
extern void stack_del_elem(void *);

/*Вывод дерева на печать*/
extern void tree_out(tt_node *, FILE *);

/*Сборка дерева и вывод результата*/
extern double tree_result(tt_node *, int *);

/*Копирование дерева*/
extern tt_node *copy_tree(tt_node *);

extern void tree_del(tt_node *tree);
#endif