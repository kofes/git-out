#include "Matrix.hpp"

t_Matrix::t_error t_Matrix::err = t_Matrix::ERR_NORM;
/*
static int count = 0;
t_Matrix::t_Matrix(){
	++count;
	std::cerr<< count << "\n";
	ptr = NULL;
}
*/
t_Matrix::t_Matrix(unsigned int count_rows, unsigned int count_columns){
/*
	++count;
	std::cerr<<count<<"\n";
*/
	if ((!count_columns)&&(!count_rows)){
		err = ERR_SIZE;
		ptr = NULL;
		return;
	}
	ptr = new MatrixBuff;
	ptr->ct_rws = count_rows;
	ptr->ct_cms = count_columns;
	ptr->ct_pts = 1;
	ptr->buff = new FL_POINT[count_rows * count_columns];
	for (unsigned int i = 0; i < count_rows * count_columns; ++i)
		ptr->buff[i] = (FL_POINT)0;
}
t_Matrix::t_Matrix(t_Matrix &from){
/*
	++count;
	std::cerr<<count<<"\n";
*/
	ptr = from.ptr;
	if (ptr != NULL)
		++ptr->ct_pts;
}

t_Matrix::~t_Matrix(){
/*
	--count;
	std::cerr<<count<<"\n";
*/
	if (ptr == NULL)
		return;
	--ptr->ct_pts;
	if (ptr->ct_pts == 0){
		delete[] ptr->buff;
		ptr->ct_rws = ptr->ct_cms = 0;
		delete ptr;
	}
	ptr = NULL;
}

t_Matrix t_Matrix::operator +(t_Matrix &right){	//Возвращается объект по значению, ибо сработает контруктор копирования
	if ((right.ptr == NULL)||(ptr == NULL)){
		err = ERR_NULL;	
		return *this;
	}
	if ((ptr->ct_rws != right.ptr->ct_rws)||(ptr->ct_cms != right.ptr->ct_cms)){
		err = ERR_DIMENSION;
		return *this;
	}
	t_Matrix ans(ptr->ct_rws, ptr->ct_cms); //Выделяется память под объект
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ptr->ct_cms; ++j)
			ans.ptr->buff[i*ptr->ct_cms + j] = ptr->buff[i*(ptr->ct_cms) + j] + right.ptr->buff[i*(ptr->ct_cms) + j];
	return ans;	//Вызывается контруктор копирования
}

t_Matrix t_Matrix::operator -(t_Matrix &right){
	if ((right.ptr == NULL)||(ptr == NULL)){
		err = ERR_NULL;	
		return *this;
	}
	if ((ptr->ct_rws != right.ptr->ct_rws)||(ptr->ct_cms != right.ptr->ct_cms)){
		err = ERR_DIMENSION;
		return *this;
	}
	t_Matrix ans(ptr->ct_rws, ptr->ct_cms);
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ptr->ct_cms; ++j)
			ans.ptr->buff[i*ptr->ct_cms + j] = ptr->buff[i*(ptr->ct_cms) + j] - right.ptr->buff[i*(ptr->ct_cms) + j];
	return ans;
}

t_Matrix t_Matrix::operator *(t_Matrix &right){
	if ((ptr == NULL)||(right.ptr == NULL)){
		err = ERR_NULL;
		return *this;
	}
	if (ptr->ct_cms != right.ptr->ct_rws){
		err = ERR_DIMENSION;	
		return *this;
	}
	t_Matrix ans(ptr->ct_rws, right.ptr->ct_cms);
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < right.ptr->ct_cms; ++j)
			for (unsigned int k = 0; k < ptr->ct_cms; ++k)
				ans.ptr->buff[i*right.ptr->ct_cms + j] += ptr->buff[i*ptr->ct_cms + k] * right.ptr->buff[k*right.ptr->ct_cms + j];
	return ans;
}

//С 16 числа прием доков на офицера запаса. Экипажная 18, паспорт, приписное.
t_Matrix t_Matrix::operator *(double addition){
	if (ptr == NULL){
		err = ERR_NULL;	
		return *this;
	}
	t_Matrix ans(ptr->ct_rws, ptr->ct_cms);
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ptr->ct_cms; ++j)
			ans.ptr->buff[i*ptr->ct_cms + j] = ptr->buff[i*ptr->ct_cms + j] * ((FL_POINT)addition);
	return ans;
}

t_Matrix operator *(double addition, t_Matrix &right){
	if (right.ptr == NULL){
		t_Matrix::err = t_Matrix::ERR_NULL;	
		return right;
	}
	t_Matrix ans(right.ptr->ct_rws, right.ptr->ct_cms);
	for (unsigned int i = 0; i < right.ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < right.ptr->ct_cms; ++j)
			ans.ptr->buff[i*right.ptr->ct_cms + j] = right.ptr->buff[i*right.ptr->ct_cms + j] * ((FL_POINT)addition);
	return ans;	
}

t_Matrix t_Matrix::operator *(float addition){
	if (ptr == NULL){
		err = ERR_NULL;	
		return *this;
	}
	t_Matrix ans(ptr->ct_rws, ptr->ct_cms);
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ptr->ct_cms; ++j)
			ans.ptr->buff[i*ptr->ct_cms + j] = ptr->buff[i*ptr->ct_cms + j] * ((FL_POINT)addition);
	return ans;
}

t_Matrix operator *(float addition, t_Matrix &right){
	if (right.ptr == NULL){
		t_Matrix::err = t_Matrix::ERR_NULL;	
		return right;
	}
	t_Matrix ans(right.ptr->ct_rws, right.ptr->ct_cms);
	for (unsigned int i = 0; i < ans.ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ans.ptr->ct_cms; ++j)
			ans.ptr->buff[i*(ans.ptr->ct_cms) + j] = right.ptr->buff[i*(ans.ptr->ct_cms) + j] * ((FL_POINT)addition);
	return ans;
}

t_Matrix t_Matrix::operator /(float addition){
	if (ptr == NULL){
		t_Matrix ans;
		err = ERR_DIV0;
		return ans;
	}
	if (((((FL_POINT)addition) - MX_FL_EPS < 0)&&(((FL_POINT)addition) + MX_FL_EPS > 0))){
		t_Matrix ans;
		err = ERR_NULL;
		return ans;
	}
	t_Matrix ans(ptr->ct_rws, ptr->ct_cms);
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ptr->ct_cms; ++j)
			ans.ptr->buff[i*(ptr->ct_cms) + j] = ptr->buff[i*(ptr->ct_cms) + j] / ((FL_POINT)addition);
	return ans;
}

t_Matrix t_Matrix::operator /(double addition){
	if (ptr == NULL){
		err = ERR_NULL;
		t_Matrix ans;
		return ans;
	}
	if (((((FL_POINT)addition) - MX_FL_EPS < 0)&&(((FL_POINT)addition) + MX_FL_EPS > 0))){
		t_Matrix ans;
		err = ERR_DIV0;
		return ans;
	}
	t_Matrix ans(ptr->ct_rws, ptr->ct_cms);
	for (unsigned int i = 0; i < ptr->ct_rws; ++i)
		for (unsigned int j = 0; j < ptr->ct_cms; ++j)
			ans.ptr->buff[i*(ptr->ct_cms) + j] = ptr->buff[i*(ptr->ct_cms) + j] / ((FL_POINT)addition);
	return ans;
}

t_Matrix t_Matrix::operator =(const t_Matrix &from){
	if (ptr == from.ptr)
		return *this;
	if (ptr != NULL){
		--ptr->ct_pts;
		if (!(ptr->ct_pts)){
			delete[] ptr->buff;
			//ptr->ct_cms = ptr->ct_rws = 0;
			delete ptr;
		}
	}
	if (from.ptr != NULL)
		++from.ptr->ct_pts;
	ptr = from.ptr;
	return *this;
}

std::ostream &operator <<(std::ostream &_out, const t_Matrix &_mtr){
	if (_mtr.ptr != NULL)
		for (unsigned int i = 0; i < _mtr.ptr->ct_rws; ++i, _out << '\n')
			for (unsigned int j = 0; j < _mtr.ptr->ct_cms; ++j)
				_out << _mtr.ptr->buff[i*(_mtr.ptr->ct_cms) + j] << '\t';
	return _out;
}

t_Matrix::t_iter t_Matrix::operator [](unsigned int _num){
	return t_iter(this, _num);
}

t_Matrix::t_iter::tt_iter t_Matrix::t_iter::operator [](unsigned int _ind){
	tt_iter iter;
	if (!((mtr->ptr == NULL)||(mtr->ptr->ct_rws < ind)||(mtr->ptr->ct_cms < _ind)))
		iter = tt_iter(mtr, ind, _ind);
	return iter;
}

FL_POINT t_Matrix::t_iter::tt_iter::operator =(FL_POINT from){
	if (mtr == NULL){
		return from;
	}
	if (mtr->ptr->ct_pts != 1){
		MatrixBuff *plur = new MatrixBuff;
		--mtr->ptr->ct_pts;
		plur->ct_rws = mtr->ptr->ct_rws;
		plur->ct_cms = mtr->ptr->ct_cms;
		plur->ct_pts = 1;
		plur->buff = new FL_POINT[plur->ct_rws * plur->ct_cms];
		for (unsigned long int count = 0; count < plur->ct_rws * plur->ct_cms; ++count)
			plur->buff[count] = mtr->ptr->buff[count];
		mtr->ptr = NULL;
		mtr->ptr = plur;
	}
	mtr->ptr->buff[ind1*(mtr->ptr->ct_cms) + ind2] = from;
	return *(mtr->ptr->buff + ind1*(mtr->ptr->ct_cms) + ind2);
}

t_Matrix::t_iter::tt_iter::operator FL_POINT(void) const{
	if (mtr == NULL){
		err = ERR_DIMENSION;	
		return 0;
	}
	return *(mtr->ptr->buff + ind1*(mtr->ptr->ct_cms) + ind2);
}
