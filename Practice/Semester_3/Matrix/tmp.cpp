#include <iostream>
using namespace std;
class t_frac{
	int a, b;
public:
	t_frac(int _a, int _b):a(_a), b(_b){}//просто присваивает a параметр _a, а b параметр _b...
	inline int get_upper(){return a;} 	//На этапе компиляции вместо этой функции подставит этот кусок кода,
										//но не делать, когда больше 10 строк кода
										//Так же реализация inline описывается обычно внутри класса,
										//а значит в заголовочном файле
										//Если в класса написать реализацию функции, то она будет inline
										//INLINE - это рекомендация компилятору, чтобы функция была подставляемой
	inline int get_lower(){return b;}
	t_frac():a(0), b(0){}
	t_frac operator +(t_frac &in2){
		return t_frac(a*in2.b + in2.a*b, b*in2.b);
	}
	t_frac operator +(int elem){
		return t_frac(a + elem*b, b);
	}
	friend ostream &operator <<(ostream &cout, t_frac &in2);
	void operator =(int in1){
		a = in1;
		b = 1;
	}
};
// '[]', '()' - можно перегружать только внутри класса
// () - позволяет создавать "функторы" или функциональные объекты
//istream &operator >>(istream *cin, t_frac &in2); //Чтобы считывать из входного потока объекты этого класса
ostream &operator <<(ostream &out, t_frac &in2){
	out << in2.a <<'/'<< in2.b;
	return out;
}
class t_array{
private:
	int *buff, num;
public:
	t_array(int _num){
		buff = new int[(num = _num)];
	}
	~t_array(){
		delete[] buff;
	}
	int &operator [](int i){
		return buff[i];
	}
	int operator [](int i) const/*Означает, что внури метода параметры не меняются(он константный*/{
		return buff[i];
	}
};
int _f_main(){
	t_frac elem(3, 5), p1, p2;
	int t = 1;
	p1 = 10;
	p2 = t_frac(11,7);
	elem = elem + t;
	cout<< elem<< '\n';
	return 0;
}
int _a_main(int argc, char *argv[]){
	t_array A(10);
	A[5] = 0;
	int b = A[4];
	const t_array C(10);
	//C[5] = 9; - ругнется
	//int e = C[5]; - ругнется, если не использовать перегрузку оператора с параметрами const, которые указаны выше	
	return 0;
}
class t_bitarray{
private:
	int *buff, num, bit;
public:
	t_bitarray(int _bit, int _num);
	~t_bitarray();
	t_iter operator[](int i){				//для записи и считывания
		return t_iter(this, i);
	};
	const t_iter operator[](int i) const{	//для присваивания элементу типа int значение элемента i в режиме "на чтение"
		return t_iter(this, i);
	}
protected:
	class t_iter{
	private:
		t_array *arr;	//Указатель на объект, с которым мы имеем дело
		int ind;	//Номер элемента, с которым связан данный итератор
	public:
		t_iter(t_array *_arr, int _ind);
		~t_iter();
		int operator int() const{		//для вывода элемента
			/*Этот перегруженный оператор(метод) возвращает int*/
			return ...;
		}
		void operator =(int val){		//для записи элемента
			return t_iter(this, i);
		}
	};
friend class t_iter;
};
t_iter operator[](int i){

}