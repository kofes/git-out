/*	Заголовочный файл для работы с классом матриц t_Matrix
**
**	Реализованы методы (перегружены операторы):
**	Сложение матриц, умножение матриц, копирование матриц,
**	умножение на число, деление на число, поэлементое обращение к объектам матрицы,
**	Получение кода ошибки - get_error(), и её очистка - off_error();
**
**	Коды ошибок:
**	ERR_NORM - Успешное выполнение
**	ERR_NULL - Пустой контейнер матрицы
**	ERR_SIZE - Несовпадение размерности матриц
**	ERR_DIMENSION - Обращение к несуществующему полю
**	ERR_DIV0 - Деление на ноль
**
**	Дополнителный макрос MX_FL_EPS - погрешность проверки на ноль
*/
#ifndef __INCLUDE_MATRIX_
#define __INCLUDE_MATRIX_

#include <iostream>
#include <fstream>

#define MX_FL_EPS (1e-5)

typedef double FL_POINT;

class t_Matrix{
public:
	enum t_error {
		ERR_NORM,
		ERR_NULL,
		ERR_SIZE,
		ERR_DIMENSION,
		ERR_DIV0
	};
protected:
	typedef struct {
		FL_POINT *buff;//Можно сделать одномерный массив
		unsigned int ct_rws, ct_cms;
		unsigned int ct_pts;
	} MatrixBuff;
	static t_error error;
	MatrixBuff *ptr;
public:
	t_Matrix(unsigned int count_rows, unsigned int count_columns);
	t_Matrix(const t_Matrix &from);
	t_Matrix(): ptr(NULL){};
	~t_Matrix();
	t_Matrix operator +(t_Matrix &);
	t_Matrix operator -(t_Matrix &);
	t_Matrix operator *(t_Matrix &);
	t_Matrix operator *(double);
	friend t_Matrix operator *(double, t_Matrix &);
	t_Matrix operator *(float);
	friend t_Matrix operator *(float, t_Matrix &);
	t_Matrix operator /(double);
	t_Matrix operator /(float);
	t_Matrix operator =(const t_Matrix &);
	friend std::ostream &operator <<(std::ostream &, const t_Matrix &);
	inline static t_error get_error(){return err;}
	inline static void off_error(){err = ERR_NORM;}
	friend class t_iter;
	class t_iter{
	private:
		MatrixBuff **mtr;
		unsigned int ind;
	public:
		t_iter(MatrixBuff **_mtr, unsigned int _ind): mtr(_mtr), ind(_ind){};
		friend class tt_iter;
		class tt_iter{
		private:
			MatrixBuff **mtr;
			unsigned int ind1, ind2;
		public:
			tt_iter(MatrixBuff **from, unsigned int _ind1, unsigned int _ind2) : mtr(from), ind1(_ind1), ind2(_ind2){};
			tt_iter(void): mtr(NULL), ind1(0), ind2(0){};
			FL_POINT operator =(FL_POINT from);
			operator FL_POINT(void) const;
		};
	public:
		const tt_iter operator [](unsigned int _ind) const;
		tt_iter operator [](unsigned int _ind);
	};
public:
	const t_iter operator [](unsigned int _num) const;
	t_iter operator [](unsigned int _num);
};


#endif
