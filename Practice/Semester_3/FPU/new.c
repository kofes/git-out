#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <stdint.h>
/*	LDBL_MANT_DIG = 64; sizeof = 16; unsigned long long
**	DBL_MANT_DIG = 53;	sizeof = 8;	unsigned long
**	FLT_MANT_DIG = 24; sizeof = 4; unsigned int
*/

#define ERR_NORM (0)
#define ERR_DATA (1)

#define DBL_TO_ULL(DOUBLE) (*(unsigned long long *)DOUBLE)

int ERR_ID = ERR_NORM;

void fadd(const double *in1, const double *in2, double *out, double *err);
void fmul(const double *in1, const double *in2, double *out, double *err);

void fadd(const double *in1, const double *in2, double *out, double *err){
	if ((in1 == NULL)||(in2 == NULL)||(out == NULL)||(err == NULL)){
		ERR_ID = ERR_DATA;
		return NULL;
	}
	char sign_in1 = (DBL_TO_ULL(in1) >> (sizeof(double) * CHAR_BIT - 1)) ? -1 : 1;
	int exp_in1 = (DBL_TO_ULL(in1) >> (DBL_MANT_DIG - 1)) & 0x7FF;
	unsigned long long mantiss_in1 = exp_in1 ? (DBL_TO_ULL(in1) & 0xFFFFFFFFFFFFF) | 0x10000000000000 : (DBL_TO_ULL(in1) & 0xFFFFFFFFFFFFF) << 1;
	exp_in1 -= 0x7FF;
	if (sign_in1 < 0)
		putchar('1');
	else
		putchar('0');
	putchar('|');
	int i;
	for (i = 0; i < 11; ++i)
		if (exp_in1 & (1 << (10 - i)))
			putchar('1');
		else
			putchar('0');
	putchar('|');
	for (i = 0; i < DBL_MANT_DIG - 1; ++i)
		if (mantiss_in1 & (1 <<  (52 - i)))
			putchar('1');
		else
			putchar('0');
	putchar('|');
	putchar('\n');
	for (i = 0; i < sizeof(double)*CHAR_BIT; ++i){
		if ((i == 1)||(i == 12))
			putchar('|');
		if (DBL_TO_ULL(in1) & (1 << (63 - i)))
			putchar('1');
		else
			putchar('0');
	}
	putchar('\n');
}
void fmul(const double *in1, const double *in2, double *out, double *err){

}


int main(){
	double in1 = 1, in2, out, err;
	fadd(&in1, &in2, &out, &err);
	return 0;
/*
	size_t i;
	union{
		float fl;
		uint32_t dw;
	}f;

	f.fl = -0.35;
	int s = (f.dw >> (sizeof(float) * CHAR_BIT - 1)) ? -1 : 1;
	int e = (f.dw >> (FLT_MANT_DIG - 1)) & 0xFF;
	int m = e ? (f.dw & 0x7FFFFF) | 0x800000 : (f.dw & 0x7FFFFF) << 1;
	e -= 127;
	if (s)
		putchar('1');
	else
		putchar('0');
	putchar('|');
	for (i = 0; i < FLT_MANT_DIG - 1; ++i)
		if (m & (1 << i))
			putchar('1');
		else
			putchar('0');
	putchar('|');

	putchar('\n');
	printf("%f, sizeof = %u\n", f.fl, sizeof(float));
	return 0;
*/
}
