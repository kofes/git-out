function [B, R] =  bord_inv(A)
	if (columns(A) ~= rows(A))
		printf("Матрица не квадратная");
		return;
	end;
	if (rank(A) ~= rows(A))
		printf("Матрица вырожденная");
		return;
	end;
	B = border(A, rows(A));
	R = norm(A * B - eye(size(A)));
end

function [B] = border(A, n)
	if (n == 1)
		B = 1/A(1, 1);
	end
	if (n == 2)
		DET = A(1, 1) * A(2, 2) - A(1, 2) * A(2, 1);
		B =	[
			A(2,2) / DET, - A(1,2) / DET;
			- A(2,1) / DET, A(1,1) / DET;
			];
		return;
	end;
	B = border(A, n - 1);
	q = zeros(1, n - 1);
	r = zeros(n - 1, 1);
	for i = 1:n-1
		q(1, i) = A(n, i);
		r(i, 1) = A(i, n);
	end;
	alpha = A(n, n);
	alpha = 1/(alpha - (q * B * r)(1, 1));
	r = (-alpha) * (B * r);
	q = (-alpha) * (q * B);
	B = B + r * q / alpha;
	for i = 1:n - 1
		B(n, i) = q(1, i);
		B(i, n) = r(i, 1);
	end
	B(n, n) = alpha;
end