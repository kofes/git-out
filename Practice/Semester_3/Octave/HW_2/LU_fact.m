function [L, U, R] = LU_fact(A)
	if (columns(A) ~= rows(A))
		printf('Матрица не квадратная');
		return;
	end;
	if (rank(A) ~= rows(A))
		printf('Матрица вырождена');
		return;
	end;
	n = rows(A);
	U = A;
	L = eye(n);
	for k = 1 : n - 1
		D = eye(n);
		max = U(k, k);
		for i = k + 1 : n                  %Обход по всем столбцам и строкам
			D(i, k) = - (U(i, k) * U(k, k)) / max;
			for j = k + 1 : n
				U(i, j) = U(i, j) - (U(i, k) * U(k, j)/ max);
			end;
		end;
		L = D * L;
		for i = k + 1 : n                     %Зануление стобца k
			U(i, k) = 0;
		end;
	end;

	R = norm(A - L*U);
end