function [X, R] = Gauss (A, F)
	count_rows = rows(A);
	count_columns = columns(A);
	mass_columns = 1 : count_columns;
	mass_rows = 1 : count_rows;
	X = 0;
	R = 0;
	Q = [A, F];
 	if (rank(A) ~= rank(Q))                %Проверка на совметсность матрицы
 		printf('Система не совместна => нет решений\n');
 		return;
	end;
	if (rank(A) ~= count_columns)          %Проверка на полноту матрицы
		printf('Система имеет бесконечное количество решений\n');
		return;
	end;
	for k = 1 : count_rows - 1               %Прямой ход #Без разницы rows/columns - матрица квадратичная!
		max = Q(mass_rows(k),mass_columns(k));
		inx_row = k;
		inx_column = k;                              %Q(i,j) - i - строка, j - столбец
		for i = k : count_rows                         %Поиск ведущего элемента по матрице
			for j = k : count_columns
				if (abs(Q(mass_rows(i), mass_columns(j))) > abs(max))
					max = Q(mass_rows(i), mass_columns(j));
					inx_row = i;
					inx_column = j;
				end;
			end;
		end;
		tmp = mass_columns(k);
		mass_columns(k) = mass_columns(inx_column);
		mass_columns(inx_column) = tmp;
		tmp = mass_rows(k);
		mass_rows(k) = mass_rows(inx_row);
		mass_rows(inx_row) = tmp;
		for i = k + 1 : count_rows                  %Обход по всем столбцам и строкам
			for j = k + 1 : count_columns
				Q(mass_rows(i), mass_columns(j)) = Q(mass_rows(i), mass_columns(j)) - (Q(mass_rows(i), mass_columns(k)) * Q(mass_rows(k), mass_columns(j))/ max);
			end;
			Q(mass_rows(i), count_columns + 1) = Q(mass_rows(i), count_columns + 1) - (Q(mass_rows(i), mass_columns(k))* Q(mass_rows(k), count_columns + 1)/max);
		end;
		for i = k + 1 : count_rows                     %Зануление стобца mass_columns(k)
			Q(mass_rows(i),mass_columns(k)) = 0;
		end;
	end;
	X = zeros(count_columns, 1);
	X(mass_rows(count_rows)) = Q(mass_rows(count_rows), count_columns + 1)/Q(mass_rows(count_rows), mass_columns(count_columns));
	for k = count_rows - 1:-1:1            %Обратный ход
		sigma = 0;
		for i = k + 1:count_columns
			sigma = sigma - Q(mass_rows(k), mass_columns(i)) * X(mass_columns(i));
		end;
		X(mass_columns(k)) = (Q(mass_rows(k), count_columns + 1) + sigma) / Q(mass_rows(k), mass_columns(k));
	end;
	R = A * X - F;                         %Вычисление вектора невязки
end
