containers: vector, list, set, map, multiset, multimap.

includes: algorithm:
				sort

Примеры:
	int main(){
		double A[N];
		sort(A, A + N);
		sort(A, A + N, cmp);	//cmp - функция компоратора, либо функциональный объект (функтор) класса. (Можно перегрузить оператор скобки у класса, и вызывать)
		vector<double> B;
		sort(B.begin(), B.end, cmp);

		vector::iterator iter;
		for (iter = B.begin; iter != B.end; ++iter){
			*iter = Rand();
		}
	}
