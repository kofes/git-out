#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ERR_NORM (0)
#define ERR_DATA (1)
#define ERR_NULL (2)

typedef struct t_itree_node{
	double mid;
	size_t size;
	size_t *left, *right;
	struct t_itree_node *LEFT, *RIGHT;
}t_itree_node;

typedef struct{
	t_itree_node *root;
}t_itree;

static double *A, *B;
static size_t *tmp;
static size_t size_tmp;

t_itree *tree_new(){
	t_itree *tree;
	if ((tree = (t_itree *)malloc(sizeof(t_itree))) == NULL)
		return NULL;
	tree->root = NULL;
	return tree;
}

void del_node(t_itree_node *node){
	if (node == NULL)
		return;
	del_node(node->LEFT);
	del_node(node->RIGHT);
	free(node->left);
	free(node->right);
	free(node);
}

void tree_del(t_itree *tree){
	if (tree == NULL)
		return;
	del_node(tree->root);
	tree->root = NULL;
	free(tree);
}

void out_node(t_itree_node *node, FILE *out){
	if (node == NULL)
		return;
	out_node(node->LEFT, out);
	out_node(node->RIGHT, out);
	size_t i;
	fprintf(out, "mid : %lf\n", node->mid);
	for (i = 0; i < node->size; ++i)
		fprintf(out, "left : %lf, right : %lf\n", node->left[i], node->right[i]);
}

void tree_out(t_itree *tree, FILE *out){
	if ((tree == NULL)||(out == NULL))
		return;
	out_node(tree->root, out);
}

double get_mid(const size_t *curr, size_t n){
	size_t i;
	double result, err, c1, c2;
	if ((curr == NULL)||(n == 0))
		return (1 / (+0.0))*0;
	for (i = 0; i < n; ++i){
		//Правый конец отрезка
		c1 = A[curr[i]] - err;
		c2 = result + c1;
		err = (c2 - result) - c1;
		result = c2;
	}
	err = 0; c1 = 0; c2 = 0;
	for (i = 0; i < n; ++i){	
		//Левый конец отрезка
		c1 = B[curr[i]] - err;
		c2 = result + c1;
		err = (c2 - result) - c1;
		result = c2;
	}
	return (result / (2 * n));
}

int cmp_left(const void *p1, const void *p2){
	return (int)(A[*(size_t *)p1] - A[*(size_t *)p2]);
}

int cmp_right(const void *p1, const void *p2){
	return (int)(B[*(size_t *)p1] - B[*(size_t *)p2]);
}

#define MIN_BUFF (16)

t_itree_node *new_node(size_t *curr, size_t n){
	double m = get_mid(curr, n);
	size_t i, i1 = n - 1, i2 = 0, k, j;
	size_t *buff, *t1, *t2;
	size_t SIZE_BUFF = MIN_BUFF, size_1 = MIN_BUFF, size_2 = MIN_BUFF;
	buff = NULL;
	k = 0; j = 0;
	if ((buff = (size_t *)malloc(MIN_BUFF*sizeof(size_t))) == NULL)
		return NULL;
	if ((t1 = (size_t *)malloc(MIN_BUFF*sizeof(size_t))) == NULL)
		return NULL;
	if ((t2 = (size_t *)malloc(MIN_BUFF*sizeof(size_t))) == NULL)
		return NULL;

	for (i = 0; i < n; ++i){
		if ((A[i] < m)&&(B[i] > m)){
			buff[k++] = i;
			if (k == SIZE_BUFF){
				SIZE_BUFF <<=1;
				buff = realloc(buff, SIZE_BUFF);
			}
			continue;
		}
		if (B[i] < m){
			t1[i1++] = i;
			if (i1 == size_1){
				size_1 <<=1;
				t1 = realloc(t1, size_1);
			}
			continue;
		}
		if (A[i] > m){
			t2[j++] = i;
			if (j = size_2){
				size_2 <<= 1;
				t2 = realloc(t2, size_2);
			}
			i2--;
		}
	}
	t_itree_node *node = NULL;
	for (i = 0; i < i1; ++i){
		buff[i] = t1[i];
	}
	for (i = 0; i < k; ++ i){
		buff[i1 + i] = buff[i];
	}
	for (i = 0; i < j; ++i){
		buff[i1+k+i] = t2[i];
	}
	free(t1);
	free(t2);
	if ((node = (t_itree_node *)malloc(sizeof(t_itree_node))) == NULL){
		free(buff);
		return NULL;
	}
	if ((node->right = (size_t *)malloc(k*sizeof(size_t))) == NULL){
		free(buff);
		free(node);
		return NULL;
	}
	node->mid = m;
	node->size = k;
	buff = realloc(buff, k);
	qsort(buff, k, sizeof(size_t), cmp_left);
	node->left = buff;
	for (i = 0; i < k; ++i){
		printf("%u ->", i);
		printf("[%u]\n", node->left[i]);
	}
	printf(":::::::::\n");
	node->right = memcpy(node->right, buff, k*sizeof(size_t));
	qsort(node->right, k, sizeof(size_t), cmp_right);
	for (i = 0; i < k; ++i){
		printf("%u ->", k);
		printf("[%lf, %lf]\n", A[node->left[i]], B[node->right[i]]);
	}
	buff = NULL;
	node->LEFT = new_node(curr, i1);
	node->RIGHT = new_node(curr + i2, n - i2);
	return node;
}

void tree_make(t_itree **tree){
	if ((tree == NULL)||(*tree == NULL))
		return;
	(*tree)->root = new_node(tmp, size_tmp);
}

int main(){
	t_itree *tree;
	FILE *in, *out;
	size_t i;
	double val;
	if ((in = fopen("input.txt", "rt")) == NULL)
		return ERR_DATA;
	if ((out = fopen("output.txt", "wt")) == NULL){
		fclose(in);
		return ERR_DATA;
	}
	tree = NULL;
	tmp = NULL;
	size_tmp = 0;
	i = 0;
	A = NULL;
	B = NULL;
	fscanf(in, "%lu", &size_tmp);
	if (!size_tmp){
		fclose(in);
		return ERR_DATA;
	}
	if ((tmp = (size_t *)malloc(size_tmp*sizeof(size_t))) == NULL){
		fclose(in);
		return ERR_NULL;
	}
	if ((A = (double *)malloc(size_tmp*sizeof(double))) == NULL){
		free(tmp);
		fclose(in);
		return ERR_NULL;
	}
	if ((B = (double *)malloc(size_tmp*sizeof(double))) == NULL){
		free(tmp);
		free(A);
		fclose(in);
		return ERR_NULL;
	}
	for (i = 0; (!feof(in))&&(i < size_tmp); ++i){
		fscanf(in, "%lf", &val);
		A[i] = val;
		fscanf(in, "%lf", &val);
		B[i] = val;
		tmp[i] = i;
	}
	for (i = 0; i < size_tmp; ++i){
		printf("%u ->", tmp[i]);	
		printf("[%lf, %lf]\n", A[i], B[i]);
	}
	tree = tree_new();
	tree_make(&tree);
	tree_out(tree, out);
	free(A);
	free(B);
	free(tmp);
	fclose(in);
	fclose(out);
	tree_del(tree);
	return ERR_NORM;
}