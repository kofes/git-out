#ifndef __INCLUDE_BOOK_
#define __INCLUDE_BOOK_

#include <cstdio>
#include <cstdlib>
#include <cstring>

typedef char *t_book_key;

typedef struct{
	t_book_key name;	//Ссылается на ключ в узле
	double num;			//Значение
	char mark;			//False - не хранит значение, True - хранит
}t_book_val;

class t_Book{
protected:
	void *root;
public:
	t_Book();
	~t_Book();
	t_book_val *add_elem(t_book_key key);
	t_book_val *get_elem(t_book_key key);
	void del_elem(t_book_key key);
	void out(FILE *out);
	void out_tree(FILE *out);
	void fill_var(FILE *in, FILE *out);
};

#endif
/*__INCLUDE_BOOK_*/