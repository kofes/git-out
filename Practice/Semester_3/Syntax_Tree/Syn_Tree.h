#ifndef __SYNTAX_TREE_
#define __SYNTAX_TREE_

#include "Book.h"
#include <cmath>
#include <cctype>
#define ST_ERR_NORM (0)
#define ST_ERR_DIV0 (1)
#define ST_ERR_OPEN (2)
#define ST_ERR_SIGN (3)
#define ST_ERR_CHAR (4)
#define ST_RU_TO_RN (5)
#define ST_ERR_DATA (6)
#define ST_ERR_NULL (7)
#define ST_EPS (1e-5)
#define ST_SIGNAL(ERR, FILE)	{							\
	switch(ERR){											\
		case 1	: fprintf(FILE, "ERROR: DIV 0\n");	break;	\
		case 2	: fprintf(FILE, "ERROR: OPEN\n");	break;	\
		case 3	: fprintf(FILE, "ERROR: SIGN\n");	break;	\
		case 4	: fprintf(FILE, "ERROR: CHAR\n");	break;	\
	}														\
}															\

/*VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
class t_Syn_Tree{
private:
	t_Book *Book;
	void *root;
public:
	t_Syn_Tree();
	~t_Syn_Tree();
	int parser(char *);
	int get_answer(double *, FILE *in, FILE *out);
	t_Syn_Tree *diff(char *, int *);	/*Спросить как копировать содержимое в новый контейнер*/
	int out(FILE *);
};
/*VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
#endif