#include "Syn_Tree.h"

#define GET_ND(NODE)	{									\
	NODE = NUMB->get_elem();								\
	NUMB->off_elem();										\
}															\

typedef struct{
	char flag;				/*0 - val, 1 - var, 2 - operand, 3 - func*/
	union{
		double val;			/*double val*/
		t_book_val *var;	/*variable*/
		char sign;			/*operand/func*/
	};
}t_elem;

typedef struct ST_node{
	struct ST_node *left, *right;
	t_elem elem;
}ST_node;

typedef struct St_node{
	struct St_node *next;
	ST_node *tree;
}St_node;

ST_node *tree_copy(ST_node *);
double tree_result(ST_node *);
void tree_del(ST_node *);
void tree_out(ST_node *, FILE *);

/*VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
class t_Stack{
private:
	St_node *head;
public:
	t_Stack();
	~t_Stack();
	ST_node *add_elem();
	ST_node *get_elem();
	void off_elem();
	void del_elem();
	int is_empty();
};
/*VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/

static t_Book *G_Book;
static t_Stack *NUMB, *SIGN;

t_Stack::t_Stack(){
	head = NULL;
}

t_Stack::~t_Stack(){
	St_node *node;
	node = head;
	while (head != NULL){
		head = head->next;
		tree_del(node->tree);
		node->tree = NULL;
		delete node;
		node = head;
	}
}

int t_Stack::is_empty(){
	if (head == NULL)
		return false;
	return true;
}

ST_node *t_Stack::add_elem(){
	St_node *node = new St_node;
	node->tree = new ST_node;
	node->next = head;
	head = node;
	node->tree->left = NULL;
	node->tree->right = NULL;
	return node->tree;
}

ST_node *t_Stack::get_elem(){
	if (head == NULL)
		return NULL;
	return head->tree;
}

void t_Stack::off_elem(){
	St_node *node;
	if (head == NULL)
		return;
	node = head;
	head = node->next;
	delete node;
}

void t_Stack::del_elem(){
	St_node *node;
	if (head == NULL)
		return;
	node = head;
	head = node->next;
	tree_del(node->tree);
	delete node;
}
/*==========================================*/
void del_node(ST_node *node){
	if (node == NULL)
		return;
	del_node(node->left);
	del_node(node->right);
	delete node;
}

void tree_del(ST_node *tree){
	if (tree == NULL)
		return;
	del_node(tree);
}
/*==========================================*/
void tree_out_node(ST_node *node, size_t count, FILE *out){
	size_t i;
	if (node == NULL)
		return;
	tree_out_node(node->left, count + 1, out);
	for (i = 0; i < 2*count; ++i)
		fprintf(out, "  ");
	switch (node->elem.flag){
		case 0	: fprintf(out, "%lf\n", node->elem.val); break;
		case 1	: fprintf(out, "%s\n", node->elem.var->name); break;
		case 2	: fprintf(out, "%c\n", node->elem.sign); break;
		case 3	:
			switch (node->elem.sign){
				case 'c': fprintf(out, "cos\n");	break;
				case 'l': fprintf(out, "log\n");	break;
				case 'e': fprintf(out, "exp\n");	break;
				case 'q': fprintf(out, "sqrt\n");	break;
				case 's': fprintf(out, "sin\n");	break;
			}
			break;
	}
	tree_out_node(node->right, count + 1, out);
}

void tree_out(ST_node *tree, FILE *out){
	if ((tree == NULL)||(out == NULL))
		return;
	tree_out_node(tree, 0, out);
}
/*==========================================*/
void node_result(ST_node *node, int *err){
	if ((node == NULL)||(node->elem.flag < 2))
		return;
	node_result(node->right, err);
	node_result(node->left, err);
	node->elem.flag = 0;
	double left, right;
	left = (node->left->elem.flag) == (0) ?
		node->left->elem.val : node->left->elem.var->num;
	if (node->right != NULL)
		right = (node->right->elem.flag) == (0) ?
			node->right->elem.val : node->right->elem.var->num;
	switch (node->elem.sign){
		case -'-':	node->elem.val = - left;
		case '*' :	node->elem.val = right * left;	break;
		case '+' :	node->elem.val = right + left;	break;
		case 'c' :	node->elem.val = cos(left);		break;
		case '-' :	node->elem.val = right - left;	break;
		case 'l' :	node->elem.val = log(left);		break;
		case '/' :
			if ((left - ST_EPS < 0)&&(left + ST_EPS > 0)){ if (err != NULL) *err = ST_ERR_DIV0; return;}
			node->elem.val = right / left;			break;
		case 'e' :	node->elem.val = exp(left);		break;
		case 'q' :	node->elem.val = sqrt(left);	break;
		case 's' :	node->elem.val = sin(left);		break;
	}
}

double tree_result(ST_node *tree, int *err){
	if (tree == NULL){
		if (err != NULL)
			*err = ST_ERR_DATA;
		return (1/(+0.0))*0.0;
	}
	if (err != NULL)
		*err = ST_ERR_NORM;
	node_result(tree, err);
	return tree->elem.val;
}
/*==========================================*/
ST_node *node_copy(ST_node *node){
	ST_node *copy;
	if (node == NULL)
		return NULL;
	copy = new ST_node;
	copy->elem = node->elem;

	/*Проблема с Book'ом - нужно менять его и указатели тоже!*/
	if (copy->elem.flag == 1)
		copy->elem.var = G_Book->add_elem(copy->elem.var->name);

	copy->left = node_copy(node->left);
	copy->right = node_copy(node->right);
	return copy;
}

ST_node *tree_copy(ST_node *head){
	if (head == NULL)
		return NULL;
	return node_copy(head);
}

/*==========================================*/

t_Syn_Tree::t_Syn_Tree(){
	Book = NULL;
	root = NULL;
}

t_Syn_Tree::~t_Syn_Tree(){	/*Как быть со списком!!!*/
	tree_del((ST_node *)root);
	root = NULL;
	delete Book;
	Book = NULL;
}

typedef int (*t_func)(char **pos);
ST_node *node_opt(ST_node *head, int *err);
int get_type(char pos);
int RS (char **pos);
int RN (char **pos);
int RU (char **pos);
int RB (char **pos);

int priority(t_elem ch){
	switch (ch.sign){
		case '(' : return -1;
		case '\0': return 0;
		case '\n': return 0;
		case ')' : return 1;
		case '+' : return 2;
		case '-' : return 2;
		case '*' : return 3;
		case '/' : return 3;
		case -'-': return 4;
	}
	return -1;
}

const static char STAT [3][8] = {
	{0, 1, 2, -1, 0, -1, -1, -1},
	{1, -2, 2, 2, -1, 1, 3, -1},
	{2, 1, -2, -2, 0, -1, -1, -1}
};

const static t_func FUNC [3][8] = {
	{&RS, &RN, &RU, NULL, &RU, NULL, NULL, NULL},
	{&RS, &RN, &RB, &RB, NULL, &RB, NULL, NULL},
	{&RS, &RN, NULL, NULL, &RU, NULL, NULL, NULL}
};

int t_Syn_Tree::parser(char *str){
	char alph_table[256], *pos = str;
	int flag, t, g, err;
	memset(alph_table, 7, 256);
	alph_table[' '] = 0;
	alph_table['-'] = 2;
	memset(alph_table + 48, 1, 10);/*0-9*/
	memset(alph_table + 65, 1, 26);
	memset(alph_table + 97, 1, 26);
	alph_table['+'] = 3; alph_table['*'] = 3; alph_table['/'] = 3;
	alph_table['('] = 4; alph_table[')'] = 5;
	alph_table['s'] = 4; alph_table['c'] = 4;
	alph_table['e'] = 4; alph_table['l'] = 4;
	alph_table['\0'] = 6; alph_table['\n'] = 6;
	G_Book = new t_Book;
	NUMB = new t_Stack;
	SIGN = new t_Stack;
	err = ST_ERR_NORM;
	flag = 0;
	while (true){
		t = alph_table[(unsigned char)(*pos)];
		g = flag;
		flag = STAT[g][t];
		if (flag < 0){
			err = (flag) == (-2) ? ST_ERR_SIGN : ST_ERR_CHAR;
			break;
		}
		if (flag == 3){
			err = RB(&pos);
			break;
		}
		err = FUNC[g][t](&pos);
		if (err == ST_RU_TO_RN){
			flag = 1;
			err = ST_ERR_NORM;
		}
		if (err != ST_ERR_NORM)
			break;
	}
	Book = G_Book;
	if (err == ST_ERR_NORM){
		root = NUMB->get_elem();	/*Добавить после Optimize!*/
		NUMB->off_elem();
		root = node_opt((ST_node *)root, &err);
	}
	G_Book = NULL;
	delete SIGN;
	delete NUMB;
	return err;
}

int RS(char **pos){
	for (; **pos == ' '; ++(*pos));
	return ST_ERR_NORM;
}

int RN(char **pos){
	t_book_key str;
	ST_node *node;
	node = NUMB->add_elem();
	if (**pos < 60){
		node->elem.flag = 0;
		node->elem.val = strtod(*pos, pos);
	} else{
		unsigned char i;
		for (i = 0; isalnum((*pos)[i]); ++i);
		str = new char[i+1];
		memcpy(str, *pos, i);
		str[i] = '\0';
		node->elem.flag = 1;
		node->elem.var = G_Book->add_elem(str);
		*pos = *pos + i;
	}
	return ST_ERR_NORM;
}

int RU(char **pos){
	char i, str[5];
	t_elem elem;
	if ((**pos == '-')||(**pos == '(')){
		elem.flag = 2;
		elem.sign = (**pos) == ('-') ? (-'-') : (**pos);
		SIGN->add_elem()->elem = elem;
		++(*pos);
		return 0;
	}
	for (i = 0; (*(*pos + i) != '\n')&&(*(*pos + i) != '\0')&&(i < 4); ++i);
	memcpy(str, *pos, i);
	str[(unsigned char)i] = '\0';
	elem.flag = 3;
	if (!strcmp(str, "sqrt")){
		elem.sign = 'q';
		++(*pos);
	}else if (!strcmp(str, "cos("))
			elem.sign = 'c';
		else if (!strcmp(str, "log("))
			elem.sign = 'l';
			else if (!strcmp(str, "exp("))
				elem.sign = 'e';
				else if (!strcmp(str, "sin("))
					elem.sign = 's';
					else{
						RN(pos);
						return ST_RU_TO_RN;
					}
	SIGN->add_elem()->elem = elem;
	*pos = *pos + 3;
	if (**pos != '(')
		return ST_ERR_CHAR;
	++(*pos);
	return ST_ERR_NORM;
}

int RB(char **pos){
	t_elem elem;
	char ch = **pos;
	elem.flag = 2;
	elem.sign = ch;
	int p = priority(elem);
	while ((SIGN->is_empty())&&(priority(SIGN->get_elem()->elem) >= p)){
		t_elem elem = SIGN->get_elem()->elem;
		char sym = elem.sign;
		SIGN->del_elem();
		ST_node *node1, *node2, *head;
		switch (sym){
			case -'-':
				GET_ND(node1);
				if (!node1->elem.flag){
					node1->elem.val = - node1->elem.val;
					NUMB->add_elem()->elem = node1->elem;
				} else{
					head = NUMB->add_elem();
					head->elem.flag = 2;
					head->left = node1;
					head->right = NULL;
					head->elem.sign = '-';
				}
				break;
			case '+' :
				GET_ND(node1); GET_ND(node2);
				head = NUMB->add_elem();
				if ((!node1->elem.flag)&&(!node2->elem.flag)){
					head->elem.flag = 0;
					head->elem.val = node2->elem.val + node1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = node1;
					head->right = node2;
					head->elem.sign = sym;
				}
				break;
			case '-' :
				GET_ND(node1); GET_ND(node2);
				head = NUMB->add_elem();
				if ((!node1->elem.flag)&&(!node2->elem.flag)){
					head->elem.flag = 0;
					head->elem.val = node2->elem.val - node1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = node1;
					head->right = node2;
					head->elem.sign = sym;
				}
				break;
			case '/' :
				GET_ND(node1); GET_ND(node2);
				head = NUMB->add_elem();
				if ((!node1->elem.flag)&&(!node2->elem.flag)){
					head->elem.flag = 0;
					if ((node1->elem.val - ST_EPS < 0)&&(node1->elem.val + ST_EPS > 0))
						return ST_ERR_DIV0;
					head->elem.val = node2->elem.val / node1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = node1;
					head->right = node2;
					head->elem.sign = sym;
				}
				break;
			case '*' :
				GET_ND(node1); GET_ND(node2);
				head = NUMB->add_elem();
				if ((!node1->elem.flag)&&(!node2->elem.flag)){
					head->elem.flag = 0;
					head->elem.val = node2->elem.val * node1->elem.val;
				} else{
					head->elem.flag = 2;
					head->left = node1;
					head->right = node2;
					head->elem.sign = sym;
				}
				break;
		}
	}
	if (ch == ')'){
		if (!SIGN->is_empty())
			return ST_ERR_OPEN;
		if (SIGN->get_elem()->elem.flag == 3){
			ST_node *node, *head;
			node = NUMB->get_elem();
			if (node->elem.flag == 0){
				head = SIGN->get_elem();
				switch (head->elem.sign){
					case 'c': node->elem.val = cos(node->elem.val); break;
					case 'l': node->elem.val = log(node->elem.val); break;
					case 'e': node->elem.val = exp(node->elem.val); break;
					case 'q': node->elem.val = sqrt(node->elem.val); break;
					case 's': node->elem.val = sin(node->elem.val); break;
				}
			} else{
				NUMB->off_elem();
				head = NUMB->add_elem();
				head->left = node;
				head->right = NULL;
				head->elem.flag = 3;
				head->elem.sign = SIGN->get_elem()->elem.sign;
			}
		}
		SIGN->del_elem();
		++(*pos);
		return ST_ERR_NORM;
	}
	if ((ch == '\0')||(ch == '\n')){
		if (SIGN->is_empty())
			return ST_ERR_OPEN;
		return ST_ERR_NORM;
	}
	elem.flag = 2;
	elem.sign = ch;
	SIGN->add_elem()->elem = elem;
	++(*pos);
	return ST_ERR_NORM;
}
int t_Syn_Tree::get_answer(double *val, FILE *in, FILE *out){
	ST_node *node;
	int err;
	if (val == NULL)
		return ST_ERR_DATA;
	G_Book = NULL;
	G_Book = new t_Book;
	node = tree_copy((ST_node *)root);
	G_Book->fill_var(in, out);
	*val = tree_result(node, &err);
	G_Book->~t_Book();
	G_Book = NULL;
	return err;
}

int t_Syn_Tree::out(FILE *out){
	tree_out((ST_node *)root, out);
	return 0;
}
/*==============================================*/
ST_node *node_opt(ST_node *node, int *err){
	if ((node == NULL)||(node->elem.flag < 2))
		return node;
	node->left = node_opt(node->left, err);
	node->right = node_opt(node->right, err);
	if ((node->left->elem.flag == 0)||
		((node->right != NULL)&&(node->right->elem.flag == 0)))
		switch (node->elem.sign){
			case '-':
				if (node->right == NULL){
					node->elem.flag = 0;
					node->elem.val = - node->left->elem.val;
					tree_del(node->left);
					node->left = NULL;
				} else{
					if (node->right->elem.flag == 0){
						if (node->left->elem.flag == 0){
							node->elem.flag = 0;
							node->elem.val = node->right->elem.val - node->left->elem.val;
							tree_del(node->right);
							tree_del(node->left);
							node->right = NULL;
							node->left = NULL;
						}else if ((node->right->elem.val - ST_EPS < 0)&&((node->right->elem.val + ST_EPS > 0))){
							tree_del(node->right);
							node->right = NULL;
						}
					}else if ((node->left->elem.val - ST_EPS < 0)&&((node->left->elem.val + ST_EPS > 0))){
						ST_node *point;
						tree_del(node->left);
						node->left = NULL;
						point = node;
						node = node->right;
						delete point;
					}
				}
				break;
			case '+':
				if (node->right == NULL){
					node->elem.flag = 0;
					node->elem.val = node->left->elem.val;
					tree_del(node->left);
					node->left = NULL;
				} else{
					if (node->right->elem.flag == 0){
						if (node->left->elem.flag == 0){
							node->elem.flag = 0;
							node->elem.val = node->right->elem.val + node->left->elem.val;
							tree_del(node->right);
							tree_del(node->left);
						}else if ((node->right->elem.val - ST_EPS < 0)&&((node->right->elem.val + ST_EPS > 0))){
							ST_node *point;
							tree_del(node->right);
							node->right = NULL;
							point = node;
							node = node->left;
							delete point;
						}
					}else if ((node->left->elem.val - ST_EPS < 0)&&((node->left->elem.val + ST_EPS > 0))){
						ST_node *point;
						tree_del(node->left);
						node->left = NULL;
						point = node;
						node = node->right;
						delete point;
					}
				}
				break;
			case '*':
				if (node->right->elem.flag == 0){
					if (node->left->elem.flag == 0){
						node->elem.flag = 0;
						node->elem.val = node->right->elem.val * node->left->elem.val;
						tree_del(node->right);
						tree_del(node->left);
					}else{
						if ((node->right->elem.val - ST_EPS < 0)&&(node->right->elem.val + ST_EPS > 0)){
							node->elem.flag = 0;
							node->elem.val = 0;
							tree_del(node->right);
							tree_del(node->left);
							node->right = NULL;
							node->left = NULL;
						}else if ((node->right->elem.val - ST_EPS < 1)&&(node->right->elem.val + ST_EPS > 1)){
							ST_node *point;
							tree_del(node->right);
							node->right = NULL;
							point = node;
							node = node->left;
							delete point;
						}
					}
				}else{
					if ((node->left->elem.val - ST_EPS < 0)&&(node->left->elem.val + ST_EPS > 0)){
							node->elem.flag = 0;
							node->elem.val = 0;
							tree_del(node->right);
							tree_del(node->left);
							node->right = NULL;
							node->left = NULL;
						}else if ((node->left->elem.val - ST_EPS < 1)&&(node->left->elem.val + ST_EPS > 1)){
							ST_node *point;
							tree_del(node->left);
							node->left = NULL;
							point = node;
							node = node->right;
							delete point;
						}
				}
				break;
			case '/':
				if (node->right->elem.flag == 0){
					if (node->left->elem.flag == 0){
						if ((node->left->elem.val - ST_EPS < 0)&&(node->right->elem.val + ST_EPS > 0)){
							*err = ST_ERR_DIV0;
							return node;
						}
						node->elem.flag = 0;
						node->elem.val = node->right->elem.val / node->left->elem.val;
						tree_del(node->right);
						tree_del(node->left);
						node->right = NULL;
						node->left = NULL;
					}else{
						if ((node->right->elem.val - ST_EPS < 0)&&(node->left->elem.val + ST_EPS > 0)){
							node->elem.flag = 0;
							node->elem.val = 0;
							tree_del(node->right);
							tree_del(node->left);
							node->right = NULL;
							node->left = NULL;
						}
					}
				}else if ((node->left->elem.val - ST_EPS < 1)&&(node->left->elem.val + ST_EPS > 1)){
					ST_node *point;
					delete node->left;
					point = node;
					node = node->right;
					delete point;
				}
				break;
			case 'c':
				node->elem.flag = 0;
				node->elem.val = cos(node->left->elem.val);
				tree_del(node->left);
				node->left = NULL;
				break;
			case 'l':
				node->elem.flag = 0;
				node->elem.val = log(node->left->elem.val);
				tree_del(node->left);
				node->left = NULL;
				break;
			case 'e':
				node->elem.flag = 0;
				node->elem.val = exp(node->left->elem.val);
				tree_del(node->left);
				node->left = NULL;
				break;
			case 'q':
				node->elem.flag = 0;
				node->elem.val = sqrt(node->left->elem.val);
				tree_del(node->left);
				node->left = NULL;
				break;
			case 's':
				node->elem.flag = 0;
				node->elem.val = sin(node->left->elem.val);
				tree_del(node->left);
				node->left = NULL;
				break;
		}
	return node;
}
/*==============================================*/
ST_node *node_diff(ST_node *node, char *X, int *err){
	if (node == NULL)
		return NULL;
	if ((node->elem.flag < 2)){
		if ((node->elem.flag == 1)&&(!strcmp(node->elem.var->name, X)))
			node->elem.val = 1;
		else
			node->elem.val = 0;
		node->elem.flag = 0;
		return node;
	}
	ST_node *head, *jnode, *shead, *rnode, *lnode;
	switch (node->elem.sign){
		case '-':
			node->right = node_diff(node->right, X, err);
			node->left = node_diff(node->left, X, err);
			return node;
		case '+':
			node->right = node_diff(node->right, X, err);
			node->left = node_diff(node->left, X, err);
			return node;
		case '*':
			printf("(1)\n");
			jnode = tree_copy(node);
			head = new ST_node;
			head->right = node;
			head->left = jnode;
			head->elem.flag = 2;
			head->elem.sign = '+';
			head->right->right = node_diff(head->right->right, X, err);
			head->left->left = node_diff(head->left->left, X, err);
			return head;
		case '/':
			rnode = new ST_node;
			lnode = new ST_node;
			rnode->left = tree_copy(node);
			rnode->left = tree_copy(node);
			lnode->left = tree_copy(node->left);
			lnode->right = tree_copy(node->left);
			node->right = rnode;
			node->left = lnode;
			rnode->elem.flag = 2;
			lnode->elem.flag = 2;
			rnode->left->elem.flag = 2;
			rnode->right->elem.flag = 2;
			rnode->elem.sign = '-';
			lnode->elem.sign = '*';
			rnode->left->elem.sign = '*';
			rnode->right->elem.sign = '*';
			rnode->right->right = node_diff(rnode->right->right, X, err);
			rnode->left->left = node_diff(rnode->left->left, X, err);
			return node;
		case 'c':
			head = new ST_node;
			head->elem.flag = 2;
			head->elem.sign = '*';
			head->left = tree_copy(node->left);
			shead = new ST_node;
			shead->elem.flag = 2;
			shead->elem.sign = '-';
			node->elem.sign = 's';
			head->right = shead;
			shead->left = node;
			shead->right = NULL;
			head->left = node_diff(head->left, X, err);
			return head;
		case 'l':
			jnode = tree_copy(node->left);
			node->right = jnode;
			node->elem.flag = 2;
			node->elem.sign = '/';
			node->right = node_diff(node->right, X, err);
			return node;
		case 'e':
			head = new ST_node;
			head->right = node;
			head->left = tree_copy(node->left);
			head->left = node_diff(head->left, X, err);
			return head;
		case 'q':
			head = new ST_node;
			head->right = new ST_node;
			head->left = tree_copy(node);
			head->right->elem.flag = 0;
			head->right->elem.val = 0.5;
			head->elem.flag = 2;
			head->elem.sign = '*';
			head->left->elem.flag = 2;
			head->left->elem.sign = '/';
			head->left->right = head->left->left;
			head->left->left = node;
			head->left->right = node_diff(head->left->right, X, err);
			return head;
		case 's':
			head = tree_copy(node);
			head->elem.flag = 2;
			head->elem.sign = '*';
			head->right = node;
			node->elem.sign = 'c';
			head->left = node_diff(head->left, X, err);
			return head;
	}
	return node;
}

t_Syn_Tree *t_Syn_Tree::diff(char *X, int *err){
	*err = ST_ERR_NORM;
	t_Syn_Tree *tree = new t_Syn_Tree;
	if ((err == NULL)||(X == NULL))
		return NULL;
	G_Book = new t_Book;
	if (Book->get_elem(X) != NULL){
		ST_node *node;
		tree->root = tree_copy((ST_node *)root);//<- переделать дерево, т.к. словарь больше, чем нужен может быть
		tree->root = node_diff((ST_node *)(tree->root), X, err);
		tree->root = node_opt((ST_node *)(tree->root), err);
	}
	else{
		tree->root = new ST_node;
		((ST_node *)(tree->root))->elem.flag = 0;
		((ST_node *)(tree->root))->elem.val = 0;
		((ST_node *)(tree->root))->right = NULL;
		((ST_node *)(tree->root))->left = NULL;
	}
	tree->Book = G_Book;
	G_Book = NULL;
	return tree;
}
/*==============================================*/
