#include "Syn_Tree.h"
int main(int argc, char *argv[]){
	int err;
	double val;
	char *str = "1 + cos(log(y))";
	t_Syn_Tree *p, *tree = new t_Syn_Tree;
	val = 0;
	ST_SIGNAL(tree->parser(str), stdout);
	tree->out(stdout);
	tree->get_answer(&val, stdin, stdout);
	p = tree->diff("y", &err);
	printf("ANSWER: %lf\n", val);
	p->out(stdout);
	printf(":::::::::\n");
	p->get_answer(&val, stdin, stdout);
	printf("ANSWER: %lf\n", val);
	/*
	*/
	//tree->out(stdout);
	return 0;
}