#include <cstdio>
#include <cmath>
#include <cctype>
#include <cstdlib>
typedef struct{
	char flag;				/*0 - val, 1 - var, 2 - operand, 3 - func*/
	union{
		double val;			/*double val*/
		char *var;			/*variable*/
		char sign;			/*operand/func*/
	};
}Qt_elem;
typedef struct{
	struct t_Queue *next;
	Qt_elem elem;
}Qt_node;
class t_Queue{
	t_Queue *head, *tail;
public:
	t_Queue();
	~t_Queue();
	Qt_elem *add_elem(Qt_elem);
	Qt_elem *get_elem();
	void del_elem();
	void out(FILE *);
};

t_Queue::t_Queue(){
	head = NULL;
	tail = NULL;
}

t_Queue::~t_Queue(){
	Qt_node *curr;
	curr = head;
	while (curr != NULL){
		node = curr;
		curr = curr->next;
		delete node;
	}
	head = NULL;
	tail = NULL;
}

Qt_elem *t_Queue::add_elem(Qt_elem){
	Qt_node *node = new node;
	node->next = NULL;
	if (tail != NULL)
		tail->next = node;
	else
		tail = node;
	if (head == NULL)
		head = node;
	return &(node->elem);
}

Qt_elem *t_Queue::get_elem(){
	return &(head->elem);
}

void t_Queue::del_elem(){
	if (head != NULL){
		node = head;
		head = node->next;
		delete node;
	}
	if (head == NULL)
		tail = NULL;
}

void t_Queue::out(FILE *out){
	Qt_node *curr;

	if (out == NULL)
		return;

	curr = head;
	while (curr != NULL){
		switch (curr->elem.flag){
			case 0	: fprintf(out, "%lf", curr->elem.val); break;
			case 1	: fprintf(out, "%s", curr->elem.var); break;
			case 2	: fprintf(out, "%c", curr->elem.sign); break;
			case 3	:
				switch (curr->elem.sign){
					case 'c': fprintf(out, "cos");	break;
					case 'l': fprintf(out, "log");	break;
					case 'e': fprintf(out, "exp");	break;
					case 'q': fprintf(out, "sqrt");	break;
					case 's': fprintf(out, "sin");	break;
				}
				break;
		}
		fprintf(out, "	");
		curr = curr->next;
	}
	fprintf(out, "\n");
}