#include "Book.h"

typedef struct t_book_node{
	struct t_book_node *child[2];
	t_book_key key;
	t_book_val val;
	char diff;
}t_book_node;

t_Book::t_Book(){	root = NULL;}

void Book_del_node(t_book_node *node){
	if (node == NULL)
		return;
	Book_del_node(node->child[0]);
	Book_del_node(node->child[1]);
	node->val.name = NULL;
	delete[] node->key;
	delete node;
	node = NULL;
}

t_Book::~t_Book(){	Book_del_node((t_book_node *)root);}

t_book_node *lit_turn(t_book_node *root, char inv){
	t_book_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

t_book_node *turn(t_book_node *node, char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

t_book_node *book_add_node(t_book_node *node, char *info, const t_book_key key, t_book_val **val){
	int i;
	if (node == NULL){
		size_t len;
		node = new t_book_node;
		
		node->diff = 0;
		node->child[0] = NULL;
		node->child[1] = NULL;
		len = strlen(key) + 1;
		node->key = new char[len];
		memcpy(node->key, key, len);
		node->val.num = 0;
		node->val.name = node->key;
		node->val.mark = 0;
		*val = &(node->val);

		return node;
	}

	i = strcmp(key, node->key);
	if (!i){
		*val = &(node->val);
		*info = 1;
		return node;
	}
	i = (i > 0);

	node->child[i] = book_add_node(node->child[i], info, key, val);
	node->diff += (1 - 2*i)*((*info + 1)%2);
	node = turn(node, i);
	if ((*info == 0)&&(node->diff == 0))
		*info = 1;
	return node;
}

t_book_val *t_Book::add_elem(const t_book_key key){

	char info = 0;
	t_book_val *val = NULL;

	root = book_add_node((t_book_node *)root, &info, key, &val);
	return val;
}

t_book_val *t_Book::get_elem(const t_book_key key){

	t_book_node *node;
	int i;
	node = (t_book_node *)root;
	while (node != NULL){
		i = strcmp(key, node->key);
		if (!i)	return &(node->val);
		i = (i > 0);
		node = node->child[i];
	}
	return NULL;
}

t_book_node *del_min(t_book_node *node){
	if (node->child[0] == NULL)
		return node->child[1];
	node->child[0] = del_min(node->child[0]);
	return turn(node, 1);
}

t_book_node *book_del_node(t_book_node *node, char *info, const t_book_key key){
	int i;
	if (node == NULL){
		*info = 1;
		return NULL;
	}
	i = strcmp(key, node->key);
	if (!i){
		t_book_node *left = node->child[0], *right = node->child[1];
		char diff = node->diff;
		node->val.name = NULL;
		delete[] node->key;
		delete node;
		if (right != NULL){
			t_book_node *min;

			min = right;
			while (min->child[0] != NULL)
				min = min->child[0];
			min->child[1] = del_min(right);
			min->child[0] = left;
			return turn(min, 0);
		}
		else
			return left;
	}
	i = (i > 0);

	node->child[i] = book_del_node(node->child[i], info, key);
	node->diff += (2*i - 1)*((*info + 1) % 2);
	node = turn(node, (i + 1) % 2);
	if ((*info == 0)&&(abs(node->diff) == 1))
		*info = 1;
	return node;
}

void t_Book::del_elem(const t_book_key key){

	char info = 0;

	root = book_del_node((t_book_node *)root, &info, key);
}

void book_out_node(t_book_node *node, FILE *out){
	if (node == NULL)
		return;
	book_out_node(node->child[0], out);
	if (!node->val.mark)
		fprintf(out, "%s\n", node->key);
	else
		fprintf(out, "%s = %lf\n", node->key, node->val.num);
	book_out_node(node->child[1], out);
}

void t_Book::out(FILE *out){

	if (out == NULL)
		return;

	book_out_node((t_book_node *)root, out);
}

void book_out_book_node(t_book_node *node, size_t count, FILE *out){
	size_t i;

	if (node == NULL)
		return;
	book_out_book_node(node->child[1], count + 1, out);
	for (i = 0; i<2*count; ++i)
		fprintf(out, "  ");
	fprintf(out, "key: %s, mark: %i, val: %lf\n", node->key, node->val.mark, node->val.num);
	book_out_book_node(node->child[0], count + 1, out);
}

void t_Book::out_tree(FILE *out){

	if (out == NULL)
		return;

	book_out_book_node((t_book_node *)root, 0, out);
}

void book_fill_node(t_book_node *node, FILE *in, FILE *out){
	if (node == NULL)
		return;
	book_fill_node(node->child[1], in, out);
	if (out != NULL)
		fprintf(out, "%s: ", node->key);
	fscanf(in, "%lf", &(node->val.num));
	node->val.mark = 2;
	book_fill_node(node->child[0], in, out);
}

void t_Book::fill_var(FILE *in, FILE *out){
	if (in == NULL)
		return;
	if (out != NULL)
	fprintf(out, "INPUT:\n");
	book_fill_node((t_book_node *)root, in, out);
}