#ifndef __INCLUDE_FRACTION_

#define __INCLUDE_FRACTION_

#include <fstream>
#include <iostream>
#include <gmpxx.h>

class t_fraction{
public:
	/*Codes of errors*/
	enum t_error {
		ERR_NORM,			/*Allright, Bro!*/
		ERR_NULL,			/*One of argues hasn't any parametres*/
		ERR_OVER_FLOW,		/*Arithmetic overflow*/
		ERR_ACCESS,			/*Can't access to parametres*/
		ERR_DIV0				/*Division by zero*/
	};
	private:
		/*struct of fraction--*/
		typedef struct{
			mpz_class on;
			mpz_class under;
			unsigned int ct_pts;
		}t_frac;
		/*--------------------*/
		t_frac *ptr;
		static t_error error;
	public:
		static inline char get_error(){return error;}
		static inline void off_error(){error = ERR_NORM;};
		/*Init/copy/distract/... t_fraction class*/
		t_fraction(): ptr(NULL){};
		t_fraction(unsigned long int _on, long int _under);
		t_fraction(const t_fraction &from);
		~t_fraction();
		/*for t_fraction class*/
		t_fraction &operator =(const t_fraction &from);	//WORK!
		t_fraction operator +(const t_fraction &with);	//WORK!
		t_fraction operator -(const t_fraction &with);	//WORK!
		t_fraction operator -();								//WORK!
		t_fraction operator *(const t_fraction &with);	//WORK!
		t_fraction operator /(const t_fraction &with);	//WORK!
		char sign();
		mpz_class numerator();
		mpz_class integer();
		mpz_class denominator();
		char operator >(const t_fraction &with);			//WORK!
		char operator <(const t_fraction &with);			//WORK!
		char operator >=(const t_fraction &with);
		char operator <=(const t_fraction &with);
		char operator ==(const t_fraction &with);			//WORK!
		/*for long int*/
		t_fraction &operator =(long int src);
		t_fraction operator +(long int src);
		t_fraction operator -(long int src);
		t_fraction operator *(long int src);
		t_fraction operator /(long int src);
		char operator >(long int src);
		char operator <(long int src);
		char operator >=(long int src);
		char operator <=(long int src);
		char operator ==(long int src);
		/*for friend long int*/
		friend char operator >(long int src, const t_fraction &with);
		friend char operator <(long int src, const t_fraction &with);
		friend char operator >=(long int src, const t_fraction &with);
		friend char operator <=(long int src, const t_fraction &with);
		friend char operator ==(long int src, const t_fraction &with);
		/*-----------------*/
		friend std::ofstream &operator <<(std::ofstream &out, const t_fraction &from);
		friend std::ostream &operator <<(std::ostream &out, const t_fraction &from);
};

#endif
