#include "fraction.hpp"

t_fraction::t_error t_fraction::error = t_fraction::ERR_NORM;

/*NOD*/

mpz_class NOD(mpz_class a, mpz_class b){
	mpz_class left, right;
	left = a * (a > 0 ? 1 : -1);
	right = b * (b > 0 ? 1 : -1);
	while ((left != 0)&&(right != 0)){
		if (left > right) left = left % right;
		else right = right % left;
	}
	return (left > right) ? left : right;
}

/*Init/copy/distract/... t_fraction class*/
t_fraction::t_fraction(unsigned long int _on, long int _under){
	ptr = new t_frac;
	ptr->on = _on;
	ptr->under = _under;
	ptr->ct_pts = 0;
}

t_fraction::t_fraction(const t_fraction &from){
	if (from.ptr != NULL)
		++from.ptr->ct_pts;
	ptr = from.ptr;
}

t_fraction::~t_fraction(){
	if (ptr != NULL)
		if (!ptr->ct_pts){
			ptr->on = ptr->under = 0;
			delete ptr;
		}else
			--ptr->ct_pts;
	ptr = NULL;
}


/*for t_fraction class*/
t_fraction &t_fraction::operator =(const t_fraction &from){

	if (ptr != NULL){
		if (!ptr->ct_pts){
			ptr->on = ptr->under = 0;
			delete ptr;
		}else
			--ptr->ct_pts;
	}

	if (from.ptr != NULL)
		++from.ptr->ct_pts;

	ptr = from.ptr;

	return *this;
}

t_fraction t_fraction::operator +(const t_fraction &with){

	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_NULL;
		t_fraction frac;
		return frac;
	}

	if ((ptr->under == 0)||(with.ptr->under == 0)){
		error = ERR_DIV0;
		t_fraction frac;
		return frac;
	}

	t_fraction result;
	result.ptr = new t_frac;
	result.ptr->ct_pts = 0;
	result.ptr->on = ptr->on * with.ptr->under + with.ptr->on * ptr->under;

	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}

	result.ptr->under = ptr->under * with.ptr->under;
	if (result.ptr->on < 0){
		result.ptr->under = - result.ptr->under;
		result.ptr->on = - result.ptr->on;
	}

	mpz_class nod = NOD(result.ptr->on,	(result.ptr->under > 0) ? result.ptr->under : -result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;
}

t_fraction t_fraction::operator -(const t_fraction &with){

	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_NULL;
		t_fraction frac;
		return frac;
	}

	if ((ptr->under == 0)||(with.ptr->under == 0)){
		error = ERR_DIV0;
		t_fraction frac;
		return frac;
	}

	t_fraction result;
	result.ptr = new t_frac;
	result.ptr->ct_pts = 0;

	result.ptr->on = ptr->on * with.ptr->under - with.ptr->on * ptr->under;
	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}
	result.ptr->under = ptr->under * with.ptr->under;
	if (result.ptr->on < 0){
		result.ptr->under = - result.ptr->under;
		result.ptr->on = - result.ptr->on;
	}
	mpz_class nod = NOD(result.ptr->on,	(result.ptr->under > 0) ? result.ptr->under : -result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;

}

t_fraction t_fraction::operator -(){
	if (ptr == NULL)
		return t_fraction();
	t_fraction result(0, 0);
	result.ptr->on = ptr->on;
	result.ptr->under = - ptr->under;
	return result;
}

t_fraction t_fraction::operator *(const t_fraction &with){

	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_NULL;
		t_fraction frac;
		return frac;
	}

	if ((ptr->under == 0)||(with.ptr->under == 0)){
		error = ERR_DIV0;
		t_fraction frac;
		return frac;
	}

	t_fraction result;
	result.ptr = new t_frac;
	result.ptr->ct_pts = 0;

	result.ptr->on = ptr->on * with.ptr->on;

	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}
	result.ptr->under = ptr->under * with.ptr->under;
	if (result.ptr->on < 0){
		result.ptr->under = - result.ptr->under;
		result.ptr->on = - result.ptr->on;
	}
	mpz_class nod = NOD(result.ptr->on,	(result.ptr->under > 0) ? result.ptr->under : -result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;

}

t_fraction t_fraction::operator /(const t_fraction &with){

		if ((ptr == NULL)||(with.ptr == NULL)){
			error = ERR_NULL;
			t_fraction frac;
			return frac;
		}

		if ((ptr->under == 0)||(with.ptr->under == 0)||(with.ptr->on == 0)){
			error = ERR_DIV0;
			t_fraction frac;
			return frac;
		}

		t_fraction result;
		result.ptr = new t_frac;
		result.ptr->ct_pts = 0;

		result.ptr->on = ptr->on * with.ptr->under;

		if (result.ptr->on == 0){
			result.ptr->under = 1;
			return result;
		}
		result.ptr->under = ptr->under * with.ptr->on;
		if (result.ptr->on < 0){
			result.ptr->under = - result.ptr->under;
			result.ptr->on = - result.ptr->on;
		}
		mpz_class nod = NOD(result.ptr->on,	(result.ptr->under > 0) ? result.ptr->under : -result.ptr->under);
		result.ptr->on = result.ptr->on / nod;
		result.ptr->under = result.ptr->under / nod;
		return result;

}

char t_fraction::sign(){
	if (ptr == NULL){
		error = ERR_ACCESS;
		return 0;
	}
	return (ptr->under > 0) ? 1 : -1;
}

mpz_class t_fraction::numerator(){
	if (ptr == NULL){
		error = ERR_ACCESS;
		return 0;
	}
	return ptr->on;
}

mpz_class t_fraction::integer(){
	if (ptr == NULL){
		error = ERR_ACCESS;
		return 0;
	}
	return ptr->on / ptr->under;
}

mpz_class t_fraction::denominator(){
	if (ptr == NULL){
		error = ERR_ACCESS;
		return 0;
	}
	return ptr->under;
}

char t_fraction::operator >(const t_fraction &with){
	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_ACCESS;
		return 0;
	}
	return (
				ptr->on *
				(ptr->under > 0 ? 1 : -1) *
				(with.ptr->under < 0 ? - with.ptr->under : with.ptr->under)
		-
				with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1) *
				(ptr->under < 0 ? - ptr->under : ptr->under)
		> 0) ?
		1 : 0;
}

char t_fraction::operator <(const t_fraction &with){
	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_ACCESS;
		return 0;
	}
	return (
				ptr->on *
				(ptr->under > 0 ? 1 : -1) *
				(with.ptr->under < 0 ? - with.ptr->under : with.ptr->under)
		-
				with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1) *
				(ptr->under < 0 ? - ptr->under : ptr->under)
		< 0)?
		1 : 0;
}

char t_fraction::operator >=(const t_fraction &with){
	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_ACCESS;
		return 0;
	}
	return (
				ptr->on *
				(ptr->under > 0 ? 1 : -1) *
				(with.ptr->under < 0 ? - with.ptr->under : with.ptr->under)
		-
				with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1) *
				(ptr->under < 0 ? - ptr->under : ptr->under)
		>= 0)?
		1 : 0;
}

char t_fraction::operator <=(const t_fraction &with){
	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_ACCESS;
		return 0;
	}
	return (
				ptr->on *
				(ptr->under > 0 ? 1 : -1) *
				(with.ptr->under < 0 ? - with.ptr->under : with.ptr->under)
		-
				with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1) *
				(ptr->under < 0 ? - ptr->under : ptr->under)
		<= 0)?
		1 : 0;
}

char t_fraction::operator ==(const t_fraction &with){
	if ((ptr == NULL)||(with.ptr == NULL)){
		error = ERR_ACCESS;
		return 0;
	}
	return (
				ptr->on *
				(ptr->under > 0 ? 1 : -1) *
				(with.ptr->under < 0 ? - with.ptr->under : with.ptr->under)
		-
				with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1) *
				(ptr->under < 0 ? - ptr->under : ptr->under)
		== 0)?
		1 : 0;
}

/*for long int*/
t_fraction &t_fraction::operator =(long int src){
	if (ptr != NULL){
		if (ptr->ct_pts){
			--ptr->ct_pts;
			ptr = new t_frac();
		}
	}else
		ptr = new t_frac();
	this->ptr->on = (src < 0) ? -src : src;
	this->ptr->under = (src < 0) ? -1 : 1;

	return *this;
}

t_fraction t_fraction::operator +(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return t_fraction();
	}

	t_fraction result;
	result.ptr = new t_frac;
	result.ptr->ct_pts = 0;
	result.ptr->on = ptr->on + src * ptr->under;
	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}
	result.ptr->under = (result.ptr->on < 0) ? -ptr->under : ptr->under;
	mpz_class nod = NOD(result.ptr->on, (result.ptr->under < 0) ? -result.ptr->under : result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;
}

t_fraction t_fraction::operator -(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return t_fraction();
	}

	t_fraction result;
	result.ptr = new t_frac;
	result.ptr->ct_pts = 0;
	result.ptr->on = ptr->on - src * ptr->under;
	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}
	result.ptr->under = (result.ptr->on < 0) ? -ptr->under : ptr->under;
	mpz_class nod = NOD(result.ptr->on, (result.ptr->under < 0) ? -result.ptr->under : result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;
}

t_fraction t_fraction::operator *(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return t_fraction();
	}

	t_fraction result;
	result.ptr = new t_frac;
	result.ptr->ct_pts = 0;
	result.ptr->on = ptr->on * src;

	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}

	result.ptr->under = (result.ptr->on < 0) ? -ptr->under : ptr->under;
	mpz_class nod = NOD(result.ptr->on, (result.ptr->under < 0) ? -result.ptr->under : result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;
}

t_fraction t_fraction::operator /(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return t_fraction();
	}
	if (src == 0){
		error = ERR_DIV0;
		return t_fraction();
	}
	t_fraction result(0, 1);
	result.ptr->under = ptr->under * src;
	result.ptr->on = ptr->on;
	if (result.ptr->on == 0){
		result.ptr->under = 1;
		return result;
	}
	mpz_class nod = NOD(result.ptr->on, (result.ptr->under < 0) ? -result.ptr->under : result.ptr->under);
	result.ptr->on = result.ptr->on / nod;
	result.ptr->under = result.ptr->under / nod;
	return result;
}

char t_fraction::operator >(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return 0;
	}
	return (ptr->on * (ptr->under > 0 ? 1 : -1) > (ptr->under < 0 ? -ptr->under : ptr->under) * src) ? 1 : 0;
}

char t_fraction::operator <(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return 0;
	}
	return (ptr->on * (ptr->under > 0 ? 1 : -1) < (ptr->under < 0 ? -ptr->under : ptr->under) * src) ? 1 : 0;
}

char t_fraction::operator >=(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return 0;
	}
	return (ptr->on * (ptr->under > 0 ? 1 : -1) >= (ptr->under < 0 ? -ptr->under : ptr->under) * src) ? 1 : 0;
}

char t_fraction::operator <=(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return 0;
	}
	return (ptr->on * (ptr->under > 0 ? 1 : -1) <= (ptr->under < 0 ? -ptr->under : ptr->under) * src) ? 1 : 0;
}

char t_fraction::operator ==(long int src){
	if (ptr == NULL){
		error = ERR_NULL;
		return 0;
	}
	return (ptr->on * (ptr->under > 0 ? 1 : -1) == (ptr->under < 0 ? -ptr->under : ptr->under) * src) ? 1 : 0;
}

/*for friend long int*/
char operator >(long int src, const t_fraction &with){
	if (with.ptr == NULL){
		t_fraction::error = t_fraction::ERR_NULL;
		return 0;
	}
	return	(with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1)
			>
				(with.ptr->under < 0 ? -with.ptr->under : with.ptr->under) *
				src)
			? 1 : 0;
}

char operator <(long int src, const t_fraction &with){
	if (with.ptr == NULL){
		t_fraction::error = t_fraction::ERR_NULL;
		return 0;
	}
	return	(with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1)
			<
				(with.ptr->under < 0 ? -with.ptr->under : with.ptr->under) *
				src)
			? 1 : 0;
}

char operator >=(long int src, const t_fraction &with){
	if (with.ptr == NULL){
		t_fraction::error = t_fraction::ERR_NULL;
		return 0;
	}
	return	(with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1)
			>=
				(with.ptr->under < 0 ? -with.ptr->under : with.ptr->under) *
				src)
			? 1 : 0;
}

char operator <=(long int src, const t_fraction &with){
	if (with.ptr == NULL){
		t_fraction::error = t_fraction::ERR_NULL;
		return 0;
	}
	return	(with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1)
			<=
				(with.ptr->under < 0 ? -with.ptr->under : with.ptr->under) *
				src)
			? 1 : 0;
}

char operator ==(long int src, const t_fraction &with){
	if (with.ptr == NULL){
		t_fraction::error = t_fraction::ERR_NULL;
		return 0;
	}
	return	(with.ptr->on *
				(with.ptr->under > 0 ? 1 : -1)
			==
				(with.ptr->under < 0 ? -with.ptr->under : with.ptr->under) *
				src)
			? 1 : 0;
}
/*--------------------------------------------------------------*/
std::ofstream &operator <<(std::ofstream &out, const t_fraction &from){

	if (from.ptr != NULL){
		if ((from.ptr->under > 0 ? 1 : -1) < 0)
			out << '-';
		out<< from.ptr->on << '/' << from.ptr->under;
	}
	return out;
}

std::ostream &operator <<(std::ostream &out, const t_fraction &from){

	if (from.ptr != NULL){
		char sign;
		if ((sign = from.ptr->under > 0 ? 1 : -1) < 0)
			out << '-';
		out<< from.ptr->on << '/' << sign * from.ptr->under;
	}
	return out;
}
