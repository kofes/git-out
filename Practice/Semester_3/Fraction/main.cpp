#include "fraction.hpp"

t_fraction *Gauss(t_fraction **A, t_fraction *F, unsigned int COUNT, t_fraction *ans){

	t_fraction Q[COUNT][COUNT + 1];

	for (unsigned int i = 0; i < COUNT; ++i){
		for (unsigned int j = 0; j < COUNT; ++j)
			Q[i][j] = A[i][j];
		Q[i][COUNT] = F[i];
	}

	for (unsigned int i = 0; i < COUNT; ++i){
		for (unsigned int j = 0; j < COUNT; ++j)
			std::cout << Q[i][j] << " ";
		std::cout << Q[i][COUNT] << std::endl;
	}

	unsigned int column[COUNT], row[COUNT];

	for (unsigned int i = 0; i < COUNT; ++i)
		column[i] = row[i] = i;

	t_fraction max;
	for (unsigned int k = 0; k < COUNT - 1; ++k){

		max = Q[row[k]][column[k]];
		unsigned int inx_row = k, inx_column = k;

		for (unsigned int i = k; i < COUNT; ++i)
			for (unsigned int j = k; j < COUNT; ++j){
				if ((max * max.sign()) - (Q[row[i]][column[j]] * Q[row[i]][column[j]].sign()) < 0){
					max = Q[row[i]][column[j]];
					inx_row = i;
					inx_column = j;
				}
			}

		if (max == 0){
			std::cout << "ERROR 0" << std::endl;
			t_fraction *result;
			return result;
		}

		unsigned int tmp = column[k];
		column[k] = column[inx_column];
		column[inx_column] = tmp;

		tmp = row[k];
		row[k] = row[inx_row];
		row[inx_row] = tmp;

		for (unsigned int i = k + 1; i < COUNT; ++ i){
			for (unsigned int j = k + 1; j < COUNT; ++j)
				Q[row[i]][column[j]] = Q[row[i]][column[j]] - (Q[row[i]][column[k]] * Q[row[k]][column[j]]) / Q[row[k]][column[k]];
			Q[row[i]][COUNT] = Q[row[i]][COUNT] - (Q[row[i]][column[k]] * Q[row[k]][COUNT]) / Q[row[k]][column[k]];
		}

		for (unsigned int i = k + 1; i < COUNT; ++i)
			Q[row[i]][column[k]] = 0;

			std::cout << "--------------" << std::endl;
			for (unsigned int i = 0; i < COUNT; ++i){
				for (unsigned int j = 0; j < COUNT; ++j)
					std::cout << Q[i][j] << " ";
				std::cout << Q[i][COUNT] << std::endl;
			}


	}
	std::cout << "---------" << std::endl;
	for (unsigned int i = 0; i < COUNT; ++i){
		for (unsigned int j = 0; j < COUNT; ++j)
			std::cout << Q[i][j] << " ";
		std::cout << Q[i][COUNT] << std::endl;
	}


	ans[column[COUNT - 1]] = Q[row[COUNT - 1]][COUNT]/Q[row[COUNT - 1]][column[COUNT - 1]];

	for (long int k = COUNT - 2; k >= 0; --k){
		t_fraction SIGMA = Q[row[k]][COUNT];
		for (unsigned int i = k + 1; i < COUNT; ++i)
			SIGMA = SIGMA - (Q[row[k]][column[i]] * ans[column[i]]);
		ans[column[k]] = SIGMA / Q[row[k]][column[k]];
	}

	t_fraction *result = new t_fraction[COUNT]();

	for (unsigned int i = 0; i < COUNT; ++i){
		result[i] = - F[i];
		for (unsigned int j = 0; j < COUNT; ++j)
			result[i] = result[i] + A[i][j] * ans[j];
	}
	return result;
}

int Gauss_init(){
	unsigned int count = 10;
	t_fraction **A, *f, *ans, *R;
	ans	= new t_fraction[count];
	f		= new t_fraction[count];
	A		= new t_fraction*[count];
	for (unsigned int i = 0; i < count; ++i)
		A[i] = new t_fraction[count];

	srand(time(NULL));

	for (unsigned int i = 0; i < count; ++i){
		f[i] = rand();
		for (unsigned int j = 0; j < count; ++j)
			A[i][j] = rand();
	}

	std::cout << "Matrix:" << std::endl;
	for (unsigned int i = 0; i < count; ++i){
		for (unsigned int j = 0; j < count; ++j)
			std::cout << A[i][j] << " ";
		std::cout << f[i] << std::endl;
	}

	R = Gauss(A, f, count, ans);

	std::cout << "ANS:" << std::endl;
	for (unsigned int i = 0; i < count; ++i)
		std::cout << "X[" << i + 1 << "] = " << ans[i] << std::endl;

	std::cout << "R:" << std::endl;
	for (unsigned int i = 0; i < count; ++i)
		std::cout << R[i] << std::endl;
	delete[] R;
	for (unsigned int i = 0; i < count; ++i)
		delete[] A[i];
	delete[] A;
	return 0;
}

using namespace std;

int test(){
	t_fraction p1(1, -20), p2, p3;
	cout << "1:         " << p1 << endl;
	p2 = p1;
	cout << "2 = 1:     " << p2 << endl;
	p3 = p2 + p1;
	cout << "3 = 2 + 1: " << p3 << endl;
	cout << "1:         " << p1 << endl;
	p3 = p3 - p1;
	cout << "3 = 3 - 1: " << p3 << endl;
	p3 = p2 * p1;
	cout << "3 = 2 * 1: " << p3 << endl;
	p3 = p2 / p1;
	cout << "3 = 2 / 1: " << p3 << endl;
	cout << "3 > 2 ?    " << ((p3 > p2) ? "TRUE" : "FALSE") << endl;
	cout << "2 == 1 ?   " << ((p2 == p1) ? "TRUE" : "FALSE") << endl;
	cout << "3 == 1 ?   " << ((p3 == p1) ? "TRUE" : "FALSE") << endl;
	cout << "3 > -2 ?   " << ((p3 > -2) ? "TRUE" : "FALSE") << endl;
	p3 = 2;
	cout << "-3:        " << -p3 << endl;
	p3 = p3 * 2;
	cout << "3 * 2:     " << p3 << endl;
	p3 = p3 - 1;
	cout << "3 - 1:     " << p3 << endl;
	p1 = p1 * 20;
	cout << "1 * 20:    " << p1 << endl;
	cout << "-3 > -2 ?  " << ((-p3 > -2) ? "TRUE" : "FALSE") << endl;
	p2 = -3;
	p1 = -2;
	cout << "-3 < -2 ?  " << ((p2 < p1) ? "TRUE" : "FALSE") << endl;
	return 0;
}

int main(){
	return Gauss_init();
}
