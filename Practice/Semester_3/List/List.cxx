#include <iostream>
#include "Book.hpp"

template <typename _elem_type>
class List{

public:

	enum List_error{
		ERROR_NORM,																			/*Нет данных об ошибке*/
		ERROR_ACCESS,																		/*Доступ к элементу/узлу/итератору закрыт*/
		ERROR_NULL,																			/*Невозможно выделить память под объект*/
		ERROR_DATA,																			/*Входные данные не корректны*/
	};

private:

	struct Node{
		Node *next, *last;
		_elem_type value;
	};

	// unsigned int size; - Добавить фичу с размером списка

	Node *head, *tail;
	Book *book;
	static List_error error;

	void m_sort(Node **head, unsigned int count);
	void sl_list(Node *left, Node *right, Node **head);
	void m_sort(bool (*comparator)(_elem_type &left, _elem_type &right), Node **head, unsigned int count);
	void sl_list(bool (*comparator)(_elem_type &left, _elem_type &right), Node *left, Node* right, Node **head);
public:

	List(): book(NULL), head(NULL), tail(NULL)	{};							/*Конструктор*/
	~List();																					/*Деструктор*/
	List(const List &src);																/*Конструктор копирования*/
	inline static List_error get_error()			{return error;}			/*Получение кода ошибки*/
	inline static void reset_error()					{error = ERROR_NORM;}	/*Сброс ошибки*/

	/*Класс итератора, для поэлементного обхода списка*/
	class iter{

	private:

		List *obj;
		Node *node;
		iter *next;
		friend class List;
	public:

		iter(): obj(NULL), node(NULL), next(NULL){};								/*Пустой конструктор*/
		iter(List *_obj, Node *_node);												/*Конструктор*/
		iter(const iter &src);															/*Конструктор копирования*/
		~iter();																				/*Деструктор*/
		iter operator ++();																/*Переход к следующему элементу*/
		iter operator --();																/*Переход к предыдущему элементу*/
		iter &operator =(const iter &src);											/*Присваивание итератору новый итератор*/
		_elem_type operator *();														/*Разыменование итератора - достаем элемент на четние*/
		void push(_elem_type src);														/*Запись значения по итератору*/
		iter push_after(_elem_type src);												/*Вставка значения в новый узел после текущего*/
		iter push_before(_elem_type src);											/*Вставка значения в новый узел перед текущим*/
		void pop();																			/*Удаление узла из списка*/
		void pop_after();																	/*Удаление узла после текущего*/
		void pop_before();																/*Удаление узла перед текущего*/
		bool operator !=(const iter &src);											/*Костыль для сравнения с List.empty()*/
	};

	iter start();																			/*Получение итератора на первый элемент списка*/
	iter end();																				/*Получение итератора на последний элемент списка*/
	iter empty();																			/*Пустой итератор, или фиктивный элемент списка*/
	List &operator =(const List &src);												/*Присваивание списка*/
	List Sort();																			/*Сортировка списка*/
	List Sort(bool (*comparator)(_elem_type &left, _elem_type &right));	/*Сортировка списка с явной передачей компаратора*/
	List Get(bool (*condition)(_elem_type &src));								/*Получение списка элементов, для которых выполняется условие*/
};

template <typename _elem_type>
typename List<_elem_type>::List_error List<_elem_type>::error = List<_elem_type>::ERROR_NORM;

template <typename _elem_type>
List<_elem_type>::~List(){
	Node *node, *curr;
	iter *node_ITER, *curr_ITER;
	node = head;

	while (node != NULL){
		curr = node;
		node = node->next;
	 	node_ITER = (iter *)(book->get(curr));
		while (node_ITER != NULL){
			curr_ITER = node_ITER;
			node_ITER = node_ITER->next;
			curr_ITER->obj = NULL;
			curr_ITER->node = NULL;
			curr_ITER->next = NULL;
		}
		delete curr;
	}

	tail = head = NULL;
	delete book;
}
/*Реализация итераторов*/
template <typename _elem_type>
List<_elem_type>::iter::iter(List *_obj, Node *_node){
	obj = _obj;
	node = _node;
	if (node != NULL){
		iter *head = (iter *)(obj->book->get(node));
		this->next = head;
		obj->book->add(node, this);
	}
}

template <typename _elem_type>
List<_elem_type>::iter::iter(const iter &src){
	obj = src.obj;
	node = src.node;
	if (obj != NULL){
		iter *prev = (iter *)(obj->book->get(node));
		next = prev;
		obj->book->add(node, this);
	}
}

template <typename _elem_type>
List<_elem_type>::iter::~iter(){
	if (obj == NULL)
		return;

	if (node == NULL){
		obj = NULL;
		return;
	}

	iter *prev = (iter *)(obj->book->get(node));
	iter *curr = prev;

	while (curr != this){
		prev = curr;
		curr = curr->next;
	}

	if (curr == prev){
		curr = curr->next;
		if (curr != NULL)
			obj->book->add(node, curr);
		else
			obj->book->del(node);
	}else
		prev->next = curr->next;

	obj = NULL;
	node = NULL;
	next = NULL;
}

template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::iter::operator ++(){
	if ((obj == NULL)||(node == NULL)){
		List::error = List::ERROR_ACCESS;
		return iter();
	}

	iter *prev = (iter *)(obj->book->get(node));
	iter *curr = prev;

	while (curr != this){
		prev = curr;
		curr = curr->next;
	}

	if (curr == prev){
		curr = curr->next;
		if (curr != NULL)
			obj->book->add(node, curr);
		else
			obj->book->del(node);
	}else
		prev->next = curr->next;

	node = node->next;
	if (node != NULL){
		prev = (iter *)(obj->book->get(node));
		next = prev;
		obj->book->add(node, this);
	}
	return *this;
}

template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::iter::operator --(){
	if ((obj == NULL)||(node == NULL)){
		List::error = ERROR_ACCESS;
		return iter();
	}
	iter *prev = (iter *)(obj->book->get(node));
	iter *curr = prev;

	while (curr != this){
		prev = curr;
		curr = curr->next;
	}

	if (curr == prev){
		curr = curr->next;
		if (curr != NULL)
			obj->book->add(node, curr);
		else
			obj->book->del(node);
	}else
		prev->next = curr->next;

	node = node->last;
	if (node != NULL){
		prev = (iter *)(obj->book->get(node));
		next = prev;
		obj->book->add(node, this);
	}
	return *this;
}

template <typename _elem_type>
typename List<_elem_type>::iter &List<_elem_type>::iter::operator =(const List::iter &src){
	if ((obj == src.obj)&&(node == src.node))
		return *this;
	obj = src.obj;
	node = src.node;
	if (node != NULL){
		iter *prev = (iter *)(obj->book->get(node));
		next = prev;
		obj->book->add(node, this);
	}
	return *this;
}

template <typename _elem_type>
_elem_type List<_elem_type>::iter::operator *(){
	if ((obj == NULL)||(node == NULL)){
		List::error = List::ERROR_ACCESS;
		return _elem_type();
	}
	return node->value;
}

template <typename _elem_type>
void List<_elem_type>::iter::push(_elem_type src){
	if ((obj == NULL)||(node == NULL)){
		List::error = List::ERROR_ACCESS;
		return;
	}
	node->value = src;
}

template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::iter::push_after(_elem_type src){
	if ((obj == NULL)||(node == NULL)){
		List::error = List::ERROR_ACCESS;
		return iter();
	}
	Node *curr = new Node();
	if (curr == NULL){
		List:error = List::ERROR_NULL;
		return iter();
	}
	curr->last = node;
	curr->next = node->next;
	node->next = curr;
	if (curr->next != NULL)
		curr->next->last = curr;
	else
		obj->tail = curr;
	curr->value = src;
	return iter(obj, curr);
}

template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::iter::push_before(_elem_type src){
	if ((obj == NULL)||(node == NULL)){
		List::error = List::ERROR_ACCESS;
		return iter();
	}

	Node *curr = new Node();

	if (curr == NULL){
		List:error = List::ERROR_NULL;
		return iter();
	}

	curr->last = node->last;
	curr->next = node;
	node->last = curr;

	if (curr->last != NULL)
		curr->last->next = curr;
	else
		obj->head = curr;

	curr->value = src;

	return iter(obj, curr);
}

template <typename _elem_type>
void List<_elem_type>::iter::pop(){
	if ((obj == NULL)||(node == NULL)){
		List::error = List::ERROR_NULL;
		return;
	}

	if (node == obj->head)
		obj->head = node->next;

	if (node == obj->tail)
		obj->tail = node->last;

	if (node->next != NULL)
		node->next->last = node->last;

	if (node->last != NULL)
		node->last->next = node->next;

	iter *curr = (iter *)(obj->book->get(node));
	iter *prev;

	while (curr != NULL){
		prev = curr;
		curr = curr->next;
		prev->node = NULL;
		prev->next = NULL;
	}

	delete node;
}

template <typename _elem_type>
void List<_elem_type>::iter::pop_after(){
	if ((obj == NULL)||(node == NULL)||(node->next == NULL)){
		List::error = List::ERROR_NULL;
		return;
	}

	Node *after = node->next;

	node->next = after->next;

	if (after->next != NULL)
		after->next->last = node;

	if (after == obj->tail)
		obj->tail = node;

	iter *curr = (iter *)(obj->book->get(after));
	iter *prev;

	while (curr != NULL){
		prev = curr;
		curr = curr->next;
		prev->node = NULL;
		prev->next = NULL;
	}

	delete after;
}

template <typename _elem_type>
void List<_elem_type>::iter::pop_before(){
	if ((obj == NULL)||(node == NULL)||(node->next == NULL)){
		List::error = List::ERROR_NULL;
		return;
	}

	Node *before = node->last;

	node->last = before->last;

	if (before->last != NULL)
		before->last->next = node;

	if (before == obj->head)
		obj->head = node;

	iter *curr = (iter *)(obj->book->get(before));
	iter *prev;

	while (curr != NULL){
		prev = curr;
		curr = curr->next;
		prev->node = NULL;
		prev->next = NULL;
	}

	delete before;
}

/*Костыль для сравнения итераторов*/
template <typename _elem_type>
bool List<_elem_type>::iter::operator !=(const iter &src){
	if ((node == NULL)&&(src.node == NULL)&&(obj == src.obj))
		return false;
	if (node != src.node)
		return true;
	return false;
}
/*-----------------------------------------------*/
template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::start(){
	if (book == NULL)
		book = new Book();

	if (head == NULL){
		head = new Node();
		head->last = NULL;
		head->next = NULL;
		tail = head;
		head->value = _elem_type();
	}

	return iter(this, head);
}

template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::end(){
	if (book == NULL)
		book = new Book();

	if (tail == NULL){
		tail = new Node();
		tail->last = NULL;
		tail->next = NULL;
		head = tail;
		tail->value = _elem_type();
	}

	return iter(this, tail);
}

template <typename _elem_type>
typename List<_elem_type>::iter List<_elem_type>::empty(){
	iter result;
	result.obj = this;
	result.node = NULL;
	result.next = NULL;
	return result;
}

template <typename _elem_type>
List<_elem_type>::List(const List<_elem_type> &src){

	if (src.head == NULL){
		head = tail = NULL;
		book = NULL;
		return;
	}

	book = new Book();

	if (book == NULL){
		List::error = List::ERROR_NULL;
		return;
	}

	Node *curr, *node, *prev;
	prev = NULL;
	curr = src.head;

	while (curr != NULL){
		node = new Node();
		if (node == NULL){
			List::error = List::ERROR_NULL;
			continue;
		}

		node->last = prev;
		node->next = NULL;

		if (prev != NULL)
			prev->next = node;
		else
			head = node;

		node->value = _elem_type(curr->value);
		prev = node;

		curr = curr->next;
	}

	tail = node;
}

template <typename _elem_type>
List<_elem_type> &List<_elem_type>::operator =(const List<_elem_type> &src){

	if (head != NULL){
		Node *node, *curr;
		iter *node_ITER, *curr_ITER;
		node = head;

		while (node != NULL){
			curr = node;
			node = node->next;
		 	node_ITER = (iter *)(book->get(curr));
			while (node_ITER != NULL){
				curr_ITER = node_ITER;
				node_ITER = node_ITER->next;
				curr_ITER->obj = NULL;
				curr_ITER->node = NULL;
				curr_ITER->next = NULL;
			}
			delete curr;
		}

		tail = head = NULL;
		delete book;
		book = NULL;
	}

	if (book != NULL){
		delete book;
		book = NULL;
	}

	if (src.head == NULL)
		return *this;

	book = new Book();
	if (book == NULL){
		List::error = List::ERROR_NULL;
		return *this;
	}

	Node *curr, *node, *prev;
	prev = NULL;
	curr = src.head;

	while (curr != NULL){
		node = new Node();
		if (node == NULL){
			List::error = List::ERROR_NULL;
			continue;
		}

		node->last = prev;
		node->next = NULL;

		if (prev != NULL)
			prev->next = node;
		else
			head = node;
		node->value = curr->value;
		prev = node;
		curr = curr->next;
	}

	tail = node;

	return *this;
}

template <typename _elem_type>
List<_elem_type> List<_elem_type>::Sort(){
	if (head == NULL)
		return List();

	List result(*this);
	unsigned int count;
	Node *node;

	node = result.head;
	count = 0;

	while (node != NULL){
		node = node->next;
		++count;
	}

	m_sort(&(result.head), count);

	Node *curr;
	result.head->last = NULL;
	curr = result.head;

	while (curr->next != NULL){
		node = curr;
		curr = curr->next;
		curr->last = node;
	}

	result.tail = curr;

	return result;
}


template <typename _elem_type>
void List<_elem_type>::m_sort(Node **head, unsigned int count){
	if (head == NULL)
		return;

	Node *p1, *p2;

	p1 = *head;
	p2 = *head;

	if (count == 1){
		(*head)->next = NULL;
		return;
	}

	if (count == 2){
		p2 = p1->next;
		p2->next = NULL;
		*head = p1;

		if (p1->value > p2->value){
			p2->next = p1;
			p1->next = NULL;
			*head = p2;
		}

		return;
	}

	unsigned int mid;
	Node *node;

	mid = count / 2;
	for (unsigned int i = 0; i < mid; ++i){
		node = p2;
		p2 = p2->next;
 	}
	node->next = NULL;
	m_sort(&p1, mid);
	m_sort(&p2, count - mid);
	sl_list(p1, p2, head);
}

template <typename _elem_type>
void List<_elem_type>::sl_list(Node *left, Node *right, Node **head){
	if (left == NULL)
		*head = right;
	else
		if (right == NULL)
			*head = left;
		else{
			Node *p1, *p2;
			p1 = left;
			p2 = right;
			if (p1->value < p2->value){
				*head = p1;
				p1 = p1->next;
			}else{
				*head = p2;
				p2 = p2->next;
			}
			Node *s1;
			s1 = *head;
			while ((p1 != NULL)&&(p2 != NULL)){
				if (p1->value < p2->value){
					s1->next = p1;
					p1 = p1->next;
				}else{
					s1->next = p2;
					p2 = p2->next;
				}
				s1 = s1->next;
			}
			if (p1 != NULL)
				s1->next = p1;
			if (p2 != NULL)
				s1->next = p2;
		}
	left = NULL;
	right = NULL;
}
/*END OF SORT WITHOUT CALLBACKS*/
template <typename _elem_type>
List<_elem_type> List<_elem_type>::Sort(bool (*comparator)(_elem_type &left, _elem_type &right)){
	if (head == NULL)
		return List();

	List result(*this);
	unsigned int count;
	Node *node;

	node = result.head;
	count = 0;

	while (node != NULL){
		node = node->next;
		++count;
	}

	m_sort(comparator, &(result.head), count);

	Node *curr;
	result.head->last = NULL;
	curr = result.head;

	while (curr->next != NULL){
		node = curr;
		curr = curr->next;
		curr->last = node;
	}

	result.tail = curr;

	return result;
}


template <typename _elem_type>
void List<_elem_type>::m_sort(bool (*comparator)(_elem_type &left, _elem_type &right), Node **head, unsigned int count){
	if (head == NULL)
		return;

	Node *p1, *p2;

	p1 = *head;
	p2 = *head;

	if (count == 1){
		(*head)->next = NULL;
		return;
	}

	if (count == 2){
		p2 = p1->next;
		p2->next = NULL;
		*head = p1;

		if (comparator(p2->value, p1->value)){
			p2->next = p1;
			p1->next = NULL;
			*head = p2;
		}

		return;
	}

	unsigned int mid;
	Node *node;

	mid = count / 2;
	for (unsigned int i = 0; i < mid; ++i){
		node = p2;
		p2 = p2->next;
 	}
	node->next = NULL;
	m_sort(comparator, &p1, mid);
	m_sort(comparator, &p2, count - mid);
	sl_list(comparator, p1, p2, head);
}

template <typename _elem_type>
void List<_elem_type>::sl_list(bool (*comparator)(_elem_type &left, _elem_type &right), Node *left, Node *right, Node **head){
	if (left == NULL)
		*head = right;
	else
		if (right == NULL)
			*head = left;
		else{
			Node *p1, *p2;
			p1 = left;
			p2 = right;
			if (comparator(p1->value, p2->value)){
				*head = p1;
				p1 = p1->next;
			}else{
				*head = p2;
				p2 = p2->next;
			}
			Node *s1;
			s1 = *head;
			while ((p1 != NULL)&&(p2 != NULL)){
				if (comparator(p1->value, p2->value)){
					s1->next = p1;
					p1 = p1->next;
				}else{
					s1->next = p2;
					p2 = p2->next;
				}
				s1 = s1->next;
			}
			if (p1 != NULL)
				s1->next = p1;
			if (p2 != NULL)
				s1->next = p2;
		}
	left = NULL;
	right = NULL;
}
/*END OF SORTS*/
template <typename _elem_type>
List<_elem_type> List<_elem_type>::Get(bool (*condition)(_elem_type &src)){

	List result;

	Node *curr, *node, *prev;
	node = NULL;
	prev = NULL;
	curr = head;

	while (curr != NULL){
		if (!condition(curr->value)){
			curr = curr->next;
			continue;
		}

		node = new Node();

		if (node == NULL){
			List::error = List::ERROR_NULL;
			continue;
		}

		node->last = prev;
		node->next = NULL;

		if (prev != NULL)
			prev->next = node;
		else
			result.head = node;

		node->value = _elem_type(curr->value);
		prev = node;

		curr = curr->next;
	}

	if (result.book == NULL)
		result.book = new Book();

	result.tail = node;

	return result;
}
