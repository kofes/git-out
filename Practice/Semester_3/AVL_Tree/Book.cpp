#include "Book.hpp"

#include <cmath>

void del_node(_book_node *node){
	if (node == NULL)
		return;
	del_node(node->child[0]);
	del_node(node->child[1]);
	delete node;
	node = NULL;
}

Book::~Book(){
	del_node(root);
	root = NULL;
}

_book_node *lit_turn(_book_node *root, char inv){
	_book_node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1) % 2];
	node->child[(inv + 1) % 2] = root;
	root = node;
	return root;
}

_book_node *turn(_book_node *node, char inv){
	if (std::abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

_book_node *add_node(_book_node *node, void *_adress, void *_iter, char *info){
	char i;
	if (node == NULL){
		node = new _book_node;
		node->diff = 0;
		node->child[0] = node->child[1] = NULL;
		node->adress = _adress;
		node->iter = _iter;
		return node;
	}
	i = (_adress > node->adress);
	if (_adress == node->adress){
		*info = 1;
		node->iter = _iter;
		return node;
	}
	node->child[i] = add_node(node->child[i], _adress, _iter, info);
	node->diff += (1 - 2*i)*((*info + 1) % 2);
	node = turn(node, i);
	if ((!(*info))&&(!(node->diff)))
		*info = 1;
	return node;
}

void Book::add(void *_adress, void *_iter){
	char info = 0;
	root = add_node(root, _adress, _iter, &info);
}

void *Book::get(void *_adress){
	_book_node *node;
	char i;
	node = root;
	while (node != NULL)
		if (node->adress == _adress)
			return node->iter;
		else{
			i = (_adress >= node->adress);
			node = node->child[i];
		}
	return NULL;
}

_book_node *del_min(_book_node *node){
	if (node->child[0] == NULL)
		return node->child[1];
	node->child[0] = del_min(node->child[0]);
	return turn(node, 1);
}

_book_node *del_node(_book_node *node, void *_adress, char *info){
	char i;
	if (node == NULL){
		*info = 1;
		return NULL;
	}
	if (node->adress == _adress){
		_book_node *left = node->child[0], *right = node->child[1];
		char diff = node->diff;
		delete node;	//<---------------->//
		if (right != NULL){
			_book_node *min;

			min = right;
			while (min->child[0] != NULL)
				min = min->child[0];
			min->child[1] = del_min(right);
			min->child[0] = left;
			return turn(min, 0);
		}else
			return left;
	}
	i = (_adress >= node->adress);
	node->child[i] = del_node(node->child[i], _adress, info);
	node->diff += (2 * i - 1) * ((*info + 1) % 2);
	node = turn(node, (i + 1) % 2);
	if ((!(*info))&&(std::abs(node->diff) == 1))
		*info = 1;
	return node;
}

void Book::del(void *_adress){
	char info = 0;
	root = del_node(root, _adress, &info);
}

void out_node(std::ostream &out, _book_node *node){
	if (node == NULL)
		return;
	out_node(out, node->child[0]);
	out << "adress: " << node->adress << ";\titer: " << node->iter << ";" << std::endl;
	out_node(out, node->child[1]);
	return;
}

std::ostream &operator <<(std::ostream &out, Book &src){
	out_node(out, src.root);
	return out;
}
