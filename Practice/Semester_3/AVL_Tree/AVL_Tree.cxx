#include <iostream>
#include "Book.hpp"


#define AVL_TEMPLATE	   template <typename _elem_type, typename _key_type>

#define AVL_PARAMETRES _elem_type, _key_type

AVL_TEMPLATE
class AVL_Tree{
public:

	enum AVL_Tree_error{
		ERROR_NORM,																									/*Нет данных об ошибке*/
		ERROR_ACCESS,																								/*Доступ к элементу/узлу/итератору закрыт*/
		ERROR_NULL,																									/*Невозможно выделить память под объект*/
		ERROR_DATA,																									/*Входные данные не корректны*/
	};

private:

	//typedef char (*tCompare)(const _elem_type &, const _elem_type &);
	struct Node{
		struct Node *child[2], *head;
		_elem_type value;
		_key_type key;
		char diff;
	};

	//tCompare elem_cmp;

	Node *root;
	Book *book;
	static AVL_Tree_error error;
	unsigned int count;

	Node *copy_tree(const Node *src);
	void delete_tree(Node *src);
	Node *add_node(Node *node, _key_type key, _elem_type value, char *info, Node **result);
	Node *turn(Node *node, char inv);
	Node *lit_turn(Node *root, char inv);
	Node *del_node(Node *node, _key_type key, char *info);
	Node *del_min(Node *node);
	void get_node(Node *node, AVL_Tree *result, bool (*condition)(const _elem_type &src));
	void up_tree(Node *curr);

public:
	AVL_Tree(): root(NULL), book(NULL), count(0) {}//, elem_cmp(NULL) {};													/*Конструктор*/
	AVL_Tree(const AVL_Tree &src);																			/*Конструктор копирования*/
	//AVL_Tree(tCompare cmp) : AVL_Tree(), elem_cmp(cmp) {};
	~AVL_Tree();																									/*Деструктор*/
	AVL_Tree &operator =(const AVL_Tree &src);															/*Присваивание дерева*/
	inline static AVL_Tree_error get_error()							{return error;}				/*Получение кода ошибки*/
	inline static AVL_Tree_error reset_error()						{error = ERROR_NORM;}		/*Сброс ошибки*/

	/*Класс итератора, для поэлементного обхода дерева*/
	class iter{

	private:

		AVL_Tree *obj;
		Node *node;
		iter *next;
		friend class AVL_Tree;

	public:

		iter(): obj(NULL), node(NULL), next(NULL)	{};													/*Пустой конструктор*/
		iter(AVL_Tree *_obj, Node *_node);																	/*Конструктор*/
		iter(const iter &src);																					/*Конструктор копирования*/
		~iter();																										/*Деструктор*/
		iter &operator =(const iter &src);																	/*Присваивание итератору новоый итератор*/
		iter operator ++();																						/*Переход к следующему элементу*/
		iter operator --();																						/*Переход к предыдущему элементу*/
		_elem_type operator *();																				/*Разыменование итератора - достаем элемент на чтение*/
		void push(_elem_type src);																				/*Вставка значения по итератору*/
		void pop();																									/*Удаление узла из дерева*/
		bool operator !=(const iter &src);

	};

	iter min();																										/*Получение итератора на минимальный элемент дерева*/
	iter max();																										/*Получение итератора на максимальный элемент дерева*/
	iter empty();																									/*Пустой итератор или фиктивный элемент списка*/
	iter push(const _key_type key, const _elem_type src);												/*Вставка элемента в дерево по ключу*/
	iter search(const _key_type key);																		/*Поиск элемента в дереве по ключу*/
	void pop(const _key_type key);																			/*Удаление элемента в дереве по ключу*/
	AVL_Tree Get(bool (*condition)(const _elem_type &src));											/*Получение поддерева, для которого выполняется условие*/
	inline unsigned int size()							{return count;}									/*Получение количества элементов*/
};

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::Node *AVL_Tree<AVL_PARAMETRES>::copy_tree(const AVL_Tree<AVL_PARAMETRES>::Node *src){
	if (src == NULL)
		return NULL;

	Node *result = new Node();
	if (result == NULL)
		return NULL;

	result->head = NULL;
	++count;

	result->child[0] = copy_tree(src->child[0]);
	result->child[1] = copy_tree(src->child[1]);

	if (result->child[0] != NULL)
	 	result->child[0]->head = result;
	if (result->child[1] != NULL)
		result->child[1]->head = result;

	result->diff = src->diff;
	result->key = _key_type(src->key);
	result->value = _elem_type(src->value);

	return result;
}

AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES>::AVL_Tree(const AVL_Tree<AVL_PARAMETRES> &src){

	if (src.root == NULL){
		root = NULL;
		book = NULL;
		count = 0;
		return;
	}

	root = copy_tree(src.root);
	book = NULL;
}

AVL_TEMPLATE
void AVL_Tree<AVL_PARAMETRES>::delete_tree(AVL_Tree<AVL_PARAMETRES>::Node *src){
	if (src == NULL)
		return;

	delete_tree(src->child[0]);
	delete_tree(src->child[1]);

	if (book != NULL){
		iter *next = (iter *)(book->get(src));
		iter *curr;
		while (next != NULL){
			curr = next;
			next = curr->next;
			curr->obj = NULL;
			curr->next = NULL;
			curr->node = NULL;
		}
		book->del(src);
	}

	delete src;
}

AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES>::~AVL_Tree(){
	delete_tree(root);
	root = NULL;
	delete book;
	count = 0;
}

/*Реализация итераторов*/
AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES>::iter::iter(AVL_Tree<AVL_PARAMETRES> *_obj, AVL_Tree<AVL_PARAMETRES>::Node *_node){
	obj = _obj;
	node = _node;
	next = NULL;

	if ((obj == NULL)||(node == NULL)){
		node = NULL;
		next = NULL;
		return;
	}

	if (obj->book == NULL)
		obj->book = new Book;

	iter *head = (iter *)(obj->book->get(node));

	if (head == NULL)
		obj->book->add(node, this);
	else{
		next = head->next;
		head->next = this;
	}
}

AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES>::iter::iter(const AVL_Tree<AVL_PARAMETRES>::iter &src){
	obj = src.obj;
	node = src.node;

	if ((obj == NULL)||(node == NULL)){
		node = NULL;
		next = NULL;
		return;
	}

	if (obj->book == NULL)
		obj->book = new Book;

	iter *head = (iter *)(obj->book->get(node));

	next = head->next;
	head->next = this;
}

AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES>::iter::~iter(){
	if ((obj == NULL)||(node == NULL)){
		node = NULL;
		obj = NULL;
		next = NULL;
		return;
	}

	iter *prev = (iter *)(obj->book->get(node));
	iter *curr = prev;

	while (curr != this){
		prev = curr;
		curr = curr->next;
	}

	if (curr == prev){
		curr = curr->next;

		if (curr != NULL)
			obj->book->add(node, curr);
		else
			obj->book->del(node);
	}else
		prev->next = curr->next;

	obj  = NULL;
	node = NULL;
	next = NULL;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter &AVL_Tree<AVL_PARAMETRES>::iter::operator =(const AVL_Tree<AVL_PARAMETRES>::iter &src){
	if (node == src.node)
		return *this;

	if ((obj != NULL)&&(node != NULL)){
		iter *prev = (iter *)(obj->book->get(node));
		iter *curr = prev;

		while (curr != this){
			prev = curr;
			curr = curr->next;
		}

		if (curr == prev){
			curr = curr->next;

			if (curr->next != NULL)
				obj->book->add(node, curr);
			else
				obj->book->del(node);
		}else
			prev->next = curr->next;
	}

	obj  = NULL;
	node = NULL;
	next = NULL;

	obj = src.obj;
	node = src.node;

	if ((obj == NULL)||(node == NULL)){
		node = NULL;
		return *this;
	}

	iter *head = (iter *)(obj->book->get(node));

	next = head->next;
	head->next = this;

	return *this;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::iter::operator ++(){
	if ((obj == NULL)||(node == NULL))
		return iter();

	iter *prev = (iter *)(obj->book->get(node));
	iter *curr = prev;

	while (curr != this){
		prev = curr;
		curr = curr->next;
	}

	if (curr == prev){
		curr = curr->next;

		if (curr != NULL)
			obj->book->add(node, curr);
		else
			obj->book->del(node);
	}else
		prev->next = curr->next;

	next = NULL;

	if (node->child[1] != NULL){
		node = node->child[1];
		while (node->child[0] != NULL)
			node = node->child[0];
	}
		else{
			_key_type *copy_key = &(node->key);
			do{
				node = node->head;
			}while((node != NULL)&&(node->key < *copy_key));
		}

	if (node != NULL){
		curr = (iter *)(obj->book->get(node));

		if (curr == NULL){
			obj->book->add(node, this);
			next = NULL;
		}else{
			next = curr->next;
			curr->next = this;
		}
	}
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::iter::operator --(){
	if ((obj == NULL)||(node == NULL))
		return iter();

	iter *prev = (iter *)(obj->book->get(node));
	iter *curr = prev;

	while (curr != this){
		prev = curr;
		curr = curr->next;
	}

	if (curr == prev){
		curr = curr->next;

		if (curr != NULL)
			obj->book->add(node, curr);
		else
			obj->book->del(node);
	}else
		prev->next = curr->next;

	next = NULL;

	if (node->child[0] != NULL){
		node = node->child[0];
		while (node->child[1] != NULL)
			node = node->child[1];
	}
		else{
			_key_type *copy_key = &(node->key);
			do{
				node = node->head;
			}while((node != NULL)&&(node->key > *copy_key));
		}

	if (node != NULL){
		curr = (iter *)(obj->book->get(node));

		if (curr == NULL){
			obj->book->add(node, this);
			next = NULL;
		}else{
			next = curr->next;
			curr->next = this;
		}
	}
}

AVL_TEMPLATE
_elem_type AVL_Tree<AVL_PARAMETRES>::iter::operator *(){
	if ((obj == NULL)||(node == NULL))
		return _elem_type();
	return node->value;
}

AVL_TEMPLATE
void AVL_Tree<AVL_PARAMETRES>::iter::push(_elem_type src){
	if ((obj == NULL)||(node == NULL))
		return;
	node->value = _elem_type(src);
}

AVL_TEMPLATE
void AVL_Tree<AVL_PARAMETRES>::up_tree(AVL_Tree<AVL_PARAMETRES>::Node *curr){
	if (curr->child[0] != NULL)
		curr->child[0]->head = curr;
	if (curr->child[1] != NULL)
		curr->child[1]->head = curr;


}

AVL_TEMPLATE
void AVL_Tree<AVL_PARAMETRES>::iter::pop(){
	if ((obj == NULL)||(node == NULL)){
		node = NULL;
		next = NULL;
		return;
	}

	obj->pop(node->key);

	node = NULL;
	next = NULL;
}
/*------------------------------*/

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::min(){
	if (root == NULL)
		return iter();

	Node *curr = root;

	while (curr->child[0] != NULL)
		curr = curr->child[0];

	iter result(this, curr);

	return result;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::max(){
	if (root == NULL)
		return iter();

	Node *curr = root;

	while (curr->child[1] != NULL)
		curr = curr->child[1];

	iter result(this, curr);

	return result;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::empty(){
	return iter(this, NULL);
}

/*ADDITION ELEMENT STARTS*/

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::Node *AVL_Tree<AVL_PARAMETRES>::lit_turn(AVL_Tree<AVL_PARAMETRES>::Node *root, char inv){
	Node *node = root->child[inv];

	root->child[inv] = node->child[(inv + 1) % 2];
	node->child[(inv + 1) % 2] = root;
	root = node;

	return root;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::Node *AVL_Tree<AVL_PARAMETRES>::turn(AVL_Tree<AVL_PARAMETRES>::Node *node, char inv){
	if (((node->diff > 0) ? (node->diff) : - (node->diff)) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
			node->child[inv]->head = node;
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::Node *AVL_Tree<AVL_PARAMETRES>::add_node(AVL_Tree<AVL_PARAMETRES>::Node *node, _key_type key, _elem_type value, char *info, AVL_Tree<AVL_PARAMETRES>::Node **result){
	char i;

	if (node == NULL){
		node = new Node;

		*result = node;

		if (node == NULL){
			*info = 1;
			return node;
		}

		node->diff = 0;
		node->head = NULL;
		node->child[0] = node->child[1] = NULL;
		node->key = _key_type(key);
		node->value = _elem_type(value);

		return node;
	}

	i = (key > node->key);

	if (key == node->key){
		*info = 1;
		*result = node;
		node->value = _elem_type(value);

		return node;
	}

	i = (i > 0);

	node->child[i] = add_node(node->child[i], key, value, info, result);

	node->diff += (1 - 2 * i) * ((*info + 1) % 2);

	node = turn(node, i);

	if (node->child[i] != NULL)
		node->child[i]->head = node;

	node->head = NULL;

	if((!(*info))&&(!(node->diff)))
		*info = 1;

	return node;
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::push(const _key_type key, const _elem_type src){
	char info = 0;
	Node *curr;

	root = add_node(root, key, src, &info, &curr);

	if (curr != NULL) ++count;

	return iter(this, curr);
}
/*ADDITION ELEMENT ENDS*/

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::iter AVL_Tree<AVL_PARAMETRES>::search(const _key_type key){
	Node *curr;
	char i;
	curr = root;

	while (curr != NULL){
		i = (key > curr->key);

		if (key == curr->key) return iter(this, curr);

		i = (i > 0);
		curr = curr->child[i];
	}

	return iter();
}

/*DELETION ELEMENT STARTS*/
AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::Node *AVL_Tree<AVL_PARAMETRES>::del_min(AVL_Tree<AVL_PARAMETRES>::Node *node){
	if (node->child[0] == NULL)
		return node->child[1];
	node->child[0] = del_min(node->child[0]);
	return turn(node, 1);
}

AVL_TEMPLATE
typename AVL_Tree<AVL_PARAMETRES>::Node *AVL_Tree<AVL_PARAMETRES>::del_node(AVL_Tree<AVL_PARAMETRES>::Node *node, _key_type key, char *info){
	char i;

	if (node == NULL){
		*info = 1;
		return NULL;
	}

	if (node->key == key){
		Node *left = node->child[0], *right = node->child[1];
		char diff = node->diff;

/**/
		if (book != NULL){
			iter *prev = (iter *)(book->get(node));
			iter *curr = prev;
			while (curr != NULL){
				prev = curr;
				curr = curr->next;
				prev->obj = NULL;
				prev->node = NULL;
				prev->next = NULL;
			}
			book->del(node);
		}
/**/
		delete node;

		if (left != NULL)
			left->head = NULL;
		if (right != NULL)
			right->head = NULL;

		--count;

		if (right != NULL){
			Node *min = right;

			while (min->child[0] != NULL)
				min = min->child[0];

			min->head = NULL;

			min->child[1] = del_min(right);
			min->child[0] = left;

			if (min->child[0] != NULL)
				min->child[0]->head = node;

			if (min->child[1] != NULL)
				min->child[1]->head = node;

			return turn(min, 0);
		}else
			return left;
	}

	i = ((key > node->key)||(key == node->key));

	node->child[i] = del_node(node->child[i], key, info);
	node->diff += (2 * i - 1) * ((*info + 1) % 2);
	node = turn(node, (i + 1) % 2);

	if (node->child[i] != NULL)
		node->child[i]->head = node;

	node->head = NULL;

	if ((!(*info))&&
		(((node->diff > 0) ? node->diff : - (node->diff)) == 1))
		*info = 1;
	return node;
}

AVL_TEMPLATE
void AVL_Tree<AVL_PARAMETRES>::pop(const _key_type key){
	char info = 0;

	root = del_node(root, key, &info);
	if (root != NULL)
		root->head = NULL;
}
/*DELETION ELEMENT ENDS*/

AVL_TEMPLATE
void AVL_Tree<AVL_PARAMETRES>::get_node(AVL_Tree<AVL_PARAMETRES>::Node *src, AVL_Tree<AVL_PARAMETRES> *result, bool (*condition)(const _elem_type &src)){
	if (src == NULL)
		return;

	if (condition(src->key))
		result->push(src->key, src->value);

	get_node(src->child[0], result, condition);
	get_node(src->child[1], result, condition);
}

AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES> AVL_Tree<AVL_PARAMETRES>::Get(bool (*condition)(const _elem_type &src)){
	AVL_Tree result;

	get_node(root, &result, condition);

	return result;
}

/**/

AVL_TEMPLATE
AVL_Tree<AVL_PARAMETRES> &AVL_Tree<AVL_PARAMETRES>::operator =(const AVL_Tree<AVL_PARAMETRES> &src){
	if (root == src.root)
		return *this;

	delete_tree(root);
	delete book;

	root = NULL;
	book = NULL;
	count = 0;

	if (src.root == NULL)
		return *this;

	root = copy_tree(src.root);
}

AVL_TEMPLATE
bool AVL_Tree<AVL_PARAMETRES>::iter::operator !=(const AVL_Tree<AVL_PARAMETRES>::iter &src){
	if ((node == NULL)&&(src.node == NULL)&&(obj == src.obj))
		return false;
	if (node != src.node)
		return true;
	return false;
}
