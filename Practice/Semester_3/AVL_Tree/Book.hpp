#ifndef __INCLUDE_BOOK_H

#include <iostream>
#include <iomanip>

typedef struct _book_node{
	_book_node *child[2];
	void *adress;
	void *iter;
	char diff;
}_book_node;

class Book{
private:
	_book_node *root;
public:
	Book(): root(NULL) {};
	~Book();
	void add(void *_adress, void *_iter);
	void *get(void *_adress);
	void del(void *_adress);
	friend std::ostream &operator <<(std::ostream &out, Book &src);
};

#endif
