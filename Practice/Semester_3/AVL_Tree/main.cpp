#include "AVL_Tree.cxx"
#include <iostream>

using namespace std;

/*TEST 1:*/
int test_1(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;
	int value;

	iter = tree.push(10, 10);
	value = *iter;
	cout << "TEST  1: " << ((value - 10) ? "BAD" : "OK") << endl;
}
/*-------*/

/*TEST 2:*/
int test_2(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	iter = tree.push(10, 10);
	tree.push(5, 5);

	cout << "TEST  2: " << ((*iter - 10) ? "BAD" : "OK") << endl;

	return 0;
}
/*-------*/

/*TEST 3:*/
int test_3(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	tree.push(45, 45);
	tree.push(10, 10);
	tree.push(5, 5);
	tree.push(30, 30);
	tree.push(25, 25);
	tree.push(40, 40);
	tree.push(35, 35);
	tree.push(15, 15);

	iter = tree.max();

	cout << "TEST  3: " << ((*iter - 45) ? "BAD" : "OK") << endl;

	return 0;
}
/*-------*/

/*TEST 4:*/
int test_4(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	tree.push(45, 45);
	tree.push(10, 10);
	tree.push(5, 5);
	tree.push(30, 30);
	tree.push(25, 25);
	tree.push(40, 40);
	tree.push(35, 35);
	tree.push(15, 15);

	iter = tree.min();

	cout << "TEST  4: " << ((*iter - 5) ? "BAD" : "OK") << endl;

	return 0;
}
/*-------*/

/*TEST 5:*/
int test_5(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	tree.push(45, 45);
	tree.push(10, 10);
	tree.push(5, 5);
	tree.push(30, 30);
	tree.push(25, 25);
	tree.push(40, 40);
	tree.push(35, 35);
	tree.push(15, 15);

	iter = tree.min();	//5
	++iter;					//10
	++iter;					//15
	++iter;					//20
	cout << "TEST  5: " << ((*iter - 20) ? "BAD" : "OK") << endl;
	return 0;
}
/*-------*/

/*TEST 6:*/
int test_6(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(10, 10);
	tree.push(20, 20);
	tree.push(0, 0);
	tree.push(5, 5);
	tree.push(15, 15);
	tree.push(25, 25);
	tree.push(-5, -5);
	tree.push(30, 30);
	tree.push(35, 35);
	tree.push(40, 40);
	tree.push(45, 45);
	iter = tree.max();	//45
	--iter;					//40
	--iter;					//35
	--iter;					//30
	--iter;					//25
	--iter;					//20
	--iter;					//15
	--iter;					//10
	cout << "TEST  6: " << ((*iter - 10) ? "BAD" : "OK") << endl;

	return 0;
}
/*-------*/

/*TEST 7:*/
int test_7(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	tree.push(45, 45);
	tree.push(10, 10);
	tree.push(5, 5);
	tree.push(30, 30);
	tree.push(25, 25);
	tree.push(40, 40);
	tree.push(35, 35);
	tree.push(15, 15);

	iter = tree.search(25);
	cout << "TEST  7: " << ((*iter - 25) ? "BAD" : "OK") << endl;
	return 0;
}
/*-------*/

/*TEST 8:*/
int test_8(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	tree.push(45, 45);
	tree.push(10, 10);
	tree.push(5, 5);
	tree.push(30, 30);
	tree.push(25, 25);
	tree.push(40, 40);
	tree.push(35, 35);
	tree.push(15, 15);

	iter = tree.search(25);
	iter.push(-50);
	cout << "TEST  8: " << ((*iter + 50) ? "BAD" : "OK") << endl;
	return 0;
}
/*-------*/

/*TEST 9:*/
int test_9(){
	AVL_Tree<int, int> tree;
	AVL_Tree<int, int>::iter iter;

	tree.push(20, 20);
	tree.push(30, 30);
	tree.push(10, 10);
	tree.push(25, 25);
	tree.push(5, 5);
	tree.push(40, 40);
	tree.push(45, 45);
	tree.push(35, 35);
	tree.push(30, 30);
	tree.pop(10);

	iter = tree.search(10);

	cout << "TEST  9: " << ((*iter) ? "BAD" : "OK") << endl;
	return 0;
}
/*-------*/

/*TEST 10:*/
int test_10(){
	AVL_Tree<int, int> tree;
	int count = 1;

	tree.push(10, 10);
	tree.push(20, 20);

	AVL_Tree<int, int> res(tree);

	tree.push(10, 5);

	cout << "TEST 10: ";

	for (AVL_Tree<int, int>::iter i = res.min(); i != res.empty(); ++count, ++i)
		if (*i - count * 10){
			cout << "BAD" << endl;
			return 0;
		}

	cout << "OK" << endl;

	return 0;
}
/*-------*/

/*TEST 11:*/
int test_11(){
	int count = 0;
	AVL_Tree<int, int> tree;

	tree.push(20, 20);
	tree.push(30, 30);
	tree.push(10, 10);
	tree.push(25, 25);
	tree.push(5, 5);
	tree.push(40, 40);
	tree.push(45, 45);
	tree.push(15, 15);
	tree.push(35, 35);
	tree.push(30, 30);

	AVL_Tree<int, int> res = tree;

	tree.push(10, 0);
	count = 1;

	cout << "TEST 11: ";
	for (AVL_Tree<int, int>::iter i = res.min(); i != res.empty(); ++count, ++i)
		if (*i - count * 5){
			cout << "BAD" << endl;
			return 0;
		}

	cout << "OK" << endl;

	return 0;
}
/*-------*/

bool cnd(const int &src){
	if (src % 10 == 0)
		return true;
	return false;
}

/*TEST 12:*/
int test_12(){
	int count = 0;
	AVL_Tree<int, int> tree;

	tree.push(20, 20);
	tree.push(30, 30);
	tree.push(10, 10);
	tree.push(25, 25);
	tree.push(5, 5);
	tree.push(40, 40);
	tree.push(45, 45);
	tree.push(15, 15);
	tree.push(35, 35);

	AVL_Tree<int, int> res = tree.Get(cnd);

	tree.push(10, 0);
	count = 1;

	cout << "TEST 12: ";
	for (AVL_Tree<int, int>::iter i = res.min(); i != res.empty(); ++count, ++i)
		if (*i % 10 != 0){
			cout << "BAD" << endl;
			return 0;
		}

	cout << "OK" << endl;

	return 0;
}
/*-------*/

/*TEST 13:*/
int test_13(){
	int count = 0;
	AVL_Tree<int, int> tree;

	tree.push(20, 20);
	tree.push(30, 30);
	tree.push(10, 10);
	tree.push(25, 25);
	tree.push(5, 5);
	tree.push(40, 40);
	tree.push(45, 45);
	tree.push(15, 15);
	tree.push(35, 35);

	AVL_Tree<int, int>::iter res = tree.search(45);

	res.pop();
	tree.push(10, 0);
	count = 1;

	cout << "TEST 13: ";
	for (AVL_Tree<int, int>::iter i = tree.min(); i != tree.empty(); ++count, ++i)
		if (*i == 45){
			cout << "BAD" << endl;
			return 0;
		}

	cout << "OK" << endl;

	return 0;
}
/*-------*/

/*TEST 14:*/
int test_14(){
	int count = 0;
	AVL_Tree<int, int> tree;

	tree.push(20, 20);
	tree.push(30, 30);
	tree.push(10, 10);
	tree.push(25, 25);
	tree.push(5, 5);
	tree.push(40, 40);
	tree.push(45, 45);
	tree.push(15, 15);
	tree.push(35, 35);

	AVL_Tree<int, int>::iter res;

	res = tree.min();				//5
	++res;							//10
	++res;							//15
	++res;							//20
	tree.push(11, 11);
	--res;							//15
	--res;							//11
	cout << "TEST 14: ";
	if (*res - 11){
		cout << "BAD" << endl;
		return 1;
	}
	cout << "OK" << endl;

	return 0;
}
/*-------*/

int main(){
	test_1();
	test_2();
	test_3();
	test_4();
	test_5();
	test_6();
	test_7();
	test_8();
	test_9();
	test_10();
	test_11();
	test_12();
	test_13();
	test_14();
	return 0;
}
