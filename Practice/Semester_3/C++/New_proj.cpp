#include <cstdio>
class class1{
	public:
		void test1(){
			printf("1: test1\n");
		}
		virtual void test2(){
			printf("1: test2\n");
		}
};
class class2: public class1{
	public:
		void test1(){
			printf("2: test1\n");
		}
		void test2(){
			printf("2: test2\n");
		}
};
/*Абстрактный класс*/
class class0{
	private:
		int a, b;
	public:
		virtual int add() = 0;	/*Абстрактный метод*/
		virtual int mul() = 0;	/*Абстрактный метод*/
};
/*c0->c1->c2; c2 *p1; p1->add();*/
/*
**	Переопределение (перекрытие) методов начального класса
**	методами класса потомка
**
int main(int argc, char const *argv[]){
	class2 q;
	q.test1();
	q.class1::test1();
	return 0;
}
*/
int main(int argc, char const *argv[]){
	class1 c1; class2 c2;
	class1 *p1 = &c1;
	class2 *p2 = &c2;
	p1->test1();
	p2->test1();
	p1 = p2;
	p1->test1(); //This is fucking amazing.
	((class2 *)p1)->test1();	//Так обычно не делают
	p1 = &c1; p2 = &c2;
	p1->test2();
	p2->test2();
	p1 = p2;
	p1->test2(); //FUCK YOU! virtual function -> function (method) in class
	return 0;
}