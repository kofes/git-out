#ifndef __INCLUDE_QUEUE_
#define __INCLUDE_QUEUE_
#include <cstdio>
#include <cstdlib>
#define ERR_NORM (0)
#define ERR_NULL (1)

typedef void *t_eue_elem;
typedef int (*function_del_elem)(void *);
typedef int (*function_new_elem)(void *);
class Queue{
	protected:
		void *head, *tail;
		size_t height;
		function_new_elem new_elem;
		function_del_elem del_elem;
	public:
		Queue(function_new_elem, function_del_elem);
		~Queue();
		bool add_tail(t_eue_elem val);
		t_eue_elem get_head();
		t_eue_elem get_tail();
		bool del_head();
		size_t get_size();
};

#endif
/*__INCLUDE_QUEUE_*/