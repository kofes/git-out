/*	Объекты класса - параметры класса
**	Можно использовать все заголовочные файлы и типы Си есть в C++
**	Однако не рекомендуется, поэтому подключаем .h как <c...>
**	<ctdlib>
======================
	Выделение памяти:
	int *A = (int *)malloc(32*sizeof(int));
	int *B = new int[32]; //new - оператор, а не функция
	int *C = new int;
	free(A);
	delete C;
	delete[] B;
*/
#include <cstdio>
class t_class{
	int a,b,c;
public:
	void set(int _a, int _b);
	int get(void);
	void run(char _s);
	t_class();
};

t_class::t_class(){
	a = b = c = 0;
}

void t_class::set(int _a, int _b){
	/*this->*/a = _a;
	b = _b;
}
int t_class::get(void){	return c;}
void t_class::run(char _s){
	switch (_s){
		case '+': c = a + b; break;
		case '-': c = a - b; break;
		case '*': c = a * b; break;
		case '/':
			if (!b){
				c = a / b;
			} else{
				c = (a) == (0) ? 1 : 0;
			}
			break;
		case '<': c = a < b; break;
		case '>': c = a > b; break;
		case '=': c = a == b; break;
	}
}

int main(){
	t_class A;
	A.set(9, 5);
	A.run('+');
	printf("%i\n", A.get());
	return 0;
}