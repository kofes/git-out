#ifndef __INCLUDE_DEQUE_
#define __INCLUDE_DEQUE_
#include "queue.h"

class DQueue: public Queue{
	public:
		DQueue(function_new_elem new_elem, function_del_elem del_elem): Queue(new_elem, del_elem) {}
		bool add_head(t_eue_elem elem);
		bool del_tail();
};

#endif