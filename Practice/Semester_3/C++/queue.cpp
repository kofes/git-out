#include "queue.h"

typedef struct t_node{
	struct t_node *last, *next;
	t_eue_elem elem;
}t_node;
Queue::Queue(function_del_elem new_it, function_new_elem del_it){
	head = tail = NULL;
	new_elem = new_it;
	del_elem = del_it;
	height = 0;
}
Queue::~Queue(){
	t_node *curr, *node;
	curr = (t_node *)head;
	while (curr != NULL){
		node = curr;
		curr = curr->next;
		del_elem(node->elem);
		node->elem = NULL;
		delete node;
	}
	height = 0;
}
bool Queue::add_tail(t_eue_elem val){
	t_node *node = new t_node;
	node->next = NULL;
	new_elem(node->elem);
	node->elem = val;
	if ((t_node *)tail != NULL)
		((t_node *)tail)->next = node;
	node->last = (t_node *)tail;
	tail = node;
	if ((t_node *)head == NULL)
		head = node;
	++height;
	return 0;
}
t_eue_elem Queue::get_head(){
	if (!height) return 0;
	return ((t_node *)head)->elem;
}
t_eue_elem Queue::get_tail(){
	if (!height) return 0;
	return ((t_node *)tail)->elem;
}
bool Queue::del_head(){
	t_node *node;
	if (head == NULL)
		return 0;
	node = (t_node *)head;
	head = node->next;
	((t_node *)head)->last = NULL;
	del_elem(node->elem);
	delete node;
	if (head == NULL)
		tail == NULL;
	--height;
	return 0;
}
size_t Queue::get_size(){ return height;}