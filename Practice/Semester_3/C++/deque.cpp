#include "deque.h"

typedef struct t_node{
	struct t_node *last, *next;
	t_eue_elem elem;
}t_node;

bool DQueue::add_head(t_eue_elem val){
	t_node *node = new t_node;
	node->next = (t_node *)head;
	node->last = NULL;
	new_elem(node->elem);
	node->elem = val;
	if ((t_node *)head != NULL)
		((t_node *)head)->last = node;
	node->next = (t_node *)head;
	head = node;
	if ((t_node *)tail == NULL)
		tail = node;
	++height;
	return 0;
}
bool DQueue::del_tail(){
	t_node *node;
	if (tail == NULL)
		return 0;
	node = (t_node *)tail;
	tail = node->last;
	((t_node *)tail)->next = NULL;
	del_elem(node->elem);
	delete node;
	if (tail == NULL)
		head == NULL;
	--height;
	return 0;
}