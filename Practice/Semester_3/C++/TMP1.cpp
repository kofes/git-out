#include <cstdio>
/*	Права доступа (Область видимости)
**	public: <- позволяет обращаться к полям напрямую
**	protected: <- Доступны из метода текущего и классов наследников.
**	private: <- по умолчанию. Позволяет обращаться лишь посредством реализаций
**	в public.
**	В плюсах class и struct отличаются лишь тем, что в первом default private,
**	а во втором public.
**	:: <- операция расширения области видимости
**	Можно реализовывать функции прямо в классе/структуре
**	Особые методы :
**	Конструктор : t_class();
**	Деструктор :
	~t_class();
	Конструктор может быть в привате, а деструктор только в public
**	Возможна перегрузка функций.
**	Для любого класса есть конструктор по умолчанию
**	Конструктор по умолчанию - конструктор, который ничего не принемает
**	Конструктор копирования - принемает на входе объект такого же класса
	t_class(const t_class &A);
	& - ссылочный тип.
	int &c = a;
	int *b = &a;
	*b = 5; //(Равносильны... c - что-то вроде псевдонима 'a'.
**	Ссылка - неперемещаемый указатель. Они должны быть сразу инициализированы.
	c = a;	//)
**	Константные методы(и функции) есть.
**	Это означает, что нельзя менять возвращаемое значение функции, поэтому
**	возможно будет писать лишь так
	const int *test();
	int main(){
	const *int a = test();
	...
	...
	}
**	Константный метод - не меняет значения полей класса
	void test() const;
	const t_class A;	//<- содержимое этого объекта не меняется
		A.test();
** Статический метод класса
	static int test();
	Могут работать только со статическими полями класса и там нет 'this'
	Их вызывают так:
		t_class::test();
**	Авторы книженций, которые можно курнуть:
	Брюс Эккель и т.д.
	Стивен Прата
	Б. Страуструп - сухо
	Гереберт Шилдт - сухо
*/
class t_class{
private:
	/*Данные*/
public:
	/*Методы*/
	t_class(){
		printf("class1\n");
	}
	t_class(int _a){
		printf("%i\n", _a);
	}
	~t_class(){
		printf("NOOO!\n");
	}
};

class t_array{
	int *A;
public:
	t_array(int n){
		A = new int[n];
	}
	~t_array(){
		delete[] A;
	}
};
class t_class1{
private:
	static int count;
public:
	int get_count(){return count;}
	t_class1(){count++;};
	~t_class1(){count--;};
};
int t_class1::count = 0;
int test1(){
	int a = 9, i = 3;
	int *b = &a;
	int &c = a;
	printf("a_%i_*b_%i_c_%i\n", a, *b, c);
	b = &i;
	c = 7;
	printf("a_%i_*b_%i_c_%i\n", a, *b, c);
	return 0;
}
int test2(){
	t_class A, B(3);
	return 0;
}
int test3(){
	t_class A, B(3);
	t_class *C = new t_class(4);
	delete C;
	return 0;
}
int test4(){
	void *C;
	C = new t_class(4);
	delete (t_class *)C;
	return 0;
}
int test5(){
	int *A = new int[32];
	delete[] A;
	return 0;
}
int test6(){	//Равносильно test5
	t_array B(32);
	return 0;
}
int test7(){
	t_class1 A, P;
	printf("value : %i\n", A.get_count());
	P.~t_class1();
	printf("value : %i\n", A.get_count());
	P = t_class1();
	return 0;
}
int main(){
	t_class1 A, B;
	printf("value : %i\n", A.get_count());
	t_class1 *C = new t_class1[32];
	printf("value : %i\n", A.get_count());
	delete[] C;
	printf("value : %i\n", A.get_count());
	return 0;
}