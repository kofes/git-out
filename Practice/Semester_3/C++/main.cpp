#include "queue.h"
#include "deque.h"
int new_elem(void *elem){
	elem = 0;
	return 0;
}

int del_elem(void *elem){
	elem = 0;
	return 0;
}

int main_quque(int argc, char const *argv[]){
	Queue p1(new_elem, del_elem);

	p1.add_tail((void *)25);
	p1.add_tail((void *)10);
	printf("size = %u, tail = %i, head = %i\n",
		p1.get_size(), (size_t)p1.get_tail(), (size_t)p1.get_head());
	p1.del_head();
	printf("size = %u, tail = %i, head = %i\n",
		p1.get_size(), (size_t)p1.get_tail(), (size_t)p1.get_head());

	return 0;
}
int main(int argc, char const *argv[]){
	DQueue p1(new_elem, del_elem);

	p1.add_tail((void *)25);
	p1.add_head((void *)10);
	printf("size = %u, tail = %i, head = %i\n",
		p1.get_size(), (size_t)p1.get_tail(), (size_t)p1.get_head());
	p1.del_head();
	printf("size = %u, tail = %i, head = %i\n",
		p1.get_size(), (size_t)p1.get_tail(), (size_t)p1.get_head());

	return 0;
}