/*
template <int num, typename T>/*Если < <...> >, то пробел между скобками обязателен*/
class t_array{
private:
	T arr[num];
	int n = num;
public:
	t_array();
	~t_array();
	T &operator [](int i);
	void operator =(const t_array &src);/*Если для текущего шаблонного класса*/
	void operator =(const t_array<num, long int> &src);
	void operator =(const t_array<num, double> &src);
}
int main() {
	t_array</*Обязан явно указать с какими параметрами завести переменную,
	такая запись называется - инстанцирование шаблона*/10, char> A1;
	t_array<5, double> A2;/*Шаблоны реализуются во время компиляции*/
	/*Объекты, созданные с помощью одного и того же шаблона не связаны "динамически",
	ибо создаются два разных класса*/
	/*Когда функция не используется для данного шаблона, то она даже не компилируется
	Из ленивых операций до ленивого компилятора*/
	return 0;
}
*/
template <typename T>
class t_stack {
private:
	struct t_node {
		T val;
		t_node *next;
	};
	t_node *head;
public:
	t_stack(): head(NULL){};
	~t_stack();
	inline void push(T src){
		/**/
	}
	inline void pop(){
		/**/
	}
	T &top(){
		/**/
	}
	~t_stack(){
		/**/
	}
};

int main(){
	t_stack<char> S1;
	t_stack<t_stack<double> > S2;
	return 0;
}

/*Part 3*/
template <typename T, int n>
struct t_class {
	T a;
	t_class (T _a): a(_a) {};
	void print();
	~t_class ();
};

template <typename T, int n>
void t_class<T, n>::print(){
	while (n--)
		std::cout << a << std::endl;
}

/*'Особая' перегрузка*/
template <int n>
void t_class<double, n>::print(){
	while (--n)
		std::cout << "double: " << a << endl;
}

/*Part 4 - получение многомерного массива*/
template <typename T, int dim, int num>
class t_mdarray{
private:
	t_mdarray<T, dim - 1, num> BUFF[num];
public:
	t_mdarray ();
	~t_mdarray ();
	t_mdarray<T, dim - 1, num> &operator [](int i){
		return BUFF[i];
	}
};

template <typename T, int num>
class t_mdarray<T, 0, num> {
private:
	T BUFF[num];
public:
	t_mdarray ();
	T &operator [](int i){
		return BUFF[i];
	}
};
