% course( название дисциплины, группа, перподаватель, день недели, номер пары, корпус, аудитория).

course(sai,  b8103b, foo, s, 2, d, 223).
course(sia,  b8103a, bar, w, 2, d, 124).
course(geo,  b8303a, sii, w, 3, d, 322).
course(oo,   b8204,  tie, w, 3, d, 945).
course(ssd,  b8401a, sea, t, 2, s, 323).
course(sata, b8303a, sii, w, 4, d, 200).

can_meet(Teacher1, Teacher2) :-
  course(Subject1, Group1, Teacher1, WeekDay, NumEx, Housing, LectureHall1),
  course(Subject2, Group2, Teacher2, WeekDay, NumEx, Housing, LectureHall2),
  not(Teacher1 = Teacher2).

cannot_meet(Teacher1, Teacher2) :-
  not(can_meet(Teacher1, Teacher2)),
  not(Teacher1 = Teacher2).
