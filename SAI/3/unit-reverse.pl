reverse(L, RL) :-
  reverse(L, [], RL).
reverse([H|T], S, RL) :-
  reverse(T, [H|S], RL).
reverse([], RL, RL).
