#include <stdio.h>
#include <stdlib.h>
#define QUEUE_NULL (NULL)

//Тип значения:
typedef struct{
  unsigned short t, m;
}t_queue_elem;
//Тип дескриптора:
typedef void *t_queue;
//Признак открепленного дескриптора:
typedef struct t_node{
	struct t_node *next;
	t_queue_elem elem;
}t_node;
typedef struct{
	t_node *head, *tail;
	size_t height;
}t_unvoid_queue;

t_queue queue_new(){
	t_unvoid_queue *queue;
	if ((queue = (t_unvoid_queue *)malloc(sizeof(t_unvoid_queue))) == NULL)
		return NULL;
	queue->head = NULL;
	queue->tail = NULL;
	queue->height = 0;
	return (t_queue *)queue;
}

void queue_del(t_queue queue){
	t_node *curr, *node;
	if (queue == NULL)
		return;
	curr = ((t_unvoid_queue *)queue)->head;
	while (curr != NULL){
		node = curr;
		curr = curr->next;
		free(node);
	}
	((t_unvoid_queue *)queue)->head = NULL;
	((t_unvoid_queue *)queue)->tail = NULL;
	((t_unvoid_queue *)queue)->height = 0;
}

size_t queue_hht(t_queue queue){
	if (queue == NULL)
		return 0;
	return ((t_unvoid_queue *)queue)->height;
}

t_queue_elem *queue_add_elem(t_queue queue){
	t_node *node;
	if (queue == NULL)
		return NULL;
	if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
		return NULL;
	node->next = NULL;
	if (((t_unvoid_queue *)queue)->tail != NULL)
		((t_unvoid_queue *)queue)->tail->next = node;
	((t_unvoid_queue *)queue)->tail = node;
	if (((t_unvoid_queue *)queue)->head == NULL)
		((t_unvoid_queue *)queue)->head = node;
	++((t_unvoid_queue *)queue)->height;
	return &(node->elem);
}
t_queue_elem *queue_get_elem(t_queue queue){
	if ((queue == NULL)||(((t_unvoid_queue *)queue)->head == NULL))
		return NULL;
	return &(((t_unvoid_queue *)queue)->head->elem);
}
void queue_del_elem(t_queue queue){
	t_node *node;
	if (queue == NULL)
		return;
	if (((t_unvoid_queue *)queue)->head != NULL){
		node = ((t_unvoid_queue *)queue)->head;
		((t_unvoid_queue *)queue)->head = node->next;
		free(node);
	}
	else return;
	if (((t_unvoid_queue *)queue)->head == NULL)
		((t_unvoid_queue *)queue)->tail = NULL;
	--((t_unvoid_queue *)queue)->height;
}
int main(){
  FILE *fp;
  t_queue que;
  t_queue_elem elem;
  unsigned int n, max, len, max_time, t, m;
  que = QUEUE_NULL;
  if ((fp = fopen("input.txt", "rt")) == NULL)
    return 1;
  que = queue_new();
  max_time = 1440;
  max = 0;
  t = 0;
  m = 0;
  fscanf(fp, "%u", &n);
  for (elem.t = 0, elem.m = 0; ((elem.t + elem.m) < max_time)&&(n != 0); --n){
    fscanf(fp, "%u%u", &t, &m);
    if (t < (elem.t + elem.m)){
      t_queue_elem node;
      node.t = elem.t + elem.m; node.m = m;
      *queue_add_elem(que) = node;
    }else{
      max = max < queue_hht(que) ? queue_hht(que) : max;
      elem.t = t; elem.m = m;
      *queue_add_elem(que) = elem;
      while (t > (elem.t + elem.m)){
        t = elem.t; m = elem.m;
        elem = *queue_get_elem(que);
        queue_del_elem(que);
      }
    }
  }
  len = queue_hht(que) + n;
  max = max < len ? len : max;
  t = (t + m) > max_time ? (max_time - m) : t;
  queue_del(que);
  fclose(fp);
  if ((fp = fopen("output.txt", "wt")) == 0)
    return 1;
  fprintf(fp, "%u %u\n", max, t + m);
  fclose(fp);
  return 0;
}
