#include <stdio.h>
#include <stdlib.h>
#include "lab-2.2.h"

typedef struct t_btree_node{
	struct t_btree_node *child[2];
	t_btree_key key;
	t_btree_val val;
	char diff;
}t_btree_node;
typedef struct{
	t_btree_node *root;
}t_utree;
t_btree btree_new(){
	t_utree *tree;
	if ((tree = (t_utree *)malloc(sizeof(t_utree))) == NULL)
		return NULL;
	tree->root = NULL;
	return (t_btree)tree;
}
void btree_del(t_btree btree){
	t_utree *tree;
	tree = (t_utree *)btree;
	void btree_del_node(t_btree_node *node){
		if (node == NULL)
			return;
		btree_del_node(node->child[0]);
		btree_del_node(node->child[1]);
		free(node->key);
		free(node);
	}
	if (tree == NULL)
		return;
	btree_del_node(tree->root);
}

t_btree_node *lit_turn(t_btree_node *root, char inv){
	t_btree_node *node = root->child[inv];
	root->child[inv] = node->child[(inv + 1)%2];
	node->child[(inv + 1)%2] = root;
	root = node;
	return root;
}

t_btree_node *turn(t_btree_node *node, char inv){

	if (abs(node->diff) == 2){
		char *diff[3];
		diff[0] = &(node->diff);
		diff[1] = &(node->child[inv]->diff);
		diff[2] = NULL;

		if (node->child[inv]->diff == (2*inv - 1)){
			diff[2] = &(node->child[inv]->child[(inv + 1)%2]->diff);
			node->child[inv] = lit_turn(node->child[inv], (inv+1)%2);
		}
		if (diff[2] == NULL){
			if (*diff[1] == 0){
				*diff[0] = (1 - 2*inv);
				*diff[1] = (2*inv - 1);
			}
			else{
				*diff[0] = 0;
				*diff[1] = 0;
			}
		}
		else{
			if (*diff[2] == 0){
				*diff[0] = 0;
				*diff[1] = 0;
			}
			else if (*diff[2] == (2*inv - 1)){
				*diff[0] = 0;
				*diff[1] = (1 - 2*inv);
				}
				else{
					*diff[0] = (2*inv - 1);
					*diff[1] = 0;
				}
				*diff[2] = 0;
		}

		return lit_turn(node, inv);
	}
	return node;
}

t_btree_val *btree_add_elem(t_btree btree, t_btree_key key){
	char info = 0;
	t_utree *tree;
	t_btree_val *val = NULL;
	tree = (t_utree *)btree;
	t_btree_node *btree_add_node(t_btree_node *node){
		char i;
		if (node == NULL){
			size_t len;
			if ((node = (t_btree_node *)malloc(sizeof(t_btree_node))) == NULL){
				info = 1;
				return NULL;
			}
			node->val = 0;
			node->diff = 0;
			node->child[0] = NULL;
			node->child[1] = NULL;
			len = strlen(key) + 1;
			if ((node->key = (t_btree_key )malloc(len)) == NULL){
				free(node);
				info = 1;
				return NULL;
			}
			memcpy(node->key, key, len - 1);
			node->key[len - 1] = '\0';
			val = &(node->val);

			return node;
		}

		i = strcmp(key, node->key);
		if (i == 0){val = &(node->val); info = 1; return node;}
		i = (i > 0);
		node->child[i] = btree_add_node(node->child[i]);
		node->diff += (1 - 2*i)*((info + 1)%2);
		node = turn(node, i);
		if ((info == 0)&&(node->diff == 0))
			info = 1;
		return node;
	}

	if (tree == NULL)
		return NULL;
	tree->root = btree_add_node(tree->root);
	return val;
}

void btree_out(t_btree btree, FILE *fout){

	t_utree *tree = (t_utree *)btree;
	void btree_out_node(t_btree_node *node){
		if (node == NULL)
			return;
		btree_out_node(node->child[0]);
		fprintf(fout, "%s\t%i\n", node->key, node->val);
		btree_out_node(node->child[1]);
	}

	if ((tree == NULL)||(fout == NULL))
		return;

	btree_out_node(tree->root);
}
