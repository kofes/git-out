#ifndef __INCLUDE_BTREE_H
#define __INCLUDE_BTREE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//Тип ключа:
typedef unsigned char *t_btree_key;

//Тип значения:
typedef int t_btree_val;

//Тип дескриптора двоичного дерева:
typedef void *t_btree;

//Признак открепленного дескриптора:
#define BTREE_NULL (NULL)

//Генерирует пустое дерево и возвращает его дескриптор:
extern t_btree btree_new();

//Освобождает память из-под дерева:
extern void btree_del(t_btree btree);

//Выполняет вставку элемента по ключу:
extern t_btree_val *btree_add_elem(t_btree btree, t_btree_key key);

//Выводит содержимое дерева в выходной поток:
extern void btree_out(t_btree btree, FILE *fout);

#endif //__INCLUDE_BTREE_H
