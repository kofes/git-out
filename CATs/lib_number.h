/**
 * Текущий заголовочный файл содержит интерфейсы функций,
 * предназначенных для работы с длинными целыми
 * посредством "умных указателей" (SmartPointer)
 * с подсчетом ссылок.
 */

#ifndef LIB_NUMBER_H
#define LIB_NUMBER_H

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * Объявление используемых типов данных:
 */

/* Тип цифры в числе */
typedef unsigned char LIB_NUMBER_t_byte;

/* Тип основания системы счисления */
typedef size_t LIB_NUMBER_t_base;

/* Тип знака числа */
typedef int LIB_NUMBER_t_sign;

/* Верхний и нижний пределы для основания системы счисления */
#define LIB_NUMBER_MAX_BASE (UCHAR_MAX + 1)
#define LIB_NUMBER_MIN_BASE (2)

/* Тип указателя на длинное целое */
typedef void *LIB_NUMBER_t_hndl;

/* Признак открепленного указателя (используется при инициализации) */
#define LIB_NUMBER_NULL (NULL)

/* Коды ошибок */
#define LIB_NUMBER_ERR_NORM (0)	//Ошибки отсутствуют;
#define LIB_NUMBER_ERR_NULL (1)	//Ошибка выделения памяти;
#define LIB_NUMBER_ERR_DATA (2)	//Некорректные входные параметры;
#define LIB_NUMBER_ERR_DIV0 (4)	//Деление на ноль;

/**
 * Функции для работы с длинными целыми:
 */

/* Генерирует число произвольной разрядности на основе целочисленной переменной
   в заданной системе счисления и связывает его с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_new(LIB_NUMBER_t_hndl *hndl, unsigned long long number);

/* Открепляет указатель от связанного с ним числа */
/* Возвращает код ошибки */
extern int LIB_NUMBER_del(LIB_NUMBER_t_hndl *hndl);

/* Выполняет умножение двух длинных чисел и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_mul(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Возвращает значение цифры по индексу */
extern LIB_NUMBER_t_byte LIB_NUMBER_get_byte(LIB_NUMBER_t_hndl hndl, size_t ind);

/* Возвращает количество цифр в числе */
extern size_t LIB_NUMBER_get_size(LIB_NUMBER_t_hndl hndl);

/* Считывает из файла число*/
extern int LIB_NUMBER_read(LIB_NUMBER_t_hndl *hndl, FILE *fin);

/* Выводит на печать число в его системе счисления и систему счисления*/
extern int LIB_NUMBER_write(LIB_NUMBER_t_hndl hndl, FILE *fout);

#endif //LIB_NUMBER_H
