#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#define LN(VOID) ((t_num *)(VOID))
#define NEW_NODE(NODE) (NODE = (t_num *)malloc(sizeof(t_num)))
#define NEW_BUFF(NODE) ((NODE)->buff = (LIB_NUMBER_t_byte *)malloc(((NODE)->size)*sizeof(LIB_NUMBER_t_byte)))

/* Тип цифры в числе */
typedef unsigned char LIB_NUMBER_t_byte;

/* Тип основания системы счисления */
typedef size_t LIB_NUMBER_t_base;

/* Тип знака числа */
typedef int LIB_NUMBER_t_sign;

/* Верхний и нижний пределы для основания системы счисления */
#define LIB_NUMBER_MAX_BASE (UCHAR_MAX + 1)
#define LIB_NUMBER_MIN_BASE (2)

/* Тип указателя на длинное целое */
typedef void *LIB_NUMBER_t_hndl;

/* Признак открепленного указателя (используется при инициализации) */
#define LIB_NUMBER_NULL (NULL)

/* Коды ошибок */
#define LIB_NUMBER_ERR_NORM (0)	//Ошибки отсутствуют;
#define LIB_NUMBER_ERR_NULL (1)	//Ошибка выделения памяти;
#define LIB_NUMBER_ERR_DATA (2)	//Некорректные входные параметры;
#define LIB_NUMBER_ERR_DIV0 (4)	//Деление на ноль;


/* Генерирует число произвольной разрядности на основе целочисленной переменной
   в заданной системе счисления и связывает его с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_new(LIB_NUMBER_t_hndl *hndl, unsigned long long number);

/* Открепляет указатель от связанного с ним числа */
/* Возвращает код ошибки */
extern int LIB_NUMBER_del(LIB_NUMBER_t_hndl *hndl);

/* Выполняет умножение двух длинных чисел и связывает результат с заданным указателем */
/* Возвращает код ошибки */
extern int LIB_NUMBER_mul(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right);

/* Возвращает значение цифры по индексу */
extern LIB_NUMBER_t_byte LIB_NUMBER_get_byte(LIB_NUMBER_t_hndl hndl, size_t ind);

/* Возвращает количество цифр в числе */
extern size_t LIB_NUMBER_get_size(LIB_NUMBER_t_hndl hndl);

/* Считывает из файла число*/
extern int LIB_NUMBER_read(LIB_NUMBER_t_hndl *hndl, FILE *fin);

/* Выводит на печать число в его системе счисления и систему счисления*/
extern int LIB_NUMBER_write(LIB_NUMBER_t_hndl hndl, FILE *fout);

typedef struct{
    LIB_NUMBER_t_byte *buff;
    size_t size;
}t_num;

int LIB_NUMBER_new(LIB_NUMBER_t_hndl *hndl, unsigned long long number){
    t_num *num;
    size_t i, size;
    long long dnum;
    unsigned char base = 10;
    if (hndl == NULL)
        return LIB_NUMBER_ERR_DATA;
    if ((num = (t_num *)malloc(sizeof(t_num))) == NULL)
        return LIB_NUMBER_ERR_NULL;
    dnum = number;
    size = 0;
    do{
        dnum /= base;
        ++size;
    }while (dnum != 0);
    if ((num->buff = (LIB_NUMBER_t_byte *)malloc(size*sizeof(LIB_NUMBER_t_byte))) == NULL){
        free(num);
        return LIB_NUMBER_ERR_NULL;
    }
    num->size = size;
    for (i = 0; i < size; ++i){
        num->buff[i] = number%base;
        number /= base;
    }
    LIB_NUMBER_del(hndl);
    *hndl = (LIB_NUMBER_t_hndl )num;
    return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_del(LIB_NUMBER_t_hndl *hndl){
    t_num *num;
    if (hndl == NULL)
        return LIB_NUMBER_ERR_DATA;
    if (*hndl == NULL)
        return LIB_NUMBER_ERR_NORM;
    num = (t_num *)(*hndl);
    free(num->buff);
    free(num);
    *hndl = NULL;
    return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_mul(LIB_NUMBER_t_hndl *result, LIB_NUMBER_t_hndl left, LIB_NUMBER_t_hndl right){
  t_num *num;
  unsigned char base = 10;
  if ((result == NULL)||(left == NULL)||(right == NULL))
    return LIB_NUMBER_ERR_DATA;
  if (((LN(left)->size == 1)&&(LN(left)->buff[0] == 0))||
      ((LN(right)->size == 1)&&(LN(right)->buff[0] == 0))){
    if ((num = (t_num *)malloc(sizeof(t_num))) == NULL)
      return LIB_NUMBER_ERR_NULL;;
    if ((num->buff = (LIB_NUMBER_t_byte *)malloc(sizeof(LIB_NUMBER_t_byte))) == NULL){
      free(num);
      return LIB_NUMBER_ERR_NULL;
    }
    num->size = 1;
    num->buff[0] = 0;
    *result = (LIB_NUMBER_t_hndl )num;
    return LIB_NUMBER_ERR_NORM;
  }
  t_num *mul(t_num *a, t_num *b){
    t_num *node;
    if (NEW_NODE(node) == NULL) return NULL;
    node->size = a->size + b->size;
    if (NEW_BUFF(node) == NULL){ free(node); return NULL;}
    size_t i, j, curr, diff;
    memset(node->buff, 0, (node->size)*sizeof(LIB_NUMBER_t_byte));
    for (i = 0; i < (a->size); ++i){
      for (j = 0, diff = 0; j < (b->size); ++j){
        // printf("i = %u, j = %u, node->buff[%u] = %i\n", (unsigned int)i, (unsigned int)j, (unsigned int)(i+j), (unsigned int)(node->buff[i+j]));
        curr = (a->buff[i])*(b->buff[j]) + diff + node->buff[i+j];
        node->buff[i+j] = curr%(base);
        diff = curr/(base);
      }
      node->buff[i+j] = diff;
    }
    return node;
  }
  num = mul(LN(left), LN(right));
  if (num == NULL) return LIB_NUMBER_ERR_NULL;
  LIB_NUMBER_del(result);
  while ((num->size > 1)&&(num->buff[num->size - 1] == 0))
    --(num->size);
  num->buff = realloc(num->buff, num->size);
  *result = (LIB_NUMBER_t_hndl )num;
  return LIB_NUMBER_ERR_NORM;
}

LIB_NUMBER_t_byte LIB_NUMBER_get_byte(LIB_NUMBER_t_hndl hndl, size_t ind){
  if ((hndl == NULL)||(ind > LN(hndl)->size))
    return 255;
  return LN(hndl)->buff[ind];
}

size_t LIB_NUMBER_get_size(LIB_NUMBER_t_hndl hndl){
  if (hndl == NULL) return 0;
  return LN(hndl)->size;
}

int LIB_NUMBER_read(LIB_NUMBER_t_hndl *hndl, FILE *fin){
  unsigned char ch;
  size_t pos;
  if ((hndl == NULL)||(fin == NULL))
    return LIB_NUMBER_ERR_DATA;
  LIB_NUMBER_del(hndl);
  if ((*hndl = (t_num *)malloc(sizeof(t_num))) == NULL)
    return LIB_NUMBER_ERR_NULL;
  if (feof(fin)){ free(*hndl); return LIB_NUMBER_ERR_DATA;}
  if ((LN(*hndl)->buff = (LIB_NUMBER_t_byte *)malloc(sizeof(LIB_NUMBER_t_byte))) == NULL){
    free(*hndl);
    return LIB_NUMBER_ERR_NULL;
  }
  LN(*hndl)->size = 1;
  pos = 0;
  while ((ch = fgetc(fin))&&(ch != '\n')){
    if (pos == LN(*hndl)->size){
      LN(*hndl)->size <<= 1;
      LN(*hndl)->buff = realloc(LN(*hndl)->buff, LN(*hndl)->size);
    }
    LN(*hndl)->buff[pos++] = ch - 48;
  }
  LN(*hndl)->buff = realloc(LN(*hndl)->buff, pos);
  LN(*hndl)->size = pos;
  for (pos = 0; pos < (LN(*hndl)->size)/2; ++pos){
    ch = LN(*hndl)->buff[pos];
    LN(*hndl)->buff[pos] = LN(*hndl)->buff[LN(*hndl)->size - pos - 1];
    LN(*hndl)->buff[LN(*hndl)->size - pos - 1] = ch;
  }
  while ((LN(*hndl)->size > 1)&&(LN(*hndl)->buff[LN(*hndl)->size - 1] == 0))
    --(LN(*hndl)->size);
  LN(*hndl)->buff = realloc(LN(*hndl)->buff, LN(*hndl)->size);
  return LIB_NUMBER_ERR_NORM;
}

int LIB_NUMBER_write(LIB_NUMBER_t_hndl hndl, FILE *fout){
  size_t i;
  if ((hndl == NULL)||(fout == NULL))
    return LIB_NUMBER_ERR_DATA;
  for (i = 0; i < LN(hndl)->size; ++i)
    fprintf(fout, "%u", LN(hndl)->buff[LN(hndl)->size-i-1]);
  fprintf(fout, "\n");
  return LIB_NUMBER_ERR_NORM;
}

int main(){
    FILE *fp;
    LIB_NUMBER_t_hndl num, POWER;
    unsigned long long n;
    num = LIB_NUMBER_NULL;
    POWER = LIB_NUMBER_NULL;
    if ((fp = fopen("input.txt", "rt")) == NULL)
        return 1;
    LIB_NUMBER_read(&num, fp);
    fscanf(fp, "%llu", &n);
    fclose(fp);
    if ((fp = fopen("output.txt", "wt")) == NULL){
        LIB_NUMBER_del(&num); return 1;
    }
    LIB_NUMBER_new(&POWER, 1);
    while (n != 0){
      if ((n % 2) == 1) LIB_NUMBER_mul(&POWER, POWER, num);
      n >>= 1;
      LIB_NUMBER_mul(&num, num, num);
    }
    if ((fp = fopen("output.txt", "wt")) == NULL)
        return 1;
    LIB_NUMBER_write(POWER, fp);
    fclose(fp);
    LIB_NUMBER_del(&num);
    LIB_NUMBER_del(&POWER);
    return 0;
}
