#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
typedef unsigned char SYM;

size_t BMH(const SYM* T, const SYM* S)
{
	size_t i, j, k, len_T, len_S, count;
	SYM alph_table[256];

	if ((S == NULL)||(T == NULL))
		return SIZE_MAX;

	len_T = strlen(T);
	len_S = strlen(S);
  count = 0;
	if (len_T<=len_S){
		for (i = 0; i<256; ++i)
			alph_table[i] = len_T;
		for (i = 1; i<len_T; ++i)
			alph_table[(T[i])] = len_T - i;

		i = len_T;
		do{
				j = len_T;
				k = i;
				while ((j>0)&&((S[k-1]) == (T[j-1]))){
						--k;
						--j;
					}
        if (j == 0){
          ++count;
          i += len_T;
        }else
				    i += alph_table[S[i]];
			}while(i<=len_S);
	}

	return count;
}

size_t fBMH(const SYM* tmp, FILE* fin){
	size_t k, l = 0, t = 1, len, m, count;
	SYM *buff, alph_table[256];
	char flag;

	if ((tmp == NULL)||(fin == NULL))
		return SIZE_MAX;

	count = 0;
	len = strlen(tmp);
	buff = (SYM *)malloc(len);
	m = fread(buff, 1, len, fin);
	for (k = 0; k < 256; ++k)
		alph_table[k] = len;
	for (k = 0; k < len - 1; ++k)
		alph_table[tmp[k]] = len - k - 1;
	while (m == len){
		flag = 1;
		for (k = l; k<len; ++k)
			if (buff[k] != tmp[k]){
				flag = 0;
				break;
			}
		if (flag){
				++count;
				m = fread(buff, 1, len, fin);
			}else{
				t = alph_table[buff[len - 1]];
				memmove(buff, buff+t, len - t);
				m -= t;
				m += fread(buff+m, 1, t, fin);
			};
	}
	free(buff);
	return count;
}

int main(){
  FILE *fp;
  SYM ch, *buff, len = 129, i = 0, *str;
  size_t count, size;
  if ((fp = fopen("input.txt", "rt")) == NULL)
    return 1;
  if ((buff = (SYM *)malloc(len*sizeof(SYM))) == NULL){
    fclose(fp); return 1;
  }
//
  size = 7000001;
  if ((str = (SYM *)malloc(size*sizeof(SYM))) == NULL){
    free(buff); fclose(fp); return 1;
  }
//
  memset(buff, '\0', len);
  memset(str, '\0', size); //
  if (feof(fp)) return 1;
  while ((!feof(fp))&&(ch = fgetc(fp))&&(ch > 31))
    buff[i++] = ch;
  len = i+1;
  buff = realloc(buff, len);
  fread(str, sizeof(SYM), size - 1, fp); //
  count = BMH(buff, str);
  free(str);
//  count = fBMH(buff, fp);
  free(buff);
  fclose(fp);
  if ((fp = fopen("output.txt", "wt")) == NULL)
    return 2;
  fprintf(fp, "%zu", count);
  fclose(fp);
  return 0;
}
