#include <stdio.h>

int main(){
  FILE *fp;
  size_t k, d, n, m , max;
  if ((fp = fopen("input.txt", "rt")) == NULL)
    return 1;
  fscanf(fp, "%u", &d);
  fscanf(fp, "%u", &m);
  fclose(fp);
  if ((fp = fopen("output.txt", "wt")) == NULL)
    return 2;
  for (n = 1, max = 0; n <= m; fprintf(fp, "%u ", max), max = 0, ++n)
    for (k = 1; k <= (n/k + n%k); ++k){
      if ((((2*n)%k) == 0)&&(((long long)((2*n)/k) - (long long)(d*(k-1))) > 0)
                          &&((((2*n)/k - d*(k-1))%2) == 0)){
        if (max < k) max = k;
      }
      if ((((2*n)%(n / k)) == 0)&&(((long long)((2*n)/(n / k)) - (long long)(d*((n / k)-1))) > 0)
                          &&((((2*n)/(n / k) - d*((n / k)-1))%2) == 0)){
        if (max < (n / k))
          max = n / k;
      }
  }
  fclose(fp);
  return 0;
}



/*
    for (k = n; k > 0; --k)
      if ((((2*n)%k) == 0)&&(((long long)((2*n)/k) - (long long)(d*(k-1))) > 0)
                          &&((((2*n)/k - d*(k-1))%2) == 0)){
        printf("%u - %u; %i\n", n, k, (((2*n)/k) - (d*(k-1))));
        break;
      }
*/
/*
#include <stdio.h>

int main(){
  FILE *fp;
  size_t k, d, n, m , max, buff, count;
  if ((fp = fopen("input.txt", "rt")) == NULL)
    return 1;
  fscanf(fp, "%u", &d);
  fscanf(fp, "%u", &m);
  fclose(fp);
  if ((fp = fopen("output.txt", "wt")) == NULL)
    return 2;
  for (n = 1, max = 0, buff = 1, count = 1; n <= m; fprintf(fp, "%u ", max), max = 0, ++n){
    if (buff == n){
        max = count;
        ++count;
        buff += 1 + d;
    }else{
        for (k = 1; k <= count; ++k){
          if ((((2*n)%k) == 0)&&(((long long)((2*n)/k) - (long long)(d*(k-1))) > 0)
                              &&((((2*n)/k - d*(k-1))%2) == 0)){
            if (max < k) max = k;
          }
          if ((((2*n)%(n / k)) == 0)&&(((long long)((2*n)/(n / k)) - (long long)(d*((n / k)-1))) > 0)
                              &&((((2*n)/(n / k) - d*((n / k)-1))%2) == 0)){
            if (max < (n / k))
              max = n / k;
          }
        }
    }
  }
  fclose(fp);
  return 0;
}

*/
