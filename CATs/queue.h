#ifndef __INCLUDE_QUEUE_H
#define __INCLUDE_QUEUE_H

#include <stdio.h>
#include <stdlib.h>

//Тип значения:
typedef struct{
  unsigned short t, m;
}t_queue_elem;

//Тип дескриптора:
typedef void *t_queue;

//Признак открепленного дескриптора:
#define QUEUE_NULL (NULL)

//Генерирует пустую очередь:
extern t_queue queue_new();

//Освобождает пямять из-под очереди:
extern void queue_del(t_queue queue);

//Выводит высоту очереди:
extern size_t queue_hht(t_queue queue);

//Выполняет вставку элемента в начало очереди:
extern t_queue_elem *queue_add_elem(t_queue queue);

//Выводит элемент с конца очереди:
extern t_queue_elem *queue_get_elem(t_queue queue);

//Выполняет удаление элемента с конца очереди:
extern void queue_del_elem(t_queue queue);

//Выводит содержимое очереди в выходной поток:
extern void queue_out(t_queue queue, FILE *fout);

#endif //__INCLUDE_QUEUE_H
