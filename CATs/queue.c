#include "queue.h"

typedef struct t_node{
	struct t_node *next;
	t_queue_elem elem;
}t_node;

typedef struct{
	t_node *head, *tail;
	size_t height;
}t_unvoid_queue;

t_queue queue_new(){

	t_unvoid_queue *queue;

	if ((queue = (t_unvoid_queue *)malloc(sizeof(t_unvoid_queue))) == NULL)
		return NULL;

	queue->head = NULL;
	queue->tail = NULL;
	queue->height = 0;

	return (t_queue *)queue;
}

void queue_del(t_queue queue){

	t_node *curr, *node;

	if (queue == NULL)
		return;

	curr = ((t_unvoid_queue *)queue)->head;

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		free(node);
	}

	((t_unvoid_queue *)queue)->head = NULL;
	((t_unvoid_queue *)queue)->tail = NULL;
	((t_unvoid_queue *)queue)->height = 0;
}

size_t queue_hht(t_queue queue){

	if (queue == NULL)
		return 0;

	return ((t_unvoid_queue *)queue)->height;
}

t_queue_elem *queue_add_elem(t_queue queue){

	t_node *node;

	if (queue == NULL)
		return NULL;

	if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
		return NULL;
	node->next = NULL;
	if (((t_unvoid_queue *)queue)->tail != NULL)
		((t_unvoid_queue *)queue)->tail->next = node;
	((t_unvoid_queue *)queue)->tail = node;
	if (((t_unvoid_queue *)queue)->head == NULL)
		((t_unvoid_queue *)queue)->head = node;
	++((t_unvoid_queue *)queue)->height;

	return &(node->elem);
}

t_queue_elem *queue_get_elem(t_queue queue){

	if ((queue == NULL)||(((t_unvoid_queue *)queue)->head == NULL))
		return NULL;
	return &(((t_unvoid_queue *)queue)->head->elem);
}

void queue_del_elem(t_queue queue){

	t_node *node;

	if (queue == NULL)
		return;

	if (((t_unvoid_queue *)queue)->head != NULL){
		node = ((t_unvoid_queue *)queue)->head;
		((t_unvoid_queue *)queue)->head = node->next;
		free(node);
	}
	else
		return;

	if (((t_unvoid_queue *)queue)->head == NULL)
		((t_unvoid_queue *)queue)->tail = NULL;

	--((t_unvoid_queue *)queue)->height;
}

void queue_out(t_queue queue, FILE *fout){

	t_node *curr;

	if ((queue == NULL)||(fout == NULL))
		return;

	curr = ((t_unvoid_queue *)queue)->head;

	while (curr != NULL){
		fprintf(fout, "%i ", curr->elem);
		curr = curr->next;
	}
}
