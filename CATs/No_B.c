#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
typedef struct{
	size_t num;
	unsigned long long *val, *ord;
}t_fact;
void FACT_write(t_fact *factor);
t_fact *FACT_new(unsigned long long NUMBER){
	t_fact *FACT;
	unsigned long long i, m, r, last;
	if (NUMBER <= 1) return NULL;
	for (m = 1, r = NUMBER; r != 0; ++m, r >>= 1);
	FACT = (t_fact *)malloc(sizeof(t_fact));
	FACT->val = (unsigned long long *)malloc(m*sizeof(unsigned long long));
	FACT->ord = (unsigned long long *)malloc(m*sizeof(unsigned long long));
	memset(FACT->val, 0, m*sizeof(unsigned long long));
	FACT->num = 0;
	last = 0;
	for (i = 2; i <= NUMBER/i; ++i){
		while (NUMBER % i == 0){
			if (i != last){
				FACT->val[FACT->num] = i;
				FACT->ord[FACT->num] = 1;
				++(FACT->num);
				last = i;
			}
			else
				++(FACT->ord[(FACT->num)-1]);
			NUMBER /= i;
		}
	}
	if (NUMBER != 1){
		FACT->val[FACT->num] = NUMBER;
		FACT->ord[FACT->num] = 1;
	}
    ++(FACT->num);
	while (FACT->val[FACT->num - 1] == 0)
    --(FACT->num);
	return FACT;
}
void FACT_del(t_fact *Fact){
	if (Fact != NULL){
    free((Fact->ord));
    free((Fact->val));
    free(Fact);
	}
}
t_fact *FACT_nul(t_fact *source){
  size_t i;
  for (i = 0; i < source->num; ++i)
    source->ord[i] = 0;
  return source;
}
int main(){
  FILE *fp;
  t_fact *BASE, *BUFF;
  unsigned long long A, B, note1, note2, q;
  size_t i;
	unsigned long long count;
  if ((fp = fopen("input.txt", "rt")) == NULL)
    return 1;
  fscanf(fp, "%llu%llu%llu", &A, &B, &q);
  fclose(fp);
  BASE = FACT_new(q);
  BUFF = FACT_new(q);
  BUFF = FACT_nul(BUFF);
  if ((fp = fopen("output.txt", "wt")) == NULL)
    return 1;
	for (i = 0; i < BUFF->num; ++i){
		note1 = A - 1;
		note2 = B;
		while (note2 != 0){
			note1 /= BUFF->val[i];
			note2 /= BUFF->val[i];
			BUFF->ord[i] += note2 - note1;
		}
		if (i == 0) count = ((BUFF->ord[i])/(BASE->ord[i]));
		count = (count) > ((BUFF->ord[i])/(BASE->ord[i])) ? ((BUFF->ord[i])/(BASE->ord[i])) : (count);
	}
  fprintf(fp, "%llu", count);
  fclose(fp);
  FACT_del(BUFF);
  FACT_del(BASE);
  return 0;
}
