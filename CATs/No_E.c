#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ERR_DATA (1)
#define ERR_NULL (2)
#define ERR_NORM (3)

int main(){
	FILE *fp;
	size_t i, j, ind, len, *d, left, right, size, k;
	unsigned char *str, flag;
	if ((fp = fopen("input.txt", "rt")) == NULL)
		return ERR_DATA;
/*
	if ((str = (unsigned char *)malloc(5000001*sizeof(char))) == NULL)
		return ERR_NULL;
	str[5000000] = '\0';
	fgets(str, 5000000, fp);
*/
	if ((str = (unsigned char *)malloc(16 * sizeof(char))) == NULL)
		return ERR_NULL;
	len = 16;
	ind = 0;
	while ((!feof(fp))&&((flag = fgetc(fp)) != EOF)){
		if (ind == len){
			len <<= 1;
			str = realloc(str, len);
		}
		if (str == NULL){
			free(str);
			return ERR_NULL;
		}
		str[ind] = flag;
		++ind;
	}
	fclose(fp);
	len = ind;
	str[len - 1] = '\0';
	printf("%s\n", str);
	if ((d = (size_t *)malloc(len * sizeof(size_t))) == NULL){
		free(str);
		return ERR_NULL;
	}
	d[0] = 1;
	left = 0;
	right = 0;
	for (i = 1; i < len; ++i){
		if (i > right){
			d[i] = 1;
			while (((int)i - (int)d[i] >= 0)&&(i + d[i] < len)&&(str[i - d[i]] == str[i + d[i]]))
				++d[i];
			if ((i < len - d[i] + 1)&&(i + d[i] - 1 > right)){
				right = i + d[i];
				left = i - d[i];
			}
		}
		else{
			j = left + (right - i);
			if ((i - d[j] + 1 <= left)&&(i + d[j] + 1 >= right)){
				d[i] = right - i;
				while (((int)i - (int)d[i] >= 0)&&(i + d[i] < len)&&(str[i - d[i]] == str[i + d[i]]))
					++d[i];
				if ((i < len - d[i] + 1)&&(i + d[i] - 1 > right)){
					right = i + d[i];
					left = i - d[i];
				}			
			}
			else
				d[i] = d[j];
		}
	}
	flag = 0;
	ind = 0;
	size = d[ind];
	for (i = 1; i < len; ++i)
		if (d[ind] < d[i]){
			ind = i;
			size = d[ind];
		}
	d[0] = 0;
	left = 0;
	right = 0;
	for (i = 1; i < len; ++i){
		if (i > right){
			d[i] = 0;
			while (((int)i - (int)d[i] - 1 >= 0)&&(i + d[i] < len)&&(str[i - d[i] - 1] == str[i + d[i]]))
				++d[i];
			if ((d[i])&&(i + d[i] - 1 > right)){
				right = i + d[i] - 1;
				left = i - d[i];
			}
		}
		else{
			j = left + (right - i) + 1;
			if (i + d[j] + 1 >= right){
				d[i] = right - i;
				while (((int)i - (int)d[i] - 1 >= 0)&&(i + d[i] < len)&&(str[i - d[i] - 1] == str[i + d[i]]))
					++d[i];
				if ((d[i])&&(i + d[i] - 1 > right)){
					right = i + d[i] - 1;
					left = i - d[i];
				}
			}
			else
				d[i] = d[j];
		}
	}
	for (i = 0; i < len; ++i)
		if ((d[i])&&(2*d[i] > 2*size - 1)){
			ind = i;
			size = d[i];
			flag = 1;
		}
	if ((fp = fopen("output.txt", "wt")) == NULL)
		fp = stdout;
	for (i = ind - size + (!flag); i < ind + size; ++ i)
		fprintf(fp, "%c", str[i]);
	free(d);
	free(str);
	return ERR_NORM;
}