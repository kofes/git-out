#include <stdio.h>
#include <math.h>

#define eps 1e-8
char ans_quad(const double a, const double b, const double c, double *x1, double *x2)
{
	double D;
	if (a == 0) {
		if (b == 0) {
			if (c == 0)
				return -1;
			else
				return 0;
		}
		*x1 = -c/b;
		return 1;
	}
	D = (b*b - 4*a*c);
	if (fabs(D) < eps) {
		*x1 = -b/(2*a);
		*x2 = *x1;
		return 1;
	}
	if (D > 0) {
		D = sqrt(D);
		*x1 = (-b + D)/(2*a);
		*x2 = -(b + D)/(2*a);
		return 2;
	}
	D = sqrt(fabs(D));
	*x1 = -b/(2*a);
	*x2 = (D/(2*a));
	return -2;
}
char main(){

	FILE *fp;
	unsigned char i, info;
	double x[3], y[3], u[3], v[3], a, b, c;
	double t1, t2;

	if ((fp = fopen("input.txt", "rt")) == NULL)
		return 1;

	for (i = 0; ((i<3)&&(!feof(fp))); ++i){
		fscanf(fp, "%lf", &x[i]);
		fscanf(fp, "%lf", &y[i]);
		fscanf(fp, "%lf", &u[i]);
		fscanf(fp, "%lf", &v[i]);
	}

	for (i = 1; i<3; ++i){
		x[i] -= x[0];
		y[i] -= y[0];
		u[i] -= u[0];
		v[i] -= v[0];
	}
	fclose(fp);
	if ((fp = fopen("output.txt", "wt")) == NULL)
		return 1;

	a = u[2]*v[1] - u[1]*v[2];
	b = x[2]*v[1] + y[1]*u[2] - x[1]*v[2] - y[2]*u[1];
	c = x[2]*y[1] - x[1]*y[2];

    info = ans_quad(a, b, c, &t1, &t2) + 2;
    printf("%.5f\n %.5f\n", t1, t2);
    if (info == 4){
        if ((t1 > 0)&&((t2 < 0)||(t2 > t1)))
            fprintf(fp, "%.5f\n", t1);
        else
            fprintf(fp, "%.5f\n", t2);
    }
    else if (info == 3)
        fprintf(fp, "%.5f\n", t1);
        else{
            t1 = -1;
            fprintf(fp, "%.5f\n", t1);
        }

	fclose(fp);
	return 0;
}
