#include "deque.h"

//nearby[0] - to head;
//nearby[1] - to tail;
typedef struct t_node{
	struct t_node *nearby[2];
	t_deque_elem elem;
}t_node;

//top[0] - head;
//top[1] - tail;
typedef struct{
	t_node *top[2];
	size_t height;
}t_unvoid_deque;

t_deque deque_new(){

	t_unvoid_deque *deque;

	if ((deque = (t_unvoid_deque *)malloc(sizeof(t_unvoid_deque))) == NULL)
		return NULL;

	deque->top[0] = NULL;
	deque->top[1] = NULL;
	deque->height = 0;

	return (t_deque *)deque;
}

void deque_del(t_deque deque){

	t_node *curr, *node;

	t_unvoid_deque *udeque = (t_unvoid_deque *)deque;

	if ((udeque == NULL)||(udeque->height == 0))
		return;

	curr = udeque->top[0];

	while (curr != NULL){
		node = curr;
		curr = curr->nearby[1];
		free(node);
	}

	udeque->top[0] = NULL;
	udeque->top[1] = NULL;
	udeque->height = 0;
}

size_t deque_hht(t_deque deque){

	t_unvoid_deque *udeque = (t_unvoid_deque *)deque;

	if (udeque == NULL)
		return 0;
	return udeque->height;
}

t_deque_elem *deque_add_elem(t_deque deque, int type){

	t_node *node;
	t_unvoid_deque *udeque = (t_unvoid_deque *)deque;

	if ((udeque == NULL)||(abs(type % 2) != type))
	 	return NULL;

	if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
	 	return NULL;

	node->nearby[(type+1)%2] = udeque->top[type];
	node->nearby[type] = NULL;
	if (udeque->top[type] != NULL)
		udeque->top[type]->nearby[type] = node;
	else
		udeque->top[(type+1)%2] = node;
	udeque->top[type] = node;
	++(udeque->height);

	return &(udeque->top[type]->elem);
}

t_deque_elem *deque_get_elem(t_deque deque, int type){

	t_unvoid_deque *udeque = (t_unvoid_deque *)deque;

	if ((udeque == NULL)||(udeque->height == 0)||(abs(type % 2) != type))
		return NULL;

	return &(udeque->top[type]->elem);
}

void deque_del_elem(t_deque deque, int type){

	t_node *node;
	t_unvoid_deque *udeque = (t_unvoid_deque *)deque;

	if ((udeque == NULL)||(udeque->height == 0)||(abs(type % 2) != type))
		return;

	node = udeque->top[type];
	udeque->top[type] = node->nearby[(type+1)%2];
	free(node);
	if (udeque->top[type] != NULL)
		udeque->top[type]->nearby[type] = NULL;
	else
		udeque->top[(type+1)%2] = NULL;
	--(udeque->height);
}

void deque_out(t_deque deque, FILE *fout, int type){

	t_node *curr;
	t_unvoid_deque *udeque = (t_unvoid_deque *)deque;

	if ((udeque == NULL)||(fout == NULL)||(abs(type % 2) != type))
		return;

	curr = udeque->top[type];

	while (curr != NULL){
		fprintf(fout, "%i ", curr->elem);
		curr = curr->nearby[(type+1)%2];
	}
}
