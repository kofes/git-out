#include "stack.h"

typedef struct t_node{
	struct t_node* next;
	t_stack_elem elem;
}t_node;

typedef struct{
	t_node *head;
	size_t height;
}t_unvoid_stack;

t_stack stack_new(){

	t_unvoid_stack *stack;

	if ((stack = (t_unvoid_stack *)malloc(sizeof(t_unvoid_stack))) == NULL)
		return NULL;
	stack->height = 0;
	stack->head = NULL;

	return (t_stack *)stack;
}

void stack_del(t_stack stack){

	t_node *curr, *node;

	if (stack == NULL)
		return;
	curr = ((t_unvoid_stack *)stack)->head;

	while (curr != NULL){
		node = curr;
		curr = curr->next;
		free(node);
	}
	((t_unvoid_stack *)stack)->head = NULL;
	((t_unvoid_stack *)stack)->height = 0;
}

size_t stack_hht(t_stack stack){

	if (stack == NULL)
		return 0;

	return ((t_unvoid_stack *)stack)->height;
}

t_stack_elem *stack_add_elem(t_stack stack){

	t_node *node;

	if (stack == NULL)
		return NULL;
	if ((node = (t_node *)malloc(sizeof(t_node))) == NULL)
		return NULL;
	node->next = ((t_unvoid_stack *)stack)->head;
	((t_unvoid_stack *)stack)->head = node;
	++((t_unvoid_stack *)stack)->height;

	return &(node->elem);
}

t_stack_elem *stack_get_elem(t_stack stack){

	if ((stack == NULL)||(((t_unvoid_stack *)stack)->head == NULL))
		return NULL;

	return &(((t_unvoid_stack *)stack)->head->elem);
}

void stack_del_elem(t_stack stack){

	t_node *node;

	if ((stack == NULL)||((t_unvoid_stack *)stack)->head == NULL)
		return;

	node = ((t_unvoid_stack *)stack)->head;
	((t_unvoid_stack *)stack)->head = node->next;
	free(node);
	--((t_unvoid_stack *)stack)->height;
}

void stack_out(t_stack stack, FILE *fout){

	t_node *curr;

	if ((stack == NULL)||(fout == NULL))
		return;

	curr = ((t_unvoid_stack *)stack)->head;

	while (curr != NULL){
		fprintf(fout, "%i ", curr->elem);
		curr = curr->next;
	}
}