#ifndef __INCLUDE_DEQUE_H
#define __INCLUDE_DEQUE_H

#include <stdio.h>
#include <stdlib.h>

//Тип значения:
typedef int t_deque_elem;

//Тип дескриптора стэка:
typedef void *t_deque;

//Признак открепленного дескриптора:
#define DEQUE_NULL (NULL)

//Возможные типы выполнения функций:
#define DEQUE_HEAD (0)	//Операции выполняются с начала дека;
#define DEQUE_TAIL (1)	//Операции выполняются с конца дека;

//Генерирует пустой дек:
extern t_deque deque_new();

//Освобождает память из-под дека:
extern void deque_del(t_deque deque);

//Выводит высоту стэка:
extern size_t deque_hht(t_deque deque);

//Выполняет вставку элемента в дек:
extern t_deque_elem *deque_add_elem(t_deque deque, int type);

//Выводит элемент из дека:
extern t_deque_elem *deque_get_elem(t_deque deque, int type);

//Выполняет удаление элемента из дека:
extern void deque_del_elem(t_deque deque, int type);

//Выводит содержимое дека в выходной поток:
extern void deque_out(t_deque deque, FILE *fout, int type);

#endif //__INCLUDE_DEQUE_H
