#ifndef __INCLUDE_STACK_H
#define __INCLUDE_STACK_H

#include <stdio.h>
#include <stdlib.h>

//Тип значения:
typedef int t_stack_elem;

//Тип дескриптора стэка:
typedef void *t_stack;

//Признак открепленного дескриптора:
#define STACK_NULL (NULL)

//Генерирует пустой стек:
extern t_stack stack_new();

//Освобождает память из-под стека:
extern void stack_del(t_stack stack);

//Выводит высоту стэка:
extern size_t stack_hht(t_stack stack);

//Выполняет вставку элемента в вершину стека:
extern t_stack_elem *stack_add_elem(t_stack stack);

//Выводит элемент с вершины стека:
extern t_stack_elem *stack_get_elem(t_stack stack);

//Выполняет удаление элемента с вершины стека:
extern void stack_del_elem(t_stack stack);

//Выводит содержимое стека в выходной поток:
extern void stack_out(t_stack stack, FILE *fout);

#endif //__INCLUDE_STACK_H
