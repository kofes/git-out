unit Stack_unit;

interface
type
	new_e = dword;
    p_node = ^t_node;
    t_node = record
    	elem : new_e;
    	next : p_node;
    end;
	p_list = class
		private
	    	head: p_node;
			h: word;
		public
			constructor in_it();
			function push_head (e:new_e):boolean;
			function pop_head (): boolean;
			function list ():boolean;
			function list_head (): boolean;
			procedure max_power (const power: word);
			destructor del_it();		
	end;
implementation
 	constructor p_list.in_it();
	begin
		//new(Result);
		head:=nil;
		h:=0;
	end;
	function p_list.push_head(e: new_e):boolean;
	var
		node: p_node;
	begin
		Result:=false;
		new(node);
		if node=nil then
			begin
				Result:=true;
				exit;
			end;
		node^.elem:=e;
		node^.next:=head;
		head:=node;
		inc(h);
	end;
	function p_list.pop_head():boolean;
	var
		node: p_node;
	begin
		Result:=false;
		node:=head;
		if node=nil then
			begin
				Result:=true;
				exit;
			end;
		head:=node^.next;
		Dispose(node);
		dec(h);
	end;
	function p_list.list ():boolean;
	var
		curr: p_node;
	begin
		Result:=false;
		curr:=head;
		if curr=nil then
			begin
				Result:=true;
				exit;
			end;
		while curr<>nil do
			begin
				writeln(curr^.elem);
				curr:=curr^.next;
			end;
	end;
	function p_list.list_head ():boolean;
	var
		curr: p_node;	
	begin
		Result:=false;
		curr:=head;
		if curr=nil then
			begin
				Result:=true;
				exit;
			end;
		writeln(curr^.elem);
	end;
{
	Normal:
		while n>0 do
			begin
				if n mod 2 = 1 then
					step:=step*x;
				x:=x*x;
				n:= n div 2;
			end;
}
	procedure p_list.max_power (const power: word);
	var
		curr, note: p_node;
		n, k : word;
	begin
		curr:=head;
		note:=head;//<-записываем сюда! Нужно два списка
		n:=power;
		k:=1;
		while (n>0) do
			begin
				if (n mod 2=1) then
					begin
						...
					end;
				n:= n div 2;
				while curr<>nil do
					begin
						curr^.elem*=note^.elem*k;
						note:=note^.next;
						if note=nil then curr:=curr^.next;
						k*=10;
					end;
			end;
		curr:=head;
		while note<>nil do
			begin
				curr^.elem:=note^.elem;
				if (curr^.next=nil) then
					begin
						new(node);
						curr^.next:=node;
						node^.next:=nil;
					end;
				curr:=curr^.next;
				note:=note^.next;
			end;
		dispose(curr);
	end;
	destructor p_list.del_it();
	var
		current, node: p_node;
	begin
		current:=head;
		while current<>nil do
			begin
				node:=current;
				current:=current^.next;
				Dispose(node);
			end;
		head:=nil;
		h:=0;
	end;
end.