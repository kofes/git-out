program DEC;

type
    Tfile = Text;
    new_e = string;
    p_node = ^t_node;
    t_node = record
    	elem : new_e;
      last, next : p_node;
    end;
    t_list = record
      head, tail, current : p_node;
    end;
function push_tail(var s: t_list; e: new_e): boolean; forward;
procedure Del_list(var s: t_list); forward;
procedure in_it(var s : t_list);
begin
	 s.head:=nil;
   s.tail:=nil;
   s.current:=nil;
end;
function out_list(var s: t_list; line: string): boolean;
var
  F: Tfile;
  elem: new_e;
begin
  Result:=false;
  if s.head <> nil then
    Del_list(s);
  assign(F, line);//<---Нужно проверить есть ли файл!
  reset(F);
  while not(EOF(F)) do
    begin
      readln(F, elem);
      if (elem='') or (elem=' ') then continue;
      Result:=push_tail(s, elem);
      if Result=true then
        break;
    end;
  close(F);
end;
function push_head (var s : t_list; e:new_e):boolean;
var
  node, nop : p_node;
begin
  Result:=false;
  new(node);
  if node=nil then
  	begin
      Result:=true;
      exit;
    end;
  node^.elem:=e;
  node^.next:=s.head;
  node^.last:=nil;
  nop:=s.head;
  if nop<>nil then
  	nop^.last:=node;
  if s.tail=nil then
    s.tail:=node;
  s.head:=node;
  s.current:=node;
end;
function push_tail (var s : t_list; e:new_e):boolean;
var
  node, nop : p_node;
begin
  Result:=false;
  new(node);
  if node=nil then
    begin
      Result:=true;
      exit;
    end;
  node^.elem:=e;
  node^.last:=s.tail;
  node^.next:=nil;
  nop:=s.tail;
  if nop<>nil then
    nop^.next:=node;
  if s.head=nil then
    s.head:=node;
  s.tail:=node;
  s.current:=node;
end;
function push_after (var s: t_list; cur_elem, new_elem: new_e): boolean;
var
  node, search, n_node : p_node;
begin
  Result:=false;
  new(node);
  if node=nil then
  	begin
      Result:=true;
  	 	exit;
    end;
  node^.elem:=new_elem;
  search:=s.head;
  while (search<>nil) and (search^.elem<>cur_elem) do
    search:=search^.next;
  if search=nil then
    begin
      writeln('Нет такого элемента...');
      dispose(node);
      exit;
    end;
  if search=s.tail then
    begin
      Dispose(node);
      Result:=push_tail(s, new_elem);
      exit;
    end;
  n_node:=search^.next;
  if n_node<>nil then
  	n_node^.last:=node;
  node^.next:=search^.next;
  node^.last:=search;
  search^.next:=node;
  s.current:=node;
end;
function push_before (var s: t_list; cur_elem, new_elem: new_e): boolean;
var
  node, search, l_node : p_node;
begin
  Result:=false;
  new(node);
  if node=nil then
  	begin
      Result:=true;
  	 	exit;
    end;
  node^.elem:=new_elem;
  search:=s.head;
  while (search<>nil) and (search^.elem<>cur_elem) do
    search:=search^.next;
  if search=nil then
    begin
      writeln('Нет такого элемента...');
      dispose(node);
      exit;
    end;
  if search=s.head then
    begin
      Dispose(node);
      Result:=push_head(s, new_elem);
      exit;
    end;
  l_node:=search^.last;
  if l_node<>nil then
  	l_node^.next:=node;
  node^.last:=search^.last;
  node^.next:=search;
  search^.last:=node;
  s.current:=node;
end;
function move_head(var s: t_list; elem: new_e): boolean;
var
  curr, node, nel: p_node;
begin
  node:=s.head;
  while (node<>nil) and (node^.elem<>elem) do
    node:=node^.next;
  if node=nil then
    begin
      Result:=true;
      exit;
    end;
  if node=s.head then
    exit;
  if node=s.tail then
    begin
    curr:=node^.last;
    curr^.next:=nil;
    s.tail:=curr;
    end;
  nel:=node^.last;//<-
  if nel<>nil then
  nel^.next:=node^.next;
  nel:=node^.next;
  if nel<>nil then
  nel^.last:=node^.last;//<-
  curr:=s.head;
  node^.next:=s.head;
  node^.last:=nil;
  curr^.last:=node;
  s.head:=node;
end;
function move_tail(var s: t_list; elem: new_e): boolean;
var
  curr, node, nel: p_node;
begin
  Result:=false;
  node:=s.head;
  while (node<>nil) and (node^.elem<>elem) do
    node:=node^.next;
  if node=nil then
    begin
      Result:=true;
      exit;
    end;
  if node=s.tail then
    exit;
  if node=s.head then
    begin
    curr:=node^.next;
    curr^.last:=nil;
    s.tail:=curr;
    end;
  nel:=node^.last;//<-
  if nel<>nil then
  nel^.next:=node^.next;
  nel:=node^.next;
  if nel<>nil then
  nel^.last:=node^.last;//<-
  curr:=s.tail;
  node^.last:=s.tail;
  node^.next:=nil;
  curr^.next:=node;
  s.tail:=node;
end;
function move_after(var s: t_list; cu_elem, elem: new_e): boolean;
var
  curr, node, nel: p_node;
begin
  Result:=false;
  curr:=s.head;
  node:=s.head;
  while (curr<>nil) and (curr^.elem<>cu_elem) do
    curr:=curr^.next;
  if curr=nil then
    begin
      Result:=true;
      exit;
    end;
  if curr=s.tail then
    begin
      Result:=move_tail(s, elem);
      exit;
    end;
  while (node<>nil) and (node^.elem<>elem) do
    node:=node^.next;
  if node=nil then
    begin
      Result:=true;
      exit;
    end;
  if node=s.tail then
    begin
      nel:=node^.last;
      nel^.next:=nil;
      s.tail:=nel;
    end;
  if node=s.head then
    begin
      nel:=node^.next;
      nel^.last:=nil;
      s.head:=nel;
    end;
  nel:=node^.last;//<-
  if nel<>nil then
  nel^.next:=node^.next;
  nel:=node^.next;
  if nel<>nil then
  nel^.last:=node^.last;//<-
  nel:=curr^.next;
  nel^.last:=node;
  curr^.next:=node;
  node^.next:=curr;
  node^.next:=nel;
end;
function move_before(var s: t_list; cu_elem, elem: new_e): boolean;
var
  curr, node, nel: p_node;
begin
  Result:=false;
  curr:=s.head;
  node:=s.head;
  while (curr<>nil) and (curr^.elem<>cu_elem) do
    curr:=curr^.next;
  if curr=nil then
    begin
      Result:=true;
      exit;
    end;
  if curr=s.head then
    begin
      Result:=move_head(s, elem);
      exit;
    end;
  while (node<>nil) and (node^.elem<>elem) do
    node:=node^.next;
  if node=nil then
    begin
      Result:=true;
      exit;
    end;
  if node=s.tail then
    begin
      nel:=node^.last;
      nel^.next:=nil;
      s.tail:=nel;
    end;
  if node=s.head then
    begin
      nel:=node^.next;
      nel^.last:=nil;
      s.head:=nel;
    end;
  nel:=node^.last;//<-
  if nel<>nil then
  nel^.next:=node^.next;
  nel:=node^.next;
  if nel<>nil then
  nel^.last:=node^.last;//<-
  nel:=curr^.last;
  nel^.next:=node;
  curr^.last:=node;
  node^.next:=curr;
  node^.last:=nel;
end;
function mark_curr(var s: t_list; e: new_e): boolean;
var
  curr: p_node;
begin
  Result:=false;
  curr:=s.head;
  while (curr<>nil) and (curr^.elem<>e) do
    curr:=curr^.next;
  if curr=nil then
    begin
      Result:=true;
      exit;
    end;
  s.current:=curr;
end;
function pop_head (var s:t_list): boolean;
var
  node, nop : p_node;
begin
  Result:=false;
  if (s.head = nil) then
    begin
      Result:=true;
      exit;
    end;
  if s.current=s.head then
    s.current:=nil;
  node:=s.head;
  s.head:=node^.next;
  if node = s.tail then
    s.tail:=nil;
  nop:=s.head;
  if s.head<>nil then
    nop^.last:=nil;
  dispose(node);
end;
function pop_tail (var s:t_list): boolean;
var
  node, nop : p_node;
begin
  Result:=false;
  if (s.head = nil) then
    begin
      Result:=true;
      exit;
    end;
  if s.current=s.tail then
    s.current:=nil;
  node:=s.tail;
  s.tail:=node^.last;
  if node = s.head then
    s.tail:=nil;
  nop:=s.tail;
  if nop<>nil then
    nop^.next:=nil;
  dispose(node);
end;
procedure del_curr(var s: t_list);
var
  curr, node: p_node;
begin
  curr:=s.current;
  if curr=nil then
    exit;
  if curr = s.head then
    begin
      pop_head(s);
      exit;
    end;
  if curr = s.tail then
    begin
      pop_tail(s);
      exit;
    end;
  node:=curr^.next;
  node^.last:=curr^.last;
  node:=curr^.last;
  node^.next:=curr^.next;
  Dispose(curr);
  s.current:=nil;
end;
function del_mark (var s: t_list; e:new_e):boolean;
var
  curr, s_curr: p_node;
begin
  Result:=false;
  curr:=s.head;
  while (curr<>nil) and (curr^.elem<>e) do
    curr:=curr^.next;
  if curr=nil then
    begin
      Result:=true;
      exit;
    end;
  if curr=s.head then
    begin
      Result:=pop_head(s);
      exit;
    end;
  if curr=s.tail then
    begin
      Result:=pop_tail(s);
      exit;
    end;
  s_curr:=curr^.last;
  if s_curr<>nil then
  	s_curr^.next:=curr^.next;
  s_curr:=curr^.next;
  if s_curr<>nil then
    s_curr^.last:=curr^.last;
  if s.current = curr then
    s.current:=nil;
  Dispose(curr);
end;
function list_curr (var s: t_list): boolean;
var
  curr: p_node;
begin
  writeln;
  Result:=false;
  curr:=s.current;
  if curr=nil then
    begin
      Result:=true;
      exit;
    end;
  writeln(curr^.elem);
end;
function list(var s:t_list):boolean;
var
  curr : p_node;
begin
  Result:=false;
  if s.head=nil then
    begin
      Result:=true;
      exit;
    end;
  writeln;
  curr:=s.head;
  while curr<>nil do
    begin
     	writeln(curr^.elem,' ');
      curr:=curr^.next;
    end;
end;
procedure Msort(var s:t_list);
begin
  
end;
function Save_list(var s: t_list; line: string): boolean;
var
  F: Tfile;
  curr: p_node;
begin
  if s.head=nil then
    begin
      Result:=true;
      exit;
    end;
  assignfile(F, line);
  rewrite(F);
  curr:=s.head;
  while curr<>nil do
    begin
      writeln(F, curr^.elem);
      curr:=curr^.next;
    end;
  close(F);
end;
procedure Del_list(var s:t_list);
var
  node : p_node;
begin
  while s.head<>nil do
     begin
          node:=s.head;
          s.head:=node^.next;
          dispose(node);
     end;
  s.tail:=nil;
  s.current:=nil;
end;
procedure Main();
var
  ch, direct : String;
  elem, cu_elem: new_e;
  note : t_list;
  flag : boolean;
begin
  in_it(note);
  writeln('DEC');
  repeat
    flag:=false;
    writeln('-----------------------------------');
    writeln('1)Считать список с файла;');
    writeln('2)Вставить элемент в начало списка;');//head - начало/корень, tail - конец/хвост.
    writeln('3)Вставить элемент в конец списка;');
    writeln('4)Вставить элемент после заданного;');
    writeln('5)Вставить элемент перед заданным;');
    writeln('6)Переместить элемент в начало списка;');
    writeln('7)Переместить элемент в конец списка;');
    writeln('8)Переместить элемент после заданного;');
    writeln('9)Переместить элемент перед заданным;');
    writeln('10)Установить текущий элемент в списке;');
    writeln('11)Удалить первый элемент в списке;');
    writeln('12)Удалить последний элемент в списке;');
    writeln('13)Удалить последний введенный элемент;');
    writeln('14)Удалить заданный элемент;');
    writeln('15)Вывести текущий элемент списка;');
    writeln('16)Вывести все элементы списка;');
    writeln('17)Отсортировать список;');
    writeln('18)Сохранить элементы списка в файл;');
    writeln('19)Удалить список;');
    writeln('0)Введите "exit" для выхода;');
    writeln('-----------------------------------');
    readln(ch);
    case ch of
      '1':
      begin
        writeln('Введите название файла и его директорию');
        readln(direct);
        flag:=out_list(note, direct);
      end;
      '2':
      begin
        writeln('Введите элемент:');
        readln(elem);
        flag:=push_head(note, elem);
      end;
      '3':
      begin
        writeln('Введите элемент:');
        readln(elem);
        flag:=push_tail(note, elem);
      end;
      '4':
      begin
        writeln('Введите элемент, после которого нужно записать:');
        readln(cu_elem);
        writeln('Введите записываемый элемент:');
        readln(elem);
        flag:=push_after(note, cu_elem, elem);
      end;
      '5':
      begin
        writeln('Введите элемент, перед которым нужно записать:');
        readln(cu_elem);
        writeln('Введите записываемый элемент:');
        readln(elem);
        flag:=push_before(note, cu_elem, elem);
      end;
      '6':
      begin
        writeln('Введите элемент, который нужно переставить');
        readln(elem);
        flag:=move_head(note, elem);
      end;
      '7':
      begin
        writeln('Введите элемент, который нужно переставить');
        readln(elem);
        flag:=move_tail(note, elem);
      end;
      '8':
      begin
        writeln('Введите элемент, после которого нужно вставить:');
        readln(cu_elem);
        writeln('Введите элемент, который нужно переставить');
        readln(elem);
        flag:=move_after(note, cu_elem, elem);
      end;
      '9':
      begin
        writeln('Введите элемент, перед которым нужно вставить:');
        readln(cu_elem);
        writeln('Введите элемент, который нужно переставить');
        readln(elem);
        flag:=move_before(note, cu_elem, elem);
      end;
      '10':
      begin
      writeln('Введите значение этого элемента');
      readln(elem);
      flag:=mark_curr(note, elem);
      end;
      '11': flag:=pop_head(note);
      '12': flag:=pop_tail(note);
      '13': del_curr(note);
      '14':
      begin
        writeln('Введите элемент, который нужно удалить:');
        readln(elem);
        flag:=del_mark(note, elem);
      end;
      '15': flag:=list_curr(note);
      '16': flag:=list(note);
      '17': writeln('Технические работы');//Msort(note);
      '18':
      begin
        writeln('Введите название файла и название его директории;');
        readln(direct);
        flag:=Save_list(note, direct);
      end;
      '19': Del_list(note);
    end;
    if flag then writeln('Ошибка :/');
  until ch='0';
end;
begin
  Main();
end.