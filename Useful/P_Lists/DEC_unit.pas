unit DEC_unit;

interface

  uses
    SysUtils;
	type
	    T_file = Text;
	    new_e = string;
	    p_node = ^t_node;
	    t_node = record
	    	elem : new_e;
	      last, next : p_node;
	    end;
	    p_list = class
      private
	      head, tail, current : p_node;
        h: dword;
      public
      	constructor in_it ();
        procedure get_new (var first, second: p_list);
        function equal_list (var first, second: p_list): boolean;
      	function out_list (line: string): boolean;
      	function push_head (e:new_e):boolean;
      	function push_tail (e:new_e):boolean;
      	function push_after (cur_elem, new_elem: new_e): boolean;
      	function push_before (cur_elem, new_elem: new_e): boolean;
      	function move_head (elem: new_e): boolean;
      	function move_tail (elem: new_e): boolean;
      	function move_after (cu_elem, elem: new_e): boolean;
      	function move_before (cu_elem, elem: new_e): boolean;
      	function mark_curr (e: new_e): boolean;
      	function pop_head (): boolean;
      	function pop_tail (): boolean;
      	procedure del_curr ();
      	function del_mark (e:new_e):boolean;
      	function list_curr (): boolean;
      	function list ():boolean;
        function Sort ():boolean;
      	function Save_list (line: string): boolean;
      	destructor Del_it ();
      protected
        function get_head: p_node;
      end;
      	procedure Main (var p: p_list);
implementation
  procedure sl_list (var first, second, head: p_node); forward;
  function p_list.get_head: p_node;
  begin
    Result:=head;
  end;
  function p_list.equal_list(var first, second: p_list): boolean;
  var
    head1, head2, node: p_node;
  begin
    Result:=false;
    if head<>nil then
      begin
        current:=head;
        while current<>nil do
          begin
            node:=current;
            current:=current^.next;
            dispose(node);
          end;
      end;
    h:=0;
    head:=nil;
    tail:=nil;
    first.sort;
    second.sort;
    head1:=first.get_head;
    head2:=second.get_head;
    while (head1<>nil) and (head2<>nil) do
      begin
        if head1^.elem<head2^.elem then
          begin
            head1:=head1^.next;
            continue;
          end;
        if head1^.elem>head2^.elem then
          begin
            head2:=head2^.next;
            continue;
          end;
        if head1^.elem=head2^.elem then
          begin
            new(node);
            if node=nil then
              begin
                Result:=true;
                exit;
              end;
            node^.elem:=head1^.elem;
            h+=1;
            if head=nil then
              begin
                head:=node;
                tail:=node;
                continue;
              end
            else
              head^.last:=node;
            node^.next:=head;
            head:=node;
          end;
      end;
  end;
  procedure p_list.get_new(var first, second: p_list);
  var
    head1, head2, node: p_node;
  begin
    if head<>nil then
      begin
        current:=head;
        while current<>nil do
          begin
            node:=current;
            current:=current^.next;
            dispose(node);
          end;
      end;
    h:=0;
    head:=nil;
    tail:=nil;
    head1:=first.get_head;
    head2:=second.get_head;
    sl_list(head1, head2, head);
    current:=head;
    current^.last:=nil;
    while current^.next<>nil do
      begin
        node:=current;
        current:=current^.next;
        current^.last:=node;
        h+=1;
      end;
    h+=1;
    tail:=current;
  end;
  procedure sl_list (var first, second, head: p_node);
    var
      p1, p2, s1: p_node; 
  begin 
    if first = nil then
      head := second
    else
      if second = nil then
        head := first
      else
        begin
          p1 := first;
          p2 := second;
          if p1^.elem < p2^.elem then  
            begin
              head := p1;
              p1 := p1^.next
            end
          else
            begin
              head := p2;
              p2 := p2^.next
            end;
          s1 := head;
          while (p1 <> nil) and (p2 <> nil) do
            begin 
              if p1^.elem < p2^.elem then  
                begin
                  s1^.next:=p1;
                  p1:=p1^.next
                end
              else 
                begin
                  s1^.next:=p2;
                  p2:=p2^.next
                end;
              s1:=s1^.next
            end;
          if p1<>nil then
            s1^.next:=p1;
          if p2<>nil then
            s1^.next:=p2;
        end;
      first:=nil;
      second:=nil;
  end;
  procedure m_sort(var head: p_node; x: word);
  var
    middle: word;
    node: p_node;
    p1, p2: p_node;
    i: word;
  begin
    if head=nil then exit;
    p1:=head;
    p2:=head;
    if (x = 1) then
      begin
        head^.next:=nil;
        exit;
      end;
    if (x = 2) then
      begin
        p2:=p1^.next;
        p2^.next:=nil;
        head:=p1;
        if p1^.elem>p2^.elem then
          begin
            p2^.next:=p1;
            p1^.next:=nil;
            head:=p2;
          end;
        exit;
      end;
    middle:=(x div 2);
    i:=0;
    while i<middle do
      begin
        node:=p2;
        p2:=p2^.next;
        i+=1;
      end;
    node^.next:=nil;
    m_sort(p1, middle);
    m_sort(p2, x-middle);
    sl_list(p1, p2, head);
  end;
  function p_list.sort: boolean;
  var
    node: p_node;
  begin
    Result:=false;
    if (head = nil) or (h = 0) then
      begin
        Result:=true;
        exit;
      end;
    m_sort(head, h);
    current:=head;
    current^.last:=nil;
    while current^.next<>nil do
      begin
        node:=current;
        current:=current^.next;
        current^.last:=node;
      end;
    tail:=current;
  end;
  constructor p_list.in_it();
  begin
    head:=nil;
    tail:=nil;
    current:=nil;
    h:=0;
  end;

  function p_list.out_list(line: string): boolean;
  var
  	F: T_file;
  	elem: new_e;
  begin
  	Result:=false;
    Result:=FileExists(line);
    if Result=false then
      begin
        Result:=true;
        exit;
      end;
    assign(F, line);
  	reset(F);
  	while not(EOF(F)) do
  	begin
  		readln(F, elem);
  		Result:=push_tail(elem);
  		if Result=true then
  		break;
      end;
  	close(F);
  end;
  function p_list.push_head(e:new_e):boolean;
  var
  	node, nop : p_node;
  begin
  	Result:=false;
  	new(node);
  	if node=nil then
  	begin
  		Result:=true;
  		exit;
  	end;
  	node^.elem:=e;
  	node^.next:=head;
  	node^.last:=nil;
  	nop:=head;
  	if nop<>nil then
  	 nop^.last:=node;
  	if tail=nil then
  	 tail:=node;
  	head:=node;
  	current:=node;
    inc(h);
  end;
  function p_list.push_tail(e:new_e):boolean;
  var
    node, nop : p_node;
  begin
    Result:=false;
    new(node);
    if node=nil then
      begin
        Result:=true;
        exit;
      end;
    node^.elem:=e;
    node^.last:=tail;
    node^.next:=nil;
    nop:=tail;
    if nop<>nil then
      nop^.next:=node;
    if head=nil then
      head:=node;
    tail:=node;
    current:=node;
    inc(h);
  end;
  function p_list.push_after(cur_elem, new_elem: new_e): boolean;
  var
    node, search, n_node : p_node;
  begin
    Result:=false;
    new(node);
    if node=nil then
    	begin
        Result:=true;
    	 	exit;
      end;
    node^.elem:=new_elem;
    search:=head;
    while (search<>nil) and (search^.elem<>cur_elem) do
      search:=search^.next;
    if search=nil then
      begin
        writeln('Нет такого элемента...');
        dispose(node);
        exit;
      end;
    if search=tail then
      begin
        Dispose(node);
        Result:=push_tail(new_elem);
        exit;
      end;
    n_node:=search^.next;
    if n_node<>nil then
    	n_node^.last:=node;
    node^.next:=search^.next;
    node^.last:=search;
    search^.next:=node;
    current:=node;
    inc(h);
  end;
  function p_list.push_before(cur_elem, new_elem: new_e): boolean;
  var
    node, search, l_node : p_node;
  begin
    Result:=false;
    new(node);
    if node=nil then
    	begin
        Result:=true;
    	 	exit;
      end;
    node^.elem:=new_elem;
    search:=head;
    while (search<>nil) and (search^.elem<>cur_elem) do
      search:=search^.next;
    if search=nil then
      begin
        writeln('Нет такого элемента...');
        dispose(node);
        exit;
      end;
    if search=head then
      begin
        Dispose(node);
        Result:=push_head(new_elem);
        exit;
      end;
    l_node:=search^.last;
    if l_node<>nil then
    	l_node^.next:=node;
    node^.last:=search^.last;
    node^.next:=search;
    search^.last:=node;
    current:=node;
    inc(h);
  end;
  function p_list.move_head(elem: new_e): boolean;
  var
    curr, node, nel: p_node;
  begin
    node:=head;
    while (node<>nil) and (node^.elem<>elem) do
      node:=node^.next;
    if node=nil then
      begin
        Result:=true;
        exit;
      end;
    if node=head then
      exit;
    if node=tail then
      begin
      curr:=node^.last;
      curr^.next:=nil;
      tail:=curr;
      end;
    nel:=node^.last;//<-
    if nel<>nil then
    nel^.next:=node^.next;
    nel:=node^.next;
    if nel<>nil then
    nel^.last:=node^.last;//<-
    curr:=head;
    node^.next:=head;
    node^.last:=nil;
    curr^.last:=node;
    head:=node;
  end;
  function p_list.move_tail(elem: new_e): boolean;
  var
    curr, node, nel: p_node;
  begin
    Result:=false;
    node:=head;
    while (node<>nil) and (node^.elem<>elem) do
      node:=node^.next;
    if node=nil then
      begin
        Result:=true;
        exit;
      end;
    if node=tail then
      exit;
    if node=head then
      begin
      curr:=node^.next;
      curr^.last:=nil;
      tail:=curr;
      end;
    nel:=node^.last;//<-
    if nel<>nil then
    nel^.next:=node^.next;
    nel:=node^.next;
    if nel<>nil then
    nel^.last:=node^.last;//<-
    curr:=tail;
    node^.last:=tail;
    node^.next:=nil;
    curr^.next:=node;
    tail:=node;
  end;
  function p_list.move_after(cu_elem, elem: new_e): boolean;
  var
    curr, node, nel: p_node;
  begin
    Result:=false;
    curr:=head;
    node:=head;
    while (curr<>nil) and (curr^.elem<>cu_elem) do
      curr:=curr^.next;
    if curr=nil then
      begin
        Result:=true;
        exit;
      end;
    if curr=tail then
      begin
        Result:=move_tail(elem);
        exit;
      end;
    while (node<>nil) and (node^.elem<>elem) do
      node:=node^.next;
    if node=nil then
      begin
        Result:=true;
        exit;
      end;
    if node=tail then
      begin
        nel:=node^.last;
        nel^.next:=nil;
        tail:=nel;
      end;
    if node=head then
      begin
        nel:=node^.next;
        nel^.last:=nil;
        head:=nel;
      end;
    nel:=node^.last;//<-
    if nel<>nil then
    nel^.next:=node^.next;
    nel:=node^.next;
    if nel<>nil then
    nel^.last:=node^.last;//<-
    nel:=curr^.next;
    nel^.last:=node;
    curr^.next:=node;
    node^.next:=curr;
    node^.next:=nel;
  end;
  function p_list.move_before(cu_elem, elem: new_e): boolean;
  var
    curr, node, nel: p_node;
  begin
    Result:=false;
    curr:=head;
    node:=head;
    while (curr<>nil) and (curr^.elem<>cu_elem) do
      curr:=curr^.next;
    if curr=nil then
      begin
        Result:=true;
        exit;
      end;
    if curr=head then
      begin
        Result:=move_head(elem);
        exit;
      end;
    while (node<>nil) and (node^.elem<>elem) do
      node:=node^.next;
    if node=nil then
      begin
        Result:=true;
        exit;
      end;
    if node=tail then
      begin
        nel:=node^.last;
        nel^.next:=nil;
        tail:=nel;
      end;
    if node=head then
      begin
        nel:=node^.next;
        nel^.last:=nil;
        head:=nel;
      end;
    nel:=node^.last;//<-
    if nel<>nil then
    nel^.next:=node^.next;
    nel:=node^.next;
    if nel<>nil then
    nel^.last:=node^.last;//<-
    nel:=curr^.last;
    nel^.next:=node;
    curr^.last:=node;
    node^.next:=curr;
    node^.last:=nel;
  end;
  function p_list.mark_curr(e: new_e): boolean;
  var
    curr: p_node;
  begin
    Result:=false;
    curr:=head;
    while (curr<>nil) and (curr^.elem<>e) do
      curr:=curr^.next;
    if curr=nil then
      begin
        Result:=true;
        exit;
      end;
    current:=curr;
  end;
  function p_list.pop_head(): boolean;
  var
    node, nop : p_node;
  begin
    Result:=false;
    if (head = nil) then
      begin
        Result:=true;
        exit;
      end;
    if current=head then
      current:=nil;
    node:=head;
    head:=node^.next;
    if node = tail then
      tail:=nil;
    nop:=head;
    if head<>nil then
      nop^.last:=nil;
    dispose(node);
    dec(h);
  end;
  function p_list.pop_tail(): boolean;
  var
    node, nop : p_node;
  begin
    Result:=false;
    if (head = nil) then
      begin
        Result:=true;
        exit;
      end;
    if current=tail then
      current:=nil;
    node:=tail;
    tail:=node^.last;
    if node = head then
      head:=nil;
    nop:=tail;
    if nop<>nil then
      nop^.next:=nil;
    dispose(node);
    dec(h);
  end;
  procedure p_list.del_curr();
  var
    curr, node: p_node;
  begin
    curr:=current;
    if curr=nil then
      exit;
    if curr = head then
      begin
        pop_head();
        exit;
      end;
    if curr = tail then
      begin
        pop_tail();
        exit;
      end;
    node:=curr^.next;
    node^.last:=curr^.last;
    node:=curr^.last;
    node^.next:=curr^.next;
    Dispose(curr);
    current:=nil;
    dec(h);
  end;
  function p_list.del_mark(e:new_e):boolean;
  var
    curr, s_curr: p_node;
  begin
    Result:=false;
    curr:=head;
    while (curr<>nil) and (curr^.elem<>e) do
      curr:=curr^.next;
    if curr=nil then
      begin
        Result:=true;
        exit;
      end;
    if curr=head then
      begin
        Result:=pop_head();
        exit;
      end;
    if curr=tail then
      begin
        Result:=pop_tail();
        exit;
      end;
    s_curr:=curr^.last;
    if s_curr<>nil then
    	s_curr^.next:=curr^.next;
    s_curr:=curr^.next;
    if s_curr<>nil then
      s_curr^.last:=curr^.last;
    if current = curr then
      current:=nil;
    Dispose(curr);
    dec(h);
  end;
  function p_list.list_curr(): boolean;
  var
    curr: p_node;
  begin
    writeln;
    Result:=false;
    curr:=current;
    if curr=nil then
      begin
        Result:=true;
        exit;
      end;
    writeln(curr^.elem);
  end;
  function p_list.list():boolean;
  var
    curr : p_node;
  begin
    Result:=false;
    if head=nil then
      begin
        Result:=true;
        exit;
      end;
    writeln;
    curr:=head;
    while curr<>nil do
      begin
       	writeln(curr^.elem,' ');
        curr:=curr^.next;
      end;
  end;
  function p_list.save_list(line: string): boolean;
  var
    F: T_file;
    curr: p_node;
  begin
    if head=nil then
      begin
        Result:=true;
        exit;
      end;
  {
    Result:=FileExists(line);
    if Result=false then
      begin
        Result:=true;
        exit;
      end
    else
      Result:=false;
  }
    assignfile(F, line);
    rewrite(F);
    curr:=head;
    while curr^.next<>nil do
      begin
        writeln(F, curr^.elem);
        curr:=curr^.next;
      end;
    write(F, curr^.elem);
    close(F);
  end;
  destructor p_list.Del_it();
  var
  	node, curr : p_node;
  begin
    curr:=head;
  	while curr<>nil do
  		begin
  			node:=curr;
  			curr:=curr^.next;
  			dispose(node);
  		end;
  	tail:=nil;
  	current:=nil;
    h:=0;
  end;
  procedure Main(var p: p_list);
  var
  	ch, direct : string;
  	elem, cu_elem: new_e;
  	flag : boolean;
  begin
    p:=p_list.in_it;
  	writeln('DEC');
  	repeat
  	    flag:=false;
  	    writeln('---------------------------------------');
  	    writeln('1)Считать список с файла;');
  	    writeln('2)Вставить элемент в начало списка;');//head - начало/корень, tail - конец/хвост.
  	    writeln('3)Вставить элемент в конец списка;');
  	    writeln('4)Вставить элемент после заданного;');
  	    writeln('5)Вставить элемент перед заданным;');
  	    writeln('6)Переместить элемент в начало списка;');
  	    writeln('7)Переместить элемент в конец списка;');
  	    writeln('8)Переместить элемент после заданного;');
  	    writeln('9)Переместить элемент перед заданным;');
  	    writeln('10)Установить текущий элемент в списке;');
  	    writeln('11)Удалить первый элемент в списке;');
  	    writeln('12)Удалить последний элемент в списке;');
  	    writeln('13)Удалить последний введенный элемент;');
  	    writeln('14)Удалить заданный элемент;');
  	    writeln('15)Вывести текущий элемент списка;');
  	    writeln('16)Вывести все элементы списка;');
  	    writeln('17)Отсортировать список(не реализовано);');
  	    writeln('18)Сохранить элементы списка в файл;');
  	    writeln('19)Удалить список;');
  	    writeln('0)Введите "exit" для выхода;');
  	    writeln('---------------------------------------');
  	    readln(ch);
  		case ch of
  			'1':
  			begin
  				writeln('Введите название файла и его директорию');
  				readln(direct);
          p.Del_it();
          p:=p_list.in_it();
  				flag:=p.out_list(direct);
  			end;
  			'2':
  			begin
  				writeln('Введите элемент:');
  				readln(elem);
  				flag:=p.push_head(elem);
  			end;
  			'3':
  			begin
  				writeln('Введите элемент:');
  				readln(elem);
  				flag:=p.push_tail(elem);
  			end;
  			'4':
  			begin
  				writeln('Введите элемент, после которого нужно записать:');
  				readln(cu_elem);
  				writeln('Введите записываемый элемент:');
  				readln(elem);
  				flag:=p.push_after(cu_elem, elem);
  			end;
  			'5':
  			begin
  				writeln('Введите элемент, перед которым нужно записать:');
  				readln(cu_elem);
  				writeln('Введите записываемый элемент:');
  				readln(elem);
  				flag:=p.push_before(cu_elem, elem);
  			end;
  			'6':
  			begin
  				writeln('Введите элемент, который нужно переставить');
  				readln(elem);
  				flag:=p.move_head(elem);
  			end;
  			'7':
  			begin
  				writeln('Введите элемент, который нужно переставить');
  				readln(elem);
  				flag:=p.move_tail(elem);
  			end;
  			'8':
  			begin
  				writeln('Введите элемент, после которого нужно вставить:');
  				readln(cu_elem);
  				writeln('Введите элемент, который нужно переставить');
  				readln(elem);
  				flag:=p.move_after(cu_elem, elem);
  			end;
  			'9':
  			begin
  				writeln('Введите элемент, перед которым нужно вставить:');
  				readln(cu_elem);
  				writeln('Введите элемент, который нужно переставить');
  				readln(elem);
  				flag:=p.move_before(cu_elem, elem);
  			end;
  			'10':
  			begin
  				writeln('Введите значение этого элемента');
  				readln(elem);
  				flag:=p.mark_curr(elem);
  			end;
  			'11': flag:=p.pop_head();
  			'12': flag:=p.pop_tail();
  			'13': p.del_curr();
  			'14':
  			begin
  				writeln('Введите элемент, который нужно удалить:');
  				readln(elem);
  				flag:=p.del_mark(elem);
  			end;
  			'15': flag:=p.list_curr();
  			'16': flag:=p.list();
  			'17': flag:=p.sort;
  			'18':
  			begin
  				writeln('Введите название файла и название его директории;');
  				readln(direct);
  				flag:=p.Save_list(direct);
  			end;
  			'19': 
        begin
          p.Del_it();
          p:=p_list.in_it();
        end;
  	    end;
        if flag then writeln('Ошибка :/');
    until ch='0';
  end;
end.