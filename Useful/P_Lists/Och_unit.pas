unit Och_unit;

interface
	type
	    new_e = string;
	    p_node = ^t_node;
	    t_node = record
	    	elem : new_e;
	    	last : p_node;
	    end;
	    p_list = class
	    	private
		    	head, tail: p_node;
	        	h: word;
	        public
				constructor in_it ();
				function push_head (e:new_e):boolean;
				function pop_tail (): boolean;
				function list_head (): boolean;
				function list ():boolean;
				destructor Del_it();
	    end;
implementation
	constructor p_list.in_it();
	begin
		tail:=nil;
		head:=nil;
		h:=0;
	end;
	function p_list.push_head (e:new_e):boolean;
	var
		node, curr: p_node;
	begin
		Result:=false;
		new(node);
		if node=nil then
			begin
				Result:=true;
				exit;
			end;
		node^.elem:=e;
		curr:=head;
		if curr<>nil then
			curr^.last:=node;
		node^.last:=nil;
		head:=node;
		if tail=nil then
			tail:=head;
		inc(h);
	end;
	function p_list.pop_tail (): boolean;
	var
		node, curr: p_node;
	begin
		Result:=false;
		node:=tail;
		if node=nil then
			begin
				Result:=true;
				exit;
			end;
		if head=node then
			head:=nil;
		curr:=node^.last;
		node^.last:=nil;
		dispose(node);
		tail:=curr;
		if h>0 then
			dec(h);
	end;
	function p_list.list_head (): boolean;
	var
		curr: p_node;
	begin
		Result:=false;
		curr:=head;
		if curr=nil then
			begin
				Result:=true;
				exit;
			end;
		writeln(curr^.elem);
	end;
	function p_list.list ():boolean;
	var
		curr: p_node;
	begin
		Result:=false;
		curr:=tail;
		if curr=nil then
			begin
				Result:=true;
				exit;
			end;
		while curr<>nil do
			begin
				writeln(curr^.elem);
				curr:=curr^.last;
			end;
	end;
	destructor p_list.Del_it();
	var
		curr, node: p_node;
	begin
		curr:=tail;
		if curr=nil then
			exit;
		while curr<>nil do
			begin
				node:=curr;
				curr:=curr^.last;
				Dispose(node);
			end;
	end;
end.