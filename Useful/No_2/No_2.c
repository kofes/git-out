#include <stdio.h>
#include <stdlib.h>

typedef long t_elem;
typedef long elem_swp;

char swap(elem_swp* A, elem_swp* B)
{
	elem_swp tmp;

	tmp = *A;
	*A = *B;
	*B = tmp;

	return 1;
}

t_elem NOD(t_elem A, t_elem B, t_elem* X, t_elem* Y)
{
	t_elem x1, x2, y1, y2, r, q;
	
	if (A == 0)
	{
		*X = 0;
		*Y = 1;
		return B;
	}
	if (B == 0)
	{
		*Y = 0;
		*X = 1;
		return A;
	}
	char dir = 0;
	if (A < B)
		dir = swap(&A, &B);

	x2 = 1;
	x1 = 0;
	y2 = 0;
	y1 = 1;

	while (B > 0)
	{
		q = A / B;
		r = A % B;
		*X = x2 - q*x1;
		*Y = y2 - q*y1;

		A = B;
		B = r;
		x2 = x1;
		x1 = *X;
		y2 = y1;
		y1 = *Y;
	}

	if (dir == 1)
	{
		*X = y2;
		*Y = x2;
	}
	else
	{
		*X = x2;
		*Y = y2;
	}

	return A;
}

void main()
{
	t_elem a = 0, b = 0, d = 0, x = 0, y = 0;
	printf("A: ");
	scanf("%li", &a);
	printf("B: ");
	scanf("%li", &b);
	printf("\n");
	//swap(&a, &b);
	d = NOD(a, b, &x, &y);

	printf("NOD(%li, %li) = %li = (%li*%li) + (%li*%li)\n", a, b, d, x, a, y, b);
}