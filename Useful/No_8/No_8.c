#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define BITMAP_SIZE ((UCHAR_MAX+1)/CHAR_BIT)

typedef unsigned char SYM;

typedef 
	struct
		{
			SYM bits[BITMAP_SIZE];
		}ent_sym;

typedef ent_sym bit_plur;
char bit_set(bit_plur *mask)
{
	char i;
	if (mask == NULL)
		return 1;
	for (i=0; i<BITMAP_SIZE; ++i)
		mask->bits[i] = 0;
	return 0;
}
char bit_add(bit_plur *mask, SYM sym)
{
	if (mask == NULL)
		return 1;
	mask->bits[sym/CHAR_BIT] = (mask->bits[sym/CHAR_BIT] | (1<<(sym % CHAR_BIT)));
	return 0;
}
char bit_del(bit_plur *mask, SYM sym)
{
	if (mask == NULL)
		return 1;
	mask->bits[sym/CHAR_BIT] = (mask->bits[sym/CHAR_BIT] & (!(1<<(sym % CHAR_BIT))));
	return 0;
}
char bit_check(bit_plur *mask, SYM sym)
{
	if ((mask->bits[sym/CHAR_BIT] & (1<<(sym % CHAR_BIT))) != 0)
		return 1;
	return 0;
}
char bit_uni(bit_plur *mask1, bit_plur *mask2, bit_plur *mask_out)
{
	SYM i;
	if((mask1 == NULL)||(mask2 == NULL)||(mask_out == NULL))
		return 1;
	for (i=0; i<BITMAP_SIZE; ++i)
		mask_out->bits[i] = (mask1->bits[i])|(mask2->bits[i]);
	return 0;
}
char bit_irs(bit_plur *mask1, bit_plur *mask2, bit_plur *mask_out)
{
	SYM i;
	if((mask1 == NULL)||(mask2 == NULL)||(mask_out == NULL))
		return 1;
	for (i=0; i<BITMAP_SIZE; ++i)
		mask_out->bits[i] = (mask1->bits[i])&(mask2->bits[i]);
	return 0;
}
char bit_dif(bit_plur *mask1, bit_plur *mask2, bit_plur *mask_out)
	{
	SYM i;
	if((mask1 == NULL)||(mask2 == NULL)||(mask_out == NULL))
		return 1;
	for (i=0; i<BITMAP_SIZE; ++i)
		mask_out->bits[i] = (mask1->bits[i])&(~(mask2->bits[i]));
	return 0;
}
void test1()
{
	bit_plur p1;
	SYM symbol;
	scanf("%c", &symbol);
	
	bit_set(&p1);
	
	bit_add(&p1, symbol);

	if (bit_check(&p1, symbol) == 1)
		printf("YES!\n");
	else
		printf("NO!\n");

	bit_del(&p1, symbol);

	if (bit_check(&p1, symbol) == 1)
		printf("YES\n");
	else
		printf("NO\n");
}
void test2()
{
	bit_plur p1, p2, p3;
	unsigned short i;

	bit_set(&p1);
	bit_set(&p2);

	for (i=0; i<128; ++i)
	{
		bit_add(&p1, i);
		printf("%i ", i);	
	}
	printf("\n");
	printf("\n");
	for (i=0; i<256; ++i)
	{
		bit_add(&p2, i);
		printf("%i ", i);
	}
	printf("\n");
	printf("\n");

	bit_uni(&p1, &p2, &p3);
	
	for (i=0; i<256; ++i)
		if (bit_check(&p3, i) == 1)
			printf("%i ", i);
	printf("\n");
}
void test3()
{
	bit_plur p1, p2, p3;
	unsigned short i;

	bit_set(&p1);
	bit_set(&p2);

	for (i=128; i<256; ++i)
	{
		bit_add(&p1, i);
		printf("%i ", i);
	}
	printf("\n");
	printf("\n");
	for (i=64; i<200; ++i)
	{
		bit_add(&p2, i);
		printf("%i ", i);	
	}
	printf("\n");
	printf("\n");
	bit_irs(&p1, &p2, &p3);
	
	for (i=0; i<256; ++i)
		if (bit_check(&p3, i) == 1)
			printf("%i ", i);
	printf("\n");
}
void test4()
{
	bit_plur p1, p2, p3;
	unsigned short i;

	bit_set(&p1);
	bit_set(&p2);

	for (i=42; i<225; ++i)
		bit_add(&p1, i);
	for (i=64; i<145; ++i)
		bit_add(&p2, i);

	bit_dif(&p1, &p2, &p3);
	
	for (i=0; i<256; ++i)
		if (bit_check(&p3, i) == 1)
			printf("%i ", i);
	printf("\n");
}
char main()
{
	//test1(); //WORK
	test2(); //WORK
	//test3(); //WORK
	//test4(); //WORK
	
	return 0;
}