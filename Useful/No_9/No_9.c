#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>

typedef
	struct
		{
			uint16_t int_part, numer;
			int16_t denom;
		}t_fract;

typedef char (*f_fract)(const t_fract*, const t_fract*, t_fract*);

int32_t NOD(int32_t A, int32_t B)
{
	int32_t R, Q;

	if ((A < 0)||(B < 0))
		return -1;
	if (A<B)
	{
		int32_t tmp;

		tmp = A;
		A = B;
		B = A;
	}
	while (B>0)
	{
		int32_t tmp;

		tmp = A % B;
		A = A / B;
		B = tmp;
	}
	return A;
}
char frct_sum(const t_fract *A, const t_fract *B, t_fract *num)
{
	int32_t chA, chB;
	size_t root_num;
	uint16_t NOK;

	if ((num == NULL)||(A == NULL)||(B == NULL)||(A->denom == 0)||(B->denom == 0))
		return 1;
	if ((A->denom == 0)||(B->denom == 0))
		return 1;
	{
	char signA, signB;
	//Получение знаков дробей
	signA = (A->denom)/(abs(A->denom));
	signB = (B->denom)/(abs(B->denom));
	//Получение числителей без целой части
	chA = (((A->int_part)*(A->denom)) + ((A->numer)*signA));
	chB = (((B->int_part)*(B->denom)) + ((B->numer)*signB));
	}
	
	if ((abs(chA) > LONG_MAX/abs(B->denom))||(abs(chB) > LONG_MAX/abs(A->denom)))
		return 1;
	
	chA = chA * abs(B->denom);
	chB = chB * abs(A->denom);

	if (abs(A->denom) > LONG_MAX/abs(B->denom))
		return 1;
	
	num->denom = (abs((A->denom)*(B->denom)));
	
	if ((chA + chB) < 0)
		num->denom = -(num->denom);

	num->int_part = abs((chA + chB)/(num->denom));
	num->numer = abs((chA + chB) % (num->denom));

	return 0;
}
char frct_dif(const t_fract *A, const t_fract *B, t_fract *num)
{
	int32_t chA, chB;
	uint16_t NOK;
	if ((num == NULL)||(A == NULL)||(B == NULL)||(A->denom == 0)||(B->denom == 0))
		return 1;

	if ((A->denom == 0)||(B->denom == 0))
		return 1;

	{	
		char signA, signB;
		//Получение знаков дробей
		signA = (A->denom)/abs(A->denom);
		signB = (B->denom)/abs(B->denom);
		//Получение числителей без целой части
		chA = (((A->int_part)*(A->denom)) + ((A->numer)*signA));
		chB = (((B->int_part)*(B->denom)) + ((B->numer)*signB));
	}

	if ((abs(chA) > LONG_MAX/abs(B->denom))||(abs(chB) > LONG_MAX/abs(A->denom)))
		return 1;

	chA = chA * abs(B->denom);
	chB = chB * abs(A->denom);

	if (abs(A->denom) > LONG_MAX/abs(B->denom))
		return 1;

	num->denom = abs((A->denom)*(B->denom));
	if ((chA - chB) < 0)
		num->denom = -(num->denom);

	num->int_part = abs((chA - chB)/(num->denom));
	num->numer = abs((chA - chB) % num->denom);

	return 0;
}
char frct_prod(const t_fract *A, const t_fract *B, t_fract *num)
{
char sign;
	int32_t chA, chB;
	uint16_t nod;
	if ((num == NULL)||(A == NULL)||(B == NULL)||(A->denom == 0)||(B->denom == 0))
		return 1;
	if ((A->denom == 0)||(B->denom == 0))
		return 1;
	{
		char signA, signB;

		//Получение знаков дробей
		signA = (A->denom)/abs(A->denom);
		signB = (B->denom)/abs(B->denom);
		//Получение числителей без целой части
		chA = (((A->int_part)*(A->denom)) + ((A->numer)*signA));
		chB = (((B->int_part)*(B->denom)) + ((B->numer)*signB));

		sign = signA * signB;
	}

	if (abs(chA)> LONG_MAX/abs(chB))
		return 1;
	chA = abs(chA * chB);
	if (abs(A->denom) > LONG_MAX/abs(B->denom))
		return 1;
	chB = abs((A->denom) * (B->denom));

	num->denom = sign*chB;
	num->int_part = abs(chA/chB);
	num->numer = abs(chA % chB);

	return 0;
}
char frct_quot(const t_fract *A, const t_fract *B, t_fract *num)
{
	char sign;
	int32_t chA, chB;
	uint16_t nod;
	if ((num == NULL)||(A == NULL)||(B == NULL)||(A->denom == 0)||(B->denom == 0))
		return 1;
	if ((A->denom == 0)||(B->denom == 0))
		return 1;
	{
		char signA, signB;

		//Получение знаков дробей
		signA = (A->denom)/abs(A->denom);
		signB = (B->denom)/abs(B->denom);
		//Получение числителей без целой части
		chA = (((A->int_part)*(A->denom)) + ((A->numer)*signA));
		chB = (((B->int_part)*(B->denom)) + ((B->numer)*signB));

		sign = signA * signB;
	}

	if (abs(chA) > LONG_MAX/abs(B->denom))
		return 1;
	chA = abs(chA * (B->denom));
	if (abs(chB) > LONG_MAX/abs(A->denom))
		return 1;
	chB = abs(chB * (A->denom));

	num->denom = sign*chB;
	num->int_part = chA / chB;
	num->numer = chA % chB;

	return 0;
}

char main()
{
	f_fract oper[4];
	t_fract A, B, C;

	oper[0] = frct_sum;
	oper[1] = frct_dif;
	oper[2] = frct_prod;
	oper[3] = frct_quot;
	
	A.int_part = 1;		B.int_part = 2;
	A.numer = 3;		B.numer = 4;
	A.denom = 5;		B.denom = -6;
	C.int_part = 0;
	C.numer = 0;
	C.denom = 0;
	if (oper[2](&A, &B, &C))
		printf("%d\t %d\t %i\n", C.int_part, C.numer, C.denom);


	return 0;
