#include <stdio.h>
#include <stdlib.h>
#define n (10)
size_t *LOS(size_t *, size_t *);
int main(){
	size_t size;
	size_t *Mass, *p;
	Mass = (size_t *)malloc(n*sizeof(size_t));
	Mass[0] = 0; Mass[1] = 2; Mass[3] = 3;
	Mass[4] = 1; Mass[5] = 5; Mass[6] = 1;
	Mass[7] = 4; Mass[8] = 3; Mass[9] = 0;
	p = LOS(Mass, &size);
	free(Mass);
	printf("N = %u\n", size);
	while (size != 0){
		printf("%i ",p[size-1]);
		--size;
	}
	printf("\n");
	free(p);
	return 0;
}
size_t *LOS(size_t *M, size_t *count){
	size_t *d, *p;
	size_t i, j, ans, pos;
	d = (size_t *)malloc(n*sizeof(size_t));
	p = (size_t *)malloc(n*sizeof(size_t));
	for (i = 0; i < n; ++i){
		d[i] = 1;
		p[i] = -1;
		for (j = 0; j < i; ++j)
			if (M[j] < M[i])
				if (1 + d[j] > d[i]){
					d[i] = d[j] + 1;
					p[i] = j;
			}
	}
	ans = d[0];
	pos = 0;
	for (i = 1; i < n; ++i)
		if (ans < d[i]){
			pos = i;
			ans = d[i];
	}
	size_t *str;
	free(d);
	str = (size_t *)malloc(ans*sizeof(size_t));
	*count = ans;
	d = str;
	while (pos != -1){
		*d = M[pos];
		++d;
		pos = p[pos];
	}
	return str;
}