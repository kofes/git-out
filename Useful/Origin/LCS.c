#include <stdio.h>
#include <string.h>
#include <stdlib.h>
static size_t n;
static size_t m;
char *lcs_length(char *, char *, size_t *);
int main(){
	char A[] = "Hello world";
	char B[] = "Hell word";
	size_t count;
	m = strlen(A);
	n = strlen(B);
	printf("N = %i, M = %i\n", n, m);
	printf("%s\n", lcs_length(A, B, &count));
	return 0;
}
char *lcs_length(char *A, char *B, size_t *count){
	int i, j;
	size_t L[m+1][n+1];
	for (i = 0; i < m; ++i)
		for (j = 0; j < n; ++j)
			L[i][j] = 0;
	for (i = m; i >= 0; i--)
		for (j = n; j >= 0; j--){
			if ((A[i] == '\0') || (B[j] == '\0'))
				L[i][j] = 0;
			else if (A[i] == B[j])
				L[i][j] = 1 + L[i+1][j+1];
				else
					L[i][j] = (L[i+1][j]) > (L[i][j+1]) ? L[i+1][j] : L[i][j+1];
		}
	*count =  L[0][0];
	char *str, *p;
	str = (char *)malloc(*count+1);
	p = str;
	i = 0; j = 0;
	while ((i < m)&&(j < n)){
		if (A[i] == B[j]){
			*p = A[i];
			++p;
			i++; j++;
		}
		else if (L[i+1][j] >= L[i][j+1])
			i++;
			else
				j++;
	}
	str[*count] = '\0';
	return str;
}