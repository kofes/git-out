#include <stdio.h>
#include <string.h>
#define TRUE (1)
#define FALSE (0)
#define n (2)
#define m (5)
static FILE *fp;
int RAZp(int d);
int RAZb(int d);
int RAZur(void);
int SOCp(int d);
int SOCb(int d);
static char CH[] = "Putin";
static char M[n];
static char B[m];
int num;
int main(){
	//fp = fopen("out.txt", "wt");
	fp = stdout;
	num = 0;
	RAZur();
	//fclose(fp);
	return 0;
}
/*
 *	Перестановки с/без повторений делаются
 *	при помощи функций размещения, при
 *	m = n.
**/
//Размещения с повторениями
int RAZp(int d){
	int i;
	if (d == n){
		for (i = 0; i < n; ++i)
			fprintf(fp,"%c", CH[M[i]]);
		fprintf(fp,"\n");
		return 0;
	}
	for (i = 0; i < m; ++i){
		M[d] = i;
		RAZp(d+1);
	}
	return 0;
}
//Размещения без повторений
int RAZb(int d){
	int i;
	if (d == n){
		for (i = 0; i < n; ++i)
			fprintf(fp,"%c", CH[M[i]]);
		fprintf(fp,"\n");
		return 0;
	}
	for (i = 0; i < m; ++i)
		if (!B[i]){
			B[i] = TRUE;
			M[d] = i;
			RAZb(d+1);
			B[i] = FALSE;
		}
	return 0;
}
//Сочетания с повторений
int SOCp(int d){
	int i;
	if (d == n){
		for (i = 0; i < n; ++i)
			fprintf(fp, "%i", M[i] + 1);
		fprintf(fp, "\n");
		return 0;
	}
	for (i = d; i < m; ++i){
		M[d] = i;
		SOCp(d+1);
	}
	return 0;
}
//Сочетания без повторений
int SOCb(int d){
	int i;
	if (d == n){
		for (i = 0; i < n; ++i)
			fprintf(fp, "%i", M[i]);
		fprintf(fp, "\n");
		return 0;
	}
	for (i = d; i < m; ++i)
    if (!B[i]){
			B[i] = TRUE;
			M[d] = i;
			SOCb(d+1);
			B[i] = FALSE;
		}
	return 0;
}

static int A[n];
int RAZur(void){
	int i;
	for (i = 0; i < n; ++i)
		A[i] = i;
	for (i = 0; i < n; ++i)
		fprintf(fp, "%i", A[i]);
	fprintf(fp, "\n");
	while ((A[0] != n-1)&&(A[n-1] != 0)){
		i = n - 2;
		while ((i != 0)&&(A[i] > A[i + 1]))
			--i;
		if (i != 0){
			int j = i + 1;
			while (A[i] < A[j])
				++j;
			int temp = A[i];
			A[i] = A[j];
			A[j] = temp;
		}
		int j, k;
		for (j = i + 1, k = n - 1; j <= k; ++j, --k){
			int temp = A[j];
			A[j] = A[k];
			A[k] = temp;
		}
		for (i = 0; i < n; ++i)
			fprintf(fp, "%i", A[i]);
		fprintf(fp, "\n");
	}
	return (i >= 0);
}