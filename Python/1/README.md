DEF_COLLECTION
string <=> '...' or "... %d %..." %_INT_ %...
range()
list() <=> []
dict() <=> {}

DEF_CICLES
for _ITER_ in _COLLECTION_:
	...
for _ITER_ in range([start,] stop [, step]):
	...
while (_CONDITION_):
	...

DEF_ERROR
try:
	...
except:
	...
or

try:
	...
except:
	pass//do nothing

or

try:
	...
except ZeroDivisionError:
	print('...')//IF OK!
finally:
	print('...')//IF BAD!

raise TypeError("...")

/*Условные операторы*/
if (...)
	...
elif (...)
	...

not/or/and - логические операторы

DEF_FUNCTIONS

def doesNothing()
	...
	pass//Ничего не вернет
def doSomething()
	...
	return _OUTPUT_//Вернет результат

dir() <- возвращает все возможные функции

def UseGlobal():
	global _GLOBAL_VAR_
	...
print(...,...,...)//<- вернет несколько объектов

# This is comment

'''
This is also comment
'''

print _NAME_FUNCTION_.__doc__ //<- вернет 
документацию(комментарий)


DEF_FILES_INPUT

.pyc = Python Compile file
(Only computer understands)

.py = Python Writeable file
.pyw = -||-

_NAME_ = raw_input("...") // Ввод строки
//
import random as _NAME_
...
int = _NAME.randrange(int(_NUM_))
...
//

//
import PH as _NAME_//Позволяет подключать файл(скрипт), написанный 
ранее
...
//

DEF_CLASSES

class _NAME_CLASS_:
	pass
or
class _NAME_CLASS_:
	def __init__(self [, ...])//Конструктор - можно не 
указывать...
		self._NAME_LOCAL_VAR_ = ...
	def _NAME_FUNC_(self [,...]):
		...
...
VAR = _NAME_CLASS_()
