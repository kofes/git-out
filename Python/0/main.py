import fileinput

variable = list([1, 2, 3, 4, 5])

print(variable)

a = [1, 4 , 2, 3123, 12, -21]

a.sort()
print(a)
a.reverse()
print(a)
print(a.index(4))
a[:] = [] #Очищение списка по старому
a.clear() #Очищение списка сейчас

print(len(a))

a.append('232')
print(a)
a.remove('232')
print(a)

a.append(2231)
del a[0]
print(a)

var = {'name' : 'Nastya', 'surname' : 'Kolesnikova'}
var['age'] = 21
print(var['name'])

for key, value in var.items() :
    print(key, value)

str = tuple('just another string')
print(str)
