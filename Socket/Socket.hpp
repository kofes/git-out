#pragma once

#include "Net.hpp"
namespace net {
enum class Error {
  NORM,
  NOT_CREATED_SOCKET,
  NOT_BOUND_SOCKET,
  NOT_SET_NON_BLOCKING_SOCKET,
  DATA_SOCKET,
  NOT_SENT,
};

inline bool InitializeSockets () {
  #if PLATFORM == PLATFORM_WINDOWS
    WSADATA WsaData;
    return WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR;
  #else
    return true;
  #endif
};

inline void ShutdownSockets () {
  #if PLATFORM == PLATFORM_WINDOWS
    WSACleanup();
  #endif
};

class Socket {
public:
    Socket () : socket(0) {};
    ~Socket () {Close();};
    bool Open (unsigned short port) {
      socket = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

      if (socket <= 0) {
        error = Error::NOT_CREATED_SOCKET;
        return false;
      }

      sockaddr_in address;
      address.sin_family = AF_INET;
      address.sin_addr.s_addr = INADDR_ANY;
      address.sin_port = htons(port);/*Перевод в big-endian*/

      if (bind(socket, (const sockaddr*) &address, sizeof(sockaddr_in)) < 0) {
        error = Error::NOT_BOUND_SOCKET;
        return false;
      }

      #if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
        int nonBlocking = 1;
        if (fcntl(socket, F_SETFL, O_NONBLOCK, nonBlocking) == -1) {
          error = Error::NOT_SET_NON_BLOCKING_SOCKET;
          return false;
        }
      #elif PLATFORM == PLATFORM_WINDOWS
        DWORD nonBlocking = 1;
        if (ioctlsocket(socket, FIONBIO, &nonBlocking) != 0) {
          error = Error::NOT_SET_NON_BLOCKING_SOCKET;
          return false;
        }
      #endif

      return true;
    };
    void Close () {
      if (socket != 0) {
        #if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
          close(socket);
        #elif PLATFORM == PLATFORM_WINDOWS
          closesocket (socket);
        #endif
      }
    };
    bool IsOpen () const {return socket != 0;};
    bool Send (const Address& destination, const void* data, int size) {
      if (data == nullptr || size <= 0) {
        error = Error::DATA_SOCKET;
        return false;
      }
      if (socket == 0)
        return false;

      if (destination.GetAddress() == 0 || destination.GetPort() == 0) {
        error = Error::DATA_SOCKET;
        return false;
      }

      sockaddr_in sock_address;
      sock_address.sin_family = AF_INET;
      sock_address.sin_addr.s_addr = htonl(destination.GetAddress());
      sock_address.sin_port = htons(destination.GetPort());

      int sent_bytes = sendto(socket, (const char*)data, size, 0, (sockaddr*) &sock_address, sizeof(sockaddr_in));

      if (sent_bytes != size) {
        error = Error::NOT_SENT;
        return false;
      }
      return true;
    };
    int Receive (Address& sender, void* data, int size) {
      if (data == nullptr || size <= 0) {
        error = Error::DATA_SOCKET;
        return false;
      }
      if (socket == 0)
        return false;

      #if PLATFORM == PLATFORM_WINDOWS
      typedef int socklen_t;
      #endif

      sockaddr_in from;
      from.sin_family = AF_INET;
      from.sin_addr.s_addr = htonl(sender.GetAddress());
      from.sin_port = htons(sender.GetPort());

      socklen_t fromLength = sizeof(sender.GetAddress());

      return recvfrom(socket, (char*)data, size, 0, (sockaddr*) &from, &fromLength);
    };
    inline static Error getError () {return error;};
    inline static void offError () {error = Error::NORM;};
private:
  int socket;
  static Error error;
};

Error Socket::error = Error::NORM;
};
