#include "Net.hpp"
#include <cstdio>
#include <iostream>

const int port = 30000;
int test1 () {
  net::Socket socket;
  if (!socket.Open(port))
    return 1;
  const char data[] = "hello world!";
  socket.Send(net::Address(127,0,0,1, port), data, sizeof(data));

  while (true) {
    net::Address sender;
    unsigned char buffer[256];
    int bytes_read = socket.Receive(sender, buffer, sizeof(buffer));
    if (bytes_read <= 0)
      break;
    for (unsigned char i = 0; i < bytes_read; ++i)
      putchar(buffer[i]);
    putchar('\n');
  }
  return 0;
}

int test2 () {
  net::Socket s1, s2;
  if (!s1.Open(port)) {
    std::cout << 1 << std::endl;
    return 1;
  }
  if (!s2.Open(port)) {
    std::cout << 2 << std::endl;
    return 2;
  }
}

#define check(n) if ( !n ) { printf( "check failed\n" ); exit(1); }

int test3 () {
  const int ServerPort = 30000;
	const int ClientPort = 30001;
	const int ProtocolId = 0x11112222;
	const float DeltaTime = 0.001f;
	const float TimeOut = 0.1f;


  net::Connection client( ProtocolId, TimeOut );
	net::Connection server( ProtocolId, TimeOut );

	check( client.Start( ClientPort ) );
	check( server.Start( ServerPort ) );

	client.Connect( net::Address(127,0,0,1,ServerPort ) );
	server.Listen();

	while ( true )
	{
		if ( client.IsConnected() && server.IsConnected() )
			break;

		if ( !client.IsConnecting() && client.ConnectFailed() )
			break;

		unsigned char client_packet[] = "client to server";
		client.SendPacket( client_packet, sizeof( client_packet ) );

		unsigned char server_packet[] = "server to client";
		server.SendPacket( server_packet, sizeof( server_packet ) );

		while ( true )
		{
			unsigned char packet[256];
			int bytes_read = client.ReceivePacket( packet, sizeof(packet) );
			if ( bytes_read <= 0 )
				break;
      for (unsigned char i = 0; i < bytes_read; ++i)
        putchar(packet[i]);
      putchar('\n');
		}

		while ( true )
		{
			unsigned char packet[256];
			int bytes_read = server.ReceivePacket( packet, sizeof(packet) );
			if ( bytes_read <= 0 )
				break;
      for (unsigned char i = 0; i < bytes_read; ++i)
        putchar(packet[i]);
      putchar('\n');
		}

		client.Update( DeltaTime );
		server.Update( DeltaTime );

		wait( DeltaTime );
	}

	check( client.IsConnected() );
	check( server.IsConnected() );
}

int main () {
  test1();
  test2();
  test3();
  return 0;
}
