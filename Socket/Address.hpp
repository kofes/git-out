#pragma once

#include "Net.hpp"
namespace net {
class Address {
public:
  Address () : address(0), port(0) {};
  Address (unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port) : port(port) {
    address = (a << 24) | (b << 16) | (c << 8) | d;
  };
  Address (unsigned int address, unsigned short port) : address(address), port(port) {};
  unsigned int GetAddress () const {
    return address;
  };
  inline unsigned short GetPort () const {
    return port;
  };
  inline bool operator== (const Address& other) const {
    return address == other.address && port == other.port;
  };
  inline bool operator!= (const Address& other) const {
    return !(*this == other);
  };
private:
  unsigned int address;
  unsigned short port;
};
};
