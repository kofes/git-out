#pragma once

#include "Net.hpp"
namespace net {
class Connection {
public:
  enum class Mode {
    None,
    Client,
    Server,
  };
  Connection (unsigned int protocolId, float timeout): protocolId(protocolId), timeout(timeout), mode(Mode::None), running(false) {ClearData();};
  ~Connection () {if (running) Stop();}
  bool Start (int port) {
    if (running) return false;
    if (!socket.Open(port))
      return false;
    running = true;
    return true;
  };
  void Stop () {
    if (!running) return;
    ClearData();
    socket.Close();
    running = false;
  };
  void Listen () {
    ClearData();
    mode = Mode::Server;
    state = State::Listening;
  };
  void Connect (const Address& to) {
    ClearData();
    mode = Mode::Client;
    state = State::Connecting;
    address = to;
  };
  inline bool IsConnecting () const {return state == State::Connecting;};
  inline bool ConnectFailed () const {return state == State::ConnectFail;};
  inline bool IsConnected () const {return state == State::Connected;};
  inline bool IsListening () const {return state == State::Listening;};
  inline Mode GetMode () const {return mode;};
  void Update (float deltaTime) {
    if (!running) return;
    timeoutAccumulator += deltaTime;
    if (timeoutAccumulator > timeout) {
      if (state == State::Connecting) {
        ClearData();
        state = State::ConnectFail;
      } else if (state == State::Connected) {
        ClearData();
        if (state == State::Connecting)
          state = State::ConnectFail;
      }
    }
  };
  bool SendPacket (const unsigned char data[], int size) {
    if (!running) return false;
    if (address.GetAddress() == 0)
      return false;
    unsigned char packet[size+4];
    packet[0] = (unsigned char)(protocolId >> 24);
    packet[1] = (unsigned char)((protocolId >> 16) & 0xFF);
    packet[2] = (unsigned char)((protocolId >> 8) & 0xFF);
    packet[3] = (unsigned char)((protocolId >> 24) & 0xFF);
    memcpy(&packet[4], data, size);
    return socket.Send(address, packet, size+4);
  }
  int ReceivePacket (unsigned char data[], int size) {
    if (!running) return 0;
    unsigned char packet[size+4];
    Address sender;
    int bytes_read = socket.Receive(sender, packet, size + 4);
    if (bytes_read <= 4)
      return 0;
    if (packet[0] != (unsigned char)(protocolId >> 24) ||
        packet[1] != (unsigned char)((protocolId >> 16) & 0xFF) ||
        packet[2] != (unsigned char)((protocolId >> 8) & 0xFF) ||
        packet[3] != (unsigned char)((protocolId >> 24) & 0xFF))
      return 0;
/*Правки сервера для соединения с несколькими клиентами:
1. IsConnected -  нужно проверять для текущего клиента!
2. Вместо присваивания адреса нужно добавлять его в map.
3. Так же из (2) вытекает, что в проверке на правильность sender'а нужно проверять есть ли он в map.
4.
*/
    if (mode == Mode::Server && !IsConnected()) {//1
      state = State::Connected;
      address = sender;//2
    }
    if (sender == address) {
      if (mode == Mode::Client && state == State::Connecting) {
        state = State::Connected;
      }
      timeoutAccumulator = 0.0f;
      memcpy(data, &packet[4], size - 4);
      return size - 4;
    }
    return 0;
  }
protected:
  void ClearData () {
    state = State::Disconnected;
    timeoutAccumulator = 0.0f;
    address = Address();
  };
private:
  enum class State {
    Disconnected,
    Listening,
    Connecting,
    ConnectFail,
    Connected,
  };

  unsigned int protocolId;
  float timeout;

  bool running;
  Mode mode;
  State state;
  Socket socket;
  float timeoutAccumulator;
  Address address;
};
};
