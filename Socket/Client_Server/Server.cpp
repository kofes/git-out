#include "../Net.hpp"
#include <cstdio>
#include <iostream>

void ServerOut (const int ServerPort) {
  if (!net::InitializeSockets()) {
    std::cout << "Can't init sockets" << std::endl;
    return;
  }
  net::Socket sock;
  char packet[256];
  if (!sock.Open(ServerPort)) {
    std::cout << "socket can't open" << std::endl;
    return;
  }
  while (true) {
    net::Address sender;
    int bytes_read = sock.Receive(sender, packet, sizeof(packet));
    if (bytes_read < 0 || bytes_read > 256) continue;
    std::cout << "Packet received: ";
    for (int i = 0; i < bytes_read && packet[i] > 0; ++i)
      putchar(packet[i]);
    putchar('\n');
    for (unsigned char i = 0; i < bytes_read && packet[i] != '\0' && packet[i] != '\n'; ++i)
      packet[i] = 0;
    bytes_read = 0;
  }
  sock.Close();
  net::ShutdownSockets();
}

int main(int argc, char const *argv[]) {
  int ServerPort = 30000;
  if (argc > 1) ServerPort = std::atoi(argv[1]);
  ServerOut(ServerPort);
  return 0;
}
