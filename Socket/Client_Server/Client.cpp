#include "../Net.hpp"
#include <cstdio>
#include <iostream>
#include <cstdlib>

void ClientPrint (const int ServerPort, const int ClientPort) {
  if (!net::InitializeSockets()) {
    std::cout << "Can't init sockets" << std::endl;
    return;
  }
  char packet[100];
  net::Socket sock;
  if (!sock.Open(ClientPort)) {
    std::cout << "socket can't open" << std::endl;
    return;
  }
  while (true) {
    std::cin >> packet;
    if (sock.Send(net::Address(127,0,0,1,ServerPort), packet, sizeof(packet)))
      std::cout << "Packet sent" << std::endl;
    else
      std::cout << "Packet not sent" << std::endl;
    // net::wait(1);
  }
  sock.Close();
  net::ShutdownSockets();
}

int main(int argc, char const *argv[]) {
  int ServerPort = 30000;
  int ClientPort = 30001;
  if (argc > 2) {
    ServerPort = std::atoi(argv[1]);
    ClientPort = std::atoi(argv[2]);
  }
  ServerPrint(ServerPort, ClientPort);
  return 0;
}
