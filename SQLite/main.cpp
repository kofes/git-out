#include <iostream>
#include <sqlite3.h>
#include <string>

int main() {
	sqlite3 *hndl_db;
	sqlite3_stmt *hndl_stmt;
	std::string SQL_text;
	int err;

/*Создаем базу данных main_database.db*/
	err = sqlite3_open_v2("~/Documents/main_database.db", &hndl_db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE, NULL);

	if (err != SQLITE_OK) {
		std::cout << "main_database error: " << sqlite3_errstr(err) << std::endl;
		return 1;
	}
/*Создаем таблицу MAIN, если она ещё не существует*/
	SQL_text = "CREATE TABLE IF NOT EXISTS MAIN(id INTEGER PRIMARY KEY, name CHAR(32), surname CHAR(64));";
/*Подготавливаем запрос на создание таблицы*/
	err = sqlite3_prepare_v2(hndl_db, SQL_text.c_str(), -1, &hndl_stmt, NULL);

	if (err != SQLITE_OK) {
		std::cout << "sql_prepare error: " << sqlite3_errstr(err) << std::endl;
		return 1;
	}
/*Делаем запрос на создание таблицы*/
	err = sqlite3_step(hndl_stmt);

	if (err != SQLITE_DONE) {
		std::cout << "sql_step error: " << sqlite3_errstr(err) << std::endl;
		return 1;
	}
/*Сбрасываем запрос*/
	err = sqlite3_reset(hndl_stmt);

	if (err != SQLITE_OK) {
		std::cout << "sql_step error: " << sqlite3_errstr(err) << std::endl;
		return 1;
	}
/*Очищаем запрос*/
	err = sqlite3_finalize(hndl_stmt);

	if (err != SQLITE_OK) {
		std::cout << "stmt_finalize error: " <<
		sqlite3_errstr(err) << std::endl;
		return 1;
	}
/*Закрываем базу данных main_database.db*/
	err = sqlite3_close_v2(hndl_db);

	if (err != SQLITE_OK) {
		std::cout << "main_database error: " << sqlite3_errstr(err) << std::endl;
		return 1;
	}

	return 0;
}
