#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sqlite3.h>
using namespace std;
#ifdef _SQLITE3_H_

class t_DataBase{
private:
	sqlite3 *db = NULL;
	static int err;
	static char *err_vec;
public:
	t_DataBase(): db(NULL){};
	t_DataBase(string);
	~t_DataBase(){
		delete[] err_vec;
		sqlite3_close(db);
	};
	inline static int chk_error(){return err;};
	inline string get_error(){if (err_vec == NULL) return "";
							return err_vec;
							};
	inline static void off_error(){ err = SQLITE_OK,
								delete[] err_vec;
								};
	inline void execute_SQL(const string SQL){
		if (err_vec != NULL)
			delete[] err_vec;
		err_vec = NULL;
		err = sqlite3_exec(db, SQL.c_str(), 0, 0, &err_vec);
	};
	void open(string dir){
		if (err_vec != NULL)
			delete[] err_vec;
		err_vec = NULL;
		if (db != NULL)
			sqlite3_close(db);
		db = NULL;
		err = sqlite3_open(dir.c_str(), &db);
		err_vec = new char[strlen(sqlite3_errmsg(db)) + 1];
		memcpy(err_vec, sqlite3_errmsg(db), strlen(sqlite3_errmsg(db)));
	};
};

int t_DataBase::err = SQLITE_OK;
char *t_DataBase::err_vec = NULL;
t_DataBase::t_DataBase(string dir){
	if (err_vec != NULL)
		delete[] err_vec;
	err_vec = NULL;
	db = NULL;
	err = sqlite3_open(dir.c_str(), &db);
	err_vec = new char[strlen(sqlite3_errmsg(db)) + 1];
	memcpy(err_vec, sqlite3_errmsg(db), strlen(sqlite3_errmsg(db)));
}
#endif

int main(int argc, char **argv){
	t_DataBase db;
	string db_dir;
	if (argc != 2){
		cout<< "dir of DataBase: ";
		cin>>db_dir;
	}else
		db_dir = argv[1];
	db.open(db_dir);
	if (db.chk_error()){
		cerr << db.get_error() << '\n';
		db.off_error();
		return 1;
	}
	db.execute_SQL("CREATE TABLE IF NOT EXISTS foo(a, b, c);"
					"INSERT INTO FOO VALUES (1,2,3);"
					"INSERT INTO FOO SELECT * FROM FOO;");
	if (db.chk_error()){
		cerr << db.get_error() << '\n';
		db.off_error();
		return 2;
	}
	return 0;
}