#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BF_NULL (NULL)
#define BF_BUFF_SIZE (30000)
#define BF_ERR_NORM (0)
#define BF_ERR_DATA (1)
#define BF_ERR_NULL (2)
#define BF_ERR_IO (4)
/* Внутрянняя реализация указателя на буффер. */
typedef struct{
	unsigned char buff[BF_BUFF_SIZE];
	size_t count;
}t_buff;
/* Внутренняя реализации указателя на ячейку. */
typedef struct{
	t_buff *bf_buff;
	unsigned char *block;
	size_t pos;
}t_char;
/* Тип указателя на "память языка". */
typedef void *bf_t_buff_hndl;
/* Тип указателя на ячейку. */
typedef void *bf_t_char_hndl;
/* Конечный автомат

-------------------------------------------------------------------------
-------------------------------------------------------------------------

**/

int bf_buff_del(bf_t_buff_hndl bf_pointer){
	t_buff *buff = (t_buff *)bf_pointer;
	if ((buff == NULL)||(buff->count != 0))
		return BF_ERR_DATA;
	free(buff);
	return BF_ERR_NORM;
}
int bf_buff_new(bf_t_buff_hndl *bf_pointer){
	t_buff *buff;
	if ((bf_pointer == NULL)||(*bf_pointer != NULL))
		return BF_ERR_DATA;
	if (*bf_pointer != NULL)
		if (bf_buff_del(*bf_pointer))
			return BF_ERR_DATA;
	if ((buff = (t_buff *)malloc(sizeof(t_buff))) == NULL)
		return BF_ERR_NULL;
	memset(buff->buff, BF_BUFF_SIZE, 0);
	*bf_pointer = (bf_t_buff_hndl )buff;
	return BF_ERR_NORM;
}
int bf_char_off(bf_t_buff_hndl bf_pointer, bf_t_char_hndl *bf_char){
	t_buff *buff = (t_buff *)bf_pointer;
	t_char *pc = (t_char *)(*bf_char);
	if ((buff == NULL)||(pc == NULL)||(pc->block != (buff->buff + pc->pos)))
		return BF_ERR_DATA;
	--(buff->count);
	*bf_char = NULL;
	return BF_ERR_NORM;
}
int bf_new_char(bf_t_buff_hndl bf_pointer, bf_t_char_hndl *bf_char){
	t_buff *buff = (t_buff *)bf_pointer;
	t_char *pc;
	if ((buff == NULL)||(bf_char == NULL))
		return BF_ERR_DATA;
	if (*bf_char != NULL)
		if (bf_char_off(pc->bf_buff, bf_char))
			return BF_ERR_DATA;
	if ((pc = (t_char *)malloc(sizeof(t_char))) == NULL)
		return BF_ERR_NULL;
	pc->pos = 0;
	pc->block = buff->buff;
	pc->bf_buff = buff;
	*bf_char = (bf_t_char_hndl )pc;
	return BF_ERR_NORM;
}
int bf_right_char(bf_t_buff_hndl bf_pointer, bf_t_char_hndl *bf_char){
	t_buff *buff = (t_buff *)bf_pointer;
	t_char *pc;
	if ((buff == NULL)||(bf_char == NULL)||(*bf_char == NULL)||(pc->block != (buff->buff + pc->pos)))
		return BF_ERR_DATA;
	pc = (t_char *)(*bf_char);
	pc->pos = (pc->pos + 1) % BF_BUFF_SIZE;
	pc->block = buff->buff + pc->pos;
	*bf_char = (bf_t_char_hndl )pc;
	return BF_ERR_NORM;
}
int bf_left_char(bf_t_buff_hndl bf_pointer, bf_t_char_hndl *bf_char){
	t_buff *buff = (t_buff *)bf_pointer;
	t_char *pc;
	if ((buff == NULL)||(bf_char == NULL)||(*bf_char == NULL)||(pc->block != (buff->buff + pc->pos)))
		return BF_ERR_DATA;
	pc = (t_char *)(*bf_char);
	pc->pos = (BF_BUFF_SIZE + pc->pos - 1) % BF_BUFF_SIZE;
	pc->block = buff->buff + pc->pos;
	*bf_char = (bf_t_char_hndl )pc;
	return BF_ERR_NORM;
}
int bf_start_char(bf_t_buff_hndl bf_pointer, bf_t_char_hndl *bf_char){
	t_buff *buff = (t_buff *)bf_pointer;
	t_char *pc;
	if ((buff == NULL)||(bf_char == NULL)||(*bf_char == NULL)||(pc->block != (buff->buff + pc->pos)))
		return BF_ERR_DATA;
	pc = (t_char *)(*bf_char);
	pc->pos = 0;
	pc->block = buff->buff;
	*bf_char = (bf_t_char_hndl )pc;
	return BF_ERR_NORM;
}
int bf_read_char(bf_t_char_hndl bf_char, FILE *fin){
	t_char *pc = (t_char *)bf_char;
	if ((pc == NULL)||(fin == NULL))
		return BF_ERR_DATA;
	if (!fread(pc->block, sizeof(char), 1, fin))
		return BF_ERR_IO;
	return BF_ERR_NORM;
}
int bf_write_char(bf_t_char_hndl bf_char, FILE *fout){
	t_char *pc = (t_char *)bf_char;
	if ((pc == NULL)||(fout == NULL))
		return BF_ERR_DATA;
	if (!fwrite(pc->block, sizeof(char), 1, fout))
		return BF_ERR_IO;
	return BF_ERR_NORM;
}
int bf_last_char(bf_t_char_hndl bf_char){
	t_char *pc = (t_char *)bf_char;
	if (pc == NULL)
		return BF_ERR_DATA;
	*(pc->block) = *(pc->bf_buff->buff + (BF_BUFF_SIZE + pc->pos - 1) % BF_BUFF_SIZE);
	return BF_ERR_NORM;
}
int main(){
	bf_t_buff_hndl p;
	bf_t_char_hndl pc;
	p = BF_NULL;
	bf_buff_new(&p);
	bf_buff_del(&p);
	return 0;
}