#include <stdio.h>
#include <string.h>
int main(){
	unsigned char buff[256], Table[256], N, len, err;
	unsigned short i;
	memset(Table, 256, 0);
	for (i = 48; i < 58; ++i)
		Table[i] = 1;
	for (i = 65; i < 91; ++i)
		Table[i] = 1;
	for (i = 97; i < 123; ++i)
		Table[i] = 1;
	memset(buff, 256, '\0');
	scanf("%u", &N);
	scanf("%s", &buff);
	len = strlen(buff);
	if (len < N) err = 1;
	for (i = 0; (!err)&&(i < len); ++i)
		if (!Table[buff[i]]) err = 1;
	if (err) printf("NO\n");
	else printf("YES\n");
	return 0;
}