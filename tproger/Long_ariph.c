#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LONG_NUM_NULL (NULL)
typedef void *t_num;

extern int long_num_plus(t_num *result, t_num left, t_num right);
extern int long_num_minus(t_num *result, t_num left, t_num right);

typedef struct{
	char *NUM;
	char SIGN;
}t_long_num;

size_t max(size_t num1, size_t num2){
	if (num1 > num2)
		return num1;
	else
		return num2;
}
size_t min(size_t num1, size_t num2){
	if (num1 < num2)
		return num1;
	else
		return num2;
}
int long_num_del(t_num lnum){
	t_long_num *num = (t_long_num *)lnum;
	if (num == NULL)
		return 1;
	free(num->NUM);
	free(num);
	return 0;
}

int long_num_read(t_num *lnum, size_t size, FILE *fin){
	t_long_num *num;
	size_t len;
	char i, *p, buff[size + 1];
	if ((lnum == NULL)||(fin == NULL))
		return 1;
	if (*lnum != NULL)
		if (long_num_del(*lnum))
			return 1;
	memset(buff, '\0', size + 1);
	fgets(buff, size, fin);
	len = strlen(buff);
	if ((num = (t_long_num *)malloc(sizeof(t_long_num))) == NULL)
		return 1;
	if ((num->NUM = (char *)malloc((len) * sizeof(char))) == NULL){
		free(num);
		return 1;
	}
	memset(num->NUM, '#', len);
	p = num->NUM;
	i = 0;
	num->SIGN = 1;
	if (buff[0] == '-'){
		num->SIGN = -1;
		i = 0;
	}
	for ( ; (buff[i] != '\n')&&(buff[i] != '\0')&&(buff[len-i-2] != '-'); ++i){
		*p = buff[len - i - 2] - 48;
		++p;
	}
	*lnum = (t_num )num;
	return 0;
}

int long_num_size(t_num lnum, size_t *size){
	t_long_num *num = (t_long_num *)lnum;
	char *p;
	if ((num == NULL)||(size == NULL))
		return 1;
	p = num->NUM;
	*size = 0;
	while (*p != '#'){
		++p;
		++(*size);
	}
	return 0;
}
int long_num_write(t_num lnum, FILE *fout){
	t_long_num *num = (t_long_num *)lnum;
	char *p;
	size_t size;
	if ((num == NULL)||(fout == NULL))
		return 1;
	long_num_size(lnum, &size);
	p = num->NUM;
	if (num->SIGN == -1)
		fprintf(fout, "-");
	while (size != 0)
		fprintf(fout, "%u", p[--size]);
	fprintf(fout, "\n");
	return 0;
}

int long_num_copy(t_num *result, t_num source){
	t_long_num *num;
	size_t size, i;
	if ((result == NULL)||(source == NULL))
		return 1;
	if (long_num_size(source, &size))
		return 1;
	if ((num = (t_long_num *)malloc(sizeof(t_long_num))) == NULL)
		return 2;
	if ((num->NUM = (char *)malloc((size + 1)*sizeof(char))) == NULL){
		free(num);
		return 2;
	}
	num->SIGN = ((t_long_num *)source)->SIGN;
	num->NUM[size] = '#';
	for (i = 0; i < size; ++i)
		num->NUM[i] = ((t_long_num *)source)->NUM[i];
	if (*result != NULL)
		long_num_del(*result);
	*result = (t_num )num;
	return 0;
}

int long_num_cmp(t_num left, t_num right){
	t_long_num *num[2];
	size_t size[2];
	char *p[2], SIGN;
	num[0] = (t_long_num *)left;
	long_num_size(left, &(size[0]));
	num[1] = (t_long_num *)right;
	long_num_size(right, &(size[1]));
	if (num[0]->SIGN > num[1]->SIGN)
		return 1;
	if (num[0]->SIGN < num[1]->SIGN)
		return -1;
	SIGN = num[0]->SIGN;
	if ((SIGN*((int)size[0] - (int)size[1])) > 0)
		return 1;
	if ((SIGN*((int)size[0] - (int)size[1])) < 0)
		return -1;
	p[0] = num[0]->NUM + size[0] - 1;
	p[1] = num[1]->NUM + size[1] - 1;
	while ((p[0] != num[0]->NUM)&&(p[1] != num[1]->NUM)){
		if ((SIGN*(*(p[0]) - *(p[1]))) > 0)
			return 1;
		if ((SIGN*(*(p[0]) - *(p[1]))) < 0)
			return -1;
		--p[0];
		--p[1];
	}
	if (SIGN*(*(p[0]) - *(p[1])) > 0)
		return 1;
	if (SIGN*(*(p[0]) - *(p[1])) < 0)
		return -1; 
	return 0;
}

int long_num_plus(t_num *result, t_num left, t_num right){
	t_long_num *num[3];
	char *p[3];
	char SIGN[2], carry;
	size_t mi, ma, i, size[2];
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return 1;
	num[1] = (t_long_num *)left;
	num[2] = (t_long_num *)right;
	SIGN[0] = num[1]->SIGN;
	SIGN[1] = num[2]->SIGN;
	if (SIGN[0] != SIGN[1]){
		((t_long_num *)right)->SIGN *= (-1);
		long_num_minus(result, left, right);
		((t_long_num *)right)->SIGN *= (-1);
		return 0;
	}
	long_num_size(left, &(size[0]));
	long_num_size(right, &(size[1]));
	mi = min(size[0], size[1]);
	ma = max(size[0], size[1]);
	if ((num[0] = (t_long_num *)malloc(sizeof(t_long_num))) == NULL)
		return 2;
	if ((num[0]->NUM = (char *)malloc((ma + 2) * sizeof(char))) == NULL){
		free(num[0]);
		return 2;
	}
	if (SIGN[0] == SIGN[1])
		num[0]->SIGN = SIGN[0];
	num[0]->NUM[ma + 1] = '#';
	p[0] = num[0]->NUM;
	p[1] = num[1]->NUM;
	p[2] = num[2]->NUM;
	p[0][0] = 0;
	carry = 0;
	for (i = 0; i < mi; ++i){
		p[0][i] += (SIGN[0]*p[1][i] + SIGN[1]*p[2][i])%10;
		p[0][i] = abs(p[0][i]);
		p[0][i+1] = (SIGN[0]*p[1][i] + SIGN[1]*p[2][i])/10;
	}
	while (i < size[0]){
		p[0][i] = SIGN[0]*p[1][i];
		p[0][i] = abs(p[0][i]);
		++i;
	}
	while (i < size[1]){
		p[0][i] = SIGN[1]*p[2][i];
		p[0][i] = abs(p[0][i]);
		++i;
	}
	if (*result != NULL){
		long_num_del(*result);
	}
	if (p[0][i] == 0)
		p[0][i] = '#';
	*result = (t_num )num[0];
	return 0;
}

int long_num_minus(t_num *result, t_num left, t_num right){
	t_long_num *num[3];
	char *p[3];
	char SIGN[2], carry, ch;
	size_t mi, ma, i, size[2];
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return 1;
	num[1] = (t_long_num *)left;
	num[2] = (t_long_num *)right;
	SIGN[0] = num[1]->SIGN;
	SIGN[1] = num[2]->SIGN;
	if (SIGN[0] != SIGN[1]){
		((t_long_num *)right)->SIGN *= (-1); 
		long_num_plus(result, left, right);
		((t_long_num *)right)->SIGN *= (-1);
		return 0;
	}
	long_num_size(left, &(size[0]));
	long_num_size(right, &(size[1]));
	mi = min(size[0], size[1]);
	ma = max(size[0], size[1]);
	if ((num[0] = (t_long_num *)malloc(sizeof(t_long_num))) == NULL)
		return 2;
	if ((num[0]->NUM = (char *)malloc((ma + 2) * sizeof(char))) == NULL){
		free(num[0]);
		return 2;
	}
	num[0]->SIGN = SIGN[0];
	((t_long_num *)left)->SIGN *= num[0]->SIGN;
	long_num_write(left, stdout);
	((t_long_num *)right)->SIGN *= num[0]->SIGN;
	long_num_write(right, stdout);
	ch = long_num_cmp(left, right);
	num[0]->NUM[ma + 1] = '#';
	p[0] = num[0]->NUM;
	p[1] = num[1]->NUM;
	p[2] = num[2]->NUM;
	p[0][0] = 0;
	carry = 0;
	for (i = 0; i < mi; ++i){
		p[0][i] += (p[1][i] - p[2][i])%10;
			carry = (p[0][i] < 0);
			p[0][i] += carry*10;
		p[0][i] = abs(p[0][i]);
		printf("!%i!\n", p[0][i]);
		p[0][i+1] = (p[1][i] - p[2][i])/10 - carry;
	}
	while (i < size[0]){
		if (p[0][i] == '#')
			p[0][i] = 0;
		p[0][i] += p[1][i];
			carry = (p[0][i] < 0);
			p[0][i] += carry*10;
			p[0][i+1] -= carry;
		p[0][i] = abs(p[0][i]);
		++i;
	}
	while (i < size[1]){
		if (p[0][i] == '#')
			p[0][i] = 0;
		p[0][i] -= p[2][i];
			carry = (p[0][i] < 0);
			p[0][i] += carry*10;
			p[0][i+1] -= carry;
		p[0][i] = abs(p[0][i]);
		++i;
	}
	if (carry){
		p[0][i] = '#';
		carry = 10;
		for (i = 0; i < ma; ++i){
			p[0][i] = carry - p[0][i];
			carry = 9;
		}
	}
	((t_long_num *)left)->SIGN *= num[0]->SIGN;
	((t_long_num *)right)->SIGN *= num[0]->SIGN;
	if (ch == -1)
		num[0]->SIGN = -(SIGN[1]);
	if (*result != NULL){
		long_num_del(*result);
	}
	if (p[0][i] == 0)
		p[0][i] = '#';
	if (p[0][i-1] == 0)
		p[0][i-1] = '#';
	*result = (t_num )num[0];
	return 0;
}

int long_num_multi(t_num *result, t_num left, t_num right){
	t_long_num *num[3];
	size_t i, j, size[2];
	unsigned short curr, diff;
	num[1] = (t_long_num *)left;
	num[2] = (t_long_num *)right;
	//if (right == NULL)
	if ((result == NULL)||(left == NULL)||(right == NULL))
		return 1;
	if ((num[0] = (t_long_num *)malloc(sizeof(t_long_num))) == NULL)
		return 2;
	printf("\nLLOL\n");
	long_num_size(left, &(size[0]));
	long_num_size(right, &(size[1]));
	if ((num[0]->NUM = (char *)malloc((size[0] + size[1] + 2)*sizeof(char))) == NULL){
		free(num[0]);
		return 2;
	}
	memset(num[0]->NUM, 0, size[0]+size[1]+2);
	num[0]->NUM[size[0]+size[1]+1] = '#';
	num[0]->SIGN = (num[1]->SIGN)*(num[2]->SIGN);
	for (i = 0; num[1]->NUM[i] != '#'; ++i){
		for (j = 0, diff = 0; num[2]->NUM[j] != '#'; ++j){
			curr = (num[1]->NUM[i])*(num[2]->NUM[j]) + diff + num[0]->NUM[i+j];
			num[0]->NUM[i+j] = curr%10;
			diff = curr/10;
		printf("diff = %i, NUM[%i] = %i\n", diff, i+j, num[0]->NUM[i+j]);
		}
		num[0]->NUM[i+j] = diff;
		printf("NUM[%i] = %i\n", i+j+1, num[0]->NUM[i+j+1]);
	}
	for (i = 0; num[0]->NUM[size[0]+size[1]-i] == 0; ++i)
		num[0]->NUM[size[0]+size[1]-i] = '#';
	long_num_write((t_num )(num[0]), stdout);
	if (*result != NULL)
		long_num_del(*result);
	*result = (t_num )(num[0]);
	return 0;
}

int main(){
	FILE *fp;
	t_num num[2];
	unsigned char i, n;
	size_t size;
	num[0] = LONG_NUM_NULL;
	num[1] = LONG_NUM_NULL;
	if ((fp = fopen("input.txt", "rt")) == NULL)
		return 1;
	long_num_read(&(num[0]), 51, fp);
	long_num_copy(&(num[1]), num[0]);
	fscanf(fp, "%u", &n);
	fclose(fp);
//	for (i = 0; i < n; ++i)
//		long_num_multi(&(num[0]), num[0], num[1]);
	long_num_write(num[0], stdout);
	long_num_write(num[1], stdout);
	long_num_del(num[0]);
	long_num_del(num[1]);
	return 0;
}