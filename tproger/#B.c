#include <stdio.h>
#include <string.h>

int main(){
	FILE *fp;
	char len = 80, flag[2], buff[82], i;
	if ((fp = stdin) == NULL)
		return 1;
	memset(buff, '#', 82);
	fgets(buff, 82, fp);
	fclose(fp);
	fp = stdout;
	if ((buff[81] != '\0')||((buff[len - 1] != '1')&&(buff[len - 1] != '0'))){
		for (i = 0; i < 80; ++i)
			fprintf(fp, "0");
		printf("!%c!\n", buff[81]);
		return 0;
	}
	flag[0] = 1;
	flag[1] = 1;
	for (i = 0; i < 4; ++i){
		if (buff[i] != '1')
			flag[0] = 0;
		if (buff[len - i - 1] != '1')
			flag[1] = 0;
	}
	if (flag[0] != flag[1]){
		if (flag[0])
			for (i = 0; i < len; ++i)
				fprintf(fp, "%c", buff[i]);
		else
			for (i = 0; i < len; ++i)
				fprintf(fp, "%c", buff[len - i - 1]);
	}
	else
		for (i = 0; i < len; ++i)
			fprintf(fp, "0");
	fprintf(fp, "\n");
	return 0;
}