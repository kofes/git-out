/* Задача решена для входных данных меньше
 * SIZE_MAX. На тестах 28-52 не пройдет!
**/
#include <stdio.h>

int main(){
	unsigned long long num, i;
	scanf("%u", &num);
	for (i = 2; i <= num/i; ++i){
		while (num % i == 0){
			num /= i;
			printf("%zu ", i);
		}
	}
	if (num != 1)
		printf("%llu", num);
	printf("\n");
	return 0;
}