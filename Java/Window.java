import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Window extends JFrame{
	private int count = 0;
	private JLabel countLabel;
	private JButton countUP;
	private JButton countDown;


	private void CountOperations(){
		countLabel = new JLabel("Count: " + count);
		countUP = new JButton("+1");
		countDown = new JButton("-1");

		JPanel buttonsPanel = new JPanel(new FlowLayout());
		add(countLabel, BorderLayout.NORTH);

		buttonsPanel.add(countUP);
		buttonsPanel.add(countDown);
		add(buttonsPanel, BorderLayout.SOUTH);
		InitListeners();
	}
	private void InitListeners(){

		countUP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				++count;
				updateCounter();
			}
		});

		countDown.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				if (count <= 0)
					return;
				--count;
				updateCounter();
			}
		});
	}

	private void updateCounter(){
		countLabel.setText("Count: " + count);
	}


	public Window(){
		super("Window");
		CountOperations();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
/*Main function...*/
	public static void main(String[] args){
		Window app = new Window();
		app.setVisible(true);
		app.pack();
	}
}
