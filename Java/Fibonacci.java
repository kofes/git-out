public class Fibonacci{
	public static long getFibbonacciNumber(int n){
		if (n <= 0){
			return 0;
		}
		long prev = 0;
		long curr = 1;
		for (int i = 0; i < n; ++i){
			curr = prev + curr;
			prev = curr - prev;
		}
		return prev;
	}

	public static void main(String[] args) {
		for (int i = 0; i <= Integer.parseInt(args[0]); ++i){
			System.out.printf("fib(%d) = %d\n", i, getFibbonacciNumber(i));
		}
	}
}