/*
1. Glew
2. freeGlut(SDL, GLFW) для работы с окнами (SDL с поддержкой звука, GLFW -
	SDL с другим интерфейсом). Документация для freeGlut BAD.
*/
//===============================
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
GLuint shader_program;
GLint VERTEX_ID;
GLuint VAO_ID;
GLuint VBO_ID;
GLint ERR_ID;
void init_gl(float R, float G, float B){
	glClearColor((R/256), (G/256), (B/256), 0.0f);
}
void log_shader(GLuint shader){
	int err_length = 0;
	char *err_vector;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &ERR_ID);
	if (ERR_ID == GL_FALSE){
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &err_length);
		err_vector = new char[err_length];
		glGetShaderInfoLog(shader, err_length, NULL, err_vector);
		cout<<"Info_Log: "<< err_vector << "\n";
		glDeleteShader(shader);
		delete[] err_vector;
	}
}
GLuint init_shader(string file, GLuint type){
	GLuint id = glCreateShader(type);
	string source;
	ifstream from(file.c_str(), ios::in);
	if (from.is_open()){
		string str = "";
		while (getline(from, str))
			source += str + "\n";
		from.close();
	}
	const char *str = source.c_str();
	glShaderSource(id, 1, &str, NULL);
	glCompileShader(id);
	cout << "vertex shader compiled\n";
	log_shader(id);
	return id;
}
void init_shader_program(string vert_file, string frag_file){
	GLuint vertex_shader, fragment_shader;

	vertex_shader = init_shader(vert_file, GL_VERTEX_SHADER);
	if (ERR_ID == GL_FALSE) return;
	fragment_shader = init_shader(frag_file, GL_FRAGMENT_SHADER);
	if (ERR_ID == GL_FALSE){
		glDeleteShader(vertex_shader);
		return;
	}
//<<<<<<<<<<<<<<<<<<LINK_AND_COMPILATION>>>>>>>>>>>>>>>>>>>>>
	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glLinkProgram(shader_program);

	int err_length;
	glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &err_length);
	glGetProgramiv(shader_program, GL_LINK_STATUS, &ERR_ID);
	if(ERR_ID == GL_FALSE){
		cout << "error attach shaders\n";
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		glDeleteProgram(shader_program);
		return;
	}
//<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	const char* name = "pos";
	VERTEX_ID = glGetAttribLocation(shader_program, name);
	if(VERTEX_ID == -1){
		cout << "could not bind " << name;
		return;
	}
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}
void init_vbo(){
	float vert[] = {
		-1.0f,	-1.0f,	-0.5f,
		0.0f,	1.0f,	-0.5f,
		1.0f,	-1.0f,	-0.5f
	};
	glGenBuffers(1, &VBO_ID);
	if (VBO_ID < 0){
		cout << "could not generate buffers\n";
		return;
	}
	glBindBuffer(GL_ARRAY_BUFFER, VBO_ID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*9, vert, GL_STATIC_DRAW);
}
void init_vao(){
	glGenVertexArrays(1, &VAO_ID);
	glBindVertexArray(VAO_ID);
	glEnableVertexAttribArray(VERTEX_ID);
	glVertexAttribPointer(VERTEX_ID, 3, GL_FLOAT, GL_FALSE, 0, NULL);
}
//=======================================
void render(void){
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(shader_program);
	glBindVertexArray(VAO_ID);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);
	glUseProgram(0);

	glutSwapBuffers();
}
int main(int argc, char *argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(320, 240);
	glutInitContextVersion(3,3);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow("Test_Vesion_of_Triangle");
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err){
		cerr<<"Error: "<<glewGetErrorString(err)<<'\n';
		return 1;
	}

	init_gl(0,0,0);
	init_vbo();
	init_shader_program("vertex.glsl", "fragment.glsl");
	if (ERR_ID == GL_FALSE){
		cout<<"problem with shaders";
		return 1;
	}
	init_vao();

	glutDisplayFunc(render);
	cout<<"Status: Using GLEW "<<glewGetString(GLEW_VERSION)<<'\n';

	glutMainLoop();

	glDisableVertexAttribArray(VERTEX_ID);
	glBindVertexArray(0);
	glUseProgram(0);
	return 0;
}
