#include "GLUT.hpp"

#include <iostream>
void GLUT::Default(std::string label, int *argc, char **argv){
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(320, 240);
	// glutInitWindowPosition(150, 150);
	glutInitContextVersion(3,3);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutCreateWindow(label.c_str());
}

void GLUT::Init(int *argc, char **argv){
	glutInit(argc, argv);
}

void GLUT::DisplayMode(unsigned int mode){
	glutInitDisplayMode(mode);
}

void GLUT::WindowSize(int width, int height){
	glutInitWindowSize(width, height);
}

void GLUT::WindowPos(int x, int y){
	glutInitWindowPosition(x, y);
}

void GLUT::ContextVersion(int major, int minor){
	glutInitContextVersion(major, minor);
}

void GLUT::ContextProfile(int profile){
	glutInitContextProfile(profile);
}

void GLUT::CreateWindow(std::string label){
	glutCreateWindow(label.c_str());
}

void GLUT::Display(void (*render)(void)){
	glutDisplayFunc(render);
}

void GLUT::Reshape(void (*reshape)(int w, int h)){
	glutReshapeFunc(reshape);
}

void GLUT::MainLoop(){
	glutMainLoop();
}
