#ifndef __INCLUDE_MY_GLUT_

#define __INCLUDE_MY_GLUT_

#include <GL/freeglut.h>
#include <string>

namespace GLUT{
	void Default(std::string, int *, char **);
	void Init(int *argc, char **argv);
	void DisplayMode(unsigned int);
	void WindowSize(int width, int height);
	void WindowPos(int x, int y);
	void ContextVersion(int, int);
	void ContextProfile(int);
	void CreateWindow(std::string);
	void Display(void (*render)(void));
	void Reshape(void (*reshape)(int w, int h));
	void MainLoop();
}

#endif
