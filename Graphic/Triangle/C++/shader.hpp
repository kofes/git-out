#ifndef __INCLUDE_SHADER_

#define __INCLUDE_SHADER_

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>

class shader_program;

class shader{
private:
	GLuint ID;
	GLuint TYPE;
	GLint ERR;
	friend class shader_program;
public:
	shader(GLuint);
	shader(GLuint, std::ifstream &file);
	shader(GLuint, std::string code);
	~shader();
	void exec(std::ifstream &file);
	void exec(std::string code);
	void compile();
	GLuint GET_TYPE();
	GLint ERR_COMP();
	inline void ERR_OFF(){ERR = 0;}
	std::string ERR_STR();
};

class shader_program{
private:
	unsigned int *count_pts;
	GLuint ID;
	GLint ERR;
public:
	shader_program();
	shader_program(const shader_program &);
	~shader_program();
	shader_program operator =(const shader_program &);
	void add(shader &src);
	void link();
	GLint ERR_LINK();
	inline void ERR_OFF(){ERR = 0;}
	std::string ERR_STR();
	GLint get_attribute(const std::string &attribute);
	inline GLuint id(){
		return ID;
	}
};

#endif
