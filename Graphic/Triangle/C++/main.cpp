#include "shader.hpp"
#include "GLUT.hpp"

using namespace std;

GLuint ShPr;
GLuint VAO_ID;

shader_program init_shader_program(GLint &VERTEX_ID){
	shader_program sp;
	shader sh_vert(GL_VERTEX_SHADER), sh_frag(GL_FRAGMENT_SHADER);
	ifstream from("fragment.glsl", ios::in);
	sh_frag.exec(from);
	sh_frag.compile();
	if (sh_frag.ERR_COMP() == GL_FALSE){
		cerr << "Error: " << sh_frag.ERR_STR() << endl;
		return shader_program();
	}else
		cerr << "Fragment shader: compiled" << endl;
	from.close();
	from.open("vertex.glsl", ios::in);
	sh_vert.exec(from);
	sh_vert.compile();
	if (sh_vert.ERR_COMP() == GL_FALSE){
		cerr << "Error: " << sh_vert.ERR_STR() << endl;
		return shader_program();
	}else
		cerr << "Vertex shader: compiled" << endl;
	sp.add(sh_frag);
	sp.add(sh_vert);
	sp.link();
	if (sp.ERR_LINK() == GL_FALSE){
		cerr << "Error: " << sp.ERR_STR() << endl;
		return shader_program();
	}
	const char name[] = "pos";
	VERTEX_ID = sp.get_attribute(name);
	if (VERTEX_ID == -1){
		cout << "could not bind" << name << endl;
		return shader_program();
	}
	return sp;
}

void init_V_O(GLint VERTEX_ID){
	float vert[] = {
		-1.0f,	-1.0f,	-0.5f,
		0.0f,	1.0f,	-0.5f,
		1.0f,	-1.0f,	-0.5f
	};
	GLuint VBO_ID;
	glGenBuffers(1, &VBO_ID);
	if (VBO_ID < 0){
		cout << "could not generate buffers\n";
		return;
	}
	glBindBuffer(GL_ARRAY_BUFFER, VBO_ID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*9, vert, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO_ID);
	glBindVertexArray(VAO_ID);
	glEnableVertexAttribArray(VERTEX_ID);
	glVertexAttribPointer(VERTEX_ID, 3, GL_FLOAT, GL_FALSE, 0, NULL);

}

void render(){
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(ShPr);
	glBindVertexArray(VAO_ID);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);
	glUseProgram(0);

	glutSwapBuffers();
}

int main(int argc, char *argv[]){
	GLint VERTEX_ID;
	GLUT::Default("MAIN", &argc, argv);
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK){
		cerr << "Error: "<< glewGetErrorString(err) << endl;
		return 1;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	shader_program sp = init_shader_program(VERTEX_ID);
	ShPr = sp.id();
	init_V_O(VERTEX_ID);


	GLUT::Display(render);
	cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION) << endl;
	GLUT::MainLoop();
	return 0;
}
