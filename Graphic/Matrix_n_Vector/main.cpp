#include "mat_vec.hpp"

using namespace std;

/*Vector's tests*/
void test_1_1(){
	char flag = 0;
	t_vector p(5, 1.5);
	cout << "test init: ";
	for (char i = 0; i < 5; ++i)
		if (fabs(p[i] - 1.5) > VEC_EPS){
			flag = true;
			break;
		}
	cout << ((!flag) ? "OK!" : "BAD!") << endl;
}

void test_1_2(){
	char flag;
	t_vector p(5, 20.17), p2;
	p2 = p;
	p2[0] = 10;
	for (unsigned int i = 1; (!flag)&&(i < 5); ++i)
		if (fabs(p[i] - p2[i]) > VEC_EPS)
			flag = true;
	cout << "test copy: ";
	if (!flag)
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_3(){
	char flag;
	t_vector p(3, 0);
	p[0] = 2;
	p[1] = 2;
	p[2] = 1;
	flag = (fabs(p.Length() - 3) < VEC_EPS) ? 0 : 1;

	cout << "test Length: ";
	if (!flag)
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_4(){
	t_vector p(3, 0.0000001), p2;
	cout << "test Normalize's: ";
	p2 = p.Normalize();
	if (p.get_error() != t_vector::ERR_ZERO_LONG){
		cout << "BAD!" << endl;
		return;
	}
	p.off_error();
	p.NormalizeInPlace();
	if (p.get_error() != t_vector::ERR_ZERO_LONG){
		cout << "BAD!" << endl;
		return;
	}
	p.off_error();
	p2 = t_vector(3, 0);
	p2[2] = 77.12;
	p2[1] = 0.41;
	p2[0] = 34.12;
	p2.NormalizeInPlace();
	if (p2.Length() - VEC_EPS > 1){
		cout << "BAD!" << endl;
		return;
	}

	cout << "OK!" << endl;
}

void test_1_5(){
	t_vector p, p2(3, -0.13213), p3;
	p3 = t_vector(3, 0);
	p3[0] = 0.23;
	p3[1] = 0.99;
	p3[2] = 1.5525;
	p = p3 + p2;
	cout << "test operator +: ";
	if ((fabs(p[0] - (p3[0] + p2[0])) < VEC_EPS)&&(fabs(p[1] - (p3[1] + p2[1])) < VEC_EPS)&&(fabs(p[2] - (p3[2] + p2[2])) < VEC_EPS))
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_6(){
	t_vector p, p2(3, -9412.4214), p3;
	p3 = t_vector(3, 0);
	p3[0] = 10024.2412;
	p3[1] =192512.12521;
	p3[2] = 535823.414124;
	p = p3 - p2;
	cout << "test operator -: ";
	if ((fabs(p[0] - (p3[0] - p2[0])) < VEC_EPS)&&(fabs(p[1] - (p3[1] - p2[1])) < VEC_EPS)&&(fabs(p[2] - (p3[2] - p2[2])) < VEC_EPS))
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_7(){
	t_vector p, p2(3, 2.312), p3;
	p3 = t_vector(3, 0);
	p3[0] = 3.4125;
	p3[1] = 5125.54233;
	p3[2] = 123.15867;
	p = p3 ^ p2;
	cout << "test operator MultComp: ";
	if ((fabs(p[0] - (p3[0] * p2[0])) < VEC_EPS)&&(fabs(p[1] - (p3[1] * p2[1])) < VEC_EPS)&&(fabs(p[2] - (p3[2] * p2[2])) < VEC_EPS))
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_8(){
	t_vector p(3, 0), p2(3, 2132.41);
	p[0] = 1.321;
	p[1] = 0.512;
	p[2] = 34.123;
	cout << "test operator dot: ";
	if (fabs((p[0] * p2[0] + p[1] * p2[1] + p[2] * p2[2]) - p * p2) < VEC_EPS)
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_9(){
	t_vector p(3, 0), p2(3, 0), p3;
	p[0] = 0.5125;			p2[0] = - 24.241;
	p[1] = 12.3123;		p2[1] = -333.2312;
	p[3] = 515.23444;		p2[2] = 0.4442;
	p3 = p(p2);
	cout << "test operator cross: ";
	if (fabs(p3[0] + p2(p)[0] + p3[1] + p2(p)[1] + p3[2] + p2(p)[2]) < VEC_EPS)
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_1_10(){
	t_vector p, p1(4, 0);
	p1[0] = 2.33;
	p1[1] = -5525.124;
	p1[2] = 41241.441;
	p1[3] = 0.616;
	p = p1 * 1.5;
	cout << "test MultSclr: ";
	if (fabs((p[0] + p[1] + p[2] + p[3]) - (p1[0] + p1[1] + p1[2] + p1[3])*1.5) < VEC_EPS)
		cout << "OK!" << endl;
	else
		cout << "BAD!" << endl;
}

void test_2_1(){
	FL_POINT buff[9] = {
		-1.23f,	23.32f,	22.3f,
		-0.22,	96.332f,	76.223f,
		-235.22f,-0.2,		33.32f
	};
	t_matrix p(2, 3), p2, p3(3), p4(3, buff);

	cout << "test init: ";
	for (unsigned i = 0; i < 3; ++i)
		for (unsigned j = 0; j < 3; ++j)
			if (fabs(p4[i][j] - buff[i*3 + j]) > MX_FL_EPS){
				cout << "BAD!" << endl;
				return;
			}
	p2[0][0] = 1;
	if (p2.get_error() == t_matrix::ERR_NORM){
		cout << "BAD!" << endl;
		return;
	}
	p2.off_error();

	cout << "OK!" << endl;

}
#include <cstdlib>
void test_2_2(){
	FL_POINT buff[8] = {
		rand()/233, rand()/233, rand()/233, rand()/233,
		rand()/233, rand()/233, rand()/233, rand()/233
	};
	FL_POINT buff2[8] = {
		rand() / 347, rand() / 347, rand() / 347, rand() / 347,
		rand() / 347, rand() / 347, rand() / 347, rand() / 347
	};
	t_matrix p1(2, 4, buff), p2(2, 4, buff2), p3;
	p3 = p1 + p2;
	cout << "test +: ";
	for (unsigned int i = 0; i < 2; ++i)
		for (unsigned int j = 0; j < 4; ++j)
			if (fabs(p3[i][j] - (buff[i*4 + j] + buff2[i*4 + j])) > MX_FL_EPS){
				cout << "BAD!" << endl;
				return;
			}
	cout << "OK!" << endl;
}

void test_2_3(){
	FL_POINT buff[8] = {
		rand()/523, rand()%234, rand()%231, rand()%2332,
		rand()/71, rand()/385, rand()%2334, rand()%519
	};
	FL_POINT buff2[8] = {
		rand() / 1.52, rand() / 347.421, rand() / 44.32, rand() / 0.6125,
		rand() / 6612.321, rand() / -61.23, rand() / -0.442, rand() / 1000.232
	};
	t_matrix p1(2, 4, buff), p2(2, 4, buff2), p3;
	p3 = p1 - p2;
	cout << "test -: ";
	for (unsigned int i = 0; i < 2; ++i)
		for (unsigned int j = 0; j < 4; ++j)
			if (fabs(p3[i][j] - (buff[i*4 + j] - buff2[i*4 + j])) > MX_FL_EPS){
				cout << "BAD!" << endl;
				return;
			}
	cout << "OK!" << endl;
}

void test_2_4(){
	FL_POINT buff[8] = {
		1.42,			33.34,	 	12.23,	-0.323,
		0.322,		0.1,			0.99f,	-1.1
	};
	FL_POINT buff2[8] = {
		8.52,			12.342,
		-55.3,		32.52,
		-0.5252,		-23.231,
		61.232, 		13.3
	};
	t_matrix p1(2, 4, buff), p2(4, 2, buff2), p3, p4(2);
	p3 = p1 * p2;
	cout << "test *: ";
	for (unsigned int i = 0; i < 4; ++i){
		p4[0][0] += buff[i] * buff2[i*2];
		p4[0][1] += buff[i] * buff2[i*2 + 1];
		p4[1][0] += buff[4 + i] * buff2[i*2];
		p4[1][1] += buff[4 + i] * buff2[i*2 + 1];
	}
	for (unsigned int i = 0; i < 2; ++i)
		for (unsigned int j = 0; j < 2; ++j)
			if (fabs(p3[i][j] - p4[i][j]) > MX_FL_EPS){
				cout << "BAD!" << endl;
				return;
			}
	cout << "OK!" << endl;
}

void test_2_5(){
	FL_POINT buff[9] = {
		11.32, -0.424, 9.32,
		2.323, -0.2, -10.23,
		23.44, -24.0, 12.02
	};
	t_matrix p(3, buff);
	cout << "test Determinant(if n > 4x4, eps >> 1e-4): ";
	if (fabs(p.Det() - (buff[0]*(buff[4]*buff[8] - buff[5]*buff[7]) - buff[1]*(buff[3]*buff[8] - buff[5]*buff[6]) + buff[2]*(buff[3]*buff[7] - buff[4]*buff[6]))) > 0.1){
		cout << "BAD!" << endl;
		return;
	}
	cout << "OK!" << endl;
}

void test_2_6(){
	FL_POINT buff[16] = {
		0,		3,		-1,	2,
		2,		1,		0,		0,
		-2,	-1,	0,		2,
		-5,	7,		1,		1
	};
	t_matrix p(4, buff), pt;
	pt = p.Inv();
	pt = p*pt;
	cout << "test Inverse: ";
	for (unsigned int i = 0; i < 4; ++i)
		for (unsigned int j = 0; j < 4; ++j)
			if (fabs(pt[i][j] - (i == j ? 1 : 0)) > MX_FL_EPS){
				cout << "BAD!" << endl;
				return;
			}
	cout << "OK!" << endl;
}

void test_2_7(){
	FL_POINT buff[16] = {
		0,		3,		-1,	2,
		2,		1,		0,		0,
		-2,	-1,	0,		2,
		-5,	7,		1,		1
	};
	t_matrix p1(4, buff), p2;
	p2 = p1.T();
	cout << "test Transpose: ";
	for (unsigned int i = 0; i < 4; ++i)
		for (unsigned int j = 0; j < 4; ++j)
			if (fabs(p2[i][j] - p1[j][i]) > MX_FL_EPS){
				cout << "BAD!" << endl;
				return;
			}
	cout << "OK!" << endl;
}

void test_2_8(){
	FL_POINT buff[16] = {
		0,		3,		-1,	2,
		2,		1,		0,		0,
		-2,	-1,	0,		2,
		-5,	7,		1,		1
	};
	t_matrix m(4, buff);
	t_vector v(4, 0), res;
	v[0] = 1;
	v[1] = 10;
	v[2] = -1;
	v[3] = 2;
	res = m * v;
	cout << "test Matrix x Vector: ";
	for (unsigned int i = 0; i < 4; ++i){
		FL_POINT sum = 0;
		for (unsigned int j = 0; j < 4; ++j)
			sum += m[i][j] * v[j];
		if (fabs(res[i] - sum) > VEC_EPS){
			cout << "BAD!" << endl;
			return;
		}
	}
	cout << "OK!" << endl;
}

int main(){
	cout << "Tests for class t_vector:" << endl;
	test_1_1();
	test_1_2();
	test_1_3();
	test_1_4();
	test_1_5();
	test_1_6();
	test_1_7();
	test_1_8();
	test_1_9();
	test_1_10();
	cout << "Tests for class Matrix:" << endl;
	test_2_1();
	test_2_2();
	test_2_3();
	test_2_4();
	test_2_5();
	test_2_6();
	test_2_7();
	test_2_8();
	return 0;
}
