#ifndef __INCLUDE_MY_TEXTURE_

#define __INCLUDE_MY_TEXTURE_

#include <SOIL/SOIL.h>
#include <string>

class texture{
private:
	int width, height;
	unsigned char *image;
public:
	texture();
	texture(const std::string &src);
	~texture();
	void exec(const std::string &src);
	inline const unsigned char *_image() const{return image;};
	inline int _width()								{return width;};
	inline int _height()								{return height;};
};

#endif
