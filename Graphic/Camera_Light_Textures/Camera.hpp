#ifndef __CAMERA_INCLUDE_

#include "mat_vec.hpp"

#define __CAMERA_PI 		(3.1415926535897932384626433832795f)

class Camera{
private:
	t_vector						pos;
	float							fovy, as, near, far,
									speed,
 									yaw_angle, pitch_angle;
public:
	Camera						();
	Camera						(float _fovy, float _as, float _near, float _far);
	inline float _speed		(){ return speed;}
	inline void set_speed	(float __SPEED_){ speed = __SPEED_;}
	inline t_vector _target	(){
		t_vector target(4, 0);
		float yaw = yaw_angle * __CAMERA_PI / 180;
		float pitch = pitch_angle * __CAMERA_PI / 180;
		target[0] = sin(yaw) * cos(pitch);
		target[1] = -sin(pitch);
		target[2] = cos(pitch) * cos(yaw);
		return target;
	}
	inline t_vector _up		(){
		t_vector up(4, 0);
		float yaw = yaw_angle * __CAMERA_PI / 180;
		float pitch = pitch_angle * __CAMERA_PI / 180;
		up[0] = sin(yaw) * sin(pitch);
		up[1] = cos(pitch);
		up[2] = cos(yaw) * sin(pitch);
		return up;
	}
	inline t_vector _side	(){
		t_vector side(4, 0);
		float yaw = yaw_angle * __CAMERA_PI / 180;
		float pitch = pitch_angle * __CAMERA_PI / 180;
		side[0] = cos(yaw);
		side[1] = 0;
		side[2] = -sin(yaw);
		return side;
	}
	inline t_vector _pos		(){
		return pos;
	}
	void Move					(t_vector &);
	void Pitch					(float angle);
	void Yaw						(float angle);
	void Zoom					(float angle);
	t_matrix Matrix			();
};

namespace vec_action{
	t_matrix Translate		(t_vector &move);
	t_matrix Rotate			(t_vector &axis, unsigned short angle);
	t_matrix Scale				(t_vector &scale);
}

namespace vec_projection{
	t_matrix Ortho				(t_matrix &src, float left, float right, float top, float bottom, float near, float far);
	t_matrix Frustum			(t_matrix &src, float left, float right, float top, float bottom, float near, float far);
	t_matrix Perspective		(t_matrix &src, float fovy, float as, float near, float far);
}

#endif
