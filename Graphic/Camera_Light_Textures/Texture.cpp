#include "Texture.hpp"

texture::texture(){
	width = height = 0;
	image = NULL;
}

texture::texture(const std::string &src){
	image = SOIL_load_image(src.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
}

texture::~texture(){
	width = height = 0;
	if (image != NULL)
		SOIL_free_image_data(image);
	image = NULL;
}

void texture::exec(const std::string &src){
	if (image != NULL)
		SOIL_free_image_data(image);
	image = NULL;
	width = height = 0;
	image = SOIL_load_image(src.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
}
