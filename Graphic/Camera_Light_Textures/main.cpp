#include "mat_vec.hpp"
#include "shader.hpp"
#include "GLUT.hpp"
#include "Camera.hpp"
#include "Geometry.hpp"
#include "Texture.hpp"
using namespace std;
using namespace geometry;

GLuint solid_shader_program;

GLuint	VAO_LINES	 = 0,
			VAO_SPHERE	 = 0,
			VAO_CUBE		 = 0,
			VAO_SQUARE	 = 0,
			VAO_LIGHT	 = 0,
			VAO_CUBE_MAP = 0
			;

GLuint	TEXTURE_CUBE		= 0,
			TEXTURE_SPHERE		= 0,
			TEXTURE_SQUARE		= 0,
			TEXTURE_LINES		= 0,
			TEXTURE_NORMAL		= 0,
			TEXTURE_SPECULAR	= 0,
			TEXTURE_DIFFUSE	= 0
			;

GLint		UNIFORM_MVP					= -1,
			UNIFORM_MODEL				= -1,
			UNIFORM_SOLID_COLOR		= -1,
			UNIFORM_AMBIENT			= -1,
			UNIFORM_SOLID_EMISSIVE	= -1,
			UNIFORM_SOLID_DIFFUSE	= -1,
			UNIFORM_SOLID_SPECULAR	= -1,
			UNIFORM_SOLID_SHINESS	= -1,
			UNIFORM_SOLID_TOGGLE		= -1,
			VERTEX_SOLID_POSITION	= -1,
			VERTEX_SOLID_NORMAL		= -1,
			VERTEX_LIGHT_POSITION	= -1,
			VERTEX_EYE_POSITION		= -1,

			VERTEX_SOLID_TANGENT			= -1,
			VERTEX_SOLID_BITANGENT		= -1,
			UNIFORM_SAMPLER_DIFFUSE		= -1,
			UNIFORM_SAMPLER_NORMAL		= -1,
			UNIFORM_SAMPLER_SPECULAR	= -1
			;

GLint		VERTEX_TEXTURE_COORDINATES		= -1,
			UNIFORM_TEXTURE_SAMPLER			= -1
			;

Camera camera;
t_vector diffuse_light_pos(3, 1.0f);

t_vector ambient(3, 1.0f);
t_vector diffuse(3, 1.0f);
t_vector specular(3, 1.0f);
t_vector emissive(3, 1.0f);

float solid_shiness = 1;

int mouse_button;
int width, height;
bool MACHINE[256];

shader_program init_shader_program(){
	shader_program sp;
	shader sh_vert(GL_VERTEX_SHADER), sh_frag(GL_FRAGMENT_SHADER);
	ifstream from("solid_fragment.glsl", ios::in);

	sh_frag.exec(from);
	sh_frag.compile();

	if (sh_frag.ERR_COMP() == GL_FALSE){
		cout << "Solid fragment shader:\n" << sh_frag.ERR_STR() << endl;
		return shader_program();
	}
	cout << "Solid fragment shader: compiled" << endl;

	from.close();
	from.open("solid_vertex.glsl", ios::in);
	sh_vert.exec(from);
	sh_vert.compile();

	if (sh_vert.ERR_COMP() == GL_FALSE){
		cout << "Solid vertex shader:\n" << sh_vert.ERR_STR() << endl;
		return shader_program();
	}
	cout << "Solid vertex shader: compiled" << endl;

	sp.add(sh_frag);
	sp.add(sh_vert);
	sp.link();

	if (sp.ERR_LINK() == GL_FALSE){
		cout << "Error: " << sp.ERR_STR() << endl;
		return shader_program();
	}

	const char solid_position[] = "solid_position";
	VERTEX_SOLID_POSITION = sp.get_attribute(solid_position);
	if (VERTEX_SOLID_POSITION == -1){
		cout << "could not bind " << solid_position << endl;
		return shader_program();
	}

	const char solid_normal[] = "solid_normal";
	VERTEX_SOLID_NORMAL = sp.get_attribute(solid_normal);
	if (VERTEX_SOLID_NORMAL == -1){
		cout << "could not bind " << solid_normal << endl;
		return shader_program();
	}

	const char light_position[] = "light_position";
	VERTEX_LIGHT_POSITION = sp.get_attribute(light_position);
	if (VERTEX_LIGHT_POSITION == -1){
		cout << "could not bind " << light_position << endl;
		return shader_program();
	}

	const char eye_position[] = "eye_position";
	VERTEX_EYE_POSITION = sp.get_attribute(eye_position);
	if (VERTEX_EYE_POSITION == -1){
		cout << "could not bind " << eye_position << endl;
		return shader_program();
	}
	const char texture_coordinates[] = "texture_coordinates";
	VERTEX_TEXTURE_COORDINATES = sp.get_attribute(texture_coordinates);
	if (VERTEX_TEXTURE_COORDINATES == -1){
		cout << "could not bind " << texture_coordinates << endl;
		return shader_program();
	}

	const char mvp[] = "mvp";
	UNIFORM_MVP = sp.get_uniform(mvp);
	if (UNIFORM_MVP == -1){
		cout << "could not bind " << mvp << endl;
		return shader_program();
	}

	const char model[] = "model";
	UNIFORM_MODEL = sp.get_uniform(model);
	if (UNIFORM_MODEL == -1){
		cout << "could not bind " << model << endl;
		return shader_program();
	}

	const char solid_color[] = "solid_color";
	UNIFORM_SOLID_COLOR = sp.get_uniform(solid_color);
	if (UNIFORM_SOLID_COLOR == -1){
		cout << "could not bind " << solid_color << endl;
		return shader_program();
	}

	const char ambient[] = "ambient";
	UNIFORM_AMBIENT = sp.get_uniform(ambient);
	if (UNIFORM_AMBIENT == -1){
		cout << "could not bind " << ambient << endl;
		return shader_program();
	}

	const char solid_emissive[] = "solid_emissive";
	UNIFORM_SOLID_EMISSIVE = sp.get_uniform(solid_emissive);
	if (UNIFORM_SOLID_EMISSIVE == -1){
		cout << "could not bind " << solid_emissive << endl;
		return shader_program();
	}

	const char solid_diffuse[] = "solid_diffuse";
	UNIFORM_SOLID_DIFFUSE = sp.get_uniform(solid_diffuse);
	if (UNIFORM_SOLID_DIFFUSE == -1){
		cout << "could not bind " << solid_diffuse << endl;
		return shader_program();
	}

	const char solid_specular[] = "solid_specular";
	UNIFORM_SOLID_SPECULAR = sp.get_uniform(solid_specular);
	if (UNIFORM_SOLID_SPECULAR == -1){
		cout << "could not bind " << solid_specular << endl;
		return shader_program();
	}

	const char solid_shiness[] = "solid_shiness";
	UNIFORM_SOLID_SHINESS = sp.get_uniform(solid_shiness);
	if (UNIFORM_SOLID_SHINESS == -1){
		cout << "could not bind " << solid_shiness << endl;
		return shader_program();
	}

	const char solid_toggle[] = "solid_toggle";
	UNIFORM_SOLID_TOGGLE = sp.get_uniform(solid_toggle);
	if (UNIFORM_SOLID_TOGGLE == -1){
		cout << "could not bind " << solid_toggle << endl;
		return shader_program();
	}

	const char texture_sampler[] = "texture_sampler";
	UNIFORM_TEXTURE_SAMPLER = sp.get_uniform(texture_sampler);
	if (UNIFORM_TEXTURE_SAMPLER == -1){
		cout << "could not bind " << texture_sampler << endl;
		return shader_program();
	}


	const char solid_tangent[] = "solid_tangent";
	VERTEX_SOLID_TANGENT = sp.get_attribute(solid_tangent);
	if (VERTEX_SOLID_TANGENT == -1){
		cout << "could not bind " << solid_tangent << endl;
		return shader_program();
	}

	const char solid_bitangent[] = "solid_bitangent";
	VERTEX_SOLID_BITANGENT = sp.get_attribute(solid_bitangent);
	if (VERTEX_SOLID_BITANGENT == -1){
		cout << "could not bind " << solid_bitangent << endl;
		return shader_program();
	}

	const char diffuse_sampler[] = "diffuse_sampler";
	UNIFORM_SAMPLER_DIFFUSE = sp.get_uniform(diffuse_sampler);
	if (UNIFORM_SAMPLER_DIFFUSE == -1){
		cout << "could not bind " << diffuse_sampler << endl;
		return shader_program();
	}

	const char normal_sampler[] = "normal_sampler";
	UNIFORM_SAMPLER_NORMAL = sp.get_uniform(normal_sampler);
	if (UNIFORM_SAMPLER_NORMAL == -1){
		cout << "could not bind " << normal_sampler << endl;
		return shader_program();
	}

	const char specular_sampler[] = "specular_sampler";
	UNIFORM_SAMPLER_SPECULAR = sp.get_uniform(specular_sampler);
	if (UNIFORM_SAMPLER_SPECULAR == -1){
		cout << "could not bind " << specular_sampler << endl;
		return shader_program();
	}

	return sp;
}

void init_texture_images(){
	texture cube("textures/brick_18.jpg");
	glGenTextures(1, &TEXTURE_CUBE);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_CUBE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, cube._width(), cube._height(), 0, GL_RGB, GL_UNSIGNED_BYTE, cube._image());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	texture sphere("textures/moon.png");
	glGenTextures(1, &TEXTURE_SPHERE);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_SPHERE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, sphere._width(), sphere._height(), 0, GL_RGB, GL_UNSIGNED_BYTE, sphere._image());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	texture square("textures/wood-texture-008.jpg");
	glGenTextures(1, &TEXTURE_SQUARE);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_SQUARE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, square._width(), square._height(), 0, GL_RGB, GL_UNSIGNED_BYTE, square._image());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

	texture normal("textures/normal.bmp");
	glGenTextures(1, &TEXTURE_NORMAL);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_NORMAL);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, normal._width(), normal._height(), 0, GL_RGB, GL_UNSIGNED_BYTE, normal._image());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);

/*
	texture diffuse("textures/normal.jpg");
	glGenTextures(1, &TEXTURE_DIFFUSE);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_DIFFUSE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, diffuse._width(), diffuse._height(), 0, GL_RGB, GL_UNSIGNED_BYTE, diffuse._image());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
*/
	texture specular("textures/specular.jpg");
	glGenTextures(1, &TEXTURE_SPECULAR);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_SPECULAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, specular._width(), specular._height(), 0, GL_RGB, GL_UNSIGNED_BYTE, specular._image());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void keys(unsigned char button, int x, int y){

	if (button == 'w')	MACHINE['w'] = true;
	if (button == 's')	MACHINE['s'] = true;
	if (button == 'a')	MACHINE['a'] = true;
	if (button == 'd')	MACHINE['d'] = true;
	if (button == ' ')	MACHINE[' '] = true;
	if (button == 'c')	MACHINE['c'] = true;
	if (button == 'z')	MACHINE['z'] = true;
	if (button == 'x')	MACHINE['x'] = true;
	if (button == 'q')	MACHINE['q'] = true;
	if (button == 'e')	MACHINE['e'] = true;
	if (button == '+')	MACHINE['+'] = true;
	if (button == '-')	MACHINE['-'] = true;
	if (button == 'f')	MACHINE['f'] = true;
	if (button == '9')	MACHINE['9'] = true;
	if (button == '3')	MACHINE['3'] = true;
	if (button == '4')	MACHINE['4'] = true;
	if (button == '6')	MACHINE['6'] = true;
	if (button == '8')	MACHINE['8'] = true;
	if (button == '2')	MACHINE['2'] = true;
}

void keysUN(unsigned char button, int x, int y){

	if (button == 'w')	MACHINE['w'] = false;
	if (button == 's')	MACHINE['s'] = false;
	if (button == 'a')	MACHINE['a'] = false;
	if (button == 'd')	MACHINE['d'] = false;
	if (button == ' ')	MACHINE[' '] = false;
	if (button == 'c')	MACHINE['c'] = false;
	if (button == 'z')	MACHINE['z'] = false;
	if (button == 'x')	MACHINE['x'] = false;
	if (button == 'q')	MACHINE['q'] = false;
	if (button == 'e')	MACHINE['e'] = false;
	if (button == '+')	MACHINE['+'] = false;
	if (button == '-')	MACHINE['-'] = false;
	if (button == 'f')	MACHINE['f'] = false;
	if (button == '9')	MACHINE['9'] = false;
	if (button == '3')	MACHINE['3'] = false;
	if (button == '4')	MACHINE['4'] = false;
	if (button == '6')	MACHINE['6'] = false;
	if (button == '8')	MACHINE['8'] = false;
	if (button == '2')	MACHINE['2'] = false;
}

void Mouse(int button, int state, int x, int y){
	mouse_button = button;
}

void MouseCoords(int x, int y){
	GLUT::MousePos(width/2, height/2);

	camera.Yaw((width/2 - x)*0.05f);
	camera.Pitch((y - height/2)*0.05f);
}

void MouseMotionCoords(int x, int y){
	GLUT::MousePos(width/2, height/2);
	if(mouse_button == GLUT_LEFT_BUTTON){
		camera.Yaw((width/2 - x)*0.05f);
		camera.Pitch((y - height/2)*0.05f);
	}
}

void Idle(){
	/*READ MACHINE[]*/
	float angle_ROTATE = 3,
			angle_ZOOM = 0.1;
	t_vector move;

	move = 	camera._target() *	camera._speed() *
				((MACHINE['w'] ? 1 : 0)	-	(MACHINE['s'] ? 1 : 0));
	move = 	move +
				camera._side() *		camera._speed() *
				((MACHINE['a'] ? 1 : 0)	-	(MACHINE['d'] ? 1 : 0));
	move =	move +
				camera._up() *			camera._speed() *
				((MACHINE[' '] ? 1 : 0)	-	(MACHINE['c'] ? 1 : 0));

	// cout << "MOVE: " << move << endl;

	camera.Move	(move);

	camera.Pitch(angle_ROTATE * ((MACHINE['z'] ? 1 : 0) - (MACHINE['x'] ? 1 : 0)));
	camera.Yaw	(angle_ROTATE * ((MACHINE['q'] ? 1 : 0) - (MACHINE['e'] ? 1 : 0)));

	camera.Zoom	(angle_ZOOM * ((MACHINE['+'] ? 1 : 0) - (MACHINE['-'] ? 1 : 0)));

	camera =	(MACHINE['f']) ? Camera(75.0f, width / (float)height, 0.5f, 100.0f) : camera;

	glVertexAttrib4f(VERTEX_EYE_POSITION,	camera._pos()[0],
														camera._pos()[1],
														camera._pos()[2],
														1.0f);
	/*CAMERA DONE*/

	diffuse_light_pos[0] = diffuse_light_pos[0] + 0.1 * ((MACHINE['4'] ? 1 : 0) - (MACHINE['6'] ? 1 : 0));
	diffuse_light_pos[1] = diffuse_light_pos[1] + 0.1 * ((MACHINE['9'] ? 1 : 0) - (MACHINE['3'] ? 1 : 0));
	diffuse_light_pos[2] = diffuse_light_pos[2] + 0.1 * ((MACHINE['8'] ? 1 : 0) - (MACHINE['2'] ? 1 : 0));
	/*-----------*/
	GLUT::PostRedisplay();
}

void render(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(solid_shader_program);

	glUniform3f(UNIFORM_AMBIENT,			ambient[0],
													ambient[1],
													ambient[2]);

	diffuse[0]	= 0.3f;	diffuse[1]  = 0.3f;	diffuse[2]  = 0.3f;
	glUniform3f(UNIFORM_SOLID_DIFFUSE,	diffuse[0],
													diffuse[1],
													diffuse[2]);

	specular[0] = 1.0f;	specular[1] = 1.0f;	specular[2] = 1.0f;
	glUniform3f(UNIFORM_SOLID_SPECULAR,	specular[0],
													specular[1],
													specular[2]);

	solid_shiness = 3.0f;
	glUniform1f(UNIFORM_SOLID_SHINESS,	solid_shiness);
/*------------*/
	t_matrix result;
	t_matrix Model(4);
	int slices = 32;
	int stacks = 32;
	float buff[16];
	int numIndicies = (slices * stacks + slices) * 6;

	for (int i = 0; i < 4; ++i)
		Model[i][i] = 1.0f;

	glUniform1i(UNIFORM_SOLID_TOGGLE, 1);
/*LIGHT*/
	result = camera.Matrix();

	if (!VAO_LIGHT) VAO_LIGHT = SolidSphere(-1, VERTEX_SOLID_NORMAL, VERTEX_SOLID_POSITION, 1.0f, slices, stacks);

	t_vector vec_light(3, 0);

	Model = vec_action::Translate	(diffuse_light_pos);
	vec_light[0] = 0.1f;	vec_light[1] = 0.1f;	vec_light[2] = 0.1f;
	Model = vec_action::Scale		(vec_light) * Model;

	result = Model * result;

	glVertexAttrib3f(VERTEX_LIGHT_POSITION, diffuse_light_pos[0], diffuse_light_pos[1], diffuse_light_pos[2]);

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = result[i][j];
	glUniformMatrix4fv(UNIFORM_MVP, 1, GL_FALSE, buff);

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = Model[i][j];
	glUniformMatrix4fv(UNIFORM_MODEL, 1, GL_FALSE, buff);

	glBindVertexArray(VAO_LIGHT);

	glUniform3f(UNIFORM_SOLID_COLOR, 1.0f, 1.0f, 1.0f);
	emissive[0] = 0.9f;	emissive[1] = 0.9f;	emissive[2] = 0.9f;
	glUniform3f(UNIFORM_SOLID_EMISSIVE,	emissive[0],
													emissive[1],
													emissive[2]);
	specular[0] = 1.0f;	specular[1] = 1.0f;	specular[2] = 1.0f;
	glUniform3f(UNIFORM_SOLID_SPECULAR,	specular[0],
													specular[1],
													specular[2]);
	solid_shiness = 3.0f;
	glUniform1f(UNIFORM_SOLID_SHINESS,	solid_shiness);

	glDrawElements(GL_TRIANGLES, numIndicies, GL_UNSIGNED_INT, 0);
/*--------------*/
glBindVertexArray(0);
/*INIT LINES*/
	result = camera.Matrix();

	int LINES_COUNT = 271;

	if (!VAO_LINES) VAO_LINES = GRID(VERTEX_SOLID_POSITION, LINES_COUNT);

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = result[i][j];
	glUniformMatrix4fv(UNIFORM_MVP, 1, GL_FALSE, buff);

	for (int i = 0; i < 4; ++i)
			buff[i*4 + i] = 1.0f;
	glUniformMatrix4fv(UNIFORM_MODEL, 1, GL_FALSE, buff);

	glBindVertexArray(VAO_LINES);

	glUniform3f(UNIFORM_SOLID_COLOR, 0.0, 0.5, 0.7);

	emissive[0] = 0.5f;	emissive[1] = 0.5f;	emissive[2] = 0.5f;
	glUniform3f(UNIFORM_SOLID_EMISSIVE,	emissive[0],
													emissive[1],
													emissive[2]);

	specular[0] = 1.0f;	specular[1] = 1.0f;	specular[2] = 1.0f;
	glUniform3f(UNIFORM_SOLID_SPECULAR,	specular[0],
													specular[1],
													specular[2]);

	solid_shiness = 3.0f;
	glUniform1f(UNIFORM_SOLID_SHINESS,	solid_shiness);

	glDrawArrays(GL_LINES, 0, 3 * 2 * (LINES_COUNT * 2));
/*---------*/
	glUniform1i(UNIFORM_SOLID_TOGGLE, 0);
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
/*INIT SPHERE*/
	result = camera.Matrix();

	if (!VAO_SPHERE) VAO_SPHERE = SolidSphere(VERTEX_TEXTURE_COORDINATES, VERTEX_SOLID_NORMAL, VERTEX_SOLID_POSITION, 1.0f, slices, stacks);

	t_vector vec_sphere(3, 0);

	vec_sphere[0] = 0.5f;	vec_sphere[1] = 0.25f;	vec_sphere[2] = 0.5f;
	Model = vec_action::Translate	(vec_sphere);
	vec_sphere[0] = 0.2f;	vec_sphere[1] = 0.2f;	vec_sphere[2] = 0.2f;
	Model = vec_action::Scale		(vec_sphere) * Model;

	result = Model * result;

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = result[i][j];
	glUniformMatrix4fv(UNIFORM_MVP, 1, GL_FALSE, buff);

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = Model[i][j];
	glUniformMatrix4fv(UNIFORM_MODEL, 1, GL_FALSE, buff);

	glBindVertexArray(VAO_SPHERE);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_SPHERE);

	glUniform3f(UNIFORM_SOLID_COLOR, 1.0f, 1.0f, 1.0f);

	emissive[0] = 0.0f;	emissive[1] = 0.0f;	emissive[2] = 0.0f;
	glUniform3f(UNIFORM_SOLID_EMISSIVE,	emissive[0],
													emissive[1],
													emissive[2]);

	specular[0] = 1.0f;	specular[1] = 1.0f;	specular[2] = 1.0f;
	glUniform3f(UNIFORM_SOLID_SPECULAR,	specular[0],
													specular[1],
													specular[2]);
	solid_shiness = 3.0f;
	glUniform1f(UNIFORM_SOLID_SHINESS,	solid_shiness);
	glDrawElements(GL_TRIANGLES, numIndicies, GL_UNSIGNED_INT, 0);
/*----------*/

glBindTexture(GL_TEXTURE_2D, 0);
glBindVertexArray(0);
/*INIT CUBE*/
	result = camera.Matrix();
	if (!VAO_CUBE) VAO_CUBE = SolidCube(-1, -1, VERTEX_TEXTURE_COORDINATES, VERTEX_SOLID_NORMAL, VERTEX_SOLID_POSITION, 1.0f);

	t_vector vec_cube(3, 0);

	vec_cube[0] = -0.5f;	vec_cube[1] = 0.25f;	vec_cube[2] = 0.5f;
	Model = vec_action::Translate	(vec_cube);
	vec_cube[0] = 0.2f;	vec_cube[1] = 0.2f;	vec_cube[2] = 0.2f;
	Model = vec_action::Scale		(vec_cube)	* Model;

	result = Model * result;

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = result[i][j];
	glUniformMatrix4fv(UNIFORM_MVP, 1, GL_FALSE, buff);

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = Model[i][j];
	glUniformMatrix4fv(UNIFORM_MODEL, 1, GL_FALSE, buff);

	glBindVertexArray(VAO_CUBE);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_CUBE);

	glUniform3f(UNIFORM_SOLID_COLOR, 1.0f, 1.0f, 1.0f);

	emissive[0] = 0.0f;	emissive[1] = 0.0f;	emissive[2] = 0.0f;
	glUniform3f(UNIFORM_SOLID_EMISSIVE,	emissive[0],
													emissive[1],
													emissive[2]);

	specular[0] = 1.0f;	specular[1] = 1.0f;	specular[2] = 1.0f;
	glUniform3f(UNIFORM_SOLID_SPECULAR,	specular[0],
													specular[1],
													specular[2]);
	solid_shiness = 5.0f;
	glUniform1f(UNIFORM_SOLID_SHINESS,	solid_shiness);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
/*---------*/

glBindTexture(GL_TEXTURE_2D, 0);
glBindVertexArray(0);
/*INIT SQUARE*/
	result = camera.Matrix();
	if (!VAO_SQUARE) VAO_SQUARE = SolidSquare(VERTEX_TEXTURE_COORDINATES, VERTEX_SOLID_NORMAL, VERTEX_SOLID_POSITION, 1.0f);

	t_vector vec_square(3, 0);

	vec_square[0] = 0.0f; vec_square[1] = 0.05f; vec_square[2] = 0.0f;
	Model = vec_action::Translate	(vec_square);
	vec_square[0] = 1.0f; vec_square[1] = 1.0f; vec_square[2] = 1.0f;
	Model = vec_action::Scale		(vec_square) * Model;

	result = result * Model;

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = result[i][j];
	glUniformMatrix4fv(UNIFORM_MVP, 1, GL_FALSE, buff);

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			buff[i*4 + j] = Model[i][j];
	glUniformMatrix4fv(UNIFORM_MODEL, 1, GL_FALSE, buff);

	glBindVertexArray(VAO_SQUARE);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_SQUARE);

	glUniform3f(UNIFORM_SOLID_COLOR, 1.0f, 1.0f, 1.0f);

	emissive[0] = 0.1f;	emissive[1] = 0.1f;	emissive[2] = 0.1f;
	glUniform3f(UNIFORM_SOLID_EMISSIVE,	emissive[0],
													emissive[1],
													emissive[2]);

	specular[0] = 0.1f;	specular[1] = 0.1f;	specular[2] = 0.1f;
	glUniform3f(UNIFORM_SOLID_SPECULAR,	specular[0],
													specular[1],
													specular[2]);

	solid_shiness = 2.0f;
	glUniform1f(UNIFORM_SOLID_SHINESS,	solid_shiness);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
/*-----------*/
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUniform1i(UNIFORM_SOLID_TOGGLE, 2);

/*TEXTURE MAPPING CUBE!*/
	result = camera.Matrix();

	if (!VAO_CUBE_MAP) VAO_CUBE_MAP = SolidCube(VERTEX_SOLID_TANGENT, VERTEX_SOLID_BITANGENT, VERTEX_TEXTURE_COORDINATES, VERTEX_SOLID_NORMAL, VERTEX_SOLID_POSITION, 0.1f);

	t_vector map_vec(4, 1.0f);

	map_vec[0] = 0.0f;	map_vec[1] = 0.25f;	map_vec[2] = 1.0f;
	Model = vec_action::Translate(map_vec);
	map_vec[0] = 1.5f;	map_vec[1] = 1.5f;	map_vec[2] = 1.5f;
	Model = vec_action::Scale(map_vec) * Model;
	result = Model * result;

	for (unsigned int i = 0; i < 4; ++i)
		for (unsigned int j = 0; j < 4; ++j)
			buff[i*4 + j] = result[i][j];
	glUniformMatrix4fv(UNIFORM_MVP, 1, GL_FALSE, buff);

	for (unsigned int i = 0; i < 4; ++i)
		for (unsigned int j = 0; j < 4; ++j)
			buff[i*4 + j] = Model[i][j];
	glUniformMatrix4fv(UNIFORM_MODEL, 1, GL_FALSE, buff);
	glBindVertexArray(VAO_CUBE_MAP);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_CUBE);
	glUniform1i(UNIFORM_SAMPLER_DIFFUSE, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_NORMAL);
	glUniform1i(UNIFORM_SAMPLER_NORMAL, 1);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, TEXTURE_SPECULAR);
	glUniform1i(UNIFORM_SAMPLER_SPECULAR, 1);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

/*----------------*/
	glutSwapBuffers();
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

int main(int argc, char *argv[]){
	width		= 600;
	height	= 600;

	for (unsigned short i = 0; i < 256; ++i)
		MACHINE[i] = false;

	GLUT::Default("MAIN", &argc, argv, width, height);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LESS);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK){
		cerr << "Error: "<< glewGetErrorString(err) << endl;
		return 1;
	}

	ambient[0]	= 0.1f;	ambient[1]	= 0.1f;	ambient[2]	= 0.1f;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0, width, height);

	shader_program sp = init_shader_program();
	solid_shader_program = sp.id();

	init_texture_images();

	camera = Camera(75.0f, width / (float)height, 0.5f, 100.0f);
	camera.set_speed(0.08f);

	GLUT::Display(render);

	cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION) << endl;

	GLUT::Idle(Idle);
	GLUT::Keyboard(keys);
	GLUT::KeyboardUN(keysUN);
	GLUT::Mouse(Mouse);
	GLUT::MouseMotion(MouseMotionCoords);

	GLUT::MainLoop();
	glUseProgram(0);

	return 0;
}
