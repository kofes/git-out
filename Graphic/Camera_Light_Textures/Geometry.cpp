#include "Geometry.hpp"

void CalcTangentVector(	const float vertex0[3],		const float vertex1[3],		const float vertex2[3],
								const float texCoord0[2],	const float texCoord1[2],	const float texCoord2[2],
								float tangent[3],				float bitangent[3]
							 ){
	t_vector deltaPos0(3, 0.0f);
	deltaPos0[0] = vertex1[0] - vertex0[0];
	deltaPos0[1] = vertex1[1] - vertex0[1];
	deltaPos0[2] = vertex1[2] - vertex0[2];

	t_vector deltaPos1(3, 0.0f);
	deltaPos1[0] = vertex2[0] - vertex0[0];
	deltaPos1[1] = vertex2[1] - vertex0[1];
	deltaPos1[2] = vertex2[2] - vertex0[2];

	t_vector deltaUV0(2, 0.0f);
	deltaUV0[0] = texCoord1[0] - texCoord0[0];
	deltaUV0[1] = texCoord1[1] - texCoord0[1];

	t_vector deltaUV1(2, 0.0f);
	deltaUV1[0] = texCoord2[0] - texCoord0[0];
	deltaUV1[1] = texCoord2[1] - texCoord0[1];

	float det = 1.0f / (deltaUV0[0] * deltaUV1[1]) - (deltaUV0[1] * deltaUV1[0]);

	t_vector t = ((deltaPos0 * deltaUV1[1]) - (deltaPos1 * deltaUV0[1])) * det;
	t_vector b = ((deltaPos1 * deltaUV0[0]) - (deltaPos0 * deltaUV1[0])) * det;

   t.NormalizeInPlace();
   b.NormalizeInPlace();

	tangent[0] = t[0];
	tangent[1] = t[1];
	tangent[2] = t[2];

	bitangent[0] = b[0];
	bitangent[1] = b[1];
	bitangent[2] = b[2];
}

GLuint geometry::GRID(GLint VERTEX, unsigned int COUNT){
	float c = (COUNT - (COUNT % 2 == 1)) / 20.0f;

	float vert[] = {
		-c,	0.0f,	-c,
		-c,	0.0f,	+c,
	};

	float buff[2 * 3 * (COUNT * 2)];

	for (int i = 0; i < COUNT; ++i){
		buff[i*6] = vert[0] + 0.1 * i;			buff[i*6 + 1] = vert[1];				buff[i*6 + 2] = -1.0f + vert[2];
		buff[i*6 + 3] = vert[3] + 0.1 * i;		buff[i*6 + 4] = vert[4];				buff[i*6 + 5] = -1.0f + vert[5];

		buff[(COUNT + i)*6] = vert[0];			buff[(COUNT + i)*6 + 1] = vert[1];	buff[(COUNT + i)*6 + 2] = -1.0f + vert[2]  + 0.1 * i;
		buff[(COUNT + i)*6 + 3] = -vert[3];		buff[(COUNT + i)*6 + 4] = vert[4];	buff[(COUNT + i)*6 + 5] = -1.0f - vert[5]  + 0.1 * i;

	};

	GLuint vao;

	glGenVertexArrays(1, &vao);
	if (vao < 0){
		std::cout << "could not generate buffers" << std::endl;
		return 0;
	}
	glBindVertexArray(vao);

	GLuint vbos;
	glGenBuffers(1, &vbos);

	glBindBuffer(GL_ARRAY_BUFFER, vbos);
	glBufferData(GL_ARRAY_BUFFER, (2 * 3 * (COUNT * 2)) * sizeof(float), buff, GL_STATIC_DRAW);
	glVertexAttribPointer(VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VERTEX);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return vao;
}

GLuint geometry::SolidCube(GLint TANGENT, GLint BITANGENT, GLint TEXTURE, GLint NORMAL, GLint VERTEX, float radius){
	float positions[] = {
		// Positive Z Face.
       -radius, -radius,  radius,
        radius, -radius,  radius,
        radius,  radius,  radius,
       -radius,  radius,  radius,
      // Negative Z Face.
        radius, -radius, -radius,
       -radius, -radius, -radius,
       -radius,  radius, -radius,
        radius,  radius, -radius,
      // Positive Y Face.
       -radius,  radius,  radius,
        radius,  radius,  radius,
        radius,  radius, -radius,
       -radius,  radius, -radius,
      // Negative Y Face.
       -radius, -radius, -radius,
        radius, -radius, -radius,
        radius, -radius,  radius,
       -radius, -radius,  radius,
      // Positive X Face.
        radius, -radius,  radius,
        radius, -radius, -radius,
        radius,  radius, -radius,
        radius,  radius,  radius,
      // Negative X Face.
       -radius, -radius, -radius,
       -radius, -radius,  radius,
       -radius,  radius,  radius,
       -radius,  radius, -radius
	};
	GLuint indicies[] = {
		0, 1, 2, 2, 3, 0,
		4, 5, 6, 6, 7, 4,
		8, 9, 10,10,11,8,
		12,13,14,14,15,12,
		16,17,18,18,19,16,
		20,21,22,22,23,20
	};
	float normals[] = {
		 0.0f,  0.0f,  1.0f,
		 0.0f,  0.0f,  1.0f,
		 0.0f,  0.0f,  1.0f,
		 0.0f,  0.0f,  1.0f,

		 0.0f,  0.0f, -1.0f,
		 0.0f,  0.0f, -1.0f,
		 0.0f,  0.0f, -1.0f,
		 0.0f,  0.0f, -1.0f,

		 0.0f,  1.0f,  0.0f,
		 0.0f,  1.0f,  0.0f,
		 0.0f,  1.0f,  0.0f,
		 0.0f,  1.0f,  0.0f,

		 0.0f, -1.0f,  0.0f,
		 0.0f, -1.0f,  0.0f,
		 0.0f, -1.0f,  0.0f,
		 0.0f, -1.0f,  0.0f,

		 1.0f,  0.0f,  0.0f,
		 1.0f,  0.0f,  0.0f,
		 1.0f,  0.0f,  0.0f,
		 1.0f,  0.0f,  0.0f,

		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f,
		-1.0f,  0.0f,  0.0f
	};
	float texture_coords[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f
	};

	float tangents[24 * 3];
	float bitangents[24 * 3];

	for (unsigned int i = 0; i < 24; i += 4){
		float tangent_vector[3] = {0.0f, 0.0f, 0.0f};
		float bitangent_vector[3] = {0.0f, 0.0f, 0.0f};

		float pos[3][3];
		pos[0][0] = positions[i*3];		pos[0][1] = positions[i*3 + 1];	pos[0][2] = positions[i*3 + 1];
		pos[1][0] = positions[i*3 + 3];	pos[1][1] = positions[i*3 + 4];	pos[1][2] = positions[i*3 + 5];
		pos[2][0] = positions[i*3 + 9];	pos[2][1] = positions[i*3 + 10];	pos[2][2] = positions[i*3 + 11];

		float norm[3];
		norm[0] = normals[i*3];	norm[1] = normals[i*3 + 1];	norm[2] = normals[i*3 + 2];

		float texcoord[3][2];
		texcoord[0][0] = texture_coords[i*2];		texcoord[0][1] = texture_coords[i*2 + 1];
		texcoord[1][0] = texture_coords[i*2 + 2];	texcoord[1][1] = texture_coords[i*2 + 3];
		texcoord[2][0] = texture_coords[i*2 + 6];	texcoord[2][1] = texture_coords[i*2 + 7];

		CalcTangentVector(pos[0], pos[1], pos[2],
								texcoord[0], texcoord[1], texcoord[2],
								tangent_vector, bitangent_vector);

		tangents[i*3]		= tangent_vector[0];	tangents[i*3 + 1]	= tangent_vector[1];	tangents[i*3 + 2]	= tangent_vector[2];
		tangents[i*3 + 3]	= tangent_vector[0];	tangents[i*3 + 4]	= tangent_vector[1];	tangents[i*3 + 5]	= tangent_vector[2];
		tangents[i*3 + 6]	= tangent_vector[0];	tangents[i*3 + 7]	= tangent_vector[1];	tangents[i*3 + 8]	= tangent_vector[2];
		tangents[i*3 + 9]	= tangent_vector[0];	tangents[i*3 + 10]= tangent_vector[1];	tangents[i*3 + 11]= tangent_vector[2];

		bitangents[i*3]		= bitangent_vector[0];	bitangents[i*3 + 1]	= bitangent_vector[1];	bitangents[i*3 + 2]	= bitangent_vector[2];
		bitangents[i*3 + 3]	= bitangent_vector[0];	bitangents[i*3 + 4]	= bitangent_vector[1];	bitangents[i*3 + 5]	= bitangent_vector[2];
		bitangents[i*3 + 6]	= bitangent_vector[0];	bitangents[i*3 + 7]	= bitangent_vector[1];	bitangents[i*3 + 8]	= bitangent_vector[2];
		bitangents[i*3 + 9]	= bitangent_vector[0];	bitangents[i*3 + 10]	= bitangent_vector[1];	bitangents[i*3 + 11]	= bitangent_vector[2];
	}

	GLuint vao;
   glGenVertexArrays(1, &vao);
	if (vao < 0){
		std::cout << "could not generate buffers" << std::endl;
		return 0;
	}
   glBindVertexArray(vao);

   GLuint vbos[6];
   glGenBuffers(6, vbos);

   glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
   glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), positions, GL_STATIC_DRAW);
	glVertexAttribPointer(VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(VERTEX);

	if (NORMAL != -1){
		glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
		glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), normals, GL_STATIC_DRAW);
		glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_TRUE, 0, 0);
		glEnableVertexAttribArray(NORMAL);
	}

	if (TEXTURE != -1){
		glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
		glBufferData(GL_ARRAY_BUFFER, 24 * 2 * sizeof(float), texture_coords, GL_STATIC_DRAW);
		glVertexAttribPointer(TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TEXTURE);
	}

	if ((TANGENT != -1)&&(BITANGENT != -1)){
		glBindBuffer(GL_ARRAY_BUFFER, vbos[3]);
		glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), tangents, GL_STATIC_DRAW);
		glVertexAttribPointer(TANGENT, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TANGENT);

		glBindBuffer(GL_ARRAY_BUFFER, vbos[4]);
		glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), bitangents, GL_STATIC_DRAW);
		glVertexAttribPointer(BITANGENT, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(BITANGENT);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[5]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return vao;
}

GLuint geometry::SolidSquare(GLint TEXTURE, GLint NORMAL, GLint VERTEX, float radius){
	float positions[] = {
		-radius,  0.0f, -radius,
		 radius,  0.0f, -radius,
		-radius,  0.0f,  radius,
		 radius,  0.0f,  radius
	};
	GLuint indicies[] = {
		0, 1, 2,
		1, 2, 3
	};
	float normals[] = {
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};
	float texture_coords[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f
	};

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	if (vao < 0){
		std::cout << "could not generate buffers" << std::endl;
		return 0;
	}

	GLuint vbos[4];
	glGenBuffers(4, vbos);

	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), positions, GL_STATIC_DRAW);
	glVertexAttribPointer(VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(VERTEX);

	if (NORMAL != -1){
		glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
		glBufferData(GL_ARRAY_BUFFER, 3 * 6 * sizeof(float), normals, GL_STATIC_DRAW );
		glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_TRUE, 0, 0 );
		glEnableVertexAttribArray(NORMAL);
	}

	if (TEXTURE != -1){
		glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
		glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), texture_coords, GL_STATIC_DRAW);
		glVertexAttribPointer(TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TEXTURE);
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return vao;
}

GLuint geometry::SolidSphere(GLint TEXTURE, GLint NORMAL, GLint VERTEX, float radius, int slices, int stacks){

    const float pi = 3.1415926535897932384626433832795f;
    const float _2pi = 2.0f * pi;

    std::vector<float> positions;
	 std::vector<float> normals;
	 std::vector<float> texture_coords;

    for(unsigned int i = 0; i <= stacks; ++i){
        float V = i / (float)stacks;
        float phi = V * pi;

        for (unsigned int j = 0; j <= slices; ++j){
            float U = j / (float)slices;
            float theta = U * _2pi;

            float X = cos(theta) * sin(phi);
            float Y = cos(phi);
            float Z = sin(theta) * sin(phi);

				positions.push_back(X * radius);	positions.push_back(Y * radius);	positions.push_back(Z * radius);
				normals.push_back(X);				normals.push_back(Y);				normals.push_back(Z);
				texture_coords.push_back(U);		texture_coords.push_back(V);
        }
    }

    std::vector<GLuint> indicies;

    for (unsigned int i = 0; i < slices * stacks + slices; ++i){
        indicies.push_back(i);
        indicies.push_back(i + slices + 1);
        indicies.push_back(i + slices);

        indicies.push_back(i + slices + 1);
        indicies.push_back(i);
        indicies.push_back(i + 1);
    }

    GLuint vao;
    glGenVertexArrays(1, &vao);
	 if (vao < 0){
		 std::cout << "could not generate buffers" << std::endl;
		 return 0;
	 }
    glBindVertexArray(vao);

    GLuint vbos[4];
    glGenBuffers(4, vbos);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(float), positions.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(VERTEX);

	 if (NORMAL != -1){
		 glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
		 glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), normals.data(), GL_STATIC_DRAW);
		 glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_TRUE, 0, 0);
		 glEnableVertexAttribArray(NORMAL);
	 }

	 if (TEXTURE != -1){
		 glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
	    glBufferData(GL_ARRAY_BUFFER, texture_coords.size() * sizeof(float), texture_coords.data(), GL_STATIC_DRAW);
	    glVertexAttribPointer(TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, 0);
	    glEnableVertexAttribArray(TEXTURE);
	 }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies.size() * sizeof(GLuint), indicies.data(), GL_STATIC_DRAW);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    return vao;
}
