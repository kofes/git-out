#ifndef __INCLUDE_MY_GLUT_

#define __INCLUDE_MY_GLUT_

#include <GL/freeglut.h>
#include <string>

namespace GLUT{
	inline void Default(std::string label, int *argc, char **argv, int width, int height)
																		{
																		 glutInit(argc, argv);
																		 glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
																		 glutInitWindowSize(width, height);
																		 glutInitWindowPosition(1000, 100);
																		 glutInitContextVersion(3, 3);
																		 glutInitContextProfile(GLUT_CORE_PROFILE);

																		 glutCreateWindow(label.c_str());}
	inline void Init(int *argc, char **argv) 				{glutInit(argc, argv);}
	inline void DisplayMode(unsigned int mode)			{glutInitDisplayMode(mode);}
	inline void WindowSize(int width, int height)		{glutInitWindowSize(width, height);}
	inline void WindowPos(int x, int y)						{glutInitWindowPosition(x, y);}
	inline void ContextVersion(int major, int minor)	{glutInitContextVersion(major, minor);}
	inline void ContextProfile(int profile)				{glutInitContextProfile(profile);}
	inline void CreateWindow(std::string label)			{glutCreateWindow(label.c_str());}
	inline void Display(void (*render)(void))				{glutDisplayFunc(render);}
	inline void Reshape(void (*reshape)(int w, int h))	{glutReshapeFunc(reshape);}
	inline void MainLoop()										{glutMainLoop();}
	inline void PostRedisplay()								{glutPostRedisplay();}
	inline void Timer(unsigned int msecs, void (*update)(int value), int value)
																		{glutTimerFunc(msecs, update, value);}
	inline void Keyboard(void (*keys)(unsigned char button, int x, int y))
																		{glutKeyboardFunc(keys);}
	inline void KeyboardUN(void (*keys)(unsigned char button, int x, int y))
																		{glutKeyboardUpFunc(keys);}
	inline void MousePassive(void (*coords)(int x, int y))
																		{glutPassiveMotionFunc(coords);}
	inline void MouseMotion(void (*coords)(int x, int y))
																		{glutMotionFunc(coords);}
	inline void MousePos(int x, int y)						{glutWarpPointer(x, y);}
	inline void Mouse(void (*mouse_describe)(int button, int state, int x, int y))
																		{glutMouseFunc(mouse_describe);}
	inline void Idle(void (*idle)(void))					{glutIdleFunc(idle);}
}

#endif
