#version 330 core
out vec3 color;

uniform vec3	solid_color;

uniform vec3	ambient;

uniform vec3	solid_emissive;
uniform vec3	solid_diffuse;
uniform vec3	solid_specular;
uniform float	solid_shiness;

uniform int		solid_toggle;

in vec3 SOLID_NORMAL;

in vec3 EYE_NORMAL;

in vec3 LIGHT_NORMAL;

in float LIGHT_DISTANCE;

in vec2 TEXTURE_COORDINATES;
uniform sampler2D texture_sampler;
uniform sampler2D diffuse_sampler;
uniform sampler2D normal_sampler;
uniform sampler2D specular_sampler;

void main(){
	float intensive = 1 / (0.5 + 0.3 * LIGHT_DISTANCE + 0.5 * LIGHT_DISTANCE * LIGHT_DISTANCE);
	vec3 Emissive = solid_emissive;
	vec3 L = LIGHT_NORMAL;//LIGHT_DIRECTIONAL_TANGENT
	vec3 E = EYE_NORMAL;//EYE_DIRECTIONAL_TANGENT

	switch (solid_toggle){
		case 0:{
			vec3 Ambient  = ambient;

			vec3 N = SOLID_NORMAL;
			float NdotL = max(dot(N, L), 0);
			vec3 Diffuse = NdotL * solid_diffuse * intensive;

			vec3 R = reflect(-L, N);
			float RdotE = max(dot(R, E), 0);
			vec3 Specular = pow(RdotE, solid_shiness) * solid_specular * intensive;

			color = (Emissive + Ambient + Diffuse * solid_color + Specular * solid_color) * texture(texture_sampler, TEXTURE_COORDINATES).rgb;
		}
		break;
		case 1:{
			vec3 Ambient = ambient;

			vec3 N = SOLID_NORMAL;
			float NdotL = max(dot(N, L), 0);
			vec3 Diffuse = NdotL * solid_diffuse * intensive;

			vec3 R = reflect(-L, N);
			float RdotE = max(dot(R, E), 0);
			vec3 Specular = pow(RdotE, solid_shiness) * solid_specular * intensive;

			color = (Emissive + Ambient + Diffuse + Specular) * solid_color;
		}
		break;
		case 2:{
			float LightPower = 5.0;

			vec3 Diffuse	= texture(diffuse_sampler, TEXTURE_COORDINATES).rgb;
			vec3 Ambient	= ambient * Diffuse;
			vec3 Specular	= texture(specular_sampler, TEXTURE_COORDINATES).rgb * solid_specular.x;

			vec3 texture_tangent = normalize(texture(normal_sampler, vec2(TEXTURE_COORDINATES.x, -TEXTURE_COORDINATES.y)).rgb * 2.0 - 1.0);

			vec3 N = texture_tangent;

			float cosTheta = clamp(dot(N, L), 0, 1);
			Diffuse = Diffuse * solid_color * LightPower * cosTheta * intensive;

			vec3 R = reflect(-L, N);

			float cosAlpha = clamp(dot(E, R), 0, 1);
			Specular = Specular * solid_color * LightPower * pow(cosAlpha, solid_shiness) * intensive;

			color = Ambient + Diffuse + Specular;
		}
		break;
	}//END!
}
