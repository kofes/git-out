#ifndef __INCLUDE_MY_GEOMETRY_

#include <iostream>
#include <vector>
#include <cmath>
#include <GL/glew.h>
#include "mat_vec.hpp"

namespace geometry{
	GLuint GRID			(GLint VERTEX, unsigned int COUNT);
	GLuint SolidSphere(GLint TEXTURE, GLint NORMAL, GLint VERTEX, float radius, int slices, int stacks);
	GLuint SolidSquare(GLint TEXTURE, GLint NORMAL, GLint VERTEX, float radius);
	GLuint SolidCube	(GLint DIFFUSE, GLint SPECULAR, GLint TEXTURE, GLint NORMAL, GLint VERTEX, float radius);
}

#endif
