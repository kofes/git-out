#version 330 core

uniform int	solid_toggle;

uniform mat4 mvp;
uniform mat4 model;

in vec3 solid_position;

in vec3 solid_normal;
in vec3 solid_tangent;				//NEW
in vec3 solid_bitangent;			//NEW

in vec4 eye_position;
in vec3 light_position;

in vec2 texture_coordinates;		//vertexUV

out vec3 LIGHT_NORMAL;

out vec3 EYE_NORMAL;

out vec3 SOLID_NORMAL;
out vec2 TEXTURE_COORDINATES;
out float LIGHT_DISTANCE;

void main(){
	vec4 position = mvp * vec4(solid_position, 1.0);

	gl_Position = position;

	LIGHT_DISTANCE = distance(light_position, (model * vec4(solid_position, 1.0)).xyz);
	LIGHT_NORMAL = normalize(light_position - (model * vec4(solid_position, 1.0)).xyz);
	EYE_NORMAL = normalize((eye_position - model * vec4(solid_position, 1.0)).xyz);
	SOLID_NORMAL = normalize((model * vec4(solid_normal, 0.0)).xyz);

	TEXTURE_COORDINATES = texture_coordinates;

	if (solid_toggle == 2){
		vec3 tangent = mat3(model) * normalize(solid_tangent);
		vec3 bitangent = mat3(model) * normalize(solid_bitangent);

		mat3 TBN = transpose(mat3(
			-tangent,
			-bitangent,
			-SOLID_NORMAL
		));

		LIGHT_NORMAL = TBN * LIGHT_NORMAL;
		EYE_NORMAL = TBN * EYE_NORMAL;
	}
}
