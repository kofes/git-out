#include "Camera.hpp"

Camera::Camera(){
	pos = t_vector(3, 0);
	pitch_angle =
	yaw_angle =	0;
	pos[1] =		0.5f;
	pos[2] =		-1.5f;
	fovy =		75.0f;
	as =			1.0f;
	near =		0.1f;
	far =			100.0f;
	speed =		0.1f;
}

Camera::Camera(float _fovy, float _as, float _near, float _far){
	pos = t_vector(3, 0);
	pitch_angle =
	yaw_angle =	0;
	pos[1] =		0.5f;
	pos[2] =		-1.5f;
	fovy =		_fovy;
	as =			_as;
	near =		_near;
	far =			_far;
	speed =		0.1f;
}

void Camera::Zoom(float angle){
	fovy += angle;
	if (fovy < 73.2f)	fovy = 73.2f;
	if (fovy > 75.3f)	fovy = 75.3f;
}

void Camera::Move(t_vector &src){
	pos[0] += src[0];
	pos[1] += src[1];
	pos[2] += src[2];
}

void Camera::Pitch(float angle){
	angle += 360;
	angle -= ((short)angle / 360) * 360;
	pitch_angle = pitch_angle + angle + 360;
	pitch_angle -= ((short)pitch_angle / 360) * 360;
	if	((pitch_angle > 90)&&(pitch_angle < 180)
		||(pitch_angle > 180)&&(pitch_angle < 270))
		pitch_angle = pitch_angle > 180 ? 270 : 90;
}

void Camera::Yaw(float angle){
	angle += 360;
	angle -= ((short)angle / 360) * 360;
	yaw_angle = yaw_angle + angle + 360;
	yaw_angle -= ((short)yaw_angle / 360) * 360;
}

t_matrix Camera::Matrix(){
	t_matrix result(4);
	t_vector target(3, 0), up(3, 0), side;
	float yaw = yaw_angle * __CAMERA_PI / 180;
	float pitch = pitch_angle * __CAMERA_PI / 180 + __CAMERA_PI;

	up[0] = sin(yaw) * sin(pitch);	target[0] = sin(yaw) * cos(pitch);
	up[1] = cos(pitch);					target[1] = -sin(pitch);
	up[2] = cos(yaw) * sin(pitch);	target[2] = cos(pitch) * cos(yaw);
	side = up(target);

	for (unsigned int i = 0; i < 3; ++i){
		result[i][0] = side[i];
		result[i][1] =	up[i];
		result[i][2] = target[i];
	}

	result[3][0] = -(pos * side);
	result[3][1] = -(pos * up);
	result[3][2] = -(pos * target);
	result[3][3] = 1;

	t_matrix perspective(4);
	float f = cos(fovy / 2) / sin(fovy / 2);

	perspective[0][0] = f / as;
	perspective[1][1] = f;
	perspective[2][2] = (far + near) / (near - far);
	perspective[2][3] = 2 * far * near / (near - far);
	perspective[3][2] = -1;

	result = result * perspective;

	return result;
}

t_matrix vec_action::Translate(t_vector &move){
	if (move.size() < 3)
		return t_matrix();
	t_matrix result(4);
	for (unsigned int i = 0; i < result.size(); ++i)
		result[i][i] = 1.0f;
	result[3][0] = move[0];
	result[3][1] = move[1];
	result[3][2] = move[2];
	return result;
}

t_matrix vec_action::Rotate(t_vector &axis, unsigned short angle){
	if (axis.size() < 3)
		return t_matrix();
	t_matrix result(4);
	angle -= (angle / 360) * 360;
	float c = cos(angle * __CAMERA_PI / 180),
			s = sin(angle * __CAMERA_PI / 180);
	t_vector norm = axis.Normalize();
	float x = norm[0],
			y = norm[1],
			z = norm[2];
	result[0][0] = x * (1 - c) + c;				result[0][1] = y * x * (1 - c) - z * s;	result[0][2] = z * x * (1 - c) + y * s;
	result[1][0] = x * y * (1 - c) + z * s;	result[1][1] = y * y * (1 - c) + c;			result[1][2] = z * y * (1 - c) - x * s;
	result[2][0] = x * z * (1 - c) - y * s;	result[2][1] = y * z * (1 - c) + x * s;	result[2][2] = z * z * (1 - c) + c;
	result[3][3] = 1;
	return result;
}

t_matrix vec_action::Scale(t_vector &scale){
	if (scale.size() < 3)
		return t_matrix();
	t_matrix result(4);
	for (unsigned int i = 0; i < 3; ++i)
		result[i][i] = scale[i];
	result[3][3] = 1;
	return result;
}

t_matrix vec_projection::Ortho(t_matrix &src, float left, float right, float top, float bottom, float near, float far){
	if (src.rows() != 4)
		return t_matrix();
	t_matrix result(4);
	result[0][0] = 2 / (right - left);
	result[1][1] = 2 / (top - bottom);
	result[2][2] = -2 / (far - near);
	result[0][3] = -(right + left)/(right - left);
	result[1][3] = -(top + bottom)/(top - bottom);
	result[2][3] = -(far + near)/(far - near);
	result[3][3] = 1;
	result = result * src;
	return result;
}

t_matrix vec_projection::Frustum(t_matrix &src, float left, float right, float top, float bottom, float near, float far){
	if (src.rows() != 4)
		return t_matrix();
	t_matrix result(4);
	result[0][0] = 2 * near / (right - left);
	result[1][1] = 2 * near / (top - bottom);
	result[0][2] = (right + left) / (right - left);
	result[1][2] = (top + bottom)/(top - bottom);
	result[3][2] = -(far + near)/(far - near);
	result[2][3] = -1;
	result[3][2] = -2 * far * near / (far - near);
	result = result * src;
	return result;
}

t_matrix vec_projection::Perspective(t_matrix &src, float fovy, float as, float near, float far){
	if (src.rows() != 4)
		return t_matrix();
	t_matrix result(4);
	float f = cos(fovy / 2) / sin(fovy / 2);
	result[0][0] = f / as;
	result[1][1] = f;
	result[2][2] = (far + near) / (near - far);
	result[2][3] = 2 * far * near / (near - far);
	result[3][2] = -1;
	result = result * src;
	return src;
}
