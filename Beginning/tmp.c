#include <stdio.h>
#include <math.h>
#include <float.h>
//#include <stdlib.h>
/*
size_t - max указателя +1
*/
//#include <limits.h>
/*
Что будет если сложить два указателя???
		Massive
==========================
	char A[5] ={'a','52','b','f','0'}; <- Одномерный
	A[0] ~ *A
	A[i] ~ *(A+i)
	int A[5][3] <- Двумерный массив
	Статические массивы хранятся построчно
	for (i=0; i!=5*4; ++i, printf("%x\n", ((int *)A)[i]));
	for (i=0; i!=5*4; ++i, printf("%x\n", ((unsigned long)A[i])));
==========================
		String
==========================
#include <string.h>
strlen(S);
strcmp(S1, S2); <- сравнивает строки
0 - совпадение
-1 - 2 больше 1
1 - 1 больше 2
strcat(s0, s1, s2); <- конкатинация двух строк
char *S;
char S[4] = {'a', 52, 'b','\0'};
char *S = "abs";
%s - строковый формат
{
	char S[3][5] = {"abcd", "1234", "+-*f"};
	printf("%s\n", S[0]);
	printf("%s\n", S[1]);
	scanf("%s", S[0]);
	printf("%s\n", S[1]);
	unsigned int A[5][4];
	unsigned char k = 0, i, j;
	for (i=0; i!=5; ++i, printf("\n"))
		for (j=0; j!=4; ++j, A[i][j] = ++k, printf("%u\t", A[i][j]));
	
}
======================
		Unite
======================
tupedef <- объявление нового тип
	struct - в разных участках памяти
	union - в одном блоке памяти
	struct
	{
		int a,b;
		double c;
	}t_stract;
	union
	{
		double a;
		char S[sizeof(double)];
	}t_union;
	t_union D;
	D.a = 1.95;
	for (i=0; i<sizeof(double); ++i)
		printf("%u ", D.S[i]);
======================
		Files
======================
<stdio.h>
	stdin; - стандартный входной поток
	stout; - стандартный выходной поток
	fseeck - перемещение в потоке
	fflush - очищение буфера
Опр.
	stream - файловые потоки и т.д.
	thread - потоки исполнение - многопоточность(позже)
FILE - "тип", структура/указатель на файл

FILE *fp;
Файловый дескриптор - указатель типа FILE
fp = fopen("input.dat", "rt");
Если не удалось получить доступ к файлу, то fp = NULL.
`````
"r" - на чтение
"w" - на запись с удалением старого
"a" или "r+" - не стирать содержимого старого файла
`````
"t" - в текстовом режиме
"b" - в бинарном режиме
`````
fclose(fp); <- закрытие файла
fprintf(fp, "%i\n", 10);
fscanf(fp, "...", ...); <- возвращает количество считанных переменных
======================
		Function
======================
<тип> <имя> (<аргумент>)
Указатель на функцию
int test(int a, double *b); <- функция
int (*p)(int, double*); <- указатель на функцию
...
p = test; <=> p = &test <- присваивания адреса с функции
вызов:
	(*p)(...); <=?> p(...); <- вызов функции
typedef int (*t_func)(int, double*); <- "обобщение типа"
t_func p1, p2, p3; <- объявление указателей на функцию.
callback - можно вызвать функцию из другой функции
<stdlib.h> - посмотреть qsort.
Функция компоратора
======================
		Динамическая память
=======================================
	Сегмент данных - загружаются в память, и не изменяются в течении времени
	Стековая память - выделяется в случае необходимости, и высвобождается
	в конце
	Динамическая память - "куча" - нужно самому оперировать.
	<stdlib.h>
	<string.h>
	int *a;
	a = (int*)malloc(<размер блока в байтах>);
	a = (int*)malloc(sizeof(a)*10); <- выделить 10 блоков памяти типа int;
	if ((a=(int*)malloc(sizeof(a)*10))== NULL)
	{
		...
	}
	if ((b = realoc(a, <новый размер>)) != NULL)
		{
			a = b;
		}
	else
		{
			free(a); a = NULL;
		}
	calloc - более чистая работа. Выделяет блок памяти и заполняет его нулями.
	memset(...); <- заполняет блоки памяти заданным значением.
1	memcpy(restrict void *a, restrict void *b, sizeof(...)); <- Из блока памяти a, копирует ... блока памяти в блок b
	Нужно проверять - не перекрываются ли блоки памяти! - Может затереться кусок блока.
	memmov(...); <- тоже, что и memcpy, но нет неоднозначных ситуаций, но она меделеннее, чем memmcpy;
	const, volatile, restrict <- модификаторы типов.
		const - нельзя изменить значение, заданное при объявлении
		Хак для константных строк.
		restrict - на данный блок памяти ссылаться будет только этот указатель
		volatile - значение переменной может менятся извне.
		Для многопоточности хорошо.
=======================================
		Спецификаторы классов памяти
=======================================
	4 штуки:
		auto
			:
			auto int a; - переменные хранятся в стеке (неявно всегда установлено)
			время жизни - время работы блока.
		static
			:
			память под неё выделяется и хранится в течение работы всей программы
			время жизни - время работы программы
		extern
			:
			укзавает на используемый в другом блоке переменную
		register
			:
			переменные в регистрах, а не в оп. памяти\
			в целых, и не определена операция взятия адреса
=======================================
*/
double my_sqrt(double x)
{
    __asm__ ( "fsqrt" : "+t" (x) );
    return x;
}
#define eps 1e-9
void swap(double *a, double *b)
{
	double tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}
/*
0 - решения нет
1 - решение единствнно
2 - два решения
3 - имеются конмплексно сопряженные корни
-1 - бесконечно много решений
*/
char quad(double a, double b, double c, double *x1, double *x2)
{
	double D;
	if (a == 0)
		if (b == 0)
			if (c == 0)
				return -1;
			else
				return 0;
		else
			{
				*x1 = -c/b;
				*x2 = -c/b;
				return 1;
			}
	else
		{
			D = b*b - 4*a*c;
			if (D < 0)
				{
					D = -D;
					*x1 = -b/(2*a);
					*x2 = my_sqrt(D)/(2*a);
					return 3;
				}
			else
				if (((D+eps)>= 0) && ((D-eps)<=0))
					{
						*x1 = *x2 = -b/(2*a);
						return 1;
					}
				else
					{
						*x2 = -(b - my_sqrt(D))/(2*a);
						*x1 = -(b + my_sqrt(D))/(2*a);
						return 2;
					}
		}

}
void func_0(double X1, double X2)
{
	printf("Answer: ∞\n");
}
void func_1(double X1, double X2)
{
	printf("Answer: ø\n");
}
void func_2(double X1, double X2)
{
	printf("Погрешность = %le\n", eps);
	printf("Answer: {%0.3f}\n", X1);
}
void func_3(double X1, double X2)
{
	printf("Погрешность = %le\n", eps);
	printf("Answer: {%0.3lf}&{%0.3lf}\n", X1, X2);
}
void func_4(double X1, double X2)
{
	printf("Погрешность = %le\n", eps);
	printf("Answer: {%0.3lf + %0.3lf*i}&{%0.3lf - %0.3lf*i}\n", X1, X2, X1, X2);
}
void main()
{
	typedef	void (*t_proc)(double, double);
	double a, b, c, X1, X2;
	t_proc p[5];
	char qur;
	scanf("%lf\n%lf\n%lf", &a, &b, &c);
	p[0] = func_0;
	p[1] = func_1;
	p[2] = func_2;
	p[3] = func_3;
	p[4] = func_4;
	qur = quad(a, b, c, &X1, &X2);
	p[qur+1](X1, X2);
}
/*
void test1()
{
	unsigned char k = 0;
	static unsigned char i = 0;
	++i;
	++k;
	printf("%i, %i\n", k, i); 
}
void main()
{
	unsigned char l;
	for (l=0; l<=10; ++l)
		test1;
}
*/