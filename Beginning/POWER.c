#include <stdio.h>
#include <stdlib.h>
void main()
{
	float a, Am, power;
	unsigned long n, m;
	printf("Input number :\n");
	scanf("%f", &a);
	printf("\n");
	printf("Input power :\n");
	scanf("%i", &n);
	printf("\n");
	Am = a;
	m = n;
	if ((a != 0) || (n != 0))
		{
			if (a == 0)
				power = 0;
			else if (n == 0)
				power = 1;
				else if (n == 1)
					power = a;
					else
						{
							power = 1;
							while (n > 0)
								{
									if (n % 2 == 1)
										power *= a;
									n >>= 1;
									a *= a;
								}
						}
			if (((power <= 0) && ((Am > 0) || (m % 2 == 0))) || ((power >= 0) && ((Am < 0) && (m % 2 == 1))))
				printf("Error\n");
			else
				printf("Answer = %.3f\n", power);
		}
	else
		printf("Error\n");
}