#include <stdio.h>
#include <stdint.h> /*<- from C99 - 
для платформеннонезависимых типов
int32_t
uint32_t
int8_t
int16_t
int64_t
*/
#include <limits.h>
/*
INT_MAX - максимальное значение переменной типа int;
LONG_MAX
------
CHAR_BIT - сколь бит в байте
*/
#include <float.h>
/*
for |R
*/
void main()
{
	unsigned char i, n;
	unsigned long long  Fac;
	scanf("%i\b", &n);
	Fac = 1;
	for (i=1; i!=n; i++, Fac *= i);
	printf("Factorial(%i) = %i\n", n, Fac);
}