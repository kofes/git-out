#include <stdio.h>
/*duckduckgo <- поисковик */
void main()
{
	unsigned long i, j, k;
	long n;
	scanf("%d", &n);
	if (n<=0)
		printf("Nothing\n");
	else
		for (i=2, j=0; j<n; ++i)
			{
				for (k=2; k*k<=i; ++k)
					if ((i % k) == 0)
						break;

				if (k*k>=i)
				{
					printf("%i ", i);
					++j;
				}
			}
	printf("\n");
}